import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OemSummaryComponent } from './modules/oem/oem-summary/oem-summary.component';
import { DealerSummaryComponent } from './modules/dealer/dealer-summary/dealer-summary.component';
import { AuthComponent } from './leadics/authentication-module/auth/auth.component';
import { CardComponent } from './leadics/authentication-module/card/card.component';
import { ForgotPasswordComponent } from './leadics/authentication-module/forgot-password/forgot-password.component';
import { LoginComponent } from './leadics/authentication-module/login/login.component';
import { SignUpComponent } from './leadics/authentication-module/sign-up/sign-up.component';

@NgModule({
  declarations: [
    AppComponent,
    OemSummaryComponent,
    DealerSummaryComponent,
    AuthComponent,
    CardComponent,
    ForgotPasswordComponent,
    LoginComponent,
    SignUpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

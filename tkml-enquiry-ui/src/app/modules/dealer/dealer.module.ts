import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DealerSummaryComponent } from './dealer-summary/dealer-summary.component';

@NgModule({
  declarations: [DealerSummaryComponent],
  imports: [
    CommonModule
  ]
})
export class DealerModule { }

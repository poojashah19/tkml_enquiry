import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OemSummaryComponent } from './oem-summary/oem-summary.component';

@NgModule({
  declarations: [OemSummaryComponent],
  imports: [
    CommonModule
  ]
})
export class OemModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoyalityadvocacyCommonModule } from '../../oem/oem-loyalty-advocacy/loyalityadvocacy-common.module';
import { OemLoyaltyAdvocacyComponent } from '../../oem/oem-loyalty-advocacy/oem-loyalty-advocacy.component';
import { RouterModule, Route } from '@angular/router';


const route: Route[] = [
  {
    path: '',
    component: OemLoyaltyAdvocacyComponent,
    data: {
      pageName: 'dealergroup-loyalty-advocacy',
      rightSideNavExist: true
    }

  }
];

@NgModule({
  imports: [
    CommonModule, LoyalityadvocacyCommonModule, RouterModule.forChild(route),
  ],
  declarations: []
})
export class DealergroupLoyalityAdvocacyModule { }

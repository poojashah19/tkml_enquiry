import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoadingDialogModule } from 'module-layouts/loading-dialog/loading-dialog.module';
import { LICommonModule } from 'leadics/common/modules/li-common/li-common.module';





const route: Route[] = [
	{
		path: 'summary',
		loadChildren: './dealergroup-summary/dealergroup-summary.module#DealergroupSummaryModule'
	},
	{
		path: 'regional-analysis',
		loadChildren: './dealergroup-regional/dealergroup-regional.module#DealergroupRegionalModule'
	},
	{
		path: 'priority-analysis',
		loadChildren: './dealergroup-priority-analysis/dealergroup-priority-analysis.module#DealergroupPriorityAnalysisModule'
	},
	{
		path: 'kpi-performance',
		loadChildren: './dealergroup-kpi-performance/dealergroup-kpi-performance.module#DealergroupKpiPerformanceModule'
	},
	{
		path: 'kpi-trend',
		loadChildren: './dealergroup-kpi-trend/dealergroup-kpi-trend.module#DealergroupKpiTrendModule'
	},
	{
		path: 'other-diagnostics',
		loadChildren: './dealergroup-other-diagnostics/dealergroup-other-diagnostics.module#DealergroupOtherDiagnosticsModule'
	},
	{
		path: 'trend-analysis',
		loadChildren: './dealergroup-trend/dealergroup-trend.module#DealergroupTrendModule'
	},
	{
		path: 'loyalty-advocacy',
		loadChildren: './dealergroup-loyality-advocacy/dealergroup-loyality-advocacy.module#DealergroupLoyalityAdvocacyModule'
	},
	{
		path: 'dealer-ranking',
		loadChildren: './dealergroup-dealer-ranking/dealergroup-dealer-ranking.module#DealergroupDealerRankingModule'
	},



];

@NgModule({
	imports: [
		CommonModule,
		LICommonModule,
		RouterModule.forChild(route),
		LoadingDialogModule
	],
	declarations: []
})
export class DealergroupModule { }



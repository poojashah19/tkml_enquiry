import { NgModule } from '@angular/core';

import { RegionalCommonModule } from '../../oem/oem-regional-analysis/regional-common.module';
import { OemRegionalAnalysisComponent } from '../../oem/oem-regional-analysis/oem-regional-analysis.component';
import { Route, RouterModule } from '@angular/router';
const route: Route[] = [
  {
    path: '',
    component: OemRegionalAnalysisComponent,
    data: {
      pageName: 'dealergroup-regional-analysis',
      rightSideNavExist: true
    }

  }
];


@NgModule({
  imports: [
    RegionalCommonModule,RouterModule.forChild(route),
  ],
  declarations: []
})
export class DealergroupRegionalModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PriorityCommonModule } from '../../oem/oem-priority-analysis/priority-common.module';
import { OemPriorityAnalysisComponent } from '../../oem/oem-priority-analysis/oem-priority-analysis.component';
import { Route, RouterModule } from '@angular/router';




const route: Route[] = [
  {
    path: '',
    component: OemPriorityAnalysisComponent,
    data: {
      pageName: 'dealergroup-priority-analysis',
      rightSideNavExist: true
    }

  }
];
@NgModule({
  imports: [
    PriorityCommonModule, RouterModule.forChild(route),
  ],
  declarations: []
})
export class DealergroupPriorityAnalysisModule { }

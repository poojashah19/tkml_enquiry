import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { KpitrendCommonModule } from '../../oem/oem-kpi-trend/kpitrend-common.module';
import { Route, RouterModule } from '@angular/router';
import {OemKpiTrendComponent} from '../../oem/oem-kpi-trend/oem-kpi-trend.component';
const route: Route[] = [
  {
    path: '',
    component: OemKpiTrendComponent,
    data: {
      pageName: 'dealergroup-kpi-trend',
      rightSideNavExist: true
    }

  }
];


@NgModule({
  imports: [
    KpitrendCommonModule,RouterModule.forChild(route),
  ],
  declarations: []
})
export class DealergroupKpiTrendModule { }



import { DealergroupSummaryComponent } from './dealergroup-summary.component';



import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LICommonModule } from 'leadics/common/modules/li-common/li-common.module';
import { LIDataStatusModule } from 'leadics/common/components/li-data-status/li-data-status.module';
import { LIRoundProgressModule } from 'leadics/common/components/round-progress/round-progress.module';

import { AttributeScoreComponent } from './attribute-score/attribute-score.component';
import { DealerRankingScoreComponent } from './dealer-ranking-score/dealer-ranking-score.component';
import { FactorScoreComponent } from './factor-score/factor-score.component';
import { KpiScoreComponent } from './kpi-score/kpi-score.component';
import { NationalScoreComponent } from './national-score/national-score.component';
import { RegionalScoreComponent } from './regional-score/regional-score.component';
import { MatTabsModule } from '@angular/material/tabs';
const route: Route[] = [
  {
    path: '',
    component: DealergroupSummaryComponent,
    data: {
      pageName: 'dealergroup-dealergroup-summary',
      rightSideNavExist: true
    }

  }
];



@NgModule({
  imports: [
    CommonModule,
    LICommonModule,
    LIRoundProgressModule,
    RouterModule.forChild(route),
    LIDataStatusModule,MatTabsModule
  ],
  declarations:
    [NationalScoreComponent,
      RegionalScoreComponent,
      FactorScoreComponent,
      DealerRankingScoreComponent,
      AttributeScoreComponent,
      KpiScoreComponent,
      DealergroupSummaryComponent]
})
export class DealergroupSummaryModule { }

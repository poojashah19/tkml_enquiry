/*########### GENERATED AT 2018-06-15 12:31:51.751 ###########*/
import {
  Component, Input,
  OnInit, OnChanges,
  OnDestroy
} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {NameValue} from 'leadics/common/types/name-value';
import {Subscription} from 'rxjs/index';
import {ApiService} from 'leadics/common/services/api.service';
import {LoggerService} from 'leadics/common/services/logger.service';
import {ResourceService} from 'leadics/common/services/resource.service';
import {DataStatus} from 'leadics/common/enums/data-status';
import {FilterInfo, SelectedFiltersDataService} from '@varmasagi/filters';



@Component({
  selector: 'dealergroup-regional-score',
  templateUrl: './regional-score.component.html',
  styleUrls: ['./regional-score.component.css']
})
export class RegionalScoreComponent implements OnInit {
  readonly SLUG_NAME = 'dealer-summary-benchmark-scores';
  readonly URL = environment.dealerOem.URL;
  readonly DATA_STATUS = DataStatus;

  @Input() filter: FilterInfo;

  private filters: string[] = this.resource.getDivFilters(this.SLUG_NAME);
  private actionParams: NameValue[] = this.resource.getDivActionParam(this.SLUG_NAME);
  private httpSubscription: Subscription;

  public data: any;
  public dataStatus: DataStatus = DataStatus.LOADING;
  constructor(
     private apiService: ApiService,
     private logger: LoggerService,
     private fs: SelectedFiltersDataService,
     private resource: ResourceService
    ) {
    
  }

  ngOnInit() {
   this.init();
  }
  private init() {
  
	this.data = undefined;
    this.dataStatus = DataStatus.LOADING;
    if (this.httpSubscription) {
      // unsubscribe the previous call if new call is made
      this.httpSubscription.unsubscribe();
    }
    this.httpSubscription = this.apiService.getDataWithFilters(this.URL, this.actionParams, this.filters, this.fs)
      .subscribe(
        res => {
    
     this.data=res;
   
           this.formatData(res);
        },
        error => {
          this.logger.log(error);
          this.onError();
        }
      );
  }  
  
  // pack the data 
  private formatData(response: any) {
    try {
        
        this.onSuccess();
    } catch(err) {
        this.logger.log( err);
        this.onError();
    }
   
  }
  private onSuccess(): void {
    this.dataStatus = DataStatus.LOADED;
  }

  private onError(): void {
    this.dataStatus = DataStatus.DATA_ERROR;
    this.data = null;
  }

  ngOnChanges() {
    if (this.filter) {
      const isFilterApplicable: boolean = this.filters
                                        .filter(current => current === this.filter.name)
                                        .length > 0;
      // this.logger.log(this.filters, this.filter, isFilterApplicable);
      if (isFilterApplicable) {
        this.init();
      }
    }
  }

  ngOnDestroy() {
    try {
        this.httpSubscription.unsubscribe();
    } catch (err) {
        this.logger.log( err);
    }
   
  }
  
}

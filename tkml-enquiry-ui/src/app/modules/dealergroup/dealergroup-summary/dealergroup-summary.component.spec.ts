import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealergroupSummaryComponent } from './dealergroup-summary.component';

describe('DealergroupSummaryComponent', () => {
  let component: DealergroupSummaryComponent;
  let fixture: ComponentFixture<DealergroupSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealergroupSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealergroupSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

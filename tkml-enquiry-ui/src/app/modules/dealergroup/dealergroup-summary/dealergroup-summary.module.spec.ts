import { DealergroupSummaryModule } from './dealergroup-summary.module';

describe('DealergroupSummaryModule', () => {
  let dealergroupSummaryModule: DealergroupSummaryModule;

  beforeEach(() => {
    dealergroupSummaryModule = new DealergroupSummaryModule();
  });

  it('should create an instance', () => {
    expect(dealergroupSummaryModule).toBeTruthy();
  });
});

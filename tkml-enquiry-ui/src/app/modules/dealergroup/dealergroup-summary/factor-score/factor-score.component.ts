/*########### GENERATED AT 2018-06-15 12:31:51.772 ###########*/
import {
  Component, Input,
  OnInit, OnChanges,
  OnDestroy
} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {NameValue} from 'leadics/common/types/name-value';
import {Subscription} from 'rxjs/index';
import {ApiService} from 'leadics/common/services/api.service';
import {LoggerService} from 'leadics/common/services/logger.service';
import {ResourceService} from 'leadics/common/services/resource.service';
import {ICSHighChartDefaults} from '@varmasagi/highcharts-defaults';
import * as Highcharts from 'highcharts';
import {DataStatus} from 'leadics/common/enums/data-status';
import {JsonFormatterService} from 'leadics/common/services/json-formatter.service';
import {FilterInfo, SelectedFiltersDataService} from '@varmasagi/filters';


class DealerOemSummaryFactorScoresHighChartConfig extends ICSHighChartDefaults {

  constructor() {
    super();
    this.chart.type = 'column';
    this.colors = ['#4292ff', '#689F38', '#800000'];
    this.legend.enabled = false;
    this.yAxis.visible = false;
    delete this.plotOptions.scatter.marker.fillColor;
    this.plotOptions.series.minPointLength=50;
    this.plotOptions.scatter.marker.radius=6;
    this.plotOptions.series.minPointLength = 0;
    this.xAxis.labels.style.fontSize = '11px';
    this.plotOptions.column.dataLabels.style.textOutline = '0px';
  }

}


@Component({
  selector: 'dealergroup-factor-score',
  templateUrl: './factor-score.component.html',
  styleUrls: ['./factor-score.component.css']
})
export class FactorScoreComponent implements OnInit {
  readonly SLUG_NAME = 'dealer-summary-factor-scores';
  readonly URL = environment.dealerOem.URL;
  readonly DATA_STATUS = DataStatus;

  @Input() filter: FilterInfo;

  private filters: string[] = this.resource.getDivFilters(this.SLUG_NAME);
  private actionParams: NameValue[] = this.resource.getDivActionParam(this.SLUG_NAME);
  private httpSubscription: Subscription;

  public data: any;
  public hcConfig: DealerOemSummaryFactorScoresHighChartConfig;
  public Highcharts = Highcharts;
  public dataStatus: DataStatus = DataStatus.LOADING;
  constructor(
     private apiService: ApiService,
     private logger: LoggerService,
     private jsonFormatterService: JsonFormatterService,
     private fs: SelectedFiltersDataService,
     private resource: ResourceService
    ) {
    this.hcConfig = new DealerOemSummaryFactorScoresHighChartConfig();
  }

  ngOnInit() {
   this.init();
  }
  private init() {
  
	this.data = undefined;
    this.dataStatus = DataStatus.LOADING;
    if (this.httpSubscription) {
      // unsubscribe the previous call if new call is made
      this.httpSubscription.unsubscribe();
    }
    this.httpSubscription = this.apiService.getDataWithFilters(this.URL, this.actionParams, this.filters, this.fs)
      .subscribe(
        res => {
  
    
          this.formatData(res);
        },
        error => {
          this.logger.log(error);
          this.onError();
        }
      );
  }  
  
  // pack the data according to the chart requirement, use the below word wrapping if labels are overlapping.
  private formatData(response: any) {
    try {
      const fontSize = this.hcConfig.xAxis.labels.style.fontSize;
      const marginLeft = this.hcConfig.chart.marginLeft;
      if (!this.jsonFormatterService.ifAnyNonZero(response.data["maxscore"]))
      delete response.data["maxscore"];
    if (!this.jsonFormatterService.ifAnyNonZero(response.data["minscore"])) {
      delete response.data["minscore"];
    }
      const color = ['#000000'];
      response.data['factor'] = this.jsonFormatterService.wordWrapCategories(marginLeft, fontSize, response.data['factor']);
      this.hcConfig.loadDataOneBar_TwoScatterSeries(response.data, 'factor', 'Factor Score', 'score', null, 'Best Score', 'maxscore', 'Worst Score', 'minscore');
      this.onSuccess();
      this.logger.log("factor scores");
      this.logger.log(this.hcConfig);
    } catch(err) {
        this.logger.log( err);
        this.onError();
    }
   
  }
  private onSuccess(): void {
    this.dataStatus = DataStatus.LOADED;
  }

  private onError(): void {
    this.dataStatus = DataStatus.DATA_ERROR;
    this.data = null;
  }

  ngOnChanges() {
    if (this.filter) {
      const isFilterApplicable: boolean = this.filters
                                        .filter(current => current === this.filter.name)
                                        .length > 0;
      // this.logger.log(this.filters, this.filter, isFilterApplicable);
      if (isFilterApplicable) {
        this.init();
      }
    }
  }

  ngOnDestroy() {
    try {
        this.httpSubscription.unsubscribe();
    } catch (err) {
        this.logger.log( err);
    }
   
  }
  

}

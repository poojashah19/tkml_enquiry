/*########### GENERATED AT 2018-06-15 12:31:51.793 ###########*/
import {
  Component, Input,
  OnInit, OnChanges,
  OnDestroy
} from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { NameValue } from 'leadics/common/types/name-value';
import { Subscription } from 'rxjs/index';
import { ApiService } from 'leadics/common/services/api.service';
import { LoggerService } from 'leadics/common/services/logger.service';
import { ResourceService } from 'leadics/common/services/resource.service';
import { ICSHighChartDefaults } from '@varmasagi/highcharts-defaults';
import * as Highcharts from 'highcharts';
import { DataStatus } from 'leadics/common/enums/data-status';
import { JsonFormatterService } from 'leadics/common/services/json-formatter.service';
import { FilterInfo, SelectedFiltersDataService } from '@varmasagi/filters';


class DealerOemSummaryAttributeMeanScoreHighChartConfig extends ICSHighChartDefaults {

  constructor() {
    super();
    this.chart.type = 'bar';
    delete this.plotOptions.scatter.marker.fillColor;
    this.colors = ['#4292ff', '#689F38', '#800000'];
    this.legend.enabled = false;
    this.yAxis.visible = false;
    this.plotOptions.bar.pointWidth = 20;
    this.xAxis.labels.style.fontSize = '11px'
    this.plotOptions.scatter.marker.radius = 6;
    this.plotOptions.bar.dataLabels.style.textOutline = '0px';
  }

}



@Component({
  selector: 'dealergroup-attribute-score',
  templateUrl: './attribute-score.component.html',
  styleUrls: ['./attribute-score.component.css']
})
export class AttributeScoreComponent implements OnInit {
  readonly SLUG_NAME = 'oem-summary-top-bottom-attributes';
  readonly URL = environment.dealerOem.URL;
  readonly DATA_STATUS = DataStatus;

  @Input() filter: FilterInfo;

  private filters: string[] = this.resource.getDivFilters(this.SLUG_NAME);
  private actionParams: NameValue[] = this.resource.getDivActionParam(this.SLUG_NAME);
  private httpSubscription: Subscription;

  public data: any;
  public hcConfig: DealerOemSummaryAttributeMeanScoreHighChartConfig;
  public Highcharts = Highcharts;
  public dataStatus: DataStatus = DataStatus.LOADING;
  constructor(
    private apiService: ApiService,
    private logger: LoggerService,
    private jsonFormatterService: JsonFormatterService,
    private fs: SelectedFiltersDataService,
    private resource: ResourceService
  ) {
    this.hcConfig = new DealerOemSummaryAttributeMeanScoreHighChartConfig();
  }

  ngOnInit() {
    this.init();
  }
  private init() {

    this.data = undefined;
    this.dataStatus = DataStatus.LOADING;
    if (this.httpSubscription) {
      // unsubscribe the previous call if new call is made
      this.httpSubscription.unsubscribe();
    }
    this.httpSubscription = this.apiService.getDataWithFilters(this.URL, this.actionParams, this.filters, this.fs)
      .subscribe(
        res => {
          this.formatData(res);
        },
        error => {
          this.logger.log(error);
          this.onError();
        }
      );
  }

  // pack the data according to the chart requirement, use the below word wrapping if labels are overlapping.
  private formatData(response: any) {
    try {
      const cat = 'Attribute';
      const series = 'score';
      const seriesTypes = ['bar'];
      const colors = this.assignColors(response.data['top_or_bottom']);

      const fontSize = this.hcConfig.xAxis.labels.style.fontSize;
      const marginLeft = this.hcConfig.chart.marginLeft;
      response.data[cat] = this.jsonFormatterService.wordWrapCategories(marginLeft, fontSize, response.data[cat]);
      const categories = response.data[cat];
      const meanSeries = response.data['score'];
      // this.hcConfig.loadDataBarOrColumnOrLineWithColors(response.data, cat, series, colors)
      this.hcConfig.xAxis.categories = categories;
      this.hcConfig.colors = ["#7e57c2", "#7e57c2", "#7e57c2", "#7e57c2", "#7e57c2", "#9b9b9b", "#9b9b9b", "#9b9b9b", "#9b9b9b", "#9b9b9b"];
      this.hcConfig.series = [
        {
          name: ' ',
          data: meanSeries
        },

      ]
      this.onSuccess();
    } catch (err) {
      this.logger.log(err);
      this.onError();
    }

  }
  private onSuccess(): void {
    this.dataStatus = DataStatus.LOADED;
  }

  private onError(): void {
    this.dataStatus = DataStatus.DATA_ERROR;
    this.data = null;
  }
  assignColors(data: any[]): string[] {
    const colors: string[] = [];
    for (const datum of data) {
      const color = datum === 'top' ? '#7e57c2' : '#9b9b9b';
      colors.push(color);
    }
    return colors;
  }
  ngOnChanges() {
    if (this.filter) {
      const isFilterApplicable: boolean = this.filters
        .filter(current => current === this.filter.name)
        .length > 0;
      // this.logger.log(this.filters, this.filter, isFilterApplicable);
      if (isFilterApplicable) {
        this.init();
      }
    }
  }

  ngOnDestroy() {
    try {
      this.httpSubscription.unsubscribe();
    } catch (err) {
      this.logger.log(err);
    }

  }



}

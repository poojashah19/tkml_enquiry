/*########### GENERATED AT 2018-05-24 10:29:23.820 ###########*/
import {
  Component, Input,
  OnInit, OnChanges,
  OnDestroy
} from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { NameValue } from 'leadics/common/types/name-value';
import { Subscription } from 'rxjs/index';
import { ApiService } from 'leadics/common/services/api.service';
import { LoggerService } from 'leadics/common/services/logger.service';
import { ResourceService } from 'leadics/common/services/resource.service';
import { ICSHighChartDefaults } from '@varmasagi/highcharts-defaults';
import * as Highcharts from 'highcharts';
import { DataStatus } from 'leadics/common/enums/data-status';
import { JsonFormatterService } from 'leadics/common/services/json-formatter.service';
import { FilterInfo, SelectedFiltersDataService } from '@varmasagi/filters';


class OemSummaryTopBottomDealerHighChartConfig extends ICSHighChartDefaults {

  constructor() {
    super();
    this.chart.type = 'bar';
    this.legend.enabled = false;
    this.plotOptions.bar.pointWidth = 20;
    this.plotOptions.series.colorByPoint = true;
    this.chart.marginLeft = 150;
    this.plotOptions.bar.dataLabels.formatter = function () {
      return this.y;
    };
    this.plotOptions.bar.tooltip.pointFormatter = function () {
      return this.y;
    };
    this.plotOptions.bar.dataLabels.style.textOutline = '0px';
    this.xAxis.labels.style.fontSize = '10px'
    this.yAxis.visible = false;
    this.chart.marginLeft = 180;

    this.credits.enabled = true;
    this.credits.text = '  '
    this.credits.position = {
      align: 'left',
      x: 10,
    };

    this.credits.style = {
      fontSize: '10px',
    };
  }

}




@Component({
  selector: 'dealergroup-dealer-ranking-score',
  templateUrl: './dealer-ranking-score.component.html',
  styleUrls: ['./dealer-ranking-score.component.css']
})
export class DealerRankingScoreComponent implements OnInit {
  readonly SLUG_NAME = 'oem-summary-top-bottom-dealer';
  readonly URL = environment.oem.URL;
  readonly DATA_STATUS = DataStatus;

  @Input() filter: FilterInfo;

  private filters: string[] = this.resource.getDivFilters(this.SLUG_NAME);
  private actionParams: NameValue[] = this.resource.getDivActionParam(this.SLUG_NAME);
  private httpSubscription: Subscription;

  public data: any;
  public hcConfig: OemSummaryTopBottomDealerHighChartConfig;
  public Highcharts = Highcharts;
  public dataStatus: DataStatus = DataStatus.LOADING;
  constructor(
    private apiService: ApiService,
    private logger: LoggerService,
    private jsonFormatterService: JsonFormatterService,
    private fs: SelectedFiltersDataService,
    private resource: ResourceService
  ) {
    this.hcConfig = new OemSummaryTopBottomDealerHighChartConfig();
  }

  ngOnInit() {
    this.init();
  }
  private init() {

    this.data = undefined;
    this.dataStatus = DataStatus.LOADING;
    if (this.httpSubscription) {
      // unsubscribe the previous call if new call is made
      this.httpSubscription.unsubscribe();
    }
    this.httpSubscription = this.apiService.getDataWithFilters(this.URL, this.actionParams, this.filters, this.fs)
      .subscribe(
        res => {


          const data = res;
          this.logger.log('data');
          this.logger.log(data);
          this.formatData(data);
        },
        error => {
          this.logger.log(error);
          this.onError();
        }
      );
  }

  // pack the data according to the chart requirement, use the below word wrapping if labels are overlapping.
  private formatData(response: any) {
    try {
      const colors: string[] = this.assignColors(response.data.top_or_bottom);
      const fontSize = this.hcConfig.xAxis.labels.style.fontSize;
      const marginLeft = this.hcConfig.chart.marginLeft;
      response.data['Dealer'] = this.jsonFormatterService.wordWrapCategories(marginLeft, fontSize, response.data['Dealer']);
      this.hcConfig.loadDataBarOrColumnOrLineWithColors(response.data, 'Dealer', 'score', colors);
      this.onSuccess();
    } catch (err) {
      this.logger.log(err);
      this.onError();
    }

  }

  assignColors(data: any[]): string[] {
    const colors: string[] = [];
    for (const datum of data) {
      const color = datum === 'top' ? '#7e57c2' : '#9b9b9b';
      colors.push(color);
    }
    return colors;
  }
  private onSuccess(): void {
    this.dataStatus = DataStatus.LOADED;

  }

  private onError(): void {
    this.dataStatus = DataStatus.DATA_ERROR;
    this.data = null;
  }

  ngOnChanges() {
    if (this.filter) {
      const isFilterApplicable: boolean = this.filters
        .filter(current => current === this.filter.name)
        .length > 0;
      // this.logger.log(this.filters, this.filter, isFilterApplicable);
      if (isFilterApplicable) {
        this.init();
      }
    }
  }

  ngOnDestroy() {
    try {
      this.httpSubscription.unsubscribe();
    } catch (err) {
      this.logger.log(err);
    }

  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { TrendCommonModule } from '../../oem/oem-trend-analysis/trend-common.module';
import { OemTrendAnalysisComponent } from '../../oem/oem-trend-analysis/oem-trend-analysis.component';
import { RouterModule, Route } from '@angular/router';

const route: Route[] = [
  {
    path: '',
    component: OemTrendAnalysisComponent,
    data: {
      pageName: 'dealergroup-trend-analysis',
      rightSideNavExist: true
    }

  }
];
@NgModule({
  imports: [
    TrendCommonModule,
    RouterModule.forChild(route),

  ],
  declarations: []
})
export class DealergroupTrendModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {DealerrankingCommonModule} from '../../oem/oem-dealer-ranking/dealerranking-common.module';
import {OemDealerRankingComponent} from '../../oem/oem-dealer-ranking/oem-dealer-ranking.component';
import { Route, RouterModule } from '@angular/router';



const route: Route[] = [
  {
    path: '',
    component: OemDealerRankingComponent,
    data: {
      pageName: 'dealergroup-dealer-ranking',
      rightSideNavExist: true
    }

  }
];



@NgModule({
  imports: [
    DealerrankingCommonModule, RouterModule.forChild(route),
  ],
  declarations: []
})
export class DealergroupDealerRankingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OthersCommonModule } from '../../oem/oem-other-diagnostics/others-common.module';
import { OemOtherDiagnosticsComponent } from '../../oem/oem-other-diagnostics/oem-other-diagnostics.component';
import { RouterModule, Route } from '@angular/router';

const route: Route[] = [
  {
    path: '',
    component: OemOtherDiagnosticsComponent,
    data: {
      pageName: 'dealergroup-other-diagnostics',
      rightSideNavExist: true
    }

  }
];

@NgModule({
  imports: [
    OthersCommonModule, RouterModule.forChild(route),
  ],
  declarations: []
})
export class DealergroupOtherDiagnosticsModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { KpiperformanceCommonModule } from '../../oem/oem-kpi-performance/kpiperformance-common.module';
import { OemKpiPerformanceComponent } from '../../oem/oem-kpi-performance/oem-kpi-performance.component';
import { Route, RouterModule } from '@angular/router';




const route: Route[] = [
  {
    path: '',
    component: OemKpiPerformanceComponent,
    data: {
      pageName: 'dealergroup-kpi-performance',
      rightSideNavExist: true
    }

  }
];

@NgModule({
  imports: [
    KpiperformanceCommonModule, RouterModule.forChild(route),
  ],
  declarations: []
})
export class DealergroupKpiPerformanceModule { }

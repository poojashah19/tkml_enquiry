/*
* ICSsmslogDAO.java 
* Copyright (c) LeadICS Private Limited
* This software is the confidential and proprietary information of 
* LeadICS Private Limited You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with LeadICS Private Limited
* Project Name             : dealeraudit
* Module                   : Record beans
* Author                   : Subrahmanya Varma Sagi, LeadICS Private Limited
* Date                     : Mon Sep 19 09:41:35 IST 2016
* Change Revision
* ----------------------------------------------------------------
* Date            Author         Version#    Remarks/Description
*-----------------------------------------------------------------
*/
package com.leadics.Toyota.dao;

 

import java.sql.*;
import java.util.*;
import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.to.ICSsmslogRecord;
import java.text.SimpleDateFormat;


 
public class ICSsmslogDAO  extends LIDAO
{
 

 

 

	 java.sql.Connection con = null;
	
 

 

 	public ICSsmslogRecord[] loadICSsmslogRecords(String query,Connection con, boolean closeConnection) 
 	 throws Exception 
 	{
 		if (con == null)
 		{
 			con = getDatabaseConnection();
 		}
 		PreparedStatement ps = con.prepareStatement(query);
 		ResultSet rs = ps.executeQuery();
 		ArrayList recordSet = new ArrayList();
 		 while(rs.next()) 
 		{
 			ICSsmslogRecord record=new ICSsmslogRecord();
 		 try {
 			record.setID(rs.getInt("ID"));
 		 }catch(Exception e){  }
 		 try {
 			record.setURL(rs.getString("URL"));
 		 }catch(Exception e){ }
 		 try {
 			record.setResponseMessge(rs.getString("ResponseMessge"));
 		 }catch(Exception e){  }
 		 try {
 			record.setSentat(rs.getString("Sentat"));
 		 }catch(Exception e){  }
 			recordSet.add(record);
 		}


 		ICSsmslogRecord[] tempICSsmslogRecords =new ICSsmslogRecord[recordSet.size()];


 		 for (int index = 0; index < recordSet.size(); index++) {
 			 tempICSsmslogRecords[index] = (ICSsmslogRecord) (recordSet.get(index));
 		 }



rs.close();
 	ps.close();
 	con.close();
 	con=null;
 	return tempICSsmslogRecords;
 	}
 

 

 	public ICSsmslogRecord[] loadICSsmslogRecords(String query) 
 	 throws Exception 
 	{
 		 return loadICSsmslogRecords(query,null, false);
 	}
 

 

 	public boolean insertICSsmslogRecord(ICSsmslogRecord record) 
 	 throws Exception 
 	{
 		 String query = "Insert into smslog ";

 		        query += "(";

 		        query += "ID,URL,ResponseMessge,Sentat";
 			query +=")"; 
 			query +="VALUES"; 
 			query +="( ?, ?, ?,  ?"; 

 			query +=")"; 
 		if (con == null)
 		{
 			con = getDatabaseConnection();
 		}
 		PreparedStatement ps;
 		ps = con.prepareStatement(query);
 		ps.setInt(1,record.getID());
 		ps.setString(2, record.getURL());
 		ps.setString(3, record.getResponseMessge());
 		ps.setString(4, record.getSentat());

 		boolean result = ps.execute();
 		ps.close();
 		con.close();
 		con=null;
 		return result;
 	 } 
 

 

 	public boolean insertICSsmslogRecords(ICSsmslogRecord[] records) 
 	 throws Exception 
 	{
 		boolean finalresult=true;
 		for(int index=0;index< records.length;index++) { 
 		boolean result=insertICSsmslogRecord(records[index]);
 		if(result != true){ finalresult=false; }
 		 } 
 		 return finalresult;
 	 } 
 

 

 	public ICSsmslogRecord[] loadICSsmslogRecordsCond(String strCondtion) 
 	 throws Exception 
 	{
 		String query="SELECT * from smslog where 1=1 and "+strCondtion;
 	
 		 return loadICSsmslogRecords(query,null, false);
 	}
 

 

 	public String loadICSsmslogRecordsJSONCond(String strCondtion) 
 	 throws Exception 
 	{
 		 String query = "SELECT * from smslog where 1=1 and " + strCondtion;
 		 return new Gson().toJson(loadICSsmslogRecords(query, null, false));
 	}
 

 

 	public String loadICSsmslogRecordsJSONCond(String strCondtion, String strOrderby) 
 	 throws Exception 
 	{
 		 String query = "SELECT * from smslog where 1=1 and " + strCondtion + " order by " + strOrderby;
 		 return new Gson().toJson(loadICSsmslogRecords(query, null, false));
 	}
 

 

 	public String loadICSsmslogRecordsJSONQuery(String strQuery) 
 	 throws Exception 
 	{
 		 String query = strQuery;
 		 return new Gson().toJson(loadICSsmslogRecords(query, null, false));
 	}
 

 

 	public ICSsmslogRecord loadICSsmslogRecordCond(String strCondtion) 
 	 throws Exception 
 	{
 		String query="SELECT * from smslog where 1=1 and "+strCondtion;
 	
 		ICSsmslogRecord[] results =   loadICSsmslogRecords(query);
 		 if (results == null) return null;
 		if(results.length < 1) return null;
 		 return results[0];
 	}
 

 

 	public ICSsmslogRecord[] loadICSsmslogRecordsCond(String strCondtion,String strOrderby) 
 	 throws Exception 
 	{
 		String query="SELECT * from smslog where 1=1 and "+strCondtion+" order by "+strOrderby;
 	
 		 return loadICSsmslogRecords(query,null, false);
 	}
 

 

 	public  boolean  UpdateICSsmslogRecord(HashMap keyvalue,String strCondtion) 
 	 throws Exception 
 	{
 		String query="Update smslog set "; 
 		HashMap setColoumns = keyvalue;
 		Set columns = setColoumns.keySet();
 		Iterator itr = columns.iterator();
 		String setters = "";
 		while (itr.hasNext()) {
 			String column = (String) itr.next();
 			String value=(String)setColoumns.get(column);
 		query=query + column  +  "=" + value ;
 		}
 		query=query +"where 1=1 and "+strCondtion;
 		if (con == null)
 		{
 			con = getDatabaseConnection();
 		}
 		PreparedStatement ps = con.prepareStatement(query);
 		boolean result=ps.execute();
 		ps.close();
 		con.close();
 		con=null;
 		return result;
}
 

 

 	public boolean updateICSsmslogRecord(ICSsmslogRecord record,String cond) 
 	 throws Exception 
 	{
 		 String query = "Update smslog set" ;

 			query +=" ID = ? ,URL = ? ,ResponseMessge = ? ,Sentat= ? ";

 			query +=" where "+cond; 
 		if (con == null)
 		{
 			con = getDatabaseConnection();
 		}
 		PreparedStatement ps;
 		ps = con.prepareStatement(query);
 		ps.setInt(1,record.getID());
 		ps.setString(2, record.getURL());
 		ps.setString(3, record.getResponseMessge());
 		ps.setString(4, record.getSentat());

 		boolean result = ps.execute();
 		ps.close();
 		con.close();
 		con=null;
 		return result;
 	 } 
 

 

 	public boolean insertUpdateICSsmslogRecord(ICSsmslogRecord record) 
 	 throws Exception 
 	{
 		 String query = "Insert into smslog ";

 		        query += "(";

 		        query += "ID,URL,ResponseMessge,Sentat";
 			query +=")"; 
 			query +=" VALUES"; 
 			query +="( ?, ?, ?,  ?"; 

 			query +=")"; 
 		if (con == null)
 		{
 			con = getDatabaseConnection();
 		}
 		PreparedStatement ps;
 		 query += " ON DUPLICATE KEY UPDATE " ;

 			query +=" ID = ? ,URL = ? ,ResponseMessge = ? ,Sentat= ? ";
 		ps = con.prepareStatement(query);
 		ps.setInt(1,record.getID());
 		ps.setString(2, record.getURL());
 		ps.setString(3, record.getResponseMessge());
 		ps.setString(4, record.getSentat());
 		ps.setInt(5,record.getID());
 		ps.setString(6, record.getURL());
 		ps.setString(7, record.getResponseMessge());
 		ps.setString(8, record.getSentat());

 		boolean result = ps.execute();
 		ps.close();
 		con.close();
 		con=null;
 		return result;
 	 } 
 

 
}

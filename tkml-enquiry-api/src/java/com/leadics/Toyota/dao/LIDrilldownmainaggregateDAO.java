
/*
 * LIDrilldownmainaggregateDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.dao;
import com.leadics.Toyota.to.LIDrilldownmainaggregateRecord;
import com.leadics.Toyota.common.LIDAO;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIDrilldownmainaggregateDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LIDrilldownmainaggregateDAO.class.getName());


	public LIDrilldownmainaggregateRecord[] loadLIDrilldownmainaggregateRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLIDrilldownmainaggregateRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LIDrilldownmainaggregateRecord record = new LIDrilldownmainaggregateRecord();
				record.setId(rs.getString("ID"));
				record.setSheetcolomname(rs.getString("sheetcolomname"));
				record.setRawcolumnname(rs.getString("RawColumnName"));
				record.setDisplayname(rs.getString("DisplayName"));
				record.setDrilldownof(rs.getString("drilldownof"));
				record.setDrilldowncharttype(rs.getString("drilldownChartType"));
				record.setServicecity30jdp(rs.getString("SERVICECITY_30JDP"));
				record.setMonth(rs.getString("Month"));
				record.setModeldb(rs.getString("modeldb"));
				record.setFuel(rs.getString("fuel"));
				record.setDisplaynameforanswer(rs.getString("DisplayNameforAnswer"));
				record.setProblemvalue(rs.getString("problemvalue"));
				record.setIqscount(rs.getString("iqscount"));
				record.setRstatus(rs.getString("RSTATUS"));
				record.setCreatedby(rs.getString("CREATED_BY"));
				record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
				record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
				record.setModifiedby(rs.getString("MODIFIED_BY"));
				recordSet.add(record);
			}
			logger.trace("loadLIDrilldownmainaggregateRecords:Records Fetched:" + recordSet.size());
			LIDrilldownmainaggregateRecord[] tempLIDrilldownmainaggregateRecords = new LIDrilldownmainaggregateRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLIDrilldownmainaggregateRecords[index] = (LIDrilldownmainaggregateRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLIDrilldownmainaggregateRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LIDrilldownmainaggregateRecord[] loadLIDrilldownmainaggregateRecords(String query)
	throws Exception
	{
		return loadLIDrilldownmainaggregateRecords(query, null, true);
	}


	public LIDrilldownmainaggregateRecord loadFirstLIDrilldownmainaggregateRecord(String query)
	throws Exception
	{
		LIDrilldownmainaggregateRecord[] results = loadLIDrilldownmainaggregateRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LIDrilldownmainaggregateRecord loadLIDrilldownmainaggregateRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM drill_down_main_aggregate WHERE (ID = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLIDrilldownmainaggregateRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LIDrilldownmainaggregateRecord record = new LIDrilldownmainaggregateRecord();
			record.setId(rs.getString("ID"));
			record.setSheetcolomname(rs.getString("sheetcolomname"));
			record.setRawcolumnname(rs.getString("RawColumnName"));
			record.setDisplayname(rs.getString("DisplayName"));
			record.setDrilldownof(rs.getString("drilldownof"));
			record.setDrilldowncharttype(rs.getString("drilldownChartType"));
			record.setServicecity30jdp(rs.getString("SERVICECITY_30JDP"));
			record.setMonth(rs.getString("Month"));
			record.setModeldb(rs.getString("modeldb"));
			record.setFuel(rs.getString("fuel"));
			record.setDisplaynameforanswer(rs.getString("DisplayNameforAnswer"));
			record.setProblemvalue(rs.getString("problemvalue"));
			record.setIqscount(rs.getString("iqscount"));
			record.setRstatus(rs.getString("RSTATUS"));
			record.setCreatedby(rs.getString("CREATED_BY"));
			record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
			record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
			record.setModifiedby(rs.getString("MODIFIED_BY"));
			ps.close();
			logger.trace("loadLIDrilldownmainaggregateRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LIDrilldownmainaggregateRecord loadLIDrilldownmainaggregateRecord(String id)
	throws Exception
	{
		return loadLIDrilldownmainaggregateRecord(id, null, true);
	}

	public int insertLIDrilldownmainaggregateRecord(LIDrilldownmainaggregateRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "INSERT INTO drill_down_main_aggregate ";
			Query += "(";
			Query += "sheetcolomname,RawColumnName,DisplayName,drilldownof,drilldownChartType,SERVICECITY_30JDP,Month,modeldb,fuel,DisplayNameforAnswer,problemvalue,iqscount,RSTATUS,CREATED_BY,CREATED_AT,MODIFIED_AT,MODIFIED_BY";
			Query += ")";
			Query += " VALUES ";
			Query += "(";
			Query += "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
			Query += ")";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			record.setModifiedby(StringUtils.noNull(record.getModifiedby(),record.getCreatedby()));
			Query = updateQuery(Query);
			logger.trace("insertLIDrilldownmainaggregateRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			if (isOracleDatabase()) 
			{
				ps = con.prepareStatement(Query,new String[]{"ID"});
			}
			else
			{
				ps = con.prepareStatement(Query,Statement.RETURN_GENERATED_KEYS);
			}
			setStringValue(ps, 1, record.getSheetcolomname());
			setStringValue(ps, 2, record.getRawcolumnname());
			setStringValue(ps, 3, record.getDisplayname());
			setStringValue(ps, 4, record.getDrilldownof());
			setStringValue(ps, 5, record.getDrilldowncharttype());
			setStringValue(ps, 6, record.getServicecity30jdp());
			setStringValue(ps, 7, record.getMonth());
			setStringValue(ps, 8, record.getModeldb());
			setStringValue(ps, 9, record.getFuel());
			setStringValue(ps, 10, record.getDisplaynameforanswer());
			setStringValue(ps, 11, record.getProblemvalue());
			setStringValue(ps, 12, record.getIqscount());
			setStringValue(ps, 13, record.getRstatus());
			setStringValue(ps, 14, record.getCreatedby());
			setDateValue(ps, 15, fd.getCurrentSQLDateObject());
			setDateValue(ps, 16, fd.getCurrentSQLDateObject());
			setStringValue(ps, 17, record.getModifiedby());
			boolean result = ps.execute();
			logger.trace("insertLIDrilldownmainaggregateRecord\t" + result + "\t");
			int resultID = -1;
			rs = ps.getGeneratedKeys();
			if (rs.next()) 
			{
				resultID = rs.getInt(1); 
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("drill_down_main_aggregate","INSERT",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			return resultID;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public int insertLIDrilldownmainaggregateRecord(LIDrilldownmainaggregateRecord record)
	throws Exception
	{
		return insertLIDrilldownmainaggregateRecord(record, null, true);
	}

	public boolean updateLIDrilldownmainaggregateRecord(LIDrilldownmainaggregateRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			LIDrilldownmainaggregateRecord currentRecord = loadLIDrilldownmainaggregateRecord(record.getId());
			String currentRecordContent = StringUtils.noNull(currentRecord);

			String Query = "UPDATE drill_down_main_aggregate SET ";
			Query += "sheetcolomname = ?, ";
			Query += "RawColumnName = ?, ";
			Query += "DisplayName = ?, ";
			Query += "drilldownof = ?, ";
			Query += "drilldownChartType = ?, ";
			Query += "SERVICECITY_30JDP = ?, ";
			Query += "Month = ?, ";
			Query += "modeldb = ?, ";
			Query += "fuel = ?, ";
			Query += "DisplayNameforAnswer = ?, ";
			Query += "problemvalue = ?, ";
			Query += "iqscount = ?, ";
			Query += "RSTATUS = ?, ";
			Query += "CREATED_BY = ?, ";
			Query += "CREATED_AT = ?, ";
			Query += "MODIFIED_AT = ?, ";
			Query += "MODIFIED_BY = ? ";
			Query += "WHERE (ID = ?) ";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("updateLIDrilldownmainaggregateRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			setStringValue(ps, 1, record.getSheetcolomname());
			setStringValue(ps, 2, record.getRawcolumnname());
			setStringValue(ps, 3, record.getDisplayname());
			setStringValue(ps, 4, record.getDrilldownof());
			setStringValue(ps, 5, record.getDrilldowncharttype());
			setStringValue(ps, 6, record.getServicecity30jdp());
			setStringValue(ps, 7, record.getMonth());
			setStringValue(ps, 8, record.getModeldb());
			setStringValue(ps, 9, record.getFuel());
			setStringValue(ps, 10, record.getDisplaynameforanswer());
			setStringValue(ps, 11, record.getProblemvalue());
			setStringValue(ps, 12, record.getIqscount());
			setStringValue(ps, 13, record.getRstatus());
			setStringValue(ps, 14, record.getCreatedby());
			setDateValue(ps, 15, fd.getSQLDateObject(record.getCreatedat(), "yyyyMMddHHmmss"));
			setDateValue(ps, 16, fd.getCurrentSQLDateObject());
			setStringValue(ps, 17, record.getModifiedby());
			ps.setString(18, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("updateLIDrilldownmainaggregateRecord\t" + result + "\t");
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("drill_down_main_aggregate","UPDATE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, currentRecordContent, record.getActionSource());
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean updateLIDrilldownmainaggregateRecord(LIDrilldownmainaggregateRecord record)
	throws Exception
	{
		return updateLIDrilldownmainaggregateRecord(record, null, true);
	}

	public boolean deleteLIDrilldownmainaggregateRecord(LIDrilldownmainaggregateRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "DELETE FROM drill_down_main_aggregate WHERE (ID = ?)";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("deleteLIDrilldownmainaggregateRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			ps.setString(1, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("deleteLIDrilldownmainaggregateRecord\t" + result + "\t");
			ps.close();
			createAuditRecord("drill_down_main_aggregate","DELETE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			releaseDatabaseConnection(con, closeConnection);
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean deleteLIDrilldownmainaggregateRecord(LIDrilldownmainaggregateRecord record)
	throws Exception
	{
		return deleteLIDrilldownmainaggregateRecord(record, null, true);
	}

	public LIDrilldownmainaggregateRecord[] searchLIDrilldownmainaggregateRecords(LIDrilldownmainaggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "sheetcolomname", formatSearchField(searchRecord.getSheetcolomname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "RawColumnName", formatSearchField(searchRecord.getRawcolumnname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DisplayName", formatSearchField(searchRecord.getDisplayname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "drilldownof", formatSearchField(searchRecord.getDrilldownof()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "drilldownChartType", formatSearchField(searchRecord.getDrilldowncharttype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SERVICECITY_30JDP", formatSearchField(searchRecord.getServicecity30jdp()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Month", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "modeldb", formatSearchField(searchRecord.getModeldb()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "fuel", formatSearchField(searchRecord.getFuel()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DisplayNameforAnswer", formatSearchField(searchRecord.getDisplaynameforanswer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "problemvalue", formatSearchField(searchRecord.getProblemvalue()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "iqscount", formatSearchField(searchRecord.getIqscount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from drill_down_main_aggregate " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM drill_down_main_aggregate ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM drill_down_main_aggregate $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIDrilldownmainaggregateRecords(Query);
	}

	public LIDrilldownmainaggregateRecord[] searchLIDrilldownmainaggregateRecordsExact(LIDrilldownmainaggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "sheetcolomname", formatSearchField(searchRecord.getSheetcolomname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RawColumnName", formatSearchField(searchRecord.getRawcolumnname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DisplayName", formatSearchField(searchRecord.getDisplayname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "drilldownof", formatSearchField(searchRecord.getDrilldownof()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "drilldownChartType", formatSearchField(searchRecord.getDrilldowncharttype()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SERVICECITY_30JDP", formatSearchField(searchRecord.getServicecity30jdp()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Month", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "modeldb", formatSearchField(searchRecord.getModeldb()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "fuel", formatSearchField(searchRecord.getFuel()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DisplayNameforAnswer", formatSearchField(searchRecord.getDisplaynameforanswer()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "problemvalue", formatSearchField(searchRecord.getProblemvalue()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "iqscount", formatSearchField(searchRecord.getIqscount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from drill_down_main_aggregate " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM drill_down_main_aggregate ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM drill_down_main_aggregate $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIDrilldownmainaggregateRecords(Query);
	}

	public LIDrilldownmainaggregateRecord[] searchLIDrilldownmainaggregateRecordsExactUpper(LIDrilldownmainaggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "sheetcolomname", formatSearchField(searchRecord.getSheetcolomname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "RawColumnName", formatSearchField(searchRecord.getRawcolumnname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DisplayName", formatSearchField(searchRecord.getDisplayname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "drilldownof", formatSearchField(searchRecord.getDrilldownof()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "drilldownChartType", formatSearchField(searchRecord.getDrilldowncharttype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SERVICECITY_30JDP", formatSearchField(searchRecord.getServicecity30jdp()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Month", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "modeldb", formatSearchField(searchRecord.getModeldb()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "fuel", formatSearchField(searchRecord.getFuel()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DisplayNameforAnswer", formatSearchField(searchRecord.getDisplaynameforanswer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "problemvalue", formatSearchField(searchRecord.getProblemvalue()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "iqscount", formatSearchField(searchRecord.getIqscount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from drill_down_main_aggregate " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM drill_down_main_aggregate ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM drill_down_main_aggregate $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIDrilldownmainaggregateRecords(Query);
	}

	public int loadLIDrilldownmainaggregateRecordCount(LIDrilldownmainaggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "sheetcolomname", formatSearchField(searchRecord.getSheetcolomname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "RawColumnName", formatSearchField(searchRecord.getRawcolumnname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DisplayName", formatSearchField(searchRecord.getDisplayname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "drilldownof", formatSearchField(searchRecord.getDrilldownof()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "drilldownChartType", formatSearchField(searchRecord.getDrilldowncharttype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SERVICECITY_30JDP", formatSearchField(searchRecord.getServicecity30jdp()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Month", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "modeldb", formatSearchField(searchRecord.getModeldb()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "fuel", formatSearchField(searchRecord.getFuel()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DisplayNameforAnswer", formatSearchField(searchRecord.getDisplaynameforanswer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "problemvalue", formatSearchField(searchRecord.getProblemvalue()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "iqscount", formatSearchField(searchRecord.getIqscount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from drill_down_main_aggregate " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLIDrilldownmainaggregateRecordCountExact(LIDrilldownmainaggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "sheetcolomname", formatSearchField(searchRecord.getSheetcolomname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RawColumnName", formatSearchField(searchRecord.getRawcolumnname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DisplayName", formatSearchField(searchRecord.getDisplayname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "drilldownof", formatSearchField(searchRecord.getDrilldownof()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "drilldownChartType", formatSearchField(searchRecord.getDrilldowncharttype()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SERVICECITY_30JDP", formatSearchField(searchRecord.getServicecity30jdp()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Month", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "modeldb", formatSearchField(searchRecord.getModeldb()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "fuel", formatSearchField(searchRecord.getFuel()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DisplayNameforAnswer", formatSearchField(searchRecord.getDisplaynameforanswer()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "problemvalue", formatSearchField(searchRecord.getProblemvalue()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "iqscount", formatSearchField(searchRecord.getIqscount()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from drill_down_main_aggregate " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}

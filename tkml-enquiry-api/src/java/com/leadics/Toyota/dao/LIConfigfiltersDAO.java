
/*
 * LIConfigfiltersDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.dao;

import com.leadics.Toyota.to.LIConfigfiltersRecord;
import com.leadics.Toyota.common.LIDAO;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;

public class LIConfigfiltersDAO extends LIDAO {

    static LogUtils logger = new LogUtils(LIConfigfiltersDAO.class.getName());

    public LIConfigfiltersRecord[] loadLIConfigfiltersRecords(String query, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            query = query + MAX_RECORD_LIMIT_APPENDER;
            query = updateQuery(query);
            logger.trace("loadLIConfigfiltersRecords\t" + closeConnection + "\t" + query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList recordSet = new ArrayList();
            while (rs.next()) {
                LIConfigfiltersRecord record = new LIConfigfiltersRecord();
                record.setId(rs.getString("ID"));
                record.setModuleName(rs.getString("MODULE_NAME"));
                record.setFilterName(rs.getString("FILTER_NAME"));
                record.setFilterActionString(rs.getString("FILTER_ACTION_STRING"));
                record.setCreatedat(rs.getString("CREATED_AT"));
                record.setCreatedby(rs.getString("CREATED_BY"));
                record.setModifiedat(rs.getString("MODIFIED_AT"));
                record.setModifiedby(rs.getString("MODIFIED_BY"));
                recordSet.add(record);
            }
            logger.trace("loadLIConfigfiltersRecords:Records Fetched:" + recordSet.size());
            LIConfigfiltersRecord[] tempLIConfigfiltersRecords = new LIConfigfiltersRecord[recordSet.size()];
            for (int index = 0; index < recordSet.size(); index++) {
                tempLIConfigfiltersRecords[index] = (LIConfigfiltersRecord) (recordSet.get(index));
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            return tempLIConfigfiltersRecords;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigfiltersRecord[] loadLIConfigfiltersRecords(String query)
            throws Exception {
        return loadLIConfigfiltersRecords(query, null, true);
    }

    public LIConfigfiltersRecord loadFirstLIConfigfiltersRecord(String query)
            throws Exception {
        LIConfigfiltersRecord[] results = loadLIConfigfiltersRecords(query);
        if (results == null) {
            return null;
        }
        if (results.length < 1) {
            return null;
        }
        return results[0];
    }

    public LIConfigfiltersRecord loadLIConfigfiltersRecord(String id, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            String Query = "SELECT * FROM config_filters WHERE (ID = ?)";
            Query = updateQuery(Query);
            logger.trace("loadLIConfigfiltersRecord\t" + closeConnection + "\t" + id + "\t" + Query);
            ps = con.prepareStatement(Query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (!rs.next()) {
                ps.close();
                releaseDatabaseConnection(con, closeConnection);
                return null;
            }
            LIConfigfiltersRecord record = new LIConfigfiltersRecord();
            record.setId(rs.getString("ID"));
            record.setModuleName(rs.getString("MODULE_NAME"));
            record.setFilterName(rs.getString("FILTER_NAME"));
            record.setFilterActionString(rs.getString("FILTER_ACTION_STRING"));
            record.setCreatedat(rs.getString("CREATED_AT"));
            record.setCreatedby(rs.getString("CREATED_BY"));
            record.setModifiedat(rs.getString("MODIFIED_AT"));
            record.setModifiedby(rs.getString("MODIFIED_BY"));
            ps.close();
            logger.trace("loadLIConfigfiltersRecord\t" + record + "\t");
            releaseDatabaseConnection(con, closeConnection);
            return record;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigfiltersRecord loadLIConfigfiltersRecord(String id)
            throws Exception {
        return loadLIConfigfiltersRecord(id, null, true);
    }

    public int insertLIConfigfiltersRecord(LIConfigfiltersRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String Query = "INSERT INTO config_filters ";
            Query += "(";
            Query += "MODULE_NAME,FILTER_NAME,FILTER_ACTION_STRING,CREATED_AT,CREATED_BY,MODIFIED_AT,MODIFIED_BY";
            Query += ")";
            Query += " VALUES ";
            Query += "(";
            Query += "?,?,?,?,?,?,?";
            Query += ")";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            record.setModifiedby(StringUtils.noNull(record.getModifiedby(), record.getCreatedby()));
            Query = updateQuery(Query);
            logger.trace("insertLIConfigfiltersRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            if (isOracleDatabase()) {
                ps = con.prepareStatement(Query, new String[]{"ID"});
            } else {
                ps = con.prepareStatement(Query, Statement.RETURN_GENERATED_KEYS);
            }
            setStringValue(ps, 1, record.getModuleName());
            setStringValue(ps, 2, record.getFilterName());
            setStringValue(ps, 3, record.getFilterActionString());
            setStringValue(ps, 4, record.getCreatedat());
            setStringValue(ps, 5, record.getCreatedby());
            setStringValue(ps, 6, record.getModifiedat());
            setStringValue(ps, 7, record.getModifiedby());
            boolean result = ps.execute();
            logger.trace("insertLIConfigfiltersRecord\t" + result + "\t");
            int resultID = -1;
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                resultID = rs.getInt(1);
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            createAuditRecord("config_filters", "INSERT", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
            return resultID;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public int insertLIConfigfiltersRecord(LIConfigfiltersRecord record)
            throws Exception {
        return insertLIConfigfiltersRecord(record, null, true);
    }

    public boolean updateLIConfigfiltersRecord(LIConfigfiltersRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            LIConfigfiltersRecord currentRecord = loadLIConfigfiltersRecord(record.getId());
            String currentRecordContent = StringUtils.noNull(currentRecord);

            String Query = "UPDATE config_filters SET ";
            Query += "MODULE_NAME = IFNULL(?, MODULE_NAME),";
            Query += "FILTER_NAME = IFNULL(?, FILTER_NAME),";
            Query += "FILTER_ACTION_STRING = IFNULL(?, FILTER_ACTION_STRING),";
            Query += "CREATED_AT = IFNULL(?, CREATED_AT),";
            Query += "CREATED_BY = IFNULL(?, CREATED_BY),";
            Query += "MODIFIED_AT = IFNULL(?, MODIFIED_AT),";
            Query += "MODIFIED_BY = IFNULL(?, MODIFIED_BY ";
            Query += "WHERE (ID = ?) ";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            Query = updateQuery(Query);
            logger.trace("updateLIConfigfiltersRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            ps = con.prepareStatement(Query);
            setStringValue(ps, 1, record.getModuleName());
            setStringValue(ps, 2, record.getFilterName());
            setStringValue(ps, 3, record.getFilterActionString());
            setStringValue(ps, 4, record.getCreatedat());
            setStringValue(ps, 5, record.getCreatedby());
            setStringValue(ps, 6, record.getModifiedat());
            setStringValue(ps, 7, record.getModifiedby());
            ps.setString(8, noNull(record.getId()));
            boolean result = ps.execute();
            logger.trace("updateLIConfigfiltersRecord\t" + result + "\t");
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            createAuditRecord("config_filters", "UPDATE", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, currentRecordContent, record.getActionSource());
            return result;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public boolean updateLIConfigfiltersRecord(LIConfigfiltersRecord record)
            throws Exception {
        return updateLIConfigfiltersRecord(record, null, true);
    }

    public boolean deleteLIConfigfiltersRecord(LIConfigfiltersRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String Query = "DELETE FROM config_filters WHERE (ID = ?)";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            Query = updateQuery(Query);
            logger.trace("deleteLIConfigfiltersRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            ps = con.prepareStatement(Query);
            ps.setString(1, noNull(record.getId()));
            boolean result = ps.execute();
            logger.trace("deleteLIConfigfiltersRecord\t" + result + "\t");
            ps.close();
            createAuditRecord("config_filters", "DELETE", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
            releaseDatabaseConnection(con, closeConnection);
            return result;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public boolean deleteLIConfigfiltersRecord(LIConfigfiltersRecord record)
            throws Exception {
        return deleteLIConfigfiltersRecord(record, null, true);
    }

    public LIConfigfiltersRecord[] searchLIConfigfiltersRecords(LIConfigfiltersRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODULE_NAME", formatSearchField(searchRecord.getModulename()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_ACTION_STRING", formatSearchField(searchRecord.getFilteractionstring()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_filters " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_filters ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_filters $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigfiltersRecords(Query);
    }

    public LIConfigfiltersRecord[] searchLIConfigfiltersRecordsExact(LIConfigfiltersRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODULE_NAME", formatSearchField(searchRecord.getModulename()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_ACTION_STRING", formatSearchField(searchRecord.getFilteractionstring()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_filters " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_filters ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_filters $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigfiltersRecords(Query);
    }

    public LIConfigfiltersRecord[] searchLIConfigfiltersRecordsExactUpper(LIConfigfiltersRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODULE_NAME", formatSearchField(searchRecord.getModulename()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_ACTION_STRING", formatSearchField(searchRecord.getFilteractionstring()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_filters " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_filters ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_filters $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigfiltersRecords(Query);
    }

    public int loadLIConfigfiltersRecordCount(LIConfigfiltersRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODULE_NAME", formatSearchField(searchRecord.getModulename()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_ACTION_STRING", formatSearchField(searchRecord.getFilteractionstring()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select count(*) from config_filters " + WhereCondition;
        Query = updateQuery(Query);
        logger.trace("Search Count Query	" + Query + "\t");
        return loadCount(Query);
    }

    public int loadLIConfigfiltersRecordCountExact(LIConfigfiltersRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
//        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODULE_NAME", formatSearchField(searchRecord.getModulename()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_ACTION_STRING", formatSearchField(searchRecord.getFilteractionstring()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
//        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
//        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select count(*) from config_filters " + WhereCondition;
        Query = updateQuery(Query);
        logger.trace("Search Count Query	" + Query + "\t");
        return loadCount(Query);
    }
}

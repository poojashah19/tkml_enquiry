
/*
 * LIConfigdivDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.dao;
import com.leadics.Toyota.to.LIConfigdivRecord;
import com.leadics.Toyota.common.LIDAO;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIConfigdivDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LIConfigdivDAO.class.getName());


	public LIConfigdivRecord[] loadLIConfigdivRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLIConfigdivRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LIConfigdivRecord record = new LIConfigdivRecord();
				record.setId(rs.getString("id"));
				record.setModuleName(rs.getString("module_name"));
				record.setDivName(rs.getString("div_name"));
				record.setActionString(rs.getString("action_string"));
				record.setFilters(rs.getString("filters"));
				record.setCreatedat(rs.getString("created_at"));
				record.setCreatedby(rs.getString("created_by"));
				record.setModifiedat(rs.getString("modified_at"));
				record.setModifiedby(rs.getString("modified_by"));
				recordSet.add(record);
			}
			logger.trace("loadLIConfigdivRecords:Records Fetched:" + recordSet.size());
			LIConfigdivRecord[] tempLIConfigdivRecords = new LIConfigdivRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLIConfigdivRecords[index] = (LIConfigdivRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLIConfigdivRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LIConfigdivRecord[] loadLIConfigdivRecords(String query)
	throws Exception
	{
		return loadLIConfigdivRecords(query, null, true);
	}


	public LIConfigdivRecord loadFirstLIConfigdivRecord(String query)
	throws Exception
	{
		LIConfigdivRecord[] results = loadLIConfigdivRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LIConfigdivRecord loadLIConfigdivRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM config_div WHERE (id = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLIConfigdivRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LIConfigdivRecord record = new LIConfigdivRecord();
			record.setId(rs.getString("id"));
			record.setModuleName(rs.getString("module_name"));
			record.setDivName(rs.getString("div_name"));
			record.setActionString(rs.getString("action_string"));
			record.setFilters(rs.getString("filters"));
			record.setCreatedat(rs.getString("created_at"));
			record.setCreatedby(rs.getString("created_by"));
			record.setModifiedat(rs.getString("modified_at"));
			record.setModifiedby(rs.getString("modified_by"));
			ps.close();
			logger.trace("loadLIConfigdivRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LIConfigdivRecord loadLIConfigdivRecord(String id)
	throws Exception
	{
		return loadLIConfigdivRecord(id, null, true);
	}

	public int insertLIConfigdivRecord(LIConfigdivRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "INSERT INTO config_div ";
			Query += "(";
			Query += "module_name,div_name,action_string,filters,created_at,created_by,modified_at,modified_by";
			Query += ")";
			Query += " VALUES ";
			Query += "(";
			Query += "?,?,?,?,?,?,?,?";
			Query += ")";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			record.setModifiedby(StringUtils.noNull(record.getModifiedby(),record.getCreatedby()));
			Query = updateQuery(Query);
			logger.trace("insertLIConfigdivRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			if (isOracleDatabase()) 
			{
				ps = con.prepareStatement(Query,new String[]{"ID"});
			}
			else
			{
				ps = con.prepareStatement(Query,Statement.RETURN_GENERATED_KEYS);
			}
			setStringValue(ps, 1, record.getModuleName());
			setStringValue(ps, 2, record.getDivName());
			setStringValue(ps, 3, record.getActionString());
			setStringValue(ps, 4, record.getFilters());
			setStringValue(ps, 5, record.getCreatedat());
			setStringValue(ps, 6, record.getCreatedby());
			setStringValue(ps, 7, record.getModifiedat());
			setStringValue(ps, 8, record.getModifiedby());
			boolean result = ps.execute();
			logger.trace("insertLIConfigdivRecord\t" + result + "\t");
			int resultID = -1;
			rs = ps.getGeneratedKeys();
			if (rs.next()) 
			{
				resultID = rs.getInt(1); 
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("config_div","INSERT",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			return resultID;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public int insertLIConfigdivRecord(LIConfigdivRecord record)
	throws Exception
	{
		return insertLIConfigdivRecord(record, null, true);
	}

	public boolean updateLIConfigdivRecord(LIConfigdivRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			LIConfigdivRecord currentRecord = loadLIConfigdivRecord(record.getId());
			String currentRecordContent = StringUtils.noNull(currentRecord);

			String Query = "UPDATE config_div SET ";
			Query += "module_name = IFNULL(?, module_name),";
			Query += "div_name = IFNULL(?, div_name),";
			Query += "action_string = IFNULL(?, action_string),";
			Query += "filters = IFNULL(?, filters),";
			Query += "created_at = IFNULL(?, created_at),";
			Query += "created_by = IFNULL(?, created_by),";
			Query += "modified_at = IFNULL(?, modified_at),";
			Query += "modified_by = IFNULL(?, modified_by ";
			Query += "WHERE (id = ?) ";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("updateLIConfigdivRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			setStringValue(ps, 1, record.getModuleName());
			setStringValue(ps, 2, record.getDivName());
			setStringValue(ps, 3, record.getActionString());
			setStringValue(ps, 4, record.getFilters());
			setStringValue(ps, 5, record.getCreatedat());
			setStringValue(ps, 6, record.getCreatedby());
			setStringValue(ps, 7, record.getModifiedat());
			setStringValue(ps, 8, record.getModifiedby());
			ps.setString(9, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("updateLIConfigdivRecord\t" + result + "\t");
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("config_div","UPDATE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, currentRecordContent, record.getActionSource());
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean updateLIConfigdivRecord(LIConfigdivRecord record)
	throws Exception
	{
		return updateLIConfigdivRecord(record, null, true);
	}

	public boolean deleteLIConfigdivRecord(LIConfigdivRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "DELETE FROM config_div WHERE (id = ?)";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("deleteLIConfigdivRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			ps.setString(1, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("deleteLIConfigdivRecord\t" + result + "\t");
			ps.close();
			createAuditRecord("config_div","DELETE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			releaseDatabaseConnection(con, closeConnection);
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean deleteLIConfigdivRecord(LIConfigdivRecord record)
	throws Exception
	{
		return deleteLIConfigdivRecord(record, null, true);
	}

	public LIConfigdivRecord[] searchLIConfigdivRecords(LIConfigdivRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "module_name", formatSearchField(searchRecord.getModuleName()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "div_name", formatSearchField(searchRecord.getDivName()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "action_string", formatSearchField(searchRecord.getActionString()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "filters", formatSearchField(searchRecord.getFilters()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "created_at", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "created_by", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "modified_at", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "modified_by", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from config_div " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_div ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_div $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIConfigdivRecords(Query);
	}

	public LIConfigdivRecord[] searchLIConfigdivRecordsExact(LIConfigdivRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "module_name", formatSearchField(searchRecord.getModuleName()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "div_name", formatSearchField(searchRecord.getDivName()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "action_string", formatSearchField(searchRecord.getActionString()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "filters", formatSearchField(searchRecord.getFilters()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "created_at", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "created_by", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "modified_at", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "modified_by", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from config_div " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_div ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_div $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIConfigdivRecords(Query);
	}

	public LIConfigdivRecord[] searchLIConfigdivRecordsExactUpper(LIConfigdivRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "module_name", formatSearchField(searchRecord.getModuleName()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "div_name", formatSearchField(searchRecord.getDivName()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "action_string", formatSearchField(searchRecord.getActionString()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "filters", formatSearchField(searchRecord.getFilters()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "created_at", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "created_by", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "modified_at", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "modified_by", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from config_div " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_div ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_div $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIConfigdivRecords(Query);
	}

	public int loadLIConfigdivRecordCount(LIConfigdivRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "module_name", formatSearchField(searchRecord.getModuleName()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "div_name", formatSearchField(searchRecord.getDivName()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "action_string", formatSearchField(searchRecord.getActionString()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "filters", formatSearchField(searchRecord.getFilters()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "created_at", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "created_by", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "modified_at", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "modified_by", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from config_div " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLIConfigdivRecordCountExact(LIConfigdivRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "module_name", formatSearchField(searchRecord.getModuleName()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "div_name", formatSearchField(searchRecord.getDivName()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "action_string", formatSearchField(searchRecord.getActionString()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "filters", formatSearchField(searchRecord.getFilters()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "created_at", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "created_by", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "modified_at", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "modified_by", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from config_div " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}

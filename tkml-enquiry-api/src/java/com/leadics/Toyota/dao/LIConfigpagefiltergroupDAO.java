
/*
 * LIConfigpagefiltergroupDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.dao;

import com.leadics.Toyota.to.LIConfigpagefiltergroupRecord;
import com.leadics.Toyota.common.LIDAO;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;

public class LIConfigpagefiltergroupDAO extends LIDAO {

    static LogUtils logger = new LogUtils(LIConfigpagefiltergroupDAO.class.getName());

    public LIConfigpagefiltergroupRecord[] loadLIConfigpagefiltergroupRecords(String query, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            query = query + MAX_RECORD_LIMIT_APPENDER;
            query = updateQuery(query);
            logger.trace("loadLIConfigpagefiltergroupRecords\t" + closeConnection + "\t" + query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList recordSet = new ArrayList();
            while (rs.next()) {
                LIConfigpagefiltergroupRecord record = new LIConfigpagefiltergroupRecord();
                record.setId(rs.getString("ID"));
                record.setGroupfiltername(rs.getString("GROUP_FILTER_NAME"));
                record.setFiltername(rs.getString("FILTER_NAME"));
                record.setFiltertype(rs.getString("FILTER_TYPE"));
                record.setSlugName(rs.getString("FILTER_SLUG_NAME"));
                record.setFilterplacehodler(rs.getString("FILTER_PLACEHODLER"));
                record.setFilterpriority(rs.getInt("FILTER_PRIORITY"));
                record.setConditionvalue(rs.getString("CONDITION_VALUE"));
                record.setParents(rs.getString("PARENTS"));
                record.setChilds(rs.getString("CHILDS"));
                record.setDefaultindex(rs.getInt("DEFAULT_INDEX"));
                record.setContainsall(rs.getBoolean("CONTAINS_ALL"));
                record.setContainsnone(rs.getBoolean("CONTAINS_NONE"));
                record.setCreatedat(rs.getString("CREATED_AT"));
                record.setCreatedby(rs.getString("CREATED_BY"));
                record.setModifiedat(rs.getString("MODIFIED_AT"));
                record.setModifiedby(rs.getString("MODIFIED_BY"));
                recordSet.add(record);
            }
            logger.trace("loadLIConfigpagefiltergroupRecords:Records Fetched:" + recordSet.size());
            LIConfigpagefiltergroupRecord[] tempLIConfigpagefiltergroupRecords = new LIConfigpagefiltergroupRecord[recordSet.size()];
            for (int index = 0; index < recordSet.size(); index++) {
                tempLIConfigpagefiltergroupRecords[index] = (LIConfigpagefiltergroupRecord) (recordSet.get(index));
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            return tempLIConfigpagefiltergroupRecords;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigpagefiltergroupRecord[] loadLIConfigpagefiltergroupRecords(String query)
            throws Exception {
        return loadLIConfigpagefiltergroupRecords(query, null, true);
    }

    public LIConfigpagefiltergroupRecord loadFirstLIConfigpagefiltergroupRecord(String query)
            throws Exception {
        LIConfigpagefiltergroupRecord[] results = loadLIConfigpagefiltergroupRecords(query);
        if (results == null) {
            return null;
        }
        if (results.length < 1) {
            return null;
        }
        return results[0];
    }

    public LIConfigpagefiltergroupRecord loadLIConfigpagefiltergroupRecord(String id, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            String Query = "SELECT * FROM config_page_filter_group WHERE (ID = ?)";
            Query = updateQuery(Query);
            logger.trace("loadLIConfigpagefiltergroupRecord\t" + closeConnection + "\t" + id + "\t" + Query);
            ps = con.prepareStatement(Query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (!rs.next()) {
                ps.close();
                releaseDatabaseConnection(con, closeConnection);
                return null;
            }
            LIConfigpagefiltergroupRecord record = new LIConfigpagefiltergroupRecord();
            record.setId(rs.getString("ID"));
            record.setGroupfiltername(rs.getString("GROUP_FILTER_NAME"));
             record.setSlugName(rs.getString("FILTER_SLUG_NAME"));
            record.setFiltername(rs.getString("FILTER_NAME"));
            record.setFiltertype(rs.getString("FILTER_TYPE"));
            record.setFilterplacehodler(rs.getString("FILTER_PLACEHODLER"));
            record.setFilterpriority(rs.getInt("FILTER_PRIORITY"));
            record.setConditionvalue(rs.getString("CONDITION_VALUE"));
            record.setParents(rs.getString("PARENTS"));
            record.setChilds(rs.getString("CHILDS"));
            record.setDefaultindex(rs.getInt("DEFAULT_INDEX"));
            record.setContainsall(rs.getBoolean("CONTAINS_ALL"));
            record.setContainsnone(rs.getBoolean("CONTAINS_NONE"));
            record.setCreatedat(rs.getString("CREATED_AT"));
            record.setCreatedby(rs.getString("CREATED_BY"));
            record.setModifiedat(rs.getString("MODIFIED_AT"));
            record.setModifiedby(rs.getString("MODIFIED_BY"));

            ps.close();
            logger.trace("loadLIConfigpagefiltergroupRecord\t" + record + "\t");
            releaseDatabaseConnection(con, closeConnection);
            return record;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigpagefiltergroupRecord loadLIConfigpagefiltergroupRecord(String id)
            throws Exception {
        return loadLIConfigpagefiltergroupRecord(id, null, true);
    }

    public int insertLIConfigpagefiltergroupRecord(LIConfigpagefiltergroupRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String Query = "INSERT INTO config_page_filter_group ";
            Query += "(";
            Query += "GROUP_FILTER_NAME,FILTER_NAME,FILTER_TYPE,FILTER_PLACEHODLER,FILTER_PRIORITY,CONDITION_VALUE,PARENTS,CHILDS,DEFAULT_INDEX,CONTAINS_ALL,CONTAINS_NONE,CREATED_AT,CREATED_BY,MODIFIED_AT,MODIFIED_BY";
            Query += ")";
            Query += " VALUES ";
            Query += "(";
            Query += "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
            Query += ")";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            record.setModifiedby(StringUtils.noNull(record.getModifiedby(), record.getCreatedby()));
            Query = updateQuery(Query);
            logger.trace("insertLIConfigpagefiltergroupRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            if (isOracleDatabase()) {
                ps = con.prepareStatement(Query, new String[]{"ID"});
            } else {
                ps = con.prepareStatement(Query, Statement.RETURN_GENERATED_KEYS);
            }
            setStringValue(ps, 1, record.getGroupfiltername());
            setStringValue(ps, 2, record.getFiltername());
            setStringValue(ps, 3, record.getFiltertype());
            setStringValue(ps, 4, record.getFilterplacehodler());
            ps.setInt(5, record.getFilterpriority());
            setStringValue(ps, 6, record.getConditionvalue());
            setStringValue(ps, 7, record.getParents());
            setStringValue(ps, 8, record.getChilds());
            ps.setInt(9, record.getDefaultindex());
            ps.setBoolean(10, record.getContainsall());
            ps.setBoolean(11, record.getContainsnone());

            setStringValue(ps, 12, record.getCreatedat());
            setStringValue(ps, 13, record.getCreatedby());
            setStringValue(ps, 14, record.getModifiedat());
            setStringValue(ps, 15, record.getModifiedby());
            boolean result = ps.execute();
            logger.trace("insertLIConfigpagefiltergroupRecord\t" + result + "\t");
            int resultID = -1;
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                resultID = rs.getInt(1);
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            createAuditRecord("config_page_filter_group", "INSERT", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
            return resultID;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public int insertLIConfigpagefiltergroupRecord(LIConfigpagefiltergroupRecord record)
            throws Exception {
        return insertLIConfigpagefiltergroupRecord(record, null, true);
    }

    public boolean updateLIConfigpagefiltergroupRecord(LIConfigpagefiltergroupRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            LIConfigpagefiltergroupRecord currentRecord = loadLIConfigpagefiltergroupRecord(record.getId());
            String currentRecordContent = StringUtils.noNull(currentRecord);

            String Query = "UPDATE config_page_filter_group SET ";
            Query += "GROUP_FILTER_NAME = IFNULL(?, GROUP_FILTER_NAME),";
            Query += "FILTER_NAME = IFNULL(?, FILTER_NAME),";
            Query += "FILTER_TYPE = IFNULL(?, FILTER_TYPE),";
            Query += "FILTER_PLACEHODLER = IFNULL(?, FILTER_PLACEHODLER),";
            Query += "FILTER_PRIORITY = IFNULL(?, FILTER_PRIORITY),";
            Query += "CONDITION_VALUE = IFNULL(?, CONDITION_VALUE),";
            Query += "PARENTS = IFNULL(?, PARENTS),";
            Query += "CHILDS = IFNULL(?, CHILDS),";
            Query += "DEFAULT_INDEX = IFNULL(?, DEFAULT_INDEX),";
            Query += "CONTAINS_ALL = IFNULL(?, CONTAINS_ALL),";
            Query += "CONTAINS_NONE = IFNULL(?, CONTAINS_NONE),";
            Query += "CREATED_AT = IFNULL(?, CREATED_AT),";
            Query += "CREATED_BY = IFNULL(?, CREATED_BY),";
            Query += "MODIFIED_AT = IFNULL(?, MODIFIED_AT),";
            Query += "MODIFIED_BY = IFNULL(?, MODIFIED_BY ";
            Query += "WHERE (ID = ?) ";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            Query = updateQuery(Query);
            logger.trace("updateLIConfigpagefiltergroupRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            ps = con.prepareStatement(Query);
            setStringValue(ps, 1, record.getGroupfiltername());
            setStringValue(ps, 2, record.getFiltername());
            setStringValue(ps, 3, record.getFiltertype());
            setStringValue(ps, 4, record.getFilterplacehodler());
            ps.setInt(5, record.getFilterpriority());
            setStringValue(ps, 6, record.getConditionvalue());
            setStringValue(ps, 7, record.getParents());
            setStringValue(ps, 8, record.getChilds());
            ps.setInt(9, record.getDefaultindex());
            ps.setBoolean(10, record.getContainsall());
            ps.setBoolean(11, record.getContainsnone());

            setStringValue(ps, 12, record.getCreatedat());
            setStringValue(ps, 13, record.getCreatedby());
            setStringValue(ps, 14, record.getModifiedat());
            setStringValue(ps, 15, record.getModifiedby());
            ps.setString(16, noNull(record.getId()));
            boolean result = ps.execute();
            logger.trace("updateLIConfigpagefiltergroupRecord\t" + result + "\t");
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            createAuditRecord("config_page_filter_group", "UPDATE", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, currentRecordContent, record.getActionSource());
            return result;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public boolean updateLIConfigpagefiltergroupRecord(LIConfigpagefiltergroupRecord record)
            throws Exception {
        return updateLIConfigpagefiltergroupRecord(record, null, true);
    }

    public boolean deleteLIConfigpagefiltergroupRecord(LIConfigpagefiltergroupRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String Query = "DELETE FROM config_page_filter_group WHERE (ID = ?)";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            Query = updateQuery(Query);
            logger.trace("deleteLIConfigpagefiltergroupRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            ps = con.prepareStatement(Query);
            ps.setString(1, noNull(record.getId()));
            boolean result = ps.execute();
            logger.trace("deleteLIConfigpagefiltergroupRecord\t" + result + "\t");
            ps.close();
            createAuditRecord("config_page_filter_group", "DELETE", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
            releaseDatabaseConnection(con, closeConnection);
            return result;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public boolean deleteLIConfigpagefiltergroupRecord(LIConfigpagefiltergroupRecord record)
            throws Exception {
        return deleteLIConfigpagefiltergroupRecord(record, null, true);
    }

    public LIConfigpagefiltergroupRecord[] searchLIConfigpagefiltergroupRecords(LIConfigpagefiltergroupRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "GROUP_FILTER_NAME", formatSearchField(searchRecord.getGroupfiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_TYPE", formatSearchField(searchRecord.getFiltertype()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_PLACEHODLER", formatSearchField(searchRecord.getFilterplacehodler()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_PRIORITY", formatSearchField(searchRecord.getFilterpriority()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONDITION_VALUE", formatSearchField(searchRecord.getConditionvalue()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PARENTS", formatSearchField(searchRecord.getParents()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHILDS", formatSearchField(searchRecord.getChilds()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DEFAULT_INDEX", formatSearchField(searchRecord.getDefaultindex()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CONTAINS_ALL", formatSearchField(searchRecord.getContainsall()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONTAINS_NONE", formatSearchField(searchRecord.getContainsnone()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_page_filter_group " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_page_filter_group ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_page_filter_group $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigpagefiltergroupRecords(Query);
    }

    public LIConfigpagefiltergroupRecord[] searchLIConfigpagefiltergroupRecordsExact(LIConfigpagefiltergroupRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "GROUP_FILTER_NAME", formatSearchField(searchRecord.getGroupfiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_TYPE", formatSearchField(searchRecord.getFiltertype()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_PLACEHODLER", formatSearchField(searchRecord.getFilterplacehodler()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_PRIORITY", formatSearchField(searchRecord.getFilterpriority()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONDITION_VALUE", formatSearchField(searchRecord.getConditionvalue()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PARENTS", formatSearchField(searchRecord.getParents()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHILDS", formatSearchField(searchRecord.getChilds()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DEFAULT_INDEX", formatSearchField(searchRecord.getDefaultindex()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONTAINS_ALL", formatSearchField(searchRecord.getContainsall()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONTAINS_NONE", formatSearchField(searchRecord.getContainsnone()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_page_filter_group " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_page_filter_group ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_page_filter_group $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigpagefiltergroupRecords(Query);
    }

    public LIConfigpagefiltergroupRecord[] searchLIConfigpagefiltergroupRecordsExactUpper(LIConfigpagefiltergroupRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "GROUP_FILTER_NAME", formatSearchField(searchRecord.getGroupfiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_TYPE", formatSearchField(searchRecord.getFiltertype()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_PLACEHODLER", formatSearchField(searchRecord.getFilterplacehodler()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_PRIORITY", formatSearchField(searchRecord.getFilterpriority()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONDITION_VALUE", formatSearchField(searchRecord.getConditionvalue()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PARENTS", formatSearchField(searchRecord.getParents()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHILDS", formatSearchField(searchRecord.getChilds()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DEFAULT_INDEX", formatSearchField(searchRecord.getDefaultindex()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONTAINS_ALL", formatSearchField(searchRecord.getContainsall()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONTAINS_NONE", formatSearchField(searchRecord.getContainsnone()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_page_filter_group " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_page_filter_group ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_page_filter_group $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigpagefiltergroupRecords(Query);
    }

    public int loadLIConfigpagefiltergroupRecordCount(LIConfigpagefiltergroupRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "GROUP_FILTER_NAME", formatSearchField(searchRecord.getGroupfiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_TYPE", formatSearchField(searchRecord.getFiltertype()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_PLACEHODLER", formatSearchField(searchRecord.getFilterplacehodler()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_PRIORITY", formatSearchField(searchRecord.getFilterpriority()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONDITION_VALUE", formatSearchField(searchRecord.getConditionvalue()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PARENTS", formatSearchField(searchRecord.getParents()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHILDS", formatSearchField(searchRecord.getChilds()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DEFAULT_INDEX", formatSearchField(searchRecord.getDefaultindex()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CONTAINS_ALL", formatSearchField(searchRecord.getContainsall()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONTAINS_NONE", formatSearchField(searchRecord.getContainsnone()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select count(*) from config_page_filter_group " + WhereCondition;
        Query = updateQuery(Query);
        logger.trace("Search Count Query	" + Query + "\t");
        return loadCount(Query);
    }

    public int loadLIConfigpagefiltergroupRecordCountExact(LIConfigpagefiltergroupRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "GROUP_FILTER_NAME", formatSearchField(searchRecord.getGroupfiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_TYPE", formatSearchField(searchRecord.getFiltertype()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_PLACEHODLER", formatSearchField(searchRecord.getFilterplacehodler()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_PRIORITY", formatSearchField(searchRecord.getFilterpriority()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONDITION_VALUE", formatSearchField(searchRecord.getConditionvalue()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PARENTS", formatSearchField(searchRecord.getParents()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHILDS", formatSearchField(searchRecord.getChilds()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DEFAULT_INDEX", formatSearchField(searchRecord.getDefaultindex()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONTAINS_ALL", formatSearchField(searchRecord.getContainsall()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONTAINS_NONE", formatSearchField(searchRecord.getContainsnone()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select count(*) from config_page_filter_group " + WhereCondition;
        Query = updateQuery(Query);
        logger.trace("Search Count Query	" + Query + "\t");
        return loadCount(Query);
    }
    
    
}

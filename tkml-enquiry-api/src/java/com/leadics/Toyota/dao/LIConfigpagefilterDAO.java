
/*
 * LIConfigpagefilterDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.dao;

import com.leadics.Toyota.to.LIConfigpagefilterRecord;
import com.leadics.Toyota.common.LIDAO;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;

public class LIConfigpagefilterDAO extends LIDAO {

    static LogUtils logger = new LogUtils(LIConfigpagefilterDAO.class.getName());

    public LIConfigpagefilterRecord[] loadLIConfigpagefilterRecords(String query, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            query = query + MAX_RECORD_LIMIT_APPENDER;
            query = updateQuery(query);
            logger.trace("loadLIConfigpagefilterRecords\t" + closeConnection + "\t" + query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList recordSet = new ArrayList();
            while (rs.next()) {
                LIConfigpagefilterRecord record = new LIConfigpagefilterRecord();
//				record.setId(rs.getString("ID"));
                record.setPagename(rs.getString("PAGE_NAME"));
                record.setFiltername(rs.getString("FILTER_NAME"));
				record.setSlugfiltername(rs.getString("SLUG_FILTER_NAME"));
                record.setFiltertype(rs.getString("FILTER_TYPE"));
                record.setFilterplacehodler(rs.getString("FILTER_PLACEHODLER"));
                record.setFilterpriority(rs.getDouble("FILTER_PRIORITY"));
                record.setParents(rs.getString("PARENTS"));
                record.setChilds(rs.getString("CHILDS"));
                record.setDefaultindex(rs.getInt("DEFAULT_INDEX"));
                record.setContainsall(rs.getBoolean("CONTAINS_ALL"));
                record.setContainsnone(rs.getBoolean("CONTAINS_NONE"));
                record.setContainsfiltergroup(rs.getBoolean("CONTAINS_FILTER_GROUP"));
//				record.setCreatedat(rs.getString("CREATED_AT"));
//				record.setCreatedby(rs.getString("CREATED_BY"));
//				record.setModifiedat(rs.getString("MODIFIED_AT"));
//				record.setModifiedby(rs.getString("MODIFIED_BY"));
                recordSet.add(record);
            }
            logger.trace("loadLIConfigpagefilterRecords:Records Fetched:" + recordSet.size());
            LIConfigpagefilterRecord[] tempLIConfigpagefilterRecords = new LIConfigpagefilterRecord[recordSet.size()];
            for (int index = 0; index < recordSet.size(); index++) {
                tempLIConfigpagefilterRecords[index] = (LIConfigpagefilterRecord) (recordSet.get(index));
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            return tempLIConfigpagefilterRecords;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigpagefilterRecord[] loadLIConfigpagefilterRecords(String query)
            throws Exception {
        return loadLIConfigpagefilterRecords(query, null, true);
    }

    public LIConfigpagefilterRecord loadFirstLIConfigpagefilterRecord(String query)
            throws Exception {
        LIConfigpagefilterRecord[] results = loadLIConfigpagefilterRecords(query);
        if (results == null) {
            return null;
        }
        if (results.length < 1) {
            return null;
        }
        return results[0];
    }

    public LIConfigpagefilterRecord loadLIConfigpagefilterRecord(String id, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            String Query = "SELECT * FROM config_page_filter WHERE (ID = ?)";
            Query = updateQuery(Query);
            logger.trace("loadLIConfigpagefilterRecord\t" + closeConnection + "\t" + id + "\t" + Query);
            ps = con.prepareStatement(Query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (!rs.next()) {
                ps.close();
                releaseDatabaseConnection(con, closeConnection);
                return null;
            }
            LIConfigpagefilterRecord record = new LIConfigpagefilterRecord();
            record.setId(rs.getString("ID"));
            record.setPagename(rs.getString("PAGE_NAME"));
            record.setFiltername(rs.getString("FILTER_NAME"));
            record.setSlugfiltername(rs.getString("SLUG_FILTER_NAME"));
            record.setFiltertype(rs.getString("FILTER_TYPE"));
            record.setFilterplacehodler(rs.getString("FILTER_PLACEHODLER"));
         record.setFilterpriority(rs.getInt("FILTER_PRIORITY"));
            record.setParents(rs.getString("PARENTS"));
            record.setChilds(rs.getString("CHILDS"));
            record.setDefaultindex(rs.getInt("DEFAULT_INDEX"));
            record.setContainsall(rs.getBoolean("CONTAINS_ALL"));
            record.setContainsnone(rs.getBoolean("CONTAINS_NONE"));
            record.setContainsfiltergroup(rs.getBoolean("CONTAINS_FILTER_GROUP"));
            record.setCreatedat(rs.getString("CREATED_AT"));
            record.setCreatedby(rs.getString("CREATED_BY"));
            record.setModifiedat(rs.getString("MODIFIED_AT"));
            record.setModifiedby(rs.getString("MODIFIED_BY"));
            ps.close();
            logger.trace("loadLIConfigpagefilterRecord\t" + record + "\t");
            releaseDatabaseConnection(con, closeConnection);
            return record;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigpagefilterRecord loadLIConfigpagefilterRecord(String id)
            throws Exception {
        return loadLIConfigpagefilterRecord(id, null, true);
    }

    public int insertLIConfigpagefilterRecord(LIConfigpagefilterRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String Query = "INSERT INTO config_page_filter ";
            Query += "(";
            Query += "PAGE_NAME,FILTER_NAME,SLUG_FILTER_NAME,FILTER_TYPE,FILTER_PLACEHODLER,FILTER_PRIORITY,PARENTS,CHILDS,DEFAULT_INDEX,CONTAINS_ALL,CONTAINS_NONE,CONTAINS_FILTER_GROUP,CREATED_AT,CREATED_BY,MODIFIED_AT,MODIFIED_BY";
            Query += ")";
            Query += " VALUES ";
            Query += "(";
            Query += "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
            Query += ")";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            record.setModifiedby(StringUtils.noNull(record.getModifiedby(), record.getCreatedby()));
            Query = updateQuery(Query);
            logger.trace("insertLIConfigpagefilterRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            if (isOracleDatabase()) {
                ps = con.prepareStatement(Query, new String[]{"ID"});
            } else {
                ps = con.prepareStatement(Query, Statement.RETURN_GENERATED_KEYS);
            }
            setStringValue(ps, 1, record.getPagename());
            setStringValue(ps, 2, record.getFiltername());
            setStringValue(ps, 3, record.getSlugfiltername());
            setStringValue(ps, 4, record.getFiltertype());
            setStringValue(ps, 5, record.getFilterplacehodler());
            ps.setDouble(6, record.getFilterpriority());
            
            setStringValue(ps, 7, record.getParents());
            setStringValue(ps, 8, record.getChilds());
            ps.setInt(9, record.getDefaultindex());
            ps.setBoolean(10, record.getContainsall());
            ps.setBoolean(11, record.getContainsnone());
            ps.setBoolean(12, record.getContainsfiltergroup());

            setStringValue(ps, 13, record.getCreatedat());
            setStringValue(ps, 14, record.getCreatedby());
            setStringValue(ps, 15, record.getModifiedat());
            setStringValue(ps, 16, record.getModifiedby());
            boolean result = ps.execute();
            logger.trace("insertLIConfigpagefilterRecord\t" + result + "\t");
            int resultID = -1;
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                resultID = rs.getInt(1);
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            createAuditRecord("config_page_filter", "INSERT", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
            return resultID;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public int insertLIConfigpagefilterRecord(LIConfigpagefilterRecord record)
            throws Exception {
        return insertLIConfigpagefilterRecord(record, null, true);
    }

    public boolean updateLIConfigpagefilterRecord(LIConfigpagefilterRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            LIConfigpagefilterRecord currentRecord = loadLIConfigpagefilterRecord(record.getId());
            String currentRecordContent = StringUtils.noNull(currentRecord);

            String Query = "UPDATE config_page_filter SET ";
            Query += "PAGE_NAME = IFNULL(?, PAGE_NAME),";
            Query += "FILTER_NAME = IFNULL(?, FILTER_NAME),";
            Query += "SLUG_FILTER_NAME = IFNULL(?, SLUG_FILTER_NAME),";
            Query += "FILTER_TYPE = IFNULL(?, FILTER_TYPE),";
            Query += "FILTER_PLACEHODLER = IFNULL(?, FILTER_PLACEHODLER),";
            Query += "FILTER_PRIORITY = IFNULL(?, FILTER_PRIORITY),";
            Query += "PARENTS = IFNULL(?, PARENTS),";
            Query += "CHILDS = IFNULL(?, CHILDS),";
            Query += "DEFAULT_INDEX = IFNULL(?, DEFAULT_INDEX),";
            Query += "CONTAINS_ALL = IFNULL(?, CONTAINS_ALL),";
            Query += "CONTAINS_NONE = IFNULL(?, CONTAINS_NONE),";
            Query += "CONTAINS_FILTER_GROUP = IFNULL(?, CONTAINS_FILTER_GROUP),";
            Query += "CREATED_AT = IFNULL(?, CREATED_AT),";
            Query += "CREATED_BY = IFNULL(?, CREATED_BY),";
            Query += "MODIFIED_AT = IFNULL(?, MODIFIED_AT),";
            Query += "MODIFIED_BY = IFNULL(?, MODIFIED_BY ";
            Query += "WHERE (ID = ?) ";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            Query = updateQuery(Query);
            logger.trace("updateLIConfigpagefilterRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            ps = con.prepareStatement(Query);
            setStringValue(ps, 1, record.getPagename());
            setStringValue(ps, 2, record.getFiltername());
            setStringValue(ps, 3, record.getSlugfiltername());
            setStringValue(ps, 4, record.getFiltertype());
            setStringValue(ps, 5, record.getFilterplacehodler());
            ps.setDouble(6, record.getFilterpriority());
            setStringValue(ps, 7, record.getParents());
            setStringValue(ps, 8, record.getChilds());
            ps.setInt(9, record.getDefaultindex());
            ps.setBoolean(10, record.getContainsall());
            ps.setBoolean(11, record.getContainsnone());
            ps.setBoolean(12, record.getContainsfiltergroup());
            setStringValue(ps, 13, record.getCreatedat());
            setStringValue(ps, 14, record.getCreatedby());
            setStringValue(ps, 15, record.getModifiedat());
            setStringValue(ps, 16, record.getModifiedby());
            ps.setString(17, noNull(record.getId()));
            boolean result = ps.execute();
            logger.trace("updateLIConfigpagefilterRecord\t" + result + "\t");
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            createAuditRecord("config_page_filter", "UPDATE", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, currentRecordContent, record.getActionSource());
            return result;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public boolean updateLIConfigpagefilterRecord(LIConfigpagefilterRecord record)
            throws Exception {
        return updateLIConfigpagefilterRecord(record, null, true);
    }

    public boolean deleteLIConfigpagefilterRecord(LIConfigpagefilterRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String Query = "DELETE FROM config_page_filter WHERE (ID = ?)";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            Query = updateQuery(Query);
            logger.trace("deleteLIConfigpagefilterRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            ps = con.prepareStatement(Query);
            ps.setString(1, noNull(record.getId()));
            boolean result = ps.execute();
            logger.trace("deleteLIConfigpagefilterRecord\t" + result + "\t");
            ps.close();
            createAuditRecord("config_page_filter", "DELETE", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
            releaseDatabaseConnection(con, closeConnection);
            return result;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public boolean deleteLIConfigpagefilterRecord(LIConfigpagefilterRecord record)
            throws Exception {
        return deleteLIConfigpagefilterRecord(record, null, true);
    }

    public LIConfigpagefilterRecord[] searchLIConfigpagefilterRecords(LIConfigpagefilterRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PAGE_NAME", formatSearchField(searchRecord.getPagename()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SLUG_FILTER_NAME", formatSearchField(searchRecord.getSlugfiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_TYPE", formatSearchField(searchRecord.getFiltertype()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_PLACEHODLER", formatSearchField(searchRecord.getFilterplacehodler()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_PRIORITY", formatSearchField(searchRecord.getFilterpriority()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PARENTS", formatSearchField(searchRecord.getParents()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHILDS", formatSearchField(searchRecord.getChilds()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DEFAULT_INDEX", formatSearchField(searchRecord.getDefaultindex()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CONTAINS_ALL", formatSearchField(searchRecord.getContainsall()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONTAINS_NONE", formatSearchField(searchRecord.getContainsnone()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CONTAINS_FILTER_GROUP", formatSearchField(searchRecord.getContainsfiltergroup()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_page_filter " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_page_filter ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_page_filter $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigpagefilterRecords(Query);
    }

    public LIConfigpagefilterRecord[] searchLIConfigpagefilterRecordsExact(LIConfigpagefilterRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PAGE_NAME", formatSearchField(searchRecord.getPagename()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SLUG_FILTER_NAME", formatSearchField(searchRecord.getSlugfiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_TYPE", formatSearchField(searchRecord.getFiltertype()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_PLACEHODLER", formatSearchField(searchRecord.getFilterplacehodler()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_PRIORITY", formatSearchField(searchRecord.getFilterpriority()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PARENTS", formatSearchField(searchRecord.getParents()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHILDS", formatSearchField(searchRecord.getChilds()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DEFAULT_INDEX", formatSearchField(searchRecord.getDefaultindex()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONTAINS_ALL", formatSearchField(searchRecord.getContainsall()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONTAINS_NONE", formatSearchField(searchRecord.getContainsnone()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONTAINS_FILTER_GROUP", formatSearchField(searchRecord.getContainsfiltergroup()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_page_filter " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_page_filter ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_page_filter $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigpagefilterRecords(Query);
    }

    public LIConfigpagefilterRecord[] searchLIConfigpagefilterRecordsExactUpper(LIConfigpagefilterRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PAGE_NAME", formatSearchField(searchRecord.getPagename()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SLUG_FILTER_NAME", formatSearchField(searchRecord.getSlugfiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_TYPE", formatSearchField(searchRecord.getFiltertype()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_PLACEHODLER", formatSearchField(searchRecord.getFilterplacehodler()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_PRIORITY", formatSearchField(searchRecord.getFilterpriority()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PARENTS", formatSearchField(searchRecord.getParents()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHILDS", formatSearchField(searchRecord.getChilds()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DEFAULT_INDEX", formatSearchField(searchRecord.getDefaultindex()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONTAINS_ALL", formatSearchField(searchRecord.getContainsall()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONTAINS_NONE", formatSearchField(searchRecord.getContainsnone()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONTAINS_FILTER_GROUP", formatSearchField(searchRecord.getContainsfiltergroup()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_page_filter " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_page_filter ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_page_filter $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigpagefilterRecords(Query);
    }

    public int loadLIConfigpagefilterRecordCount(LIConfigpagefilterRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PAGE_NAME", formatSearchField(searchRecord.getPagename()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SLUG_FILTER_NAME", formatSearchField(searchRecord.getSlugfiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FILTER_TYPE", formatSearchField(searchRecord.getFiltertype()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_PLACEHODLER", formatSearchField(searchRecord.getFilterplacehodler()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FILTER_PRIORITY", formatSearchField(searchRecord.getFilterpriority()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PARENTS", formatSearchField(searchRecord.getParents()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHILDS", formatSearchField(searchRecord.getChilds()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DEFAULT_INDEX", formatSearchField(searchRecord.getDefaultindex()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CONTAINS_ALL", formatSearchField(searchRecord.getContainsall()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CONTAINS_NONE", formatSearchField(searchRecord.getContainsnone()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CONTAINS_FILTER_GROUP", formatSearchField(searchRecord.getContainsfiltergroup()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select count(*) from config_page_filter " + WhereCondition;
        Query = updateQuery(Query);
        logger.trace("Search Count Query	" + Query + "\t");
        return loadCount(Query);
    }

    public int loadLIConfigpagefilterRecordCountExact(LIConfigpagefilterRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PAGE_NAME", formatSearchField(searchRecord.getPagename()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_NAME", formatSearchField(searchRecord.getFiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SLUG_FILTER_NAME", formatSearchField(searchRecord.getSlugfiltername()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_TYPE", formatSearchField(searchRecord.getFiltertype()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_PLACEHODLER", formatSearchField(searchRecord.getFilterplacehodler()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FILTER_PRIORITY", formatSearchField(searchRecord.getFilterpriority()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PARENTS", formatSearchField(searchRecord.getParents()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHILDS", formatSearchField(searchRecord.getChilds()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DEFAULT_INDEX", formatSearchField(searchRecord.getDefaultindex()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONTAINS_ALL", formatSearchField(searchRecord.getContainsall()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONTAINS_NONE", formatSearchField(searchRecord.getContainsnone()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CONTAINS_FILTER_GROUP", formatSearchField(searchRecord.getContainsfiltergroup()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select count(*) from config_page_filter " + WhereCondition;
        Query = updateQuery(Query);
        logger.trace("Search Count Query	" + Query + "\t");
        return loadCount(Query);
    }
}

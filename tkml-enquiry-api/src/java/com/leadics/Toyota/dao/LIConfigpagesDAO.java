
/*
 * LIConfigpagesDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.dao;

import com.leadics.Toyota.to.LIConfigpagesRecord;
import com.leadics.Toyota.common.LIDAO;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;

public class LIConfigpagesDAO extends LIDAO {

    static LogUtils logger = new LogUtils(LIConfigpagesDAO.class.getName());

    public LIConfigpagesRecord[] loadLIConfigpagesRecords(String query, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            query = query + MAX_RECORD_LIMIT_APPENDER;
            query = updateQuery(query);
            logger.trace("loadLIConfigpagesRecords\t" + closeConnection + "\t" + query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList recordSet = new ArrayList();
            while (rs.next()) {
                LIConfigpagesRecord record = new LIConfigpagesRecord();
//				record.setId(rs.getString("ID"));
//				record.setModulename(rs.getString("MODULE_NAME"));
//				record.setSlugname(rs.getString("SLUG_NAME"));
                record.setTitle(rs.getString("TITLE"));
                record.setPageName(rs.getString("PAGE_NAME"));
                record.setIcon(rs.getString("ICON"));
                record.setSub(rs.getString("SUB"));
                record.setRouting(rs.getString("ROUTING"));
                record.setExternalLink(rs.getString("EXTERNAL_LINK"));
                record.setBudge(rs.getString("BUDGE"));
                record.setBudgeColor(rs.getString("BUDGE_COLOR"));
                record.setActive(rs.getBoolean("ACTIVE"));
                record.setGroupTItle(rs.getBoolean("GROUP_TITLE"));
//				record.setCreatedat(rs.getString("CREATED_AT"));
//				record.setCreatedby(rs.getString("CREATED_BY"));
//				record.setModifiedat(rs.getString("MODIFIED_AT"));
//				record.setModifiedby(rs.getString("MODIFIED_BY"));
                recordSet.add(record);
            }
            logger.trace("loadLIConfigpagesRecords:Records Fetched:" + recordSet.size());
            LIConfigpagesRecord[] tempLIConfigpagesRecords = new LIConfigpagesRecord[recordSet.size()];
            for (int index = 0; index < recordSet.size(); index++) {
                tempLIConfigpagesRecords[index] = (LIConfigpagesRecord) (recordSet.get(index));
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            return tempLIConfigpagesRecords;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigpagesRecord[] loadLIConfigpagesRecords(String query)
            throws Exception {
        return loadLIConfigpagesRecords(query, null, true);
    }

    public LIConfigpagesRecord loadFirstLIConfigpagesRecord(String query)
            throws Exception {
        LIConfigpagesRecord[] results = loadLIConfigpagesRecords(query);
        if (results == null) {
            return null;
        }
        if (results.length < 1) {
            return null;
        }
        return results[0];
    }

    public LIConfigpagesRecord loadLIConfigpagesRecord(String id, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            String Query = "SELECT * FROM config_pages WHERE (ID = ?)";
            Query = updateQuery(Query);
            logger.trace("loadLIConfigpagesRecord\t" + closeConnection + "\t" + id + "\t" + Query);
            ps = con.prepareStatement(Query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (!rs.next()) {
                ps.close();
                releaseDatabaseConnection(con, closeConnection);
                return null;
            }
            LIConfigpagesRecord record = new LIConfigpagesRecord();
            record.setId(rs.getString("ID"));
            record.setModuleName(rs.getString("MODULE_NAME"));
            record.setSlugName(rs.getString("SLUG_NAME"));
            record.setTitle(rs.getString("TITLE"));
            record.setPageName(rs.getString("PAGE_NAME"));
            record.setIcon(rs.getString("ICON"));
            record.setSub(rs.getString("SUB"));
            record.setRouting(rs.getString("ROUTING"));
            record.setExternalLink(rs.getString("EXTERNAL_LINK"));
            record.setBudge(rs.getString("BUDGE"));
            record.setBudgeColor(rs.getString("BUDGE_COLOR"));
            record.setActive(rs.getBoolean("ACTIVE"));
            record.setGroupTItle(rs.getBoolean("GROUP_TITLE"));
            record.setCreatedat(rs.getString("CREATED_AT"));
            record.setCreatedby(rs.getString("CREATED_BY"));
            record.setModifiedat(rs.getString("MODIFIED_AT"));
            record.setModifiedby(rs.getString("MODIFIED_BY"));
            ps.close();
            logger.trace("loadLIConfigpagesRecord\t" + record + "\t");
            releaseDatabaseConnection(con, closeConnection);
            return record;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigpagesRecord loadLIConfigpagesRecord(String id)
            throws Exception {
        return loadLIConfigpagesRecord(id, null, true);
    }

    public int insertLIConfigpagesRecord(LIConfigpagesRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String Query = "INSERT INTO config_pages ";
            Query += "(";
            Query += "MODULE_NAME,SLUG_NAME,TITLE,PAGE_NAME,ICON,SUB,ROUTING,EXTERNAL_LINK,BUDGE,BUDGE_COLOR,ACTIVE,GROUP_TITLE,CREATED_AT,CREATED_BY,MODIFIED_AT,MODIFIED_BY";
            Query += ")";
            Query += " VALUES ";
            Query += "(";
            Query += "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
            Query += ")";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            record.setModifiedby(StringUtils.noNull(record.getModifiedby(), record.getCreatedby()));
            Query = updateQuery(Query);
            logger.trace("insertLIConfigpagesRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            if (isOracleDatabase()) {
                ps = con.prepareStatement(Query, new String[]{"ID"});
            } else {
                ps = con.prepareStatement(Query, Statement.RETURN_GENERATED_KEYS);
            }
            setStringValue(ps, 1, record.getModuleName());
            setStringValue(ps, 2, record.getSlugName());
            setStringValue(ps, 3, record.getTitle());
            setStringValue(ps, 4, record.getPageName());
            setStringValue(ps, 5, record.getIcon());
            setStringValue(ps, 6, record.getSub());
            setStringValue(ps, 7, record.getRouting());
            setStringValue(ps, 8, record.getExternalLink());
            setStringValue(ps, 9, record.getBudge());
            setStringValue(ps, 10, record.getBudgeColor());
            ps.setBoolean(11, record.getActive());
            ps.setBoolean(12, record.getGrouptitle());

            setStringValue(ps, 13, record.getCreatedat());
            setStringValue(ps, 14, record.getCreatedby());
            setStringValue(ps, 15, record.getModifiedat());
            setStringValue(ps, 16, record.getModifiedby());
            boolean result = ps.execute();
            logger.trace("insertLIConfigpagesRecord\t" + result + "\t");
            int resultID = -1;
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                resultID = rs.getInt(1);
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            createAuditRecord("config_pages", "INSERT", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
            return resultID;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public int insertLIConfigpagesRecord(LIConfigpagesRecord record)
            throws Exception {
        return insertLIConfigpagesRecord(record, null, true);
    }

    public boolean updateLIConfigpagesRecord(LIConfigpagesRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            LIConfigpagesRecord currentRecord = loadLIConfigpagesRecord(record.getId());
            String currentRecordContent = StringUtils.noNull(currentRecord);

            String Query = "UPDATE config_pages SET ";
            Query += "MODULE_NAME = IFNULL(?, MODULE_NAME),";
            Query += "SLUG_NAME = IFNULL(?, SLUG_NAME),";
            Query += "TITLE = IFNULL(?, TITLE),";
            Query += "PAGE_NAME = IFNULL(?, PAGE_NAME),";
            Query += "ICON = IFNULL(?, ICON),";
            Query += "SUB = IFNULL(?, SUB),";
            Query += "ROUTING = IFNULL(?, ROUTING),";
            Query += "EXTERNAL_LINK = IFNULL(?, EXTERNAL_LINK),";
            Query += "BUDGE = IFNULL(?, BUDGE),";
            Query += "BUDGE_COLOR = IFNULL(?, BUDGE_COLOR),";
            Query += "ACTIVE = IFNULL(?, ACTIVE),";
            Query += "GROUP_TITLE = IFNULL(?, GROUP_TITLE),";
            Query += "CREATED_AT = IFNULL(?, CREATED_AT),";
            Query += "CREATED_BY = IFNULL(?, CREATED_BY),";
            Query += "MODIFIED_AT = IFNULL(?, MODIFIED_AT),";
            Query += "MODIFIED_BY = IFNULL(?, MODIFIED_BY ";
            Query += "WHERE (ID = ?) ";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            Query = updateQuery(Query);
            logger.trace("updateLIConfigpagesRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            ps = con.prepareStatement(Query);
            setStringValue(ps, 1, record.getModuleName());
            setStringValue(ps, 2, record.getSlugName());
            setStringValue(ps, 3, record.getTitle());
            setStringValue(ps, 4, record.getPageName());
            setStringValue(ps, 5, record.getIcon());
            setStringValue(ps, 6, record.getSub());
            setStringValue(ps, 7, record.getRouting());
            setStringValue(ps, 8, record.getExternalLink());
            setStringValue(ps, 9, record.getBudge());
            setStringValue(ps, 10, record.getBudgeColor());
            ps.setBoolean(11, record.getActive());
            ps.setBoolean(12, record.getGrouptitle());
            setStringValue(ps, 13, record.getCreatedat());
            setStringValue(ps, 14, record.getCreatedby());
            setStringValue(ps, 15, record.getModifiedat());
            setStringValue(ps, 16, record.getModifiedby());
            ps.setString(17, noNull(record.getId()));
            boolean result = ps.execute();
            logger.trace("updateLIConfigpagesRecord\t" + result + "\t");
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            createAuditRecord("config_pages", "UPDATE", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, currentRecordContent, record.getActionSource());
            return result;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public boolean updateLIConfigpagesRecord(LIConfigpagesRecord record)
            throws Exception {
        return updateLIConfigpagesRecord(record, null, true);
    }

    public boolean deleteLIConfigpagesRecord(LIConfigpagesRecord record, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String Query = "DELETE FROM config_pages WHERE (ID = ?)";
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            Query = updateQuery(Query);
            logger.trace("deleteLIConfigpagesRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
            ps = con.prepareStatement(Query);
            ps.setString(1, noNull(record.getId()));
            boolean result = ps.execute();
            logger.trace("deleteLIConfigpagesRecord\t" + result + "\t");
            ps.close();
            createAuditRecord("config_pages", "DELETE", record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
            releaseDatabaseConnection(con, closeConnection);
            return result;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public boolean deleteLIConfigpagesRecord(LIConfigpagesRecord record)
            throws Exception {
        return deleteLIConfigpagesRecord(record, null, true);
    }

    public LIConfigpagesRecord[] searchLIConfigpagesRecords(LIConfigpagesRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODULE_NAME", formatSearchField(searchRecord.getModuleName()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SLUG_NAME", formatSearchField(searchRecord.getSlugName()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "TITLE", formatSearchField(searchRecord.getTitle()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PAGE_NAME", formatSearchField(searchRecord.getPageName()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ICON", formatSearchField(searchRecord.getIcon()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SUB", formatSearchField(searchRecord.getSub()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ROUTING", formatSearchField(searchRecord.getRouting()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "EXTERNAL_LINK", formatSearchField(searchRecord.getExternalLink()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "BUDGE", formatSearchField(searchRecord.getBudge()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "BUDGE_COLOR", formatSearchField(searchRecord.getBudgeColor()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ACTIVE", formatSearchField(searchRecord.getActive()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "GROUP_TITLE", formatSearchField(searchRecord.getGrouptitle()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_pages " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_pages ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_pages $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigpagesRecords(Query);
    }

    public LIConfigpagesRecord[] searchLIConfigpagesRecordsExact(LIConfigpagesRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODULE_NAME", formatSearchField(searchRecord.getModuleName()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SLUG_NAME", formatSearchField(searchRecord.getSlugName()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "TITLE", formatSearchField(searchRecord.getTitle()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PAGE_NAME", formatSearchField(searchRecord.getPageName()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ICON", formatSearchField(searchRecord.getIcon()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SUB", formatSearchField(searchRecord.getSub()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ROUTING", formatSearchField(searchRecord.getRouting()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "EXTERNAL_LINK", formatSearchField(searchRecord.getExternalLink()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "BUDGE", formatSearchField(searchRecord.getBudge()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "BUDGE_COLOR", formatSearchField(searchRecord.getBudgeColor()));
////        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ACTIVE", formatSearchField(searchRecord.getActive()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "GROUP_TITLE", formatSearchField(searchRecord.getGrouptitle()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_pages " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_pages ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_pages $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigpagesRecords(Query);
    }

    public LIConfigpagesRecord[] searchLIConfigpagesRecordsExactUpper(LIConfigpagesRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODULE_NAME", formatSearchField(searchRecord.getModuleName()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SLUG_NAME", formatSearchField(searchRecord.getSlugName()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "TITLE", formatSearchField(searchRecord.getTitle()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PAGE_NAME", formatSearchField(searchRecord.getPageName()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ICON", formatSearchField(searchRecord.getIcon()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SUB", formatSearchField(searchRecord.getSub()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ROUTING", formatSearchField(searchRecord.getRouting()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "EXTERNAL_LINK", formatSearchField(searchRecord.getExternalLink()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "BUDGE", formatSearchField(searchRecord.getBudge()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "BUDGE_COLOR", formatSearchField(searchRecord.getBudgeColor()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ACTIVE", formatSearchField(searchRecord.getActive()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "GROUP_TITLE", formatSearchField(searchRecord.getGrouptitle()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from config_pages " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM config_pages ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM config_pages $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);
        logger.trace("Search Query	" + Query + "\t");
        return loadLIConfigpagesRecords(Query);
    }

    public int loadLIConfigpagesRecordCount(LIConfigpagesRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODULE_NAME", formatSearchField(searchRecord.getModuleName()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SLUG_NAME", formatSearchField(searchRecord.getSlugName()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "TITLE", formatSearchField(searchRecord.getTitle()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PAGE_NAME", formatSearchField(searchRecord.getPageName()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ICON", formatSearchField(searchRecord.getIcon()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SUB", formatSearchField(searchRecord.getSub()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ROUTING", formatSearchField(searchRecord.getRouting()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "EXTERNAL_LINK", formatSearchField(searchRecord.getExternalLink()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "BUDGE", formatSearchField(searchRecord.getBudge()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "BUDGE_COLOR", formatSearchField(searchRecord.getBudgeColor()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ACTIVE", formatSearchField(searchRecord.getActive()));
//        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "GROUP_TITLE", formatSearchField(searchRecord.getGrouptitle()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select count(*) from config_pages " + WhereCondition;
        Query = updateQuery(Query);
        logger.trace("Search Count Query	" + Query + "\t");
        return loadCount(Query);
    }

    public int loadLIConfigpagesRecordCountExact(LIConfigpagesRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODULE_NAME", formatSearchField(searchRecord.getModuleName()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SLUG_NAME", formatSearchField(searchRecord.getSlugName()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "TITLE", formatSearchField(searchRecord.getTitle()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PAGE_NAME", formatSearchField(searchRecord.getPageName()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ICON", formatSearchField(searchRecord.getIcon()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SUB", formatSearchField(searchRecord.getSub()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ROUTING", formatSearchField(searchRecord.getRouting()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "EXTERNAL_LINK", formatSearchField(searchRecord.getExternalLink()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "BUDGE", formatSearchField(searchRecord.getBudge()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "BUDGE_COLOR", formatSearchField(searchRecord.getBudgeColor()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ACTIVE", formatSearchField(searchRecord.getActive()));
//        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "GROUP_TITLE", formatSearchField(searchRecord.getGrouptitle()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
        WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select count(*) from config_pages " + WhereCondition;
        Query = updateQuery(Query);
        logger.trace("Search Count Query	" + Query + "\t");
        return loadCount(Query);
    }
}

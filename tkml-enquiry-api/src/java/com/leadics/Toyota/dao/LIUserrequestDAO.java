
/*
 * LIUserrequestDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.dao;
import com.leadics.Toyota.to.LIUserrequestRecord;
import com.leadics.Toyota.common.LIDAO;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIUserrequestDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LIUserrequestDAO.class.getName());


	public LIUserrequestRecord[] loadLIUserrequestRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLIUserrequestRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LIUserrequestRecord record = new LIUserrequestRecord();
				record.setId(rs.getString("Id"));
				record.setUserrecid(rs.getString("USERRECID"));
				record.setUserid(rs.getString("USERID"));
				record.setUname(rs.getString("UNAME"));
				record.setRequesttype(rs.getString("REQUEST_TYPE"));
				record.setComments(rs.getString("COMMENTS"));
				record.setInstitutionid(rs.getString("INSTITUTION_ID"));
				record.setMadeby(rs.getString("MADE_BY"));
				record.setMadeat(formatDBDateTime(rs.getTimestamp("MADE_AT")));
				record.setCheckedby(rs.getString("CHECKED_BY"));
				record.setCheckedat(formatDBDateTime(rs.getTimestamp("CHECKED_AT")));
				record.setMakerlastcmt(rs.getString("MAKER_LAST_CMT"));
				record.setCheckerlastcmt(rs.getString("CHECKER_LAST_CMT"));
				record.setCurrappstatus(rs.getString("CURR_APP_STATUS"));
				record.setAdminlastcmt(rs.getString("ADMIN_LAST_CMT"));
				record.setRstatus(rs.getString("RSTATUS"));
				record.setCreatedby(rs.getString("CREATED_BY"));
				record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
				record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
				record.setModifiedby(rs.getString("MODIFIED_BY"));
				recordSet.add(record);
			}
			logger.trace("loadLIUserrequestRecords:Records Fetched:" + recordSet.size());
			LIUserrequestRecord[] tempLIUserrequestRecords = new LIUserrequestRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLIUserrequestRecords[index] = (LIUserrequestRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLIUserrequestRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LIUserrequestRecord[] loadLIUserrequestRecords(String query)
	throws Exception
	{
		return loadLIUserrequestRecords(query, null, true);
	}


	public LIUserrequestRecord loadFirstLIUserrequestRecord(String query)
	throws Exception
	{
		LIUserrequestRecord[] results = loadLIUserrequestRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LIUserrequestRecord loadLIUserrequestRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM user_request WHERE (Id = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLIUserrequestRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LIUserrequestRecord record = new LIUserrequestRecord();
			record.setId(rs.getString("Id"));
			record.setUserrecid(rs.getString("USERRECID"));
			record.setUserid(rs.getString("USERID"));
			record.setUname(rs.getString("UNAME"));
			record.setRequesttype(rs.getString("REQUEST_TYPE"));
			record.setComments(rs.getString("COMMENTS"));
			record.setInstitutionid(rs.getString("INSTITUTION_ID"));
			record.setMadeby(rs.getString("MADE_BY"));
			record.setMadeat(formatDBDateTime(rs.getTimestamp("MADE_AT")));
			record.setCheckedby(rs.getString("CHECKED_BY"));
			record.setCheckedat(formatDBDateTime(rs.getTimestamp("CHECKED_AT")));
			record.setMakerlastcmt(rs.getString("MAKER_LAST_CMT"));
			record.setCheckerlastcmt(rs.getString("CHECKER_LAST_CMT"));
			record.setCurrappstatus(rs.getString("CURR_APP_STATUS"));
			record.setAdminlastcmt(rs.getString("ADMIN_LAST_CMT"));
			record.setRstatus(rs.getString("RSTATUS"));
			record.setCreatedby(rs.getString("CREATED_BY"));
			record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
			record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
			record.setModifiedby(rs.getString("MODIFIED_BY"));
			ps.close();
			logger.trace("loadLIUserrequestRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LIUserrequestRecord loadLIUserrequestRecord(String id)
	throws Exception
	{
		return loadLIUserrequestRecord(id, null, true);
	}

	public int insertLIUserrequestRecord(LIUserrequestRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "INSERT INTO user_request ";
			Query += "(";
			Query += "USERRECID,USERID,UNAME,REQUEST_TYPE,COMMENTS,INSTITUTION_ID,MADE_BY,MADE_AT,CHECKED_BY,CHECKED_AT,MAKER_LAST_CMT,CHECKER_LAST_CMT,CURR_APP_STATUS,ADMIN_LAST_CMT,RSTATUS,CREATED_BY,CREATED_AT,MODIFIED_AT,MODIFIED_BY";
			Query += ")";
			Query += " VALUES ";
			Query += "(";
			Query += "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
			Query += ")";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			record.setModifiedby(StringUtils.noNull(record.getModifiedby(),record.getCreatedby()));
			record.setMadeat(StringUtils.noNull(record.getMadeat(),record.getCreatedat()));
			record.setMadeby(StringUtils.noNull(record.getMadeby(),record.getCreatedby()));
			record.setInstitutionid(StringUtils.noNull(record.getInstitutionid(),PropertyUtil.getProperty("DefaultInstitutionId")));
			Query = updateQuery(Query);
			logger.trace("insertLIUserrequestRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			if (isOracleDatabase()) 
			{
				ps = con.prepareStatement(Query,new String[]{"ID"});
			}
			else
			{
				ps = con.prepareStatement(Query,Statement.RETURN_GENERATED_KEYS);
			}
			setStringValue(ps, 1, record.getUserrecid());
			setStringValue(ps, 2, record.getUserid());
			setStringValue(ps, 3, record.getUname());
			setStringValue(ps, 4, record.getRequesttype());
			setStringValue(ps, 5, record.getComments());
			setStringValue(ps, 6, record.getInstitutionid());
			setStringValue(ps, 7, record.getMadeby());
			setDateValue(ps, 8, fd.getSQLDateObject(record.getMadeat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 9, record.getCheckedby());
			setDateValue(ps, 10, fd.getSQLDateObject(record.getCheckedat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 11, record.getMakerlastcmt());
			setStringValue(ps, 12, record.getCheckerlastcmt());
			setStringValue(ps, 13, record.getCurrappstatus());
			setStringValue(ps, 14, record.getAdminlastcmt());
			setStringValue(ps, 15, record.getRstatus());
			setStringValue(ps, 16, record.getCreatedby());
			setDateValue(ps, 17, fd.getCurrentSQLDateObject());
			setDateValue(ps, 18, fd.getCurrentSQLDateObject());
			setStringValue(ps, 19, record.getModifiedby());
			boolean result = ps.execute();
			logger.trace("insertLIUserrequestRecord\t" + result + "\t");
			int resultID = -1;
			rs = ps.getGeneratedKeys();
			if (rs.next()) 
			{
				resultID = rs.getInt(1); 
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("user_request","INSERT",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), record.getActionSource());
			return resultID;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public int insertLIUserrequestRecord(LIUserrequestRecord record)
	throws Exception
	{
		return insertLIUserrequestRecord(record, null, true);
	}

	public boolean updateLIUserrequestRecord(LIUserrequestRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			LIUserrequestRecord currentRecord = loadLIUserrequestRecord(record.getId());
			String currentRecordContent = StringUtils.noNull(currentRecord);

			String Query = "UPDATE user_request SET ";
			Query += "USERRECID = ?, ";
			Query += "USERID = ?, ";
			Query += "UNAME = ?, ";
			Query += "REQUEST_TYPE = ?, ";
			Query += "COMMENTS = ?, ";
			Query += "INSTITUTION_ID = ?, ";
			Query += "MADE_BY = ?, ";
			Query += "MADE_AT = ?, ";
			Query += "CHECKED_BY = ?, ";
			Query += "CHECKED_AT = ?, ";
			Query += "MAKER_LAST_CMT = ?, ";
			Query += "CHECKER_LAST_CMT = ?, ";
			Query += "CURR_APP_STATUS = ?, ";
			Query += "ADMIN_LAST_CMT = ?, ";
			Query += "RSTATUS = ?, ";
			Query += "CREATED_BY = ?, ";
			Query += "CREATED_AT = ?, ";
			Query += "MODIFIED_AT = ?, ";
			Query += "MODIFIED_BY = ? ";
			Query += "WHERE (Id = ?) ";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("updateLIUserrequestRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			setStringValue(ps, 1, record.getUserrecid());
			setStringValue(ps, 2, record.getUserid());
			setStringValue(ps, 3, record.getUname());
			setStringValue(ps, 4, record.getRequesttype());
			setStringValue(ps, 5, record.getComments());
			setStringValue(ps, 6, record.getInstitutionid());
			setStringValue(ps, 7, record.getMadeby());
			setDateValue(ps, 8, fd.getSQLDateObject(record.getMadeat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 9, record.getCheckedby());
			setDateValue(ps, 10, fd.getSQLDateObject(record.getCheckedat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 11, record.getMakerlastcmt());
			setStringValue(ps, 12, record.getCheckerlastcmt());
			setStringValue(ps, 13, record.getCurrappstatus());
			setStringValue(ps, 14, record.getAdminlastcmt());
			setStringValue(ps, 15, record.getRstatus());
			setStringValue(ps, 16, record.getCreatedby());
			setDateValue(ps, 17, fd.getSQLDateObject(record.getCreatedat(), "yyyyMMddHHmmss"));
			setDateValue(ps, 18, fd.getCurrentSQLDateObject());
			setStringValue(ps, 19, record.getModifiedby());
			ps.setString(20, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("updateLIUserrequestRecord\t" + result + "\t");
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("user_request","UPDATE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), currentRecordContent, record.getActionSource());
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean updateLIUserrequestRecord(LIUserrequestRecord record)
	throws Exception
	{
		return updateLIUserrequestRecord(record, null, true);
	}

	public boolean deleteLIUserrequestRecord(LIUserrequestRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "DELETE FROM user_request WHERE (Id = ?)";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("deleteLIUserrequestRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			ps.setString(1, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("deleteLIUserrequestRecord\t" + result + "\t");
			ps.close();
			createAuditRecord("user_request","DELETE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), record.getActionSource());
			releaseDatabaseConnection(con, closeConnection);
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean deleteLIUserrequestRecord(LIUserrequestRecord record)
	throws Exception
	{
		return deleteLIUserrequestRecord(record, null, true);
	}

	public LIUserrequestRecord[] searchLIUserrequestRecords(LIUserrequestRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USERRECID", formatSearchField(searchRecord.getUserrecid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USERID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "UNAME", formatSearchField(searchRecord.getUname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "REQUEST_TYPE", formatSearchField(searchRecord.getRequesttype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "COMMENTS", formatSearchField(searchRecord.getComments()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from user_request " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM user_request ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM user_request $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIUserrequestRecords(Query);
	}

	public LIUserrequestRecord[] searchLIUserrequestRecordsExact(LIUserrequestRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USERRECID", formatSearchField(searchRecord.getUserrecid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USERID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "UNAME", formatSearchField(searchRecord.getUname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "REQUEST_TYPE", formatSearchField(searchRecord.getRequesttype()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "COMMENTS", formatSearchField(searchRecord.getComments()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from user_request " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM user_request ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM user_request $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIUserrequestRecords(Query);
	}

	public LIUserrequestRecord[] searchLIUserrequestRecordsExactUpper(LIUserrequestRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USERRECID", formatSearchField(searchRecord.getUserrecid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USERID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "UNAME", formatSearchField(searchRecord.getUname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "REQUEST_TYPE", formatSearchField(searchRecord.getRequesttype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "COMMENTS", formatSearchField(searchRecord.getComments()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from user_request " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM user_request ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM user_request $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIUserrequestRecords(Query);
	}

	public int loadLIUserrequestRecordCount(LIUserrequestRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USERRECID", formatSearchField(searchRecord.getUserrecid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USERID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "UNAME", formatSearchField(searchRecord.getUname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "REQUEST_TYPE", formatSearchField(searchRecord.getRequesttype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "COMMENTS", formatSearchField(searchRecord.getComments()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from user_request " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLIUserrequestRecordCountExact(LIUserrequestRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "USERRECID", formatSearchField(searchRecord.getUserrecid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USERID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "UNAME", formatSearchField(searchRecord.getUname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "REQUEST_TYPE", formatSearchField(searchRecord.getRequesttype()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "COMMENTS", formatSearchField(searchRecord.getComments()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from user_request " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}

/*
* ICSmaillogDAO.java 
* Copyright (c) LeadICS Private Limited
* This software is the confidential and proprietary information of 
* LeadICS Private Limited You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with LeadICS Private Limited
* Project Name             : dealeraudit
* Module                   : Record beans
* Author                   : Subrahmanya Varma Sagi, LeadICS Private Limited
* Date                     : Mon Sep 19 09:41:34 IST 2016
* Change Revision
* ----------------------------------------------------------------
* Date            Author         Version#    Remarks/Description
*-----------------------------------------------------------------
 */
package com.leadics.Toyota.dao;

import java.sql.*;
import java.util.*;
import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import java.text.SimpleDateFormat;

import com.leadics.Toyota.to.ICSmaillogRecord;

public class ICSmaillogDAO extends LIDAO {

    java.sql.Connection con = null;

    public ICSmaillogRecord[] loadICSmaillogRecords(String query, Connection con, boolean closeConnection)
            throws Exception {
        if (con == null) {
            con = getDatabaseConnection();
        }
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        ArrayList recordSet = new ArrayList();
        while (rs.next()) {
            ICSmaillogRecord record = new ICSmaillogRecord();
            try {
                record.setid(rs.getInt("id"));
            } catch (Exception e) {
                //System.out.println("Query:" + query + " -> Exception selecting id");
            }
            try {
                record.setFromID(rs.getString("FromID"));
            } catch (Exception e) {
                //System.out.println("Query:" + query + " -> Exception selecting FromID");
            }
            try {
                record.setToIDs(rs.getString("ToIDs"));
            } catch (Exception e) {
                //System.out.println("Query:" + query + " -> Exception selecting ToIDs");
            }
            try {
                record.setMailMessage(rs.getString("MailMessage"));
            } catch (Exception e) {
                //System.out.println("Query:" + query + " -> Exception selecting MailMessage");
            }
            try {
                record.setDatetime(rs.getString("Datetime"));
            } catch (Exception e) {
                //System.out.println("Query:" + query + " -> Exception selecting Datetime");
            }
            try {
                record.setRespoinseMessage(rs.getString("RespoinseMessage"));
            } catch (Exception e) {
                //System.out.println("Query:" + query + " -> Exception selecting RespoinseMessage");
            }
            recordSet.add(record);
        }

        ICSmaillogRecord[] tempICSmaillogRecords = new ICSmaillogRecord[recordSet.size()];

        for (int index = 0; index < recordSet.size(); index++) {
            tempICSmaillogRecords[index] = (ICSmaillogRecord) (recordSet.get(index));
        }

        rs.close();
        ps.close();
        con.close();
        con = null;
        return tempICSmaillogRecords;
    }

    public ICSmaillogRecord[] loadICSmaillogRecords(String query)
            throws Exception {
        return loadICSmaillogRecords(query, null, false);
    }

    public boolean insertICSmaillogRecord(ICSmaillogRecord record)
            throws Exception {
        String query = "Insert into maillog ";

        query += "(";

        query += "id,FromID,ToIDs,MailMessage,Datetime,RespoinseMessage";
        query += ")";
        query += "VALUES";
        query += "( ?, ?, ?, ?, ?,  ?";

        query += ")";
        if (con == null) {
            con = getDatabaseConnection();
        }
        PreparedStatement ps;
        ps = con.prepareStatement(query);
        ps.setInt(1, record.getid());
        ps.setString(2, record.getFromID());
        ps.setString(3, record.getToIDs());
        ps.setString(4, record.getMailMessage());
        ps.setString(5, record.getDatetime());
        ps.setString(6, record.getRespoinseMessage());

        boolean result = ps.execute();
        ps.close();
        con.close();
        con = null;
        return result;
    }

    public boolean insertICSmaillogRecords(ICSmaillogRecord[] records)
            throws Exception {
        boolean finalresult = true;
        for (int index = 0; index < records.length; index++) {
            boolean result = insertICSmaillogRecord(records[index]);
            if (result != true) {
                finalresult = false;
            }
        }
        return finalresult;
    }

    public ICSmaillogRecord[] loadICSmaillogRecordsCond(String strCondtion)
            throws Exception {
        String query = "SELECT * from maillog where 1=1 and " + strCondtion;

        return loadICSmaillogRecords(query, null, false);
    }

    public String loadICSmaillogRecordsJSONCond(String strCondtion)
            throws Exception {
        String query = "SELECT * from maillog where 1=1 and " + strCondtion;
        return new Gson().toJson(loadICSmaillogRecords(query, null, false));
    }

    public String loadICSmaillogRecordsJSONCond(String strCondtion, String strOrderby)
            throws Exception {
        String query = "SELECT * from maillog where 1=1 and " + strCondtion + " order by " + strOrderby;
        return new Gson().toJson(loadICSmaillogRecords(query, null, false));
    }

    public String loadICSmaillogRecordsJSONQuery(String strQuery)
            throws Exception {
        String query = strQuery;
        return new Gson().toJson(loadICSmaillogRecords(query, null, false));
    }

    public ICSmaillogRecord loadICSmaillogRecordCond(String strCondtion)
            throws Exception {
        String query = "SELECT * from maillog where 1=1 and " + strCondtion;

        ICSmaillogRecord[] results = loadICSmaillogRecords(query);
        if (results == null) {
            return null;
        }
        if (results.length < 1) {
            return null;
        }
        return results[0];
    }

    public ICSmaillogRecord[] loadICSmaillogRecordsCond(String strCondtion, String strOrderby)
            throws Exception {
        String query = "SELECT * from maillog where 1=1 and " + strCondtion + " order by " + strOrderby;

        return loadICSmaillogRecords(query, null, false);
    }

    public boolean UpdateICSmaillogRecord(HashMap keyvalue, String strCondtion)
            throws Exception {
        String query = "Update maillog set ";
        HashMap setColoumns = keyvalue;
        Set columns = setColoumns.keySet();
        Iterator itr = columns.iterator();
        String setters = "";
        while (itr.hasNext()) {
            String column = (String) itr.next();
            String value = (String) setColoumns.get(column);
            query = query + column + "=" + value;
        }
        query = query + "where 1=1 and " + strCondtion;
        if (con == null) {
            con = getDatabaseConnection();
        }
        PreparedStatement ps = con.prepareStatement(query);
        boolean result = ps.execute();
        ps.close();
        con.close();
        con = null;
        return result;
    }

    public boolean updateICSmaillogRecord(ICSmaillogRecord record, String cond)
            throws Exception {
        String query = "Update maillog set";

        query += " id = ? ,FromID = ? ,ToIDs = ? ,MailMessage = ? ,Datetime = ? ,RespoinseMessage= ? ";

        query += " where " + cond;
        if (con == null) {
            con = getDatabaseConnection();
        }
        PreparedStatement ps;
        ps = con.prepareStatement(query);
        ps.setInt(1, record.getid());
        ps.setString(2, record.getFromID());
        ps.setString(3, record.getToIDs());
        ps.setString(4, record.getMailMessage());
        ps.setString(5, record.getDatetime());
        ps.setString(6, record.getRespoinseMessage());

        boolean result = ps.execute();
        ps.close();
        con.close();
        con = null;
        return result;
    }

    public boolean insertUpdateICSmaillogRecord(ICSmaillogRecord record)
            throws Exception {
        String query = "Insert into maillog ";

        query += "(";

        query += "id,FromID,ToIDs,MailMessage,Datetime,RespoinseMessage";
        query += ")";
        query += " VALUES";
        query += "( ?, ?, ?, ?, ?,  ?";

        query += ")";
        if (con == null) {
            con = getDatabaseConnection();
        }
        PreparedStatement ps;
        query += " ON DUPLICATE KEY UPDATE ";

        query += " id = ? ,FromID = ? ,ToIDs = ? ,MailMessage = ? ,Datetime = ? ,RespoinseMessage= ? ";
        ps = con.prepareStatement(query);
        ps.setInt(1, record.getid());
        ps.setString(2, record.getFromID());
        ps.setString(3, record.getToIDs());
        ps.setString(4, record.getMailMessage());
        ps.setString(5, record.getDatetime());
        ps.setString(6, record.getRespoinseMessage());
        ps.setInt(7, record.getid());
        ps.setString(8, record.getFromID());
        ps.setString(9, record.getToIDs());
        ps.setString(10, record.getMailMessage());
        ps.setString(11, record.getDatetime());
        ps.setString(12, record.getRespoinseMessage());

        boolean result = ps.execute();
        ps.close();
        con.close();
        con = null;
        return result;
    }

}

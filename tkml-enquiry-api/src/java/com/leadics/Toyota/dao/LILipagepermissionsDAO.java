
/*
 * LILipagepermissionsDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.dao;
import com.leadics.Toyota.to.LILipagepermissionsRecord;
import com.leadics.Toyota.common.LIDAO;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LILipagepermissionsDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LILipagepermissionsDAO.class.getName());


	public LILipagepermissionsRecord[] loadLILipagepermissionsRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLILipagepermissionsRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LILipagepermissionsRecord record = new LILipagepermissionsRecord();
				record.setId(rs.getString("ID"));
				record.setRolename(rs.getString("ROLE_NAME"));
				record.setPagename(rs.getString("PAGE_NAME"));
				record.setCreatedat(rs.getString("CREATED_AT"));
				record.setCreatedby(rs.getString("CREATED_BY"));
				record.setModifiedat(rs.getString("MODIFIED_AT"));
				record.setModifiedby(rs.getString("MODIFIED_BY"));
				recordSet.add(record);
			}
			logger.trace("loadLILipagepermissionsRecords:Records Fetched:" + recordSet.size());
			LILipagepermissionsRecord[] tempLILipagepermissionsRecords = new LILipagepermissionsRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLILipagepermissionsRecords[index] = (LILipagepermissionsRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLILipagepermissionsRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LILipagepermissionsRecord[] loadLILipagepermissionsRecords(String query)
	throws Exception
	{
		return loadLILipagepermissionsRecords(query, null, true);
	}


	public LILipagepermissionsRecord loadFirstLILipagepermissionsRecord(String query)
	throws Exception
	{
		LILipagepermissionsRecord[] results = loadLILipagepermissionsRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LILipagepermissionsRecord loadLILipagepermissionsRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM li_page_permissions WHERE (ID = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLILipagepermissionsRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LILipagepermissionsRecord record = new LILipagepermissionsRecord();
			record.setId(rs.getString("ID"));
			record.setRolename(rs.getString("ROLE_NAME"));
			record.setPagename(rs.getString("PAGE_NAME"));
			record.setCreatedat(rs.getString("CREATED_AT"));
			record.setCreatedby(rs.getString("CREATED_BY"));
			record.setModifiedat(rs.getString("MODIFIED_AT"));
			record.setModifiedby(rs.getString("MODIFIED_BY"));
			ps.close();
			logger.trace("loadLILipagepermissionsRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LILipagepermissionsRecord loadLILipagepermissionsRecord(String id)
	throws Exception
	{
		return loadLILipagepermissionsRecord(id, null, true);
	}

	public int insertLILipagepermissionsRecord(LILipagepermissionsRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "INSERT INTO li_page_permissions ";
			Query += "(";
			Query += "ROLE_NAME,PAGE_NAME,CREATED_AT,CREATED_BY,MODIFIED_AT,MODIFIED_BY";
			Query += ")";
			Query += " VALUES ";
			Query += "(";
			Query += "?,?,?,?,?,?";
			Query += ")";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			record.setModifiedby(StringUtils.noNull(record.getModifiedby(),record.getCreatedby()));
			Query = updateQuery(Query);
			logger.trace("insertLILipagepermissionsRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			if (isOracleDatabase()) 
			{
				ps = con.prepareStatement(Query,new String[]{"ID"});
			}
			else
			{
				ps = con.prepareStatement(Query,Statement.RETURN_GENERATED_KEYS);
			}
			setStringValue(ps, 1, record.getRolename());
			setStringValue(ps, 2, record.getPagename());
			setStringValue(ps, 3, record.getCreatedat());
			setStringValue(ps, 4, record.getCreatedby());
			setStringValue(ps, 5, record.getModifiedat());
			setStringValue(ps, 6, record.getModifiedby());
			boolean result = ps.execute();
			logger.trace("insertLILipagepermissionsRecord\t" + result + "\t");
			int resultID = -1;
			rs = ps.getGeneratedKeys();
			if (rs.next()) 
			{
				resultID = rs.getInt(1); 
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("li_page_permissions","INSERT",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			return resultID;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public int insertLILipagepermissionsRecord(LILipagepermissionsRecord record)
	throws Exception
	{
		return insertLILipagepermissionsRecord(record, null, true);
	}

	public boolean updateLILipagepermissionsRecord(LILipagepermissionsRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			LILipagepermissionsRecord currentRecord = loadLILipagepermissionsRecord(record.getId());
			String currentRecordContent = StringUtils.noNull(currentRecord);

			String Query = "UPDATE li_page_permissions SET ";
			Query += "ROLE_NAME = IFNULL(?, ROLE_NAME),";
			Query += "PAGE_NAME = IFNULL(?, PAGE_NAME),";
			Query += "CREATED_AT = IFNULL(?, CREATED_AT),";
			Query += "CREATED_BY = IFNULL(?, CREATED_BY),";
			Query += "MODIFIED_AT = IFNULL(?, MODIFIED_AT),";
			Query += "MODIFIED_BY = IFNULL(?, MODIFIED_BY ";
			Query += "WHERE (ID = ?) ";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("updateLILipagepermissionsRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			setStringValue(ps, 1, record.getRolename());
			setStringValue(ps, 2, record.getPagename());
			setStringValue(ps, 3, record.getCreatedat());
			setStringValue(ps, 4, record.getCreatedby());
			setStringValue(ps, 5, record.getModifiedat());
			setStringValue(ps, 6, record.getModifiedby());
			ps.setString(7, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("updateLILipagepermissionsRecord\t" + result + "\t");
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("li_page_permissions","UPDATE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, currentRecordContent, record.getActionSource());
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean updateLILipagepermissionsRecord(LILipagepermissionsRecord record)
	throws Exception
	{
		return updateLILipagepermissionsRecord(record, null, true);
	}

	public boolean deleteLILipagepermissionsRecord(LILipagepermissionsRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "DELETE FROM li_page_permissions WHERE (ID = ?)";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("deleteLILipagepermissionsRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			ps.setString(1, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("deleteLILipagepermissionsRecord\t" + result + "\t");
			ps.close();
			createAuditRecord("li_page_permissions","DELETE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			releaseDatabaseConnection(con, closeConnection);
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean deleteLILipagepermissionsRecord(LILipagepermissionsRecord record)
	throws Exception
	{
		return deleteLILipagepermissionsRecord(record, null, true);
	}

	public LILipagepermissionsRecord[] searchLILipagepermissionsRecords(LILipagepermissionsRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ROLE_NAME", formatSearchField(searchRecord.getRolename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PAGE_NAME", formatSearchField(searchRecord.getPagename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from li_page_permissions " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM li_page_permissions ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM li_page_permissions $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLILipagepermissionsRecords(Query);
	}

	public LILipagepermissionsRecord[] searchLILipagepermissionsRecordsExact(LILipagepermissionsRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ROLE_NAME", formatSearchField(searchRecord.getRolename()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PAGE_NAME", formatSearchField(searchRecord.getPagename()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from li_page_permissions " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM li_page_permissions ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM li_page_permissions $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLILipagepermissionsRecords(Query);
	}

	public LILipagepermissionsRecord[] searchLILipagepermissionsRecordsExactUpper(LILipagepermissionsRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ROLE_NAME", formatSearchField(searchRecord.getRolename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PAGE_NAME", formatSearchField(searchRecord.getPagename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from li_page_permissions " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM li_page_permissions ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM li_page_permissions $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLILipagepermissionsRecords(Query);
	}

	public int loadLILipagepermissionsRecordCount(LILipagepermissionsRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ROLE_NAME", formatSearchField(searchRecord.getRolename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PAGE_NAME", formatSearchField(searchRecord.getPagename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from li_page_permissions " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLILipagepermissionsRecordCountExact(LILipagepermissionsRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "ID", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ROLE_NAME", formatSearchField(searchRecord.getRolename()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PAGE_NAME", formatSearchField(searchRecord.getPagename()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from li_page_permissions " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}

package com.leadics.Toyota.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static com.leadics.Toyota.common.LIAction.AUTHORIZATION;
import com.leadics.Toyota.dao.LIReportquiresDAO;
import com.leadics.Toyota.service.LIAuditlogService;
import com.leadics.Toyota.service.LISystemuserService;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.service.LIWhereconditionmapService;
import com.leadics.Toyota.to.LIAuditlogRecord;
import com.leadics.Toyota.to.LIReportquiresRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.LIWebsessionRecord;
import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.representations.EventLoggerRepresentation;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import com.leadics.utils.DateUtils;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LIService {

    static LogUtils logger = new LogUtils(LIService.class.getName());

    public static final String strWhereCluse = "<<WHERE_CONDITION>>";
    public static final String ACKNOWLEDGMENT = "ACKNOWLEDGMENT";

    public static final String CONDITION_START = "<<";
    public static final String CONDITION_END = ">>";
    public static final String QUERY_TEMPLATE = "QUERY";

    public Map loadMap(String Query, String Column1, String Column2, boolean insertBlank)
            throws Exception {
        LIDAO dao = new LIDAO();
        return dao.loadMap(Query, Column1, Column2, insertBlank);
    }

    public HashMap<String, String> buildWherecondition(HttpServletRequest request, Map<String, LIWhereconditionmapRecord> filtersMap, String query) {
        HashMap<String, String> acknowledgmentMap = new HashMap<>();
        try {
            String authHeader = request.getHeader(AUTHORIZATION);

            String whereCondition = "", condition = "";
            Set<String> whereMaps = filtersMap.keySet();

            Iterator whereMapIterator = whereMaps.iterator();

            while (whereMapIterator.hasNext()) {
                condition = "";
                String whereTemplate = (String) whereMapIterator.next();
                LIWhereconditionmapRecord whereRecord = (LIWhereconditionmapRecord) filtersMap.get(whereTemplate);
                if (whereRecord == null) {
                    continue;
                }
                if (whereRecord.getType().equalsIgnoreCase("constant")) {
                    // if the type if constant the get it from the properties file
                    String fieldValue = whereRecord.getColnames();
                    ResourceBundle rb = ResourceBundle.getBundle("Toyota", Locale.getDefault());
                    String constantReplace = rb.getString(fieldValue);
                    if (constantReplace == null) {
                        throw new Exception("NO constant value set for " + fieldValue);
                    }
                    query = query.replace(whereRecord.getWheretemplate(), constantReplace);
                    continue;
                }
                if (whereRecord.getType().equalsIgnoreCase("custom_dealer")) {
                    LISystemuserRecord userRecord = this.getUserOfSession(authHeader);
                    String replace = "and dealer = \"" + userRecord.getFirstname() + "\"";
//                    query = query.replace("<<custom_dealer>>", replace);
                    if (userRecord.getUserroleid().equalsIgnoreCase(PropertyUtil.getProperty("dealerRole"))) {
                        query = query.replace("<<custom_dealer>>", replace);
                    } else {
                        query = query.replace("<<custom_dealer>>", " ");
                    }
                    continue;
                }

                if (whereRecord.getType().equalsIgnoreCase("custom_dealer_name")) {
                    LISystemuserRecord userRecord = this.getUserOfSession(authHeader);
                    String replace = " \"" + userRecord.getFirstname() + "\"";
                    if (userRecord.getUserroleid().equalsIgnoreCase(PropertyUtil.getProperty("dealerRole"))) {
                        query = query.replace("<<custom_dealer_name>>", replace);
                    } else {
                        query = query.replace("<<custom_dealer_name>>", " ");
                    }
                    continue;
                }
                if (whereRecord.getType().equalsIgnoreCase("custom_region")) {
                    LISystemuserRecord userRecord = this.getUserOfSession(authHeader);
                    String replace = "and region = \"" + userRecord.getDregion() + "\"";
//                    query = query.replace("<<custom_region>>", replace);
                    if (userRecord.getUserroleid().equalsIgnoreCase(PropertyUtil.getProperty("dealerRole"))) {
                        query = query.replace("<<custom_region>>", replace);
                    } else {
                        query = query.replace("<<custom_region>>", " ");
                    }
                    continue;
                }
                if (whereRecord.getType().equalsIgnoreCase("custom_region_summary")) {
//                    LISystemuserRecord userRecord = this.getUserOfSession(authHeader);
                    String strArea = request.getParameter("area");
                    String strZone = request.getParameter("zone");
                    String strDealergroup = request.getParameter("dealergroup");
                    String strRegion=request.getParameter("region");
                    String replace = null;
                    if (strArea != null && !strArea.equalsIgnoreCase("All")) {
                        replace = "and region in( select distinct region from factor_aggregate where area= \"" + strArea + "\")";
                        query = query.replace("<<custom_region_summary>>", replace);
                    } else if (strZone != null && !strZone.equalsIgnoreCase("All")) {
                        replace = "and region in( select distinct region from factor_aggregate where zone= \"" + strZone + "\")";
                        query = query.replace("<<custom_region_summary>>", replace);
                    } else if (strDealergroup != null && !strDealergroup.equalsIgnoreCase("All")) {
                        replace = "and region in( select distinct region from factor_aggregate where dealergroup= \"" + strDealergroup + "\")";
                        query = query.replace("<<custom_region_summary>>", replace);
                    }else if (strRegion != null && !strRegion.equalsIgnoreCase("All")) {
                        replace = "and region = \"" + strRegion + "\"";
                        query = query.replace("<<custom_region_summary>>", replace);
                    } else {
                        query = query.replace("<<custom_region_summary>>", " ");
                    }
                    continue;
                }
                if (whereRecord.getType().equalsIgnoreCase("custom_region_name")) {
                    LISystemuserRecord userRecord = this.getUserOfSession(authHeader);
                    String replace = " \"" + userRecord.getDregion() + "\"";
                    if (userRecord.getUserroleid().equalsIgnoreCase(PropertyUtil.getProperty("dealerRole"))) {
                        query = query.replace("<<custom_region_name>>", replace);
                    } else {
                        query = query.replace("<<custom_region_name>>", " ");
                    }

                    continue;
                }

                List<String> filterList = this.convertCommaBasedToArray(whereRecord.getColnames());
                if (filterList != null) {
                    for (String filter : filterList) {
                        String values[] = request.getParameterValues(filter);

                        if (values == null || values.length == 1 && values[0].equalsIgnoreCase("All")) {

                        } else if (values.length == 1) {
                            // if it is single valued
                            condition += " and " + filter + "=\"" + values[0] + "\"";

                        } //if it is of type array 
                        else {
                            condition += " and " + filter + " in (";
                            for (String value : values) {
                                condition += "\"" + value + "\",";
                            }
                            condition = replaceLast(condition, ",", ")");

                        }

                    }
                    query = query.replace(whereTemplate, condition);
                    acknowledgmentMap.put(whereTemplate, condition);
                }

            }

            acknowledgmentMap.put(QUERY_TEMPLATE, query);

            return acknowledgmentMap;
        } catch (Exception e) {
            e.printStackTrace();
            return acknowledgmentMap;
        }
    }

    public HashMap<String, String> buildWherecondition(Map<String, String[]> params, Map<String, LIWhereconditionmapRecord> filtersMap, String query) {
        HashMap<String, String> acknowledgmentMap = new HashMap<>();
        try {
            String whereCondition = "", condition = "";
            Set<String> whereMaps = filtersMap.keySet();

            Iterator whereMapIterator = whereMaps.iterator();

            while (whereMapIterator.hasNext()) {
                condition = "";
                String whereTemplate = (String) whereMapIterator.next();
                LIWhereconditionmapRecord whereRecord = (LIWhereconditionmapRecord) filtersMap.get(whereTemplate);
                if (whereRecord == null) {
                    continue;
                }
                if (whereRecord.getType().equalsIgnoreCase("constant")) {
                    // if the type if constant the get it from the properties file
                    String fieldValue = whereRecord.getColnames();
                    ResourceBundle rb = ResourceBundle.getBundle("Toyota", Locale.getDefault());
                    String constantReplace = rb.getString(fieldValue);
                    if (constantReplace == null) {
                        throw new Exception("NO constant value set for " + fieldValue);
                    }
                    query = query.replace(whereRecord.getWheretemplate(), constantReplace);
                    continue;
                }

                List<String> filterList = this.convertCommaBasedToArray(whereRecord.getColnames());
                if (filterList != null) {
                    for (String filter : filterList) {
                        String values[] = params.get(filter);

                        if (values == null || values.length == 1 && values[0].equalsIgnoreCase("All")) {

                        } else if (values.length == 1) {
                            // if it is single valued
                            condition += " and " + filter + "=\"" + values[0] + "\"";

                        } //if it is of type array 
                        else {
                            condition += " and " + filter + " in (";
                            for (String value : values) {
                                condition += "\"" + value + "\",";
                            }
                            condition = replaceLast(condition, ",", ")");

                        }

                    }
                    query = query.replace(whereTemplate, condition);
                    acknowledgmentMap.put(whereTemplate, condition);
                }

            }

            acknowledgmentMap.put(QUERY_TEMPLATE, query);

            return acknowledgmentMap;
        } catch (Exception e) {
            e.printStackTrace();
            return acknowledgmentMap;
        }
    }

    public static String replaceLast(String string, String toReplace, String replacement) {
        int pos = string.lastIndexOf(toReplace);
        if (pos > -1) {
            return string.substring(0, pos)
                    + replacement
                    + string.substring(pos + toReplace.length(), string.length());
        } else {
            return string;
        }
    }

    public LISystemuserRecord getUserOfSession(String sessionId) throws Exception {
        LIWebsessionRecord sessionRecord;

        LIWebsessionService sessionService = new LIWebsessionService();
        String sql = " select * from web_session where SESSIONID = \"" + sessionId + "\"";

        sessionRecord = sessionService.loadFirstLIWebsessionRecord(sql);
        //System.out.println(new Gson().toJson(sessionRecord));
        sql = "select * from system_user where user_id = \"" + sessionRecord.getUserid() + "\"";
        LISystemuserRecord userRecord = new LISystemuserService().loadFirstLISystemuserRecord(sql);

        return userRecord;
    }

    public boolean isDuplicateSession(String userId) {
        try {
            String EnableDuplicateSession = StringUtils.noNull(PropertyUtil.getProperty("EnableDuplicateSession"));
            if (EnableDuplicateSession.equals("Y")) {
                return false;
            }

            String query = "";
            int maxTimeDiff = Integer.parseInt(PropertyUtil.getProperty("websessiontimeout"));

            query += "SELECT count(*) CNT FROM WEB_SESSION ";
            query += "WHERE (TIMESTAMPDIFF(MINUTE,LASTACC,NOW()) < '" + maxTimeDiff + "') ";
            query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
            query += "AND (RSTATUS = '1') ";

            LIDAO dao = new LIDAO();

            if ((dao.isMSSQL()) || (dao.isMSSQL8())) {
                query = "SELECT count(*) CNT FROM WEB_SESSION ";
                query += "WHERE (datediff(MINUTE,LASTACC,GETDATE())  < '" + maxTimeDiff + "') ";
                query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
                query += "AND (RSTATUS = '1') ";
            }

            if (dao.isOracleDatabase()) {
//				select count(*) cnt  from WEB_SESSION where ((to_number(to_date(sysdate,'YYYYMMDDHH24MISS') - to_date(LASTACC,'YYYYMMDDHH24MISS')) * 24 * 60)<'15') AND User_id='BOKADMIN' ;
                query = "SELECT count(*) CNT FROM WEB_SESSION ";
                query += "WHERE (((SYSDATE - LASTACC)*24*60)  < '" + maxTimeDiff + "') ";
                query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
                query += "AND (RSTATUS = '1') ";

                query = StringUtils.replaceString(query, "#SYSDATE#", DateUtils.getCurrentDateTime(), true);
            }

            int count = dao.loadCount(query);
            return (count > 0);
        } catch (Exception exception) {
            logger.error(exception);
            logger.error("isDuplicateSession" + getStackTrace(exception));
            return false;
        }
    }

    public boolean isValidSession(String userId, String sessionId) throws Exception {
        try {
            String query = "";
            int maxTimeDiff = Integer.parseInt(PropertyUtil.getProperty("websessiontimeout"));

            LogUtils.println("DB Check 1:");

            query += "SELECT count(*) CNT FROM web_session  ";
            query += "WHERE (TIMESTAMPDIFF(MINUTE,LASTACC,NOW()) < '" + maxTimeDiff + "') ";
            query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
            query += "AND (UPPER(SESSIONID) = UPPER('" + sessionId + "')) ";
            query += "AND (RSTATUS = '1') ";

            LIDAO dao = new LIDAO();

            if ((dao.isMSSQL()) || (dao.isMSSQL8())) {
                query = "SELECT count(*) CNT FROM WEB_SESSION ";
                query += "WHERE (datediff(MINUTE,LASTACC,GETDATE()) < '" + maxTimeDiff + "') ";
                query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
                query += "AND (UPPER(SESSIONID) = UPPER('" + sessionId + "')) ";
                query += "AND (RSTATUS = '1') ";
            }

            if (dao.isOracleDatabase()) {
                query = "SELECT count(*) CNT FROM WEB_SESSION ";
                query += "WHERE (((SYSDATE - LASTACC)*24*60) < '" + maxTimeDiff + "') ";
                query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
                query += "AND (UPPER(SESSIONID) = UPPER('" + sessionId + "')) ";
                query += "AND (RSTATUS = '1') ";

                query = StringUtils.replaceString(query, "#SYSDATE#", DateUtils.getCurrentDateTime(), true);
            }

            LogUtils.println("Query for Session Check:" + query);

            int count = dao.loadCount(query);
            System.out.println("session qurery::::::" + query);
            LogUtils.println("Query Output:" + count);
            return (count > 0);
        } catch (Exception exception) {
            logger.error(exception);
            logger.error("isValidSession" + getStackTrace(exception));
            return false;
        }
    }

    public void clearOldWebSessions(String userId) {
        try {
            String updateStatement = "";
            updateStatement += " UPDATE web_session ";
            updateStatement += " SET CLOSE_TYPE = '#CLOSETYPE#', ";
            updateStatement += " CLOSED_AT = '#CLOSEDAT#', ";
            updateStatement += " RSTATUS = '0' ";
            updateStatement += " WHERE (RSTATUS = '1')AND(USER_ID='#USERID#') ";

            updateStatement = StringUtils.replaceString(updateStatement, "#CLOSETYPE#", "AUTO_CLOSE", true);
            updateStatement = StringUtils.replaceString(updateStatement, "#CLOSEDAT#", DateUtils.getCurrentDateTimene(), true);
            updateStatement = StringUtils.replaceString(updateStatement, "#USERID#", userId, true);
            LIDAO dao = new LIDAO();
            dao.executeUpdateQuery(updateStatement);
        } catch (Exception exception) {
            logger.error(exception);
            logger.error("clearOldWebSessions" + getStackTrace(exception));
        }
    }

    public static String formatAmountNoDecimal(String inputAmount) {
        String result = formatAmount(inputAmount);
        result = StringUtils.replaceString(result, ".", "", true);
        return result;
    }

    public static String formatAmountWithZero(String inputAmount) {
        try {
            double amount = Double.parseDouble(inputAmount);
            DecimalFormat df = new DecimalFormat("#,###,###,##0.00");
            return df.format(amount);
        } catch (Exception exception) {
            //logger.error(exception);
            return inputAmount;
        }
    }

    public static String formatAmountWithCommas(String inputAmount) {
        try {
            double amount = Double.parseDouble(inputAmount);
            DecimalFormat df = new DecimalFormat("#,###,###,##0.00");
            return df.format(amount);
        } catch (Exception exception) {
            //logger.error(exception);
            return inputAmount;
        }
    }

    public static String formatAmount(String inputAmount) {
        try {
            double amount = Double.parseDouble(inputAmount);
            DecimalFormat df = new DecimalFormat("##.00");
            return df.format(amount);
        } catch (Exception exception) {
            //logger.error(exception);
            return inputAmount;
        }
    }

    public static String formatAmount(String inputAmount, String format) {
        try {
            double amount = Double.parseDouble(inputAmount);
            DecimalFormat df = new DecimalFormat(format);
            return df.format(amount);
        } catch (Exception exception) {
            //logger.error(exception);
            return inputAmount;
        }
    }

    public static String addDecimalToCurrentAmount(String inputAmount) {
        if (StringUtils.isNull(inputAmount)) {
            inputAmount = "0";
        }
        if (inputAmount.length() < 3) {
            inputAmount = "000" + inputAmount;
        }

        String mainValue = inputAmount.substring(0, inputAmount.length() - 2);
        String decimalValue = inputAmount.substring(inputAmount.length() - 2);
        return mainValue + "." + decimalValue;
    }

    public static String truncateNonDecimalValue(String inputAmount) {
        if (StringUtils.isNull(inputAmount)) {
            inputAmount = "0";
        }
        if (inputAmount.length() < 3) {
            inputAmount = "000" + inputAmount;
        }

        String mainValue = inputAmount.substring(0, inputAmount.length() - 2);
        String decimalValue = inputAmount.substring(inputAmount.length() - 2);
        return mainValue;
    }

    public void createMakerCheckerAuditEntry(String tableName, String recordId, String actionName, String createID, String comments)
            throws Exception {
        try {
            return;
        } catch (Exception exception) {
            logger.error("createMakerCheckerAuditEntry" + getStackTrace(exception));
            throw exception;
        }
    }

    public HashMap callActionEvent(String eventName, String recordID) {
        HashMap inputMap = new HashMap();
        inputMap.put("EventRecordID", recordID);
        inputMap.put("EventName", eventName);
        return inputMap;
    }

    public boolean isValidStatusForDeny(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("4")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForPasswordReset(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("1")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForPinReset(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("1")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForBlock(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("1")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForApproval(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("4")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForReset(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("0")) {
            return true;
        }
        if (inputStatus.equals("1")) {
            return true;
        }
        if (inputStatus.equals("2")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForSubmission(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return true;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("-1")) {
            return true;
        }
        if (inputStatus.equals("3")) {
            return true;
        }
        if (inputStatus.equals("5")) {
            return true;
        }
        return false;
    }

    public String getStackTrace(Exception e) {
        return StringUtils.getStackTrace(e);
    }

    public String getErrorMessage(Exception e) {
        logger.error(e);
        String message = e.toString();
        message = message.toLowerCase();

        if (message.indexOf("sqlintegrityconstraintviolation") > -1) {
            return "Duplicate Record";
        }

        if (message.indexOf("duplicate") > -1) {
            return "Duplicate Record";
        }

        if (message.indexOf("duplicate") > -1) {
            return "Duplicate Record";
        }

        return message;
    }

    public boolean isEligibleRecordForSubmit(String masterRecordID) {
        try {
            String query = "select count(*) cnt from audit_record where (master_record_code = '#MASTRECID#')";
            query = StringUtils.replaceString(query, "#MASTRECID#", masterRecordID + "", true);
            LIDAO dao = new LIDAO();
            int cnt = dao.loadCount(query);
            return (cnt > 0);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getStartDate(String loginRecordID) {
        try {
            String query = "SELECT min(schedule_date) start_date FROM audit_master where ((rstatus = '1') and (P3_ACTUAL_DATE is null) and (auditor_id = '#AUDITORID#'))";
            query = StringUtils.replaceString(query, "#AUDITORID#", loginRecordID + "", true);
            LIDAO dao = new LIDAO();
            String resultStartDate = dao.loadString(query);
            return resultStartDate;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                return DateUtils.getCurrentTime(DateUtils.YYYYMMDD);
            } catch (Exception e2) {
                return "";
            }
        }
    }

    public String getQuery(String methodName) throws Exception {
        LIReportquiresDAO reportquiresDAO = new LIReportquiresDAO();
        LIDAO dao = new LIDAO();
        LIReportquiresRecord reportsquiresRecord = reportquiresDAO
                .loadFirstLIReportquiresRecord("SELECT * FROM reportquires WHERE reportname=\'" + methodName + "\'");
        return reportsquiresRecord.getQuery();
    }

    public String getCountQuery(String methodName) throws Exception {
        LIReportquiresDAO reportquiresDAO = new LIReportquiresDAO();
        LIDAO dao = new LIDAO();
        LIReportquiresRecord reportsquiresRecord = reportquiresDAO
                .loadFirstLIReportquiresRecord("SELECT * FROM reportquires WHERE reportname=\'" + methodName + "\'");
        return reportsquiresRecord.getCountquery();
    }

    public void writeOutput(HttpServletResponse response, Object object) throws Exception {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");

        PrintWriter out = response.getWriter();
        GsonBuilder builder = new GsonBuilder();
        builder.disableHtmlEscaping();
        Gson gson = builder.create();
        String plainText = gson.toJson(object);
        String cipher = plainText;
        System.out.println("cipher : " + cipher);
        out.println(cipher);
    }

    public int eventLogger(String entityName, String message, String actionType, String sourceId) throws Exception {
        LIAuditlogService logService = new LIAuditlogService();
        LIAuditlogRecord record = new LIAuditlogRecord();
        record.setEntityname(entityName);
        record.setMessage(message);
        record.setActiontype(actionType);
        record.setSourceid(sourceId);
        return logService.insertLIAuditlogRecord(record);
    }

    public int eventLogger(EventLoggerRepresentation representation, String sourceId) throws Exception {
        LIAuditlogService logService = new LIAuditlogService();
        LIAuditlogRecord record = new LIAuditlogRecord();
        record.setEntityname(representation.getEntityName());
        record.setMessage(representation.getMessage());
        record.setActiontype(representation.getActionType());
        record.setSourceid(sourceId);
        return logService.insertLIAuditlogRecord(record);
    }

    public String getUniqueString() {
        return "" + (UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE);
    }

    public String getCurrentTimeStamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());

    }

    public Set<String> getFilterGroups(String query) {

        Set<String> groupSet = new HashSet<>();

        int i = 1;
        int start = ordinalIndexOf(query, "<<", i);
        int end = ordinalIndexOf(query, ">>", i) + 2;

        // if and only if start and end are greater than 0 then string has conditions else it will return -1
        while (start > 0 && end > start) {

            String cond = query.substring(start, end);
            groupSet.add(cond);
            // get the next where condition postions
            i++;
            start = ordinalIndexOf(query, "<<", i);
            end = ordinalIndexOf(query, ">>", i) + 2;
        }

        return groupSet;

    }

    public static int ordinalIndexOf(String str, String substr, int n) {
        int count = 0;
        int pos = -1;
        for (int i = 0; i < str.length() - 2; i++) {

            if (str.substring(i, i + substr.length()).equals(substr)) {

                count++;
                if (count == n) {
                    pos = i;
                    break;
                }
            }
        }
        return pos;
    }

    public Map<String, LIWhereconditionmapRecord> getFiltersMap(String query) throws Exception {

        // get all the filter groups by means of query
        Set<String> filterSet = this.getFilterGroups(query);

        LIWhereconditionmapService whereMapService = new LIWhereconditionmapService();
        LIWhereconditionmapRecord whereMapRecord;

        Map<String, LIWhereconditionmapRecord> filtersMap = new HashMap<>();
        Iterator filterGroupIterator = filterSet.iterator();
        while (filterGroupIterator.hasNext()) {
            String filterGroup = filterGroupIterator.next().toString();
            String queryForWhereGroup = "select * from  where_condition_map where where_template=\"" + filterGroup + "\"";

            whereMapRecord = whereMapService.loadLIWhereconditionmapRecord(queryForWhereGroup);

            if (whereMapRecord == null) {

                filtersMap.put(filterGroup, null);

            }

            // add it to the collection
            filtersMap.put(filterGroup, whereMapRecord);
        }

        return filtersMap;
    }

    public List<String> convertCommaBasedToArray(String str) {
        String groupFields = str;
        String field = "";
        if (str.contentEquals("") || str.contentEquals(" ") || str.trim().contentEquals("")) {
            return new ArrayList<>();
        }
        ArrayList<String> groupList = new ArrayList<>();
        // used to put into collection by filterGroup and its corresponding list of filter fields
        for (int i = 0; i < groupFields.length(); i++) {
            String ch = "" + groupFields.charAt(i);
            if (!ch.equals(",")) {
                //append all the characters of a field until , is reached 
                field += ch;

            } else {
                // if , is found then it is a complete field
                groupList.add(field);
                field = "";
            }
        }

        // add the last element 
        if (field != "") {
            groupList.add(field);
        }

        return groupList;
    }

    public HashMap<String, Collection> getMultiSeries(List<HashMap<String, String>> data, Set<String> categories, String seriesColumnName, String... valueColumnName) throws Exception {
        if (data == null || categories == null || seriesColumnName == null || valueColumnName == null) {
            throw new Exception("Data packing values cannot be null ");
        }
        Set<String> series;
        List<Double> values;
        HashMap<String, Collection> multiSeries = new HashMap<>();
        multiSeries.put("categories", categories);
        Iterator dataIterator = data.iterator();
        while (dataIterator.hasNext()) {
            HashMap<String, String> row = (HashMap<String, String>) dataIterator.next();
            String seriesName = row.get(seriesColumnName);
            ArrayList<String> vals = new ArrayList<>();
            for (int i = 0; i < valueColumnName.length; i++) {
                String value = row.get(valueColumnName[i]);
                vals.add(value);
            }

            if (multiSeries.get("series") == null) {
                series = new LinkedHashSet<>();
                multiSeries.put("series", series);
            } else {
                series = (Set<String>) multiSeries.get("series");
            }
            series.add(seriesName);
            multiSeries.put("series", series);

            if (multiSeries.get(seriesName) == null) {
                values = new LinkedList<>();
                multiSeries.put(seriesName, values);
            } else {
                values = (List) multiSeries.get(seriesName);
            }
            try {
                for (String val : vals) {
                    values.add(Double.parseDouble(val));
                }

            } catch (Exception e) {
                values.add(new Double(0));
            }
        }
        return multiSeries;
    }

    public HashMap<String, Collection> getMultiSeries(List<HashMap<String, String>> data, String categoryColumnName, String seriesColumnName, String... valueColumnName) throws Exception {
        if (data == null || seriesColumnName == null || categoryColumnName == null || valueColumnName == null) {
            throw new Exception("Data packing values cannot be null ");
        }
        Set<String> series;
        List<Double> values;
        Set<String> categories = new LinkedHashSet<String>();
        HashMap<String, Collection> multiSeries = new HashMap<>();

        Iterator dataIterator = data.iterator();
        while (dataIterator.hasNext()) {
            HashMap<String, String> row = (HashMap<String, String>) dataIterator.next();
            String seriesName = row.get(seriesColumnName);
            String category = row.get(categoryColumnName);
            ArrayList<String> vals = new ArrayList<>();
            for (int i = 0; i < valueColumnName.length; i++) {
                String value = row.get(valueColumnName[i]);
                vals.add(value);
            }

            if (multiSeries.get("series") == null) {
                series = new LinkedHashSet<>();
                multiSeries.put("series", series);
            } else {
                series = (Set<String>) multiSeries.get("series");
            }
            categories.add(category);
            series.add(seriesName);
            multiSeries.put("series", series);

            if (multiSeries.get(seriesName) == null) {
                values = new LinkedList<>();
                multiSeries.put(seriesName, values);
            } else {
                values = (List) multiSeries.get(seriesName);
            }
            try {
                for (String val : vals) {
                    values.add(Double.parseDouble(val));
                }

            } catch (Exception e) {
                values.add(new Double(0));
            }
        }
        multiSeries.put("categories", categories);
        return multiSeries;
    }

    public String getPostBody(HttpServletRequest request) throws IOException {
        return request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

    }

    public String getTranslateString(String str, String langcode) throws Exception {
        String strTranslated = new LIDAO().loadString("SELECT ContentTH FROM language_convertion WHERE ContentEN=\"" + str + "\" AND LangCode='" + langcode + "' LIMIT 1 ");

        if (strTranslated == null) {
            return str;
        } else {
            return strTranslated;
        }

    }

    public String getBestString(HttpServletRequest request) {
        String BestString = "Best Overall";
        String strDealergroup = request.getParameter("dealergroup");
        String strZone = request.getParameter("zone");
        String strArea = request.getParameter("area");
        String strRegion = request.getParameter("region");
        if (strDealergroup != null && !strDealergroup.equalsIgnoreCase("ALL") && !strDealergroup.equalsIgnoreCase("Study Total")) {

            BestString = "Best in " + strDealergroup;

        } else if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {

            BestString = "Best in " + strZone;

        } else if (strArea != null && !strArea.equalsIgnoreCase("ALL")) {
            BestString = "Best in " + strArea;
        } else if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            BestString = "Best in " + strRegion;
        }
        return BestString;

    }

    public String getWorstString(HttpServletRequest request) {
        String WorstString = "Worst Overall";
        String strDealergroup = request.getParameter("dealergroup");
        String strZone = request.getParameter("zone");
        String strArea = request.getParameter("area");
        String strRegion = request.getParameter("region");
        if (strDealergroup != null && !strDealergroup.equalsIgnoreCase("ALL") && !strDealergroup.equalsIgnoreCase("Study Total")) {

            WorstString = "Worst in " + strDealergroup;

        } else if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {

            WorstString = "Worst in " + strZone;

        } else if (strArea != null && !strArea.equalsIgnoreCase("ALL")) {
            WorstString = "Worst in " + strArea;
        } else if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            WorstString = "Worst in " + strRegion;
        }
        return WorstString;

    }

    public String getBestStringDg(HttpServletRequest request) throws Exception {
        String BestString = "Best Overall";
        String strDealergroup = request.getParameter("dealergroup");
        String strZone = request.getParameter("zone");
        String strArea = request.getParameter("area");
        String strRegion = request.getParameter("region");
        if (strDealergroup != null && !strDealergroup.equalsIgnoreCase("ALL") && !strDealergroup.equalsIgnoreCase("Study Total")) {
            strRegion = new LIDAO().loadString("select distinct region from factor_aggregate where dealergroup='" + strDealergroup + "' limit 1");

            BestString = "Best in " + strRegion;

        } else if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {

            BestString = "Best in " + strZone;

        } else if (strArea != null && !strArea.equalsIgnoreCase("ALL")) {
            BestString = "Best in " + strArea;
        } else if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            BestString = "Best in " + strRegion;
        }
        return BestString;

    }

    public String getWorstStringDg(HttpServletRequest request) throws Exception {
        String WorstString = "Worst Overall";
        String strDealergroup = request.getParameter("dealergroup");
        String strZone = request.getParameter("zone");
        String strArea = request.getParameter("area");
        String strRegion = request.getParameter("region");
        if (strDealergroup != null && !strDealergroup.equalsIgnoreCase("ALL") && !strDealergroup.equalsIgnoreCase("Study Total")) {
            strRegion = new LIDAO().loadString("select distinct region from factor_aggregate where dealergroup='" + strDealergroup + "' limit 1");

            WorstString = "Worst in " + strRegion;

        } else if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {

            WorstString = "Worst in " + strZone;

        } else if (strArea != null && !strArea.equalsIgnoreCase("ALL")) {
            WorstString = "Worst in " + strArea;
        } else if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            WorstString = "Worst in " + strRegion;
        }
        return WorstString;

    }
}

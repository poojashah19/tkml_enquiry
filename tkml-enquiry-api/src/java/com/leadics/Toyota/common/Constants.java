/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.common;

/**
 *
 * @author HARSHA
 */
public class Constants {

    public static final String CATEGORIES = "cat";
    public static final String MDATA = "metaData";
    public static final String DATA = "data";
    public static final String SERIES = "series";

    public static final String ATTRIBUTE_FACTOR = "SSI";
    public static final String ATTRIBUTE_LIMIT = "5";
    public static final String ATTRIBUTE_MAX_LIMIT = "99999999";
    public static final String ORDER_ASC = "asc";
    public static final String ORDER_DESC = "desc";

}

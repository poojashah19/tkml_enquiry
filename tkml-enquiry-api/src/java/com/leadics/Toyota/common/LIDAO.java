package com.leadics.Toyota.common;

import java.awt.*;
import java.text.*;
import java.util.*;
import java.io.*;
import java.security.spec.KeySpec;
import java.sql.*;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import java.util.List;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.leadics.Toyota.dao.LIAuditlogDAO;
import com.leadics.Toyota.service.LISystemuserService;
import com.leadics.Toyota.to.LIAuditlogRecord;
import com.leadics.Toyota.to.LIJsonTemplate;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.utils.DateUtils;
import com.leadics.utils.LISecurityUtil;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;

public class LIDAO {

    static LogUtils logger = new LogUtils(LIDAO.class.getName());

    protected DateUtils fd = new DateUtils();

    protected String MAX_RECORD_LIMIT_APPENDER = "";
    protected String ORDERBYSTRING = "1";
    protected String CUSTOM_CONDITION = "";
    protected String OUTER_CUSTOM_CONDITION = "";

    public LIDAO() {
    }

    static String dbtabquery[]
            = {
                "select * from sysobjects where (name not like 'sys%')and(name not like '%_PK%')and(name not like '%_FK%')and(name not like '%FK_%')and(name not like '%PK_%')and(name not like '%UQ_%')and(name not like '%uniq_%')",
                "select * from tab",
                "select TABNAME from SYSCAT.TABLES WHERE (TABSCHEMA = '2012')",
                "SHOW FULL TABLES"
            };

    public boolean isOracleDatabase() {
        String dbms = PropertyUtil.getProperty("DBMS");
        LogUtils.println("DBMS Property:" + dbms);
        return dbms.equalsIgnoreCase("ORACLE");
    }

    public ArrayList getTableList(Connection conn, int queryCode)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ArrayList list = new ArrayList();
            ps = conn.prepareStatement(dbtabquery[queryCode]);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(rs.getString(1).toLowerCase());
            }
            rs.close();
            ps.close();
            return list;
        } finally {
            releaseDatabaseConnection(rs, ps, conn, false);
        }
    }

    public ArrayList getColumnList(Connection conn, String tableName, String limitcode)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String query = "select * from " + tableName + " " + limitcode;
            ArrayList list = new ArrayList();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            ResultSetMetaData rsmetadata = rs.getMetaData();
            int colCount = rsmetadata.getColumnCount();
            for (int index = 1; index <= colCount; index++) {
                String ColName = rsmetadata.getColumnName(index);
                list.add(ColName);
            }
            rs.close();
            ps.close();
            return list;
        } finally {
            releaseDatabaseConnection(rs, ps, conn, false);
        }
    }

    public void setLimits(String offset, String rowcount) {
        if (StringUtils.isNullOrEmpty(offset)) {
            offset = "0";
        }
        if (StringUtils.isNullOrEmpty(rowcount)) {
            rowcount = "20";
        }

        MAX_RECORD_LIMIT_APPENDER = " limit " + offset + "," + rowcount;

        if (isMSSQL()) {
            MAX_RECORD_LIMIT_APPENDER = " OFFSET " + offset + " ROWS FETCH NEXT " + rowcount + " ROWS ONLY";
//			OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY
        }

        if (isMSSQL8()) {
            MAX_RECORD_LIMIT_APPENDER = "";
            CUSTOM_CONDITION += " AND (rownum >= $LMTOFFSET$) AND  (rownum <= ($LMTOFFSET$ + $LMTROWCNT$))";
            CUSTOM_CONDITION = StringUtils.replaceString(CUSTOM_CONDITION, "$LMTOFFSET$", offset, true);
            CUSTOM_CONDITION = StringUtils.replaceString(CUSTOM_CONDITION, "$LMTROWCNT$", rowcount, true);
        }

        if (isOracleDatabase()) {
            MAX_RECORD_LIMIT_APPENDER = "";
            OUTER_CUSTOM_CONDITION = "";
            OUTER_CUSTOM_CONDITION += " AND (R between  $LMTOFFSET$ AND ($LMTOFFSET$ + $LMTROWCNT$))";
            OUTER_CUSTOM_CONDITION = StringUtils.replaceString(OUTER_CUSTOM_CONDITION, "$LMTOFFSET$", offset, true);
            OUTER_CUSTOM_CONDITION = StringUtils.replaceString(OUTER_CUSTOM_CONDITION, "$LMTROWCNT$", rowcount, true);
            LogUtils.println("OUTER_CUSTOM_CONDITION:" + OUTER_CUSTOM_CONDITION);
        }
    }

    public static String DBMS = "";

    public static boolean isMSSQL() {
        String dbms = PropertyUtil.getProperty("DBMS");
        return dbms.equalsIgnoreCase("MSSQL");
    }

    public static boolean isMSSQL8() {
        String dbms = PropertyUtil.getProperty("DBMS");
        return dbms.equalsIgnoreCase("MSSQL8");
    }

    public String updateQuery(String query) {
        //LogUtils.println("DBMS:" + DBMS);
        LogUtils.println("DBMS1:" + PropertyUtil.getProperty("DBMS"));
        //LogUtils.println("DBMS:" + PropertyUtil.getProperty("DBMS"));

        if (isMSSQL()) {
            query = StringUtils.replaceString(query, " system_user", " systemuser", true);
        }

        if (isMSSQL8()) {
            query = StringUtils.replaceString(query, " system_user", " systemuser", true);
        }

        if (isOracleDatabase()) {
            if (query.toLowerCase().contains("convert(")) {
                if (query.toLowerCase().contains(",decimal")) {
                    query = StringUtils.replaceString(query, "CONVERT(", "CAST(", true);
                    query = StringUtils.replaceString(query, ",DECIMAL", " AS DECIMAL", true);
                    query = StringUtils.replaceString(query, "convert(", "CAST(", true);
                    query = StringUtils.replaceString(query, ",decimal", " AS decimal", true);
                    query = StringUtils.replaceString(query, ", decimal", " AS decimal", true);
                }
            }
        }

        LogUtils.println("Query:" + query);

        return query;
    }

    public void setCustomCondition(String customCond) {
        CUSTOM_CONDITION = "";

        if (!StringUtils.isNullOrEmpty(customCond)) {
            CUSTOM_CONDITION = customCond;
        }
    }

    public String getCustomCondition() {
        return CUSTOM_CONDITION;
    }

    public boolean hasCustomCondition() {
        if (StringUtils.isNullOrEmpty(getCustomCondition())) {
            return false;
        }
        return true;
    }

    public void setOrderBy(String orderByString) {
        ORDERBYSTRING = "1";
        if (!StringUtils.isNullOrEmpty(orderByString)) {
            ORDERBYSTRING = orderByString;
        }

        if (isMSSQL8()) {
            if (ORDERBYSTRING.equals("1")) {
                ORDERBYSTRING = "ID";
            }
        }
    }

    public String formatSearchField(String inputValue) {
        if (StringUtils.isNullOrEmpty(inputValue)) {
            return inputValue;
        }
        String resultValue = StringUtils.replaceString(inputValue, "'", "", true);
        return resultValue;
    }

    static ArrayList excludeTableList = new ArrayList();

    public boolean isTableToBeExcluded(String inputTableName) {
        try {
            inputTableName = StringUtils.noNull(inputTableName);
            inputTableName = inputTableName.toLowerCase();

            if (inputTableName.equals("audit_log")) {
                return true;
            }
            if (inputTableName.equals("intf_call_log")) {
                return true;
            }
            if (inputTableName.equals("mobilejsonrequest")) {
                return true;
            }
            if (inputTableName.equals("mobilejsonresponse")) {
                return true;
            }
            if (inputTableName.equals("mail_mesg")) {
                return true;
            }
            if (inputTableName.equals("health_check_stat")) {
                return true;
            }

            if (inputTableName.equals("web_session")) {
                return true;
            }
            if (inputTableName.equals("web_access_log")) {
                return true;
            }

            if (inputTableName.equals("sms_mesg")) {
                return true;
            }
            if (inputTableName.equals("maker_checker_audit")) {
                return true;
            }

            if (excludeTableList.isEmpty()) {
                String query = "select distinct cvalue from content_map where (ctype = 'EXCLUDE_FROM_AUDIT_LOG')";

                Connection con = null;
                PreparedStatement ps = null;
                ResultSet rs = null;
                try {
                    con = getDatabaseConnection();
                    ps = con.prepareStatement(query);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        String column = StringUtils.noNull(rs.getString("cvalue")).toLowerCase();
                    }
                    rs.close();
                    ps.close();
                    releaseDatabaseConnection(con);
                } finally {
                    releaseDatabaseConnection(rs, ps, con, true);
                }

                if (excludeTableList.isEmpty()) {
                    excludeTableList.add("audit_log");
                }
            }

            return excludeTableList.contains(inputTableName);
        } catch (Exception exception) {
            logger.error(exception);
            return false;
        }
    }

    public void createAuditRecord(String tableName, String operation, String createdBy, String modifiedBy, String createdAt, String modifiedAt, String message, String institutionId) {
        createAuditRecord(tableName, operation, createdBy, modifiedBy, createdAt, modifiedAt, message, institutionId, null, null);
    }

    public void createAuditRecord(String tableName, String operation, String createdBy, String modifiedBy, String createdAt, String modifiedAt, String message, String institutionId, String source) {
        createAuditRecord(tableName, operation, createdBy, modifiedBy, createdAt, modifiedAt, message, institutionId, null, source);
    }

    private String getFullName(LISystemuserRecord record) {
        String result = "";
        if (!StringUtils.isNullOrEmpty(record.getFirstname())) {
            result = record.getFirstname();
            result = result + " ";
        }

        if (!StringUtils.isNullOrEmpty(record.getMiddlename())) {
            result += record.getMiddlename();
            result = result + " ";
        }

        if (!StringUtils.isNullOrEmpty(record.getLastname())) {
            result += record.getLastname();
        }
        return result;
    }

    public void createAuditRecord(String tableName, String operation, String createdBy, String modifiedBy, String createdAt, String modifiedAt, String message, String institutionId, String currentRecordContent, String source) {
//        if (isTableToBeExcluded(tableName)) {
//            return;
//        }
        if (message.toUpperCase().contains("SEND_SMS")) {
            return;
        }
        if (message.toUpperCase().contains("SEND_MAIL")) {
            return;
        }

        if (!StringUtils.isSame(PropertyUtil.getProperty("dbauditenabled"), "Y")) {
            return;
        }

        String userName = "";

        try {
            LISystemuserService systemUserService = new LISystemuserService();
            LISystemuserRecord systemUserRecord = (LISystemuserRecord) systemUserService.loadLISystemuserRecord(createdBy);
            userName = StringUtils.noNull(systemUserRecord.getUserid()) + "-" + StringUtils.noNull(getFullName(systemUserRecord));
        } catch (Exception e) {

        }
        LogUtils.println("Fetched User Name " + userName);
        LogUtils.println("Source " + source);

        LIAuditlogDAO auditLogDAO = new LIAuditlogDAO();
        try {
            LIAuditlogRecord auditLogRecord = new LIAuditlogRecord();
            auditLogRecord.setCreatedat(createdAt);
            auditLogRecord.setCreatedby(createdBy);
            auditLogRecord.setModifiedat(modifiedAt);
            auditLogRecord.setModifiedby(modifiedBy);
            auditLogRecord.setUserid(userName);
            if (source != null) {
                auditLogRecord.setSourceid(source);
            }

            if (institutionId != null) {
                auditLogRecord.setInstitutionid(institutionId);
            }
            auditLogRecord.setEntityname(tableName);
            auditLogRecord.setActiontype(operation);
            String shortDateCode = DateUtils.getCurrentDate();
            int recordId = auditLogDAO.insertLIAuditlogRecord(auditLogRecord);

            if (!StringUtils.isNullOrEmpty(currentRecordContent)) {
                if (StringUtils.noNull(operation).equals("UPDATE")) {
                    String optMessage = "\n\nRequest Source " + source;
                    optMessage = optMessage + "\n\nUser " + userName;

                    message = optMessage + message;
                    message = message + "\n\nOriginal Record:\n\n" + currentRecordContent;
                }
            }

            storeRequestResponse(recordId + "", shortDateCode, message);
        } catch (Exception exception) {
            logger.error(exception);
            logger.error(StringUtils.getStackTrace(exception));
        }
    }

    static String initialDateCode = "";

    public String createAuditDirIfRequired(String fileName, String dateCode) {
        String newFileName = fileName + dateCode + "/";
        if (!dateCode.equals(initialDateCode)) {
            try {
                File f = new File(newFileName);
                f.mkdirs();
            } catch (Exception e) {

            }
        }
        return newFileName;
    }

    public void storeRequestResponse(String requestId, String dateCode, String message) {
        try {
            dateCode = StringUtils.noNull(dateCode);
            initialDateCode = StringUtils.noNull(initialDateCode);
            String fileName = StringUtils.noNull(PropertyUtil.getProperty("AuditLogFolder"));

            fileName = createAuditDirIfRequired(fileName, dateCode);
            StringUtils.writeStringToFile(message, fileName + requestId + ".txt");
        } catch (Exception exception) {
            logger.error(exception);
        }
    }

    public void storeRequestResponse(String requestId, String message) {
        try {
            String fileName = StringUtils.noNull(PropertyUtil.getProperty("AuditLogFolder"));
            StringUtils.writeStringToFile(message, fileName + requestId + ".txt");
        } catch (Exception exception) {
            logger.error(exception);
        }
    }

    public Connection getDatabaseConnection(String driver, String url, String uid, String pwd) {
        try {
            Class.forName(driver);
            Connection con = DriverManager.getConnection(url, uid, pwd);
            return con;
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
    }

    public Connection getCPDatabaseConnection() {
        LogUtils.println("Connection Pool Flag:" + PropertyUtil.getProperty("UseCP"));
        return getDirectDatabaseConnection();
    }

    public Connection getDatabaseConnection() {
        return getCPDatabaseConnection();
    }

    public Connection getDirectDatabaseConnection() {
        try {
            Class.forName(PropertyUtil.getDbdriver());
            Connection con = DriverManager.getConnection(PropertyUtil.getDburl(), PropertyUtil.getDbuid(), (new LISecurityUtil()).decryptRegular(PropertyUtil.getDbpwd()));
            return con;
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
    }

    public Connection getCBDatabaseConnection() {
        try {
            Class.forName(PropertyUtil.getProperty("CoreBankingDBDriver"));
            Connection con = DriverManager.getConnection(PropertyUtil.getProperty("CoreBankingDBURL"), PropertyUtil.getProperty("CoreBankingDBUID"), PropertyUtil.getProperty("CoreBankingDBPWD"));
            return con;
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
    }

    public Connection getSecondDatabaseConnection() {
        try {
            Class.forName(PropertyUtil.getProperty("DBDriver2"));
            Connection con = DriverManager.getConnection(PropertyUtil.getProperty("DBURL2"), PropertyUtil.getProperty("DBUid2"), (new LISecurityUtil()).decryptRegular(PropertyUtil.getProperty("DBPwd2")));
            return con;
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
    }

    public void releaseDatabaseConnection(Connection con) {
        releaseConnection(con);
    }

    public void releaseDatabaseConnection(Connection con, boolean releaseConnection) {
        if (releaseConnection) {
            releaseConnection(con);
        }
    }

    public void closeResultSet(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (Exception e) {

        }
    }

    public void closePreparedStatement(PreparedStatement ps) {
        try {
            if (ps != null) {
                ps.close();
            }
        } catch (Exception e) {

        }
    }

    public void releaseDatabaseConnection(ResultSet rs, PreparedStatement ps, Connection con, boolean closeConnectionConnection) {

        //closeResultSet(rs);
        closePreparedStatement(ps);
        releaseDatabaseConnection(con, closeConnectionConnection);
    }

    public void releaseDatabaseConnection(ResultSet rs, PreparedStatement ps, Connection con) {

        //closeResultSet(rs);
        closePreparedStatement(ps);
        releaseDatabaseConnection(con);
    }

    public void releaseConnection(Connection con) {
        try {
            con.close();
        } catch (Exception e) {
            logger.error(e);
        }
    }

    //**********************************************************************************************
    //Utility Methods
    //**********************************************************************************************
    public boolean isNull(String value) {
        if (value == null) {
            return true;
        }
        if (value.trim().length() < 1) {
            return true;
        }
        return false;
    }

    public String encodeBit(String Value) {
        if (Value == null) {
            return "0";
        }
        if (Value.equals("Y")) {
            return "1";
        } else {
            return "0";
        }
    }

    public String getYN(boolean flag) {
        if (flag == true) {
            return "Y";
        }
        return "N";
    }

    public String decodeBit(String Value) {
        if (Value == null) {
            return "N";
        }
        if (Value.equals("1")) {
            return "Y";
        } else {
            return "N";
        }
    }

    public String getCheckBoxValue(String Value) {
        if (Value == null) {
            return "N";
        } else {
            return Value;
        }
    }

    public String loadSequenceID(String Query, String Column)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = getDatabaseConnection();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            String result = "";
            while (rs.next()) {
                result = rs.getString(Column);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return result;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public String noNull(Object value) {
        if (value == null) {
            return "";
        }
        return value.toString();
    }

    public String noNull(String value) {
        if (value == null) {
            return "";
        }
        return value;
    }

    public String noNull(String value, String defaultVal) {
        if (isNull(value)) {
            return defaultVal;
        }
        return noNull(value);
    }

    public void setDateValue(PreparedStatement ps, int index, java.sql.Date value)
            throws Exception {
        if (value == null) {
            ps.setNull(index, java.sql.Types.DATE);
        } else {
            java.sql.Timestamp ts = new java.sql.Timestamp(value.getTime());
            ps.setTimestamp(index, ts);
        }
    }

    public void setStringValue(PreparedStatement ps, int index, String value)
            throws Exception {
        if (value == null) {
            ps.setNull(index, java.sql.Types.VARCHAR);
        } else if (value.trim().length() < 1) {
            ps.setNull(index, java.sql.Types.VARCHAR);
        } else {
            if (value.equals("-")) {
                value = "";
            }
            ps.setString(index, value);
        }
    }

    public boolean executeUpdateQuery(Connection con, String Query)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            ps = con.prepareStatement(Query);
            boolean result = ps.execute();
            ps.close();
            return result;
        } finally {
            releaseDatabaseConnection(rs, ps, con, false);
        }
    }

    public int executeUpdateQuery(String Query)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            LogUtils.println("Update Query:" + Query);
            con = getDatabaseConnection();
            ps = con.prepareStatement(Query);
            int result = ps.executeUpdate();
            ps.close();
            releaseDatabaseConnection(con);
            return result;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public String loadString(String Query, String columnName)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            String value = null;
            if (rs.next()) {
                value = rs.getString(columnName);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return value;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public JSONArray loadColumnListAsJSONWithName(String Query, String Column, String Name, boolean insertBlank)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            JSONArray arrayList = new JSONArray();
            if (insertBlank) {
                arrayList.add("");
            }

            while (rs.next()) {
                JSONObject obj = new JSONObject();
                obj.put(Name, rs.getString(Column));
                arrayList.add(obj);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return arrayList;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public JSONArray loadColumnListAsJSON(String Query, String Column, boolean insertBlank)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            JSONArray arrayList = new JSONArray();
            if (insertBlank) {
                arrayList.add("");
            }

            while (rs.next()) {
                arrayList.add(rs.getString(Column));
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return arrayList;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public String loadString(Connection con, String Query)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            String value = null;
            if (rs.next()) {
                value = rs.getString(1);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return value;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public String loadString(String Query)
            throws Exception {
        System.out.println("Query: " + Query);
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            String value = null;
            if (rs.next()) {
                value = rs.getString(1);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return value;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public int loadRecordCount(Connection con, String Query)
            throws Exception {
        return loadRecordCount(con, Query, false);
    }

    public int loadRecordCount(Connection con, String Query, boolean closeConn)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            int count = 0;
            if (rs.next()) {
                count = (int) Double.parseDouble(rs.getString(1));
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con, closeConn);
            return count;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConn);
        }
    }

    public int loadCount(String Query)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            LogUtils.println("Count Query:" + Query);
            con = getDatabaseConnection();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            int count = 0;
            if (rs.next()) {
                if (isOracleDatabase()) {
                    count = rs.getInt(1);
                } else {
                    count = (int) Double.parseDouble(rs.getString(1));
                }
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            LogUtils.println("Count Query:" + Query + " Result:" + count);
            return count;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public int loadCount(Connection con, String Query)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            int count = 0;
            if (rs.next()) {
                if (isOracleDatabase()) {
                    count = rs.getInt(1);
                } else {
                    count = (int) Double.parseDouble(rs.getString(1));
                }
            }
            rs.close();
            ps.close();
            return count;
        } finally {
            releaseDatabaseConnection(rs, ps, con, false);
        }
    }

    public Vector loadDoubleColumn(String Query, String Column1, String Column2, boolean insertBlank)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            Vector aVectorResult = new Vector();
            if (insertBlank) {
                Vector aVector = new Vector();
                aVector.addElement("");
                aVector.addElement("");
                aVectorResult.addElement(aVector);
            }

            while (rs.next()) {
                Vector aVector = new Vector();
                aVector.addElement(rs.getString(Column1));
                aVector.addElement(rs.getString(Column2));
                aVectorResult.addElement(aVector);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return aVectorResult;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public String loadSingleColumnAsString(String Query, String Column, boolean insertBlank)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            StringBuffer sb = new StringBuffer();
            con = getDatabaseConnection();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            if (insertBlank) {
                sb.append("\n");
            }

            while (rs.next()) {
                sb.append(rs.getString(Column) + "\n");
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return sb.toString();
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public Vector loadSingleColumn(String Query, String Column, boolean insertBlank)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            Vector aVector = new Vector();
            if (insertBlank) {
                aVector.addElement("");
            }

            while (rs.next()) {
                aVector.addElement(rs.getString(Column));
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return aVector;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public ArrayList loadColumnList(Connection conn, String Query, boolean insertBlank)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            ps = conn.prepareStatement(Query);
            rs = ps.executeQuery();
            ArrayList arrayList = new ArrayList();
            if (insertBlank) {
                arrayList.add("");
            }

            while (rs.next()) {
                arrayList.add(rs.getString(1));
            }
            rs.close();
            ps.close();

            return arrayList;
        } finally {
            releaseDatabaseConnection(rs, ps, conn, false);
        }
    }

    public ArrayList loadColumnList(String Query, String Column, boolean insertBlank)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getCPDatabaseConnection();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            ArrayList arrayList = new ArrayList();
            if (insertBlank) {
                arrayList.add("");
            }

            while (rs.next()) {
                arrayList.add(rs.getString(Column));
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return arrayList;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public Map<String, Collection> loadValues(String query, HashMap<String, String> columnInfo) throws SQLException, Exception {

        Map<String, Collection> columnMap;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            query = updateQuery(query);
            con = getCPDatabaseConnection();
            JSONArray resultArray = new JSONArray();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            columnMap = convertResultSetToColumnMap(rs, columnInfo);
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return columnMap;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }

    }

    public List<HashMap<String, String>> loadValuesRowWise(String query, HashMap<String, String> columnInfo) throws SQLException, Exception {

        List<HashMap<String, String>> columnMap;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            query = updateQuery(query);
            con = getCPDatabaseConnection();
            JSONArray resultArray = new JSONArray();
            int i = con.createStatement().executeUpdate("SET @r1=0,@x1=1,@XSUM=0;");
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            columnMap = convertResultSetToList(rs, columnInfo);
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return columnMap;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }

    }

    public List<HashMap<String, String>> convertResultSetToList(ResultSet rs, HashMap<String, String> columnInfo) throws SQLException, Exception {
        ResultSetMetaData md = rs.getMetaData();

        Set keys = columnInfo.keySet();
        int columns = keys.size();
        List<HashMap<String, String>> list = new LinkedList<>();
        LIService service = new LIService();
        while (rs.next()) {
            HashMap<String, String> row = new LinkedHashMap<>(columns);
            Iterator i = keys.iterator();
            while (i.hasNext()) {
                String key = i.next().toString();

                row.put(columnInfo.get(key), rs.getString(key));

            }

            list.add(row);
        }

        return list;
    }

    public Map<String, Collection> convertResultSetToColumnMap(ResultSet rs, HashMap<String, String> columnInfo) throws SQLException, Exception {
        ResultSetMetaData md = rs.getMetaData();
        LIService service = new LIService();
        Set keys = columnInfo.keySet();

        Map<String, Collection> columnMap = new HashMap<>();
        Map<String, Class> typeMap = new HashMap<>();

        setTypes(rs, columnInfo, columnMap, typeMap);

        while (rs.next()) {
            Iterator i = keys.iterator();
            int k = 0;
            while (i.hasNext()) {
                String key = i.next().toString();
                List currentColumn = (List) columnMap.get(key);

                Class type = typeMap.get(key);

                currentColumn.add(rs.getObject(key, type));
            }

        }

        return columnMap;
    }

    public JSONArray load3Vals(String Query, String column1Title, String column1, String column2Title, String column2, String column3Title, String column3)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getCPDatabaseConnection();
            JSONArray resultArray = new JSONArray();

            ps = con.prepareStatement(Query);

            rs = ps.executeQuery();
            Vector aVectorResult = new Vector();
            while (rs.next()) {
                JSONObject object = new JSONObject();
                object.put(column1Title, rs.getString(column1));
                object.put(column2Title, rs.getString(column2));
                object.put(column3Title, rs.getString(column3));
                resultArray.add(object);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return resultArray;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public JSONArray load4Vals(String Query, String column1Title, String column1, String column2Title, String column2, String column3Title, String column3, String column4Title, String column4)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            JSONArray resultArray = new JSONArray();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            Vector aVectorResult = new Vector();
            while (rs.next()) {
                JSONObject object = new JSONObject();
                object.put(column1Title, rs.getString(column1));
                object.put(column2Title, rs.getString(column2));
                object.put(column3Title, rs.getString(column3));
                object.put(column4Title, rs.getString(column4));
                resultArray.add(object);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return resultArray;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public JSONArray load5Vals(String Query, String column1Title, String column1, String column2Title, String column2, String column3Title, String column3, String column4Title, String column4, String column5Title, String column5)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            JSONArray resultArray = new JSONArray();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            Vector aVectorResult = new Vector();
            while (rs.next()) {
                JSONObject object = new JSONObject();
                object.put(column1Title, rs.getString(column1));
                object.put(column2Title, rs.getString(column2));
                object.put(column3Title, rs.getString(column3));
                object.put(column4Title, rs.getString(column4));
                object.put(column5Title, rs.getString(column5));
                resultArray.add(object);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return resultArray;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }

    }

    public JSONArray load5ValsDynamicArgs(String Query, String column1Title, String column1, String column2Title, String column2, String column3Title, String column3, String column4Title, String column4, String column5Title, String column5, int v1, int v2)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            JSONArray resultArray = new JSONArray();
            ps = con.prepareStatement(Query);
            ps.setInt(1, v1);
            ps.setInt(2, v2);
            rs = ps.executeQuery();
            Vector aVectorResult = new Vector();
            while (rs.next()) {
                JSONObject object = new JSONObject();
                object.put(column1Title, rs.getString(column1));
                object.put(column2Title, rs.getString(column2));
                object.put(column3Title, rs.getString(column3));
                object.put(column4Title, rs.getString(column4));
                object.put(column5Title, rs.getString(column5));
                resultArray.add(object);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return resultArray;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }

    }

    public JSONArray load4ValsDynamicArgs(String Query, String column1Title, String column1, String column2Title, String column2, String column3Title, String column3, String column4Title, String column4, int v1, int v2)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            JSONArray resultArray = new JSONArray();
            ps = con.prepareStatement(Query);
            ps.setInt(1, v1);
            ps.setInt(2, v2);
            rs = ps.executeQuery();
            Vector aVectorResult = new Vector();
            while (rs.next()) {
                JSONObject object = new JSONObject();
                object.put(column1Title, rs.getString(column1));
                object.put(column2Title, rs.getString(column2));
                object.put(column3Title, rs.getString(column3));
                object.put(column4Title, rs.getString(column4));

                resultArray.add(object);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return resultArray;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }

    }

    public JSONArray load6Vals(String Query, String column1Title, String column1, String column2Title, String column2, String column3Title, String column3, String column4Title, String column4, String column5Title, String column5, String column6Title, String column6)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            JSONArray resultArray = new JSONArray();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            Vector aVectorResult = new Vector();
            while (rs.next()) {
                JSONObject object = new JSONObject();
                object.put(column1Title, rs.getString(column1));
                object.put(column2Title, rs.getString(column2));
                object.put(column3Title, rs.getString(column3));
                object.put(column4Title, rs.getString(column4));
                object.put(column5Title, rs.getString(column5));
                object.put(column6Title, rs.getString(column6));
                resultArray.add(object);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return resultArray;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public JSONArray load2Vals(String Query, String column1Title, String column1, String column2Title, String column2)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            JSONArray resultArray = new JSONArray();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            Vector aVectorResult = new Vector();
            while (rs.next()) {
                JSONObject object = new JSONObject();
                object.put(column1Title, rs.getString(column1));
                object.put(column2Title, rs.getString(column2));
                resultArray.add(object);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return resultArray;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public JSONArray load2ValsDynamicArgs(PrintWriter out, String Query, String column1Title, String column1, String column2Title, String column2, String d1, String d2)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            JSONArray resultArray = new JSONArray();
            ps = con.prepareStatement(Query);

            ps.setString(1, d1);

            if (!StringUtils.isNullOrEmpty(d2)) {
                ps.setString(2, d2);
            }

            rs = ps.executeQuery();

            Vector aVectorResult = new Vector();
            while (rs.next()) {
                JSONObject object = new JSONObject();
                object.put(column1Title, rs.getString(column1));
                object.put(column2Title, rs.getString(column2));
                resultArray.add(object);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return resultArray;
        } catch (Exception e) {
            out.println(e);
        } finally {

            releaseDatabaseConnection(rs, ps, con, true);
        }
        return new JSONArray();
    }

    public JSONArray load3ValsDynamicArgs(PrintWriter out, String Query, String column1Title, String column1, String column2Title, String column2, String column3Title, String column3, String d1, String d2)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            JSONArray resultArray = new JSONArray();
            ps = con.prepareStatement(Query);

            ps.setString(1, d1);

            if (!StringUtils.isNullOrEmpty(d2)) {
                ps.setString(2, d2);
            }

            rs = ps.executeQuery();

            Vector aVectorResult = new Vector();
            while (rs.next()) {
                JSONObject object = new JSONObject();
                object.put(column1Title, rs.getString(column1));
                object.put(column2Title, rs.getString(column2));
                object.put(column3Title, rs.getString(column3));
                resultArray.add(object);
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return resultArray;
        } catch (Exception e) {
            out.println(e);
        } finally {

            releaseDatabaseConnection(rs, ps, con, true);
        }
        return new JSONArray();
    }

    public Map loadMap(String Query, String Column1, String Column2, boolean insertBlank)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            Map hashMap = new LinkedHashMap();
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            Vector aVectorResult = new Vector();
            if (insertBlank) {
                hashMap.put("", "");
            }

            while (rs.next()) {
                hashMap.put(rs.getString(Column1), rs.getString(Column2));
            }
            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return hashMap;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    public String addSearchCondition(String Condition, String Structure, String NullStructure, String columnName, String value) {
        String ResultCondition = "";
        if (!isNull(value)) {
            Structure = StringUtils.replaceString(Structure, "$COLNAME$", columnName, true);
            Structure = StringUtils.replaceString(Structure, "$COLVAL$", value, true);
            ResultCondition = Structure;
        } else {
            NullStructure = StringUtils.replaceString(NullStructure, "$COLNAME$", columnName, true);
            NullStructure = StringUtils.replaceString(NullStructure, "$COLVAL$", value, true);
            ResultCondition = NullStructure;
        }
        return addSearchCondition(Condition, ResultCondition);
    }

    public String addSearchCondition(String Condition, String newCondition) {
        if (isNull(Condition)) {
            return newCondition;
        }
        if (isNull(newCondition)) {
            return Condition;
        }
        return Condition + "AND" + newCondition;
    }

    public static void main(String[] args)
            throws Exception {
        LogUtils.println("Starting...");
        LIDAO dao = new LIDAO();
        int cnt = dao.loadCount("select count(*) from customer");
        LogUtils.println("Done");
    }

    public String formatDate(java.sql.Date anDate)
            throws Exception {
        if (anDate == null) {
            return "";
        }
        SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddHHmmss");
        return sd.format(anDate);
    }

    public String formatDBDateTime(java.sql.Timestamp anDate)
            throws Exception {
        if (anDate == null) {
            return "";
        }
        SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddHHmmss");
        return sd.format(anDate);
    }

    public String loadMSSQL8OrderByID(String inputCol) {
        return "ID";
    }

    public String getOuterLimitCondition() {
        return OUTER_CUSTOM_CONDITION;
    }

    public String loadOracleOrderByID(String inputCol) {
        String tempORDERBYSTRING = StringUtils.noNull(ORDERBYSTRING);
        LogUtils.println("Input Order Col:" + tempORDERBYSTRING);
        if (tempORDERBYSTRING.contains("DESC")) {
            return "ID DESC";
        }

        return "ID";
    }

    public String[] load2ValsArray(String Query, String column1Title, String column2Title) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Query = updateQuery(Query);
            con = getDatabaseConnection();
            String resultArray[] = new String[2];
            ps = con.prepareStatement(Query);
            rs = ps.executeQuery();
            Vector aVectorResult = new Vector();
            while (rs.next()) {
                resultArray[0] = rs.getString(column1Title);
                resultArray[1] = rs.getString(column2Title);
            }

            rs.close();
            ps.close();
            releaseDatabaseConnection(con);
            return resultArray;
        } finally {
            releaseDatabaseConnection(rs, ps, con, true);
        }
    }

    // inits the object respective to type of column type in sql: stores the the type into typeMap
    private void setTypes(ResultSet rs, HashMap<String, String> columnInfo, Map<String, Collection> columnMap,
            Map<String, Class> typeMap) throws SQLException, Exception {
        ResultSetMetaData md = rs.getMetaData();

        List<Integer> indexList = getColumnIndexSet(md, columnInfo);
        for (int k : indexList) {

            if ("java.lang.String".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), String.class);
                List<String> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else if ("java.lang.Byte".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), Byte.class);
                List<Byte> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else if ("java.io.InputStream".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), String.class);
                List<String> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else if ("java.io.Reader".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), String.class);
                List<String> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else if ("java.sql.Blob".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), String.class);
                List<String> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else if ("java.lang.Short".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), Integer.class);
                List<Integer> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else if ("java.lang.Integer".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), Integer.class);
                List<Integer> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else if ("java.lang.Long".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), Long.class);
                List<Long> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else if ("java.lang.Double".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), Double.class);
                List<Double> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else if ("java.math.BigDecimal".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), java.math.BigDecimal.class);
                List<java.math.BigDecimal> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else if ("java.sql.Date".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), java.sql.Date.class);
                List<java.sql.Date> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else if ("java.sql.Timestamp".equals(md.getColumnClassName(k))) {
                typeMap.put(md.getColumnLabel(k), java.sql.Timestamp.class);
                List<java.sql.Timestamp> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            } else {
                typeMap.put(md.getColumnLabel(k), String.class);
                List<String> li = new LinkedList<>();
                columnMap.put(md.getColumnLabel(k), li);
            }

        }
    }

    // send the list of indexes for the specified columnm names in columnInfo 
    private List<Integer> getColumnIndexSet(ResultSetMetaData md, Map<String, String> columnInfo) throws SQLException {
        List<Integer> li = new LinkedList<>();
        int totalCount = md.getColumnCount();
        Set keys = columnInfo.keySet();
        for (int k = 1; k <= totalCount; k++) {
            String key = md.getColumnLabel(k);
            if (keys.contains(key)) {
                li.add(k);
            }
        }

        return li;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.common;

import com.google.gson.Gson;
import com.leadics.exceptions.SessionExpiredException;
import com.leadics.Toyota.beans.LILoggedInUserUtilitiesBean;
import static com.leadics.Toyota.common.LIRecord.logger;
import com.leadics.Toyota.service.LIControllermapService;
import com.leadics.Toyota.service.LISystemuserService;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.to.LIControllermapRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.LIWebsessionRecord;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author varma.sagi
 */
public class LIAction extends HttpServlet {

    private static final Set<String> NO_SESSION_ACTIONS = new HashSet<>(
            Arrays.asList("-Systemuserview-")
    );
    public static final String AUTHORIZATION = "Authorization";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LIAction</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LIAction at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
       response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            HttpServletRequest req = request;
            HttpServletResponse res = response;
            String actionType = request.getParameter("hiddenActionType");
            response.setContentType("text/html");

            LILoggedInUserUtilitiesBean utilBean = new LILoggedInUserUtilitiesBean();
            if (!utilBean.isRequestSecure(req)) {
                return;
            }

            String tableCode = actionType.substring(0,
                    actionType.lastIndexOf('-') + 1);
            String actionCode = actionType.substring(actionType
                    .lastIndexOf('-') + 1);

            logger.debug("\nTable Code:" + tableCode + "\nAction Type:"
                    + actionType + "\nAction Code:" + actionCode);

            LIControllermapService service = new LIControllermapService();

            // validate the session before any request unless it's not login or register url or forgot
            if (hasToCheckSession(tableCode)) {
                if (!sessionValid(request, response)) {
                    response.setStatus(401);
                    throw new SessionExpiredException("Your session has Expired");
                }
            }

            LIControllermapRecord searchRecord = new LIControllermapRecord();
            searchRecord.setCode(tableCode);
            LIControllermapRecord[] records = service
                    .searchLIControllermapRecords(searchRecord);

            logger.debug("\nClass being called:" + records[0].getClassname());
            // System.out.println(tableCode);
            // System.out.println(records[0].getClassname());
            Class c = Class.forName(records[0].getClassname());
            Method m = c.getDeclaredMethod("processWebRequest", new Class[]{
                javax.servlet.http.HttpServletRequest.class,
                javax.servlet.http.HttpServletResponse.class,
                java.lang.String.class});
            Object i = c.newInstance();
            Object r = m.invoke(i, new Object[]{request, response, actionCode});

        } catch (SessionExpiredException se) {
            out.println("-2");
            response.setStatus(401);
            return;
        } catch (Exception exception) {
            out.println("Unauthorized Access");
            out.println("<H1><c>LeadICS</H1>");
            logger.error(exception);
        }
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

       response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            HttpServletRequest req = request;
            HttpServletResponse res = response;
            String actionType = request.getParameter("hiddenActionType");
            response.setContentType("text/html;charset=UTF-8");

            LILoggedInUserUtilitiesBean utilBean = new LILoggedInUserUtilitiesBean();
            if (!utilBean.isRequestSecure(req)) {
                return;
            }
            //System.out.println("*****()***********" + actionType);
            String tableCode = actionType.substring(0,
                    actionType.lastIndexOf('-') + 1);
            String actionCode = actionType.substring(actionType
                    .lastIndexOf('-') + 1);

            logger.debug("\nTable Code:" + tableCode + "\nAction Type:"
                    + actionType + "\nAction Code:" + actionCode);

            LIControllermapService service = new LIControllermapService();

            // validate the session before any request unless it's not login or register url or forgot
//            if (hasToCheckSession(tableCode)) {
//                if (!sessionValid(request, response)) {
//                    throw new SessionExpiredException("Your session has Expired");
//                }
//            }
            LIControllermapRecord searchRecord = new LIControllermapRecord();
            searchRecord.setCode(tableCode);
            LIControllermapRecord[] records = service
                    .searchLIControllermapRecords(searchRecord);

            logger.debug("\nClass being called:" + records[0].getClassname());

            Class c = Class.forName(records[0].getClassname());
            Method m = c.getDeclaredMethod("processWebRequest", new Class[]{
                javax.servlet.http.HttpServletRequest.class,
                javax.servlet.http.HttpServletResponse.class,
                java.lang.String.class});

            Object i = c.newInstance();
            Object r = m.invoke(i,
                    new Object[]{request, response, actionCode});
        } catch (SessionExpiredException se) {
            out.println("-2");
            response.setStatus(401);
            return;
        } catch (Exception exception) {

            out.println("Unauthorized Access");
            out.println("<H1><c>LeadICS</H1>");
            logger.error(exception);
        }
    }

    private boolean hasToCheckSession(String tableCode) {
        return !NO_SESSION_ACTIONS.contains(tableCode);
    }

    public boolean sessionValid(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String authHeader = request.getHeader(AUTHORIZATION);
        System.out.println("authheader:" + authHeader);
        LIWebsessionService service = new LIWebsessionService();

        LISystemuserRecord record = this.getUserOfSession(authHeader);
        System.out.println("session");
        System.out.println(record.getUserid());
        System.out.println(authHeader);
        if (authHeader != null) {
            return service.isValidSession(record.getUserid(), authHeader);
        } else {
            return false;
        }
//        return service.isValidSession(record.getUserid(), record.getSessionId());
    }

    private LISystemuserRecord getUserOfSession(String sessionId) throws Exception {
        LIWebsessionRecord sessionRecord;

        LIWebsessionService sessionService = new LIWebsessionService();
        String sql = " select * from web_session where SESSIONID = \"" + sessionId + "\"";
        System.out.println("web sesss:" + sql);
        sessionRecord = sessionService.loadFirstLIWebsessionRecord(sql);
        //System.out.println(new Gson().toJson(sessionRecord));
        sql = "select * from system_user where user_id = \"" + sessionRecord.getUserid() + "\"";
        System.out.println("system user:" + sql);
        LISystemuserRecord userRecord = new LISystemuserService().loadFirstLISystemuserRecord(sql);

        return userRecord;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

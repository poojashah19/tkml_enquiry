package com.leadics.Toyota.common;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import javax.servlet.*;
import javax.servlet.http.*;

import com.leadics.Toyota.beans.LILoggedInUserUtilitiesBean;
import com.leadics.Toyota.service.LIControllermapService;
import com.leadics.Toyota.to.LIControllermapRecord;
import com.leadics.utils.*;

public class LIAction_old extends HttpServlet {

    static LogUtils logger = new LogUtils(LIAction_old.class.getName());

    public LIAction_old() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<H1><c>LeadICS</H1>");
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {
            HttpServletRequest req = request;
            HttpServletResponse res = response;
            String actionType = request.getParameter("hiddenActionType");
            response.setContentType("text/html");

            LILoggedInUserUtilitiesBean utilBean = new LILoggedInUserUtilitiesBean();
            if (!utilBean.isRequestSecure(req)) {
                return;
            }

            String tableCode = actionType.substring(0,
                    actionType.lastIndexOf('-') + 1);
            String actionCode = actionType.substring(actionType
                    .lastIndexOf('-') + 1);

            logger.debug("\nTable Code:" + tableCode + "\nAction Type:"
                    + actionType + "\nAction Code:" + actionCode);

            LIControllermapService service = new LIControllermapService();
            LIControllermapRecord searchRecord = new LIControllermapRecord();
            searchRecord.setCode(tableCode);
            LIControllermapRecord[] records = service
                    .searchLIControllermapRecords(searchRecord);

            logger.debug("\nClass being called:" + records[0].getClassname());

            Class c = Class.forName(records[0].getClassname());
            Method m = c.getDeclaredMethod("processWebRequest", new Class[]{
                javax.servlet.http.HttpServletRequest.class,
                javax.servlet.http.HttpServletResponse.class,
                java.lang.String.class});

            Object i = c.newInstance();
            Object r = m.invoke(i,
                    new Object[]{request, response, actionCode});
        } catch (Exception exception) {
            out.println("Unauthorized Access");
            out.println("<H1><c>LeadICS</H1>");
            logger.error(exception);
        }
    }
}

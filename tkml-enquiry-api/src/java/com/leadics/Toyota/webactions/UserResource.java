/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.webactions;

import com.google.gson.Gson;
import static com.leadics.Toyota.common.LIAction.AUTHORIZATION;
import com.leadics.Toyota.common.LIService;
import com.leadics.Toyota.controller.LISystemuserController;
import com.leadics.Toyota.service.LIConfigdivService;
import com.leadics.Toyota.service.LIConfigfiltersService;
import com.leadics.Toyota.service.LIConfigpagefilterService;
import com.leadics.Toyota.service.LIConfigpagefiltergroupService;
import com.leadics.Toyota.service.LIConfigpagesService;
import com.leadics.Toyota.service.LISystemuserService;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.to.FilterProperties;
import com.leadics.Toyota.to.FilterTemplate;
import com.leadics.Toyota.to.LIConfigdivRecord;
import com.leadics.Toyota.to.LIConfigfiltersRecord;
import com.leadics.Toyota.to.LIConfigpagefilterRecord;
import com.leadics.Toyota.to.LIConfigpagefiltergroupRecord;
import com.leadics.Toyota.to.LIConfigpagesRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.LIWebsessionRecord;
import static com.leadics.Toyota.webactions.SystemuserviewWebactions.logger;
import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author HARSHA
 */
public class UserResource extends LISystemuserController {

    static LogUtils logger = new LogUtils(SystemuserviewWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                super.processWebRequest(request, response, actionType);

                if (!StringUtils.isNullOrEmpty(actionType)) {

                    Method method = this.getClass().getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class);
                    method.invoke(this, request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getPages(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LIService service = new LIService();
       String authHeader = request.getHeader(AUTHORIZATION);

        LISystemuserRecord userRecord = this.getUserOfSession(authHeader);
        String roleId = userRecord.getUserroleid();
        LIConfigpagesRecord[] pages = this.getPages(roleId);

        service.writeOutput(response, pages);

    }

    public void getFilters(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LIService service = new LIService();
       String authHeader = request.getHeader(AUTHORIZATION);
        LISystemuserRecord userRecord = this.getUserOfSession(authHeader);
        List<FilterTemplate> filters = this.getFilters(userRecord.getUserroleid());
        service.writeOutput(response, filters);
    }

    public void getFiltersConfig(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LIService service = new LIService();
       String authHeader = request.getHeader(AUTHORIZATION);
        LISystemuserRecord userRecord = this.getUserOfSession(authHeader);
        LIConfigfiltersRecord[] filtersConfig = this.getFiltersConfig(userRecord.getUserroleid());
        service.writeOutput(response, filtersConfig);
    }

    public void getDivConfig(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LIService service = new LIService();
       String authHeader = request.getHeader(AUTHORIZATION);
        LISystemuserRecord userRecord = this.getUserOfSession(authHeader);
        LIConfigdivRecord[] divConfig = this.getDivConfig(userRecord.getUserroleid());
        service.writeOutput(response, divConfig);

    }

    private LISystemuserRecord getUserOfSession(String authHeader) throws Exception {
        LIWebsessionRecord sessionRecord;

        LIWebsessionService sessionService = new LIWebsessionService();
        String sql = " select * from web_session where SESSIONID = \"" + authHeader + "\"";

        sessionRecord = sessionService.loadFirstLIWebsessionRecord(sql);
        //System.out.println(new Gson().toJson(sessionRecord));
        sql = "select * from system_user where user_id = \"" + sessionRecord.getUserid() + "\"";
        LISystemuserRecord userRecord = new LISystemuserService().loadFirstLISystemuserRecord(sql);

        return userRecord;
    }

    private LIConfigpagesRecord[] getPages(String id) throws Exception {

        String menusQuery = "SELECT A.MODULE_NAME,A.SLUG_NAME,A.TITLE,A.PAGE_NAME,A.ICON,A.SUB,A.ROUTING,A.EXTERNAL_LINK,A.BUDGE,A.BUDGE_COLOR,A.ACTIVE,"
                + "A.GROUP_TITLE FROM CONFIG_PAGES AS A \n"
                + "INNER JOIN \n"
                + "CONFIG_MODULES B ON A.MODULE_NAME = B.MODULE_NAME\n"
                + "INNER JOIN \n"
                + "USER_ROLE C ON B.ROLE_NAME = C.ROLENAME AND C.ID = " + id + ";";

        LIConfigpagesService pagesService = new LIConfigpagesService();
        return pagesService.loadLIConfigpagesRecords(menusQuery);

    }

    // get the filters with respect to page according to the user role id 
    private List<FilterTemplate> getFilters(String id) throws Exception {

        String filtersQuery = "SELECT B.SLUG_NAME as PAGE_NAME,FILTER_NAME,SLUG_FILTER_NAME,FILTER_TYPE,FILTER_PLACEHODLER,FILTER_PRIORITY,\n"
                + "    PARENTS,CHILDS,DEFAULT_INDEX,CONTAINS_ALL,CONTAINS_NONE,CONTAINS_FILTER_GROUP\n"
                + "    FROM CONFIG_PAGE_FILTER AS A \n"
                + "    INNER JOIN CONFIG_PAGES AS B \n"
                + "    ON A.PAGE_NAME = B.SLUG_NAME\n"
                + "    INNER JOIN CONFIG_MODULES AS C \n"
                + "    ON C.MODULE_NAME = B.MODULE_NAME WHERE C.ROLE_NAME = (SELECT ROLENAME FROM USER_ROLE WHERE ID = " + id + "  )";
        LIConfigpagefilterService filtersService = new LIConfigpagefilterService();

        LIConfigpagefilterRecord[] records = filtersService.loadLIConfigpagefilterRecords(filtersQuery);
        LIService service = new LIService();
        List<FilterTemplate> filterTemplate = new LinkedList<>();

        HashMap<String, List<FilterProperties>> tempTemplate = new HashMap<>();
        List<FilterProperties> li;
        if (records != null) {
            for (LIConfigpagefilterRecord record : records) {
                String pageName = record.getPagename();
                String filterName = record.getFiltername();
                String filterType = record.getFiltertype();
                String placeholder = record.getFilterplacehodler();
                double priority = record.getFilterpriority();
                String parents = record.getParents();
                String childs = record.getChilds();
                int defaultIndex = record.getDefaultindex();
                boolean containsAll = record.getContainsall();
                boolean containsNone = record.getContainsnone();
                boolean containsFilterGroup = record.getContainsfiltergroup();

                FilterProperties fp = new FilterProperties();
                fp.setName(filterName);
                fp.setType(filterType);
                fp.setPlaceholder(placeholder);
                fp.setPriority(priority);
                fp.setParents(service.convertCommaBasedToArray(parents));
                fp.setChilds(service.convertCommaBasedToArray(childs));
                fp.setDefaultIndex(defaultIndex);
                fp.setContainsAll(containsAll);
                fp.setContainsNone(containsNone);

                if (containsFilterGroup) {

                    fp.setFilterGroups(this.getFilterGroup(record.getSlugfiltername()));
                }

                li = tempTemplate.get(pageName);
                if (li == null) {
                    li = new LinkedList<>();
                }
                li.add(fp);
                tempTemplate.put(pageName, li);

            }
        }

        Set<String> pageNames = tempTemplate.keySet();

        Iterator setIterator = pageNames.iterator();

        while (setIterator.hasNext()) {
            String pageName = setIterator.next().toString();

            FilterTemplate ft = new FilterTemplate();
            ft.setPageName(pageName);
            ft.setFilterInfo(tempTemplate.get(pageName));
            filterTemplate.add(ft);
        }

        return filterTemplate;
    }

    private List<FilterProperties> getFilterGroup(String slugFilterName) throws Exception {

        String query = "select * from config_page_filter_group where GROUP_FILTER_NAME=\""
                + slugFilterName + "\"";

        LIConfigpagefiltergroupRecord[] records;
        LIConfigpagefiltergroupService service = new LIConfigpagefiltergroupService();
        records = service.loadLIConfigpagefiltergroupRecords(query);

        if (records == null) {
            return new ArrayList<>();
        }
        List<FilterProperties> li = new ArrayList<>();

        for (LIConfigpagefiltergroupRecord record : records) {

            String filterName = record.getFiltername();
            String filterType = record.getFiltertype();
            String placeholder = record.getFilterplacehodler();
            int priority = record.getFilterpriority();
            String parents = record.getParents();
            String childs = record.getChilds();
            int defaultIndex = record.getDefaultindex();
            boolean containsAll = record.getContainsall();
            boolean containsNone = record.getContainsnone();
            String conditionValue = record.getConditionvalue();
            String slugName = record.getSlugName();
            FilterProperties fp = new FilterProperties();
            fp.setName(filterName);
            fp.setType(filterType);
            fp.setPlaceholder(placeholder);
            fp.setPriority(priority);
            fp.setParents(service.convertCommaBasedToArray(parents));
            fp.setChilds(service.convertCommaBasedToArray(childs));
            fp.setDefaultIndex(defaultIndex);
            fp.setContainsAll(containsAll);
            fp.setContainsNone(containsNone);
            fp.setConditionValue(conditionValue);
            fp.setSlugName(slugName);
            li.add(fp);
        }
        return li;
    }

    private LIConfigfiltersRecord[] getFiltersConfig(String id) throws Exception {
        LIConfigfiltersRecord records[];

        LIConfigfiltersService service = new LIConfigfiltersService();

        String query = "SELECT A.* FROM CONFIG_FILTERS A \n"
                + "INNER JOIN CONFIG_MODULES B \n"
                + "ON A.MODULE_NAME = B.MODULE_NAME\n"
                + "INNER JOIN user_role C \n"
                + "ON C.ROLENAME = B.ROLE_NAME AND C.ID = " + id + "  ";

        records = service.loadLIConfigfiltersRecords(query);

        return records;
    }
    // get the div config  according to the user role id 

    private LIConfigdivRecord[] getDivConfig(String id) throws Exception {

        String query = "select * from config_div ;";

        System.out.println(query);
        LIConfigdivService divService = new LIConfigdivService();
        LIService service = new LIService();

        LIConfigdivRecord[] list = divService.loadLIConfigdivRecords(query);
        if (list == null) {
            throw new Exception("configuration for divs are missing");
        }
        for (LIConfigdivRecord div : list) {

            div.filterList = service.convertCommaBasedToArray(div.getFilters());
        }

        return list;
    }
}

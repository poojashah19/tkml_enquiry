/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.webactions;

import com.leadics.Toyota.to.LIUserroleRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.service.SendSMSService;
import com.leadics.Toyota.service.LIUserroleService;
import com.google.gson.Gson;
import static com.leadics.Toyota.common.LIAction.AUTHORIZATION;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.common.LIService;
import com.leadics.Toyota.controller.LISystemuserController;
import com.leadics.Toyota.dao.LIReportquiresDAO;
import com.leadics.Toyota.service.LIAuditlogService;
import com.leadics.Toyota.service.LIConfigpagefilterService;
import com.leadics.Toyota.service.LIConfigpagesService;
import com.leadics.Toyota.service.LIFailedloginService;
import com.leadics.Toyota.service.LIMaillogService;
import com.leadics.Toyota.service.LIOtplogService;
import com.leadics.Toyota.service.LISystemuserService;
import com.leadics.Toyota.service.LIWebaccesslogService;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.service.SendMailService;
import com.leadics.Toyota.to.LIAuditlogRecord;
import com.leadics.Toyota.to.LIFailedloginRecord;
import com.leadics.Toyota.to.LIMaillogRecord;
import com.leadics.Toyota.to.LIOtplogRecord;
import com.leadics.Toyota.to.LIReportquiresRecord;
import com.leadics.Toyota.to.LIWebaccesslogRecord;
import com.leadics.Toyota.to.LIWebsessionRecord;
import com.leadics.representations.EventLoggerRepresentation;
import com.leadics.representations.LoginRepresentation;
import com.leadics.utils.LISecurityUtil;
import com.leadics.utils.LIStatusConstants;
import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.InetAddress;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.PropertyUtils;

/**
 *
 * @author varma.sagi
 */
public class SystemuserviewWebactions extends LISystemuserController {

    static LogUtils logger = new LogUtils(SystemuserviewWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                super.processWebRequest(request, response, actionType);

                if (!StringUtils.isNullOrEmpty(actionType)) {

                    Method method = this.getClass().getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class);
                    method.invoke(this, request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
                response.setStatus(500);
                out.println("-1");
                return;
            }
        }

    }

    public void authenticate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ResourceBundle rb = ResourceBundle.getBundle("Toyota", Locale.getDefault());
        String authenticateByOtp = rb.getString("AuthenticationByOTP");
        boolean otpLogin;
        if (authenticateByOtp == null) {
            otpLogin = false;
        }
        otpLogin = authenticateByOtp.equalsIgnoreCase("y");

        PrintWriter out = response.getWriter();
        LIService service = new LIService();
        Map<String, Object> jsonOutput = new HashMap<>();
        // check if user is already blocked or not 
        // dont send otp if user is already blocked
        if (isBlocked(request, response)) {
            jsonOutput.put("status", LIStatusConstants.SUSPENDED.getValue());
            service.writeOutput(response, jsonOutput);
            return;
        }

        String parsable = service.getPostBody(request);

        LoginRepresentation rps = new Gson().fromJson(parsable, LoginRepresentation.class);

        String userName = rps.getUser_id();
        String password = rps.getUser_password();
        System.out.println("cred");
        System.out.println(userName);
        System.out.println(password);
        LISystemuserController objLISystemuserController = new LISystemuserController();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

        LISystemuserRecord userRecord = this.getUserRecord(userName, password);

        if (userRecord != null) {
            LIWebaccesslogRecord record = new LIWebaccesslogRecord();
            record.setUserid(userRecord.getUserid());
            record.setSource(request.getRemoteAddr());
            record.setCreatedat(sdf.format(new Date()));
            record.setService("login");

            int i = new LIWebaccesslogService().insertLIWebaccesslogRecord(record);

            String userRoleId = userRecord.getUserroleid();

            if (userRoleId == null) {
                throw new Exception("cant find userroleid");
            }
            LIUserroleService userRoleService = new LIUserroleService();
            LIUserroleRecord userRoleRecord = userRoleService.loadLIUserroleRecord(userRoleId);
            //String defaultURL = userRoleRecord.getDefaulturl();
            System.out.println("default rolename");

            String query = "select DEFAULT_URL FROM `user_role` where ROLENAME = \"" + userRoleRecord.getRolename() + "\"";
            LIDAO dao = new LIDAO();
            String defaultURL = dao.loadString(query);
            System.out.println("deafult url");
            System.out.println(defaultURL);
            String Rolename = userRoleRecord.getRolename();

            String entityName = userName;
            String message;
            String actionType = "login";
            String sessionId = "N.A";
            if (otpLogin) {
                message = "loginByPasswordAndOTP";
                this.sendOtp(userRecord);
                jsonOutput.put("status", LIStatusConstants.VALIDATE_OTP.getValue());
            } else {
                sessionId = createSession(userName);
                userRecord.setSessionId(sessionId);

                message = "loginByPassword";
                jsonOutput.put("status", LIStatusConstants.LIVE.getValue());
                jsonOutput.put("user", userRecord);
                jsonOutput.put("defaultRedirectURL", defaultURL);

                //System.out.println(userRoleRecord.getDefaulturl());
            }
            service.eventLogger(entityName, message, actionType, sessionId);
            service.writeOutput(response, jsonOutput);
            System.out.println(jsonOutput);

        } else {

            LIFailedloginRecord record = new LIFailedloginRecord();
            record.setUsername(request.getParameter("user_id"));
            record.setSource(request.getRemoteAddr());
            record.setCreatedat(sdf.format(new Date()));
            int i = new LIFailedloginService().insertLIFailedloginRecord(record);
            // check if user need to be blocked 
            boolean validUser = validateUserLIStatusConstants(record);

            if (!validUser) {
                jsonOutput.put("status", LIStatusConstants.SUSPENDED.getValue());
            } else {
                jsonOutput.put("status", LIStatusConstants.UNKNOWN.getValue());
            }
            service.writeOutput(response, jsonOutput);

        }

    }

    private String createSession(String userName) throws Exception {
        LIWebsessionService liWebsessionService = new LIWebsessionService();
        LIWebsessionRecord record = new LIWebsessionRecord();
        record.setUserid(userName);
        String uniqueID = "" + (UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE);
        record.setSessionid(uniqueID);
        record.setSource("Web");
        record.setRstatus("1");;
        int result = liWebsessionService.insertLIWebsessionRecord(record);
        if (result < 0) {
            throw new Exception("Cant create the session");
        }

        return uniqueID;
    }

    public void logOut(HttpServletRequest request, HttpServletResponse response) throws Exception {

        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        LISystemuserRecord userRecord = gson.fromJson(request.getParameter("currentUser"), LISystemuserRecord.class);
        LIWebsessionService service = new LIWebsessionService();

        service.closeSession(userRecord);

        String entityName = "N.A.";
        String message = "logout";
        String actionType = "logout";
        String sourceId = userRecord.getSessionId();
        service.eventLogger(entityName, message, actionType, sourceId);

    }

    public void eventLog(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String authHeader = request.getHeader(AUTHORIZATION);

        EventLoggerRepresentation representation;
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            String parsable = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

            representation = new Gson().fromJson(parsable, EventLoggerRepresentation.class);
            LIService service = new LIService();
            service.eventLogger(representation, authHeader);

            service.writeOutput(response, new Object());
        }

    }

    private void sendOtp(LISystemuserRecord record) throws Exception {

        LISecurityUtil util = new LISecurityUtil();
        String otp = util.generateOTP();

        this.sendOTPEmail(record, otp);
//        this.sendOTPMobile(record, otp);

    }

    private void sendOTPMobile(LISystemuserRecord record, String otp) throws Exception {

        SendSMSService smsService = new SendSMSService();
        HashMap<String, String> map = smsService.sendSMS(otp, record.getMobile());
        String response = map.get("responseMessage");
        String requestURL = map.get("serviceURL");

        LIOtplogRecord otpRecord = new LIOtplogRecord();
        otpRecord.setOtp(otp);
        otpRecord.setUserid(record.getUserid());
        otpRecord.setUsermobile(record.getMobile());
        otpRecord.setUseremail(record.getEmail());
        otpRecord.setRequest(requestURL);
        otpRecord.setResponse(response);

        LIOtplogService otpService = new LIOtplogService();
        otpService.insertLIOtplogRecord(otpRecord);

    }

    private void sendOTPEmail(LISystemuserRecord record, String otp) throws Exception {
        LIMaillogRecord mailLogRecord = new LIMaillogRecord();
        LIMaillogService mailLogService = new LIMaillogService();
        String body = "Your OTP : " + otp;
        String email = record.getEmail();
        HashMap inputMap = new HashMap();

        inputMap.put("MailMessage", body);

        inputMap.put("ToAddress", email);
        inputMap.put("Subject", "Leadics OTP :  ");

        SendMailService mailService = new SendMailService();
        HashMap map = mailService.sendMail(inputMap);
        String response = map.get("message").toString();

        mailLogRecord.setToids(email);
        mailLogRecord.setMailmessage(body);
        mailLogRecord.setRespoinsemessage(response);
        mailLogService.insertLIMaillogRecord(mailLogRecord);

        LIOtplogRecord otpRecord = new LIOtplogRecord();
        otpRecord.setOtp(otp);
        otpRecord.setUserid(record.getUserid());
        otpRecord.setUsermobile(record.getMobile());
        otpRecord.setUseremail(record.getEmail());
        otpRecord.setRequest("through mail");
        otpRecord.setResponse(response);

        LIOtplogService otpService = new LIOtplogService();
        otpService.insertLIOtplogRecord(otpRecord);

    }

    public void resendOTP(HttpServletRequest request, HttpServletResponse response) throws Exception {
        this.authenticate(request, response);

    }

    public void validateOTP(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String timeout = "600";
        PrintWriter out = response.getWriter();
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        LISystemuserService userService = new LISystemuserService();
        LIOtplogService otpService = new LIOtplogService();
        String userId = request.getParameter("user_id");
        String otp = request.getParameter("otp");

        String otpQuery = "select * from otp_log where user_id =\"" + userId + "\" and otp =\"" + otp + "\" "
                + " and TIMESTAMPDIFF(second,created_on, now()) <=" + timeout + ";";

        LIOtplogRecord otpRecord = otpService.loadFirstLIOtplogRecord(otpQuery);

        String userQuery = "select B.DEFAULT_URL,A.* from system_user A "
                + "INNER JOIN USER_ROLE B ON A.USER_ROLE_ID = B.ID "
                + "where user_id =\"" + userId + "\""
                + ";";

        LISystemuserRecord objLISystemuserRecord = userService.loadFirstLISystemuserRecord(userQuery);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        Map<String, Object> jsonOutput = new HashMap<>();

        if (otpRecord != null) {
            LIWebaccesslogRecord record = new LIWebaccesslogRecord();
            record.setUserid(objLISystemuserRecord.getUserid());
            record.setSource(request.getRemoteAddr());
            record.setCreatedat(sdf.format(new Date()));
            record.setService("login");
            String sessionId = createSession(userId);
            objLISystemuserRecord.setSessionId(sessionId);

            String userRoleId = objLISystemuserRecord.getUserroleid();

            int i = new LIWebaccesslogService().insertLIWebaccesslogRecord(record);

            String entityName = userId;
            String message = "loginByPasswordAndOTP";
            String actionType = "login";
            String sourceId = sessionId;
            service.eventLogger(entityName, message, actionType, sourceId);

            jsonOutput.put("status", LIStatusConstants.LIVE.getValue());
            jsonOutput.put("user", objLISystemuserRecord);

            service.writeOutput(response, jsonOutput);
        } else {
            jsonOutput.put("status", LIStatusConstants.INVALID_OTP.getValue());

            service.writeOutput(response, jsonOutput);

        }

    }

    private boolean validateUserLIStatusConstants(LIFailedloginRecord record) throws Exception {

        ResourceBundle rb = ResourceBundle.getBundle("Toyota", Locale.getDefault());
        String timestampDiff = rb.getString("failedLoginTimeDiffInSeconds");
        String maxAttempts = rb.getString("maxallowedincorrectlogin");
        // rstatus null means, those logs werent considered for blocking the user.
        String query = " select count(*) from failed_login where username=\"" + record.getUsername() + "\""
                + "and rstatus is null and timestampdiff(second, now(),created_at) <=" + timestampDiff + ";";
        LIDAO dao = new LIDAO();
        int count = dao.loadCount(query);
        if (count >= Integer.parseInt(maxAttempts)) {

            blockUser(record);
            return false;
        }
        // user is not blocked or unknown
        return true;
    }

    private void blockUser(LIFailedloginRecord record) throws Exception {

        LIDAO dao = new LIDAO();
        String query = "update `system_user` set RSTATUS =\"0\" where USER_ID =\"" + record.getUsername() + "\";";
        int result = dao.executeUpdateQuery(query);
        // we should not block the user again based on the old failed login records hence set rstatus to 0
        query = "update `failed_login` set RSTATUS =\"0\" where username =\"" + record.getUsername() + "\" "
                + "         and rstatus is null ;";
        result = dao.executeUpdateQuery(query);
    }

    private boolean isBlocked(HttpServletRequest request, HttpServletResponse response) throws Exception {
        boolean flag = false;
        String userID = request.getParameter("user_id");
        String query = "select RSTATUS FROM `system_user` where user_id = \"" + userID + "\"";
        LIDAO dao = new LIDAO();
        String result = dao.loadString(query);
        if (result == null) {
            return false;
        }
        return result.contentEquals("0");
    }

    private LISystemuserRecord getUserRecord(String username, String password) throws Exception {

        LISystemuserRecord objLISystemuserRecord;
        LISystemuserService objLISystemuserService = new LISystemuserService();

        String query = "select B.DEFAULT_url, A.* from system_user A "
                + "INNER JOIN "
                + "USER_ROLE B ON A.USER_ROLE_ID = B.ID  "
                + "WHERE USER_ID=\"" + username + "\" and  USER_PASSWORD =\"" + password + "\" ; ";
        objLISystemuserRecord = objLISystemuserService.loadFirstLISystemuserRecord(query);

        return objLISystemuserRecord;
    }
}

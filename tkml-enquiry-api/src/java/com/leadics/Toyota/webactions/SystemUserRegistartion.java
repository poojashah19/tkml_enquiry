/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.webactions;

import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.common.LIService;
import com.leadics.Toyota.controller.LISystemuserController;
import com.leadics.Toyota.dao.LIReportquiresDAO;
import com.leadics.Toyota.service.LIAuditlogService;
import com.leadics.Toyota.service.LIFailedloginService;
import com.leadics.Toyota.service.LIForgotpwdrequestService;
import com.leadics.Toyota.service.LIMaillogService;
import com.leadics.Toyota.service.LIOtplogService;
import com.leadics.Toyota.service.LISystemuserService;
import com.leadics.Toyota.service.LIUserverifyService;
import com.leadics.Toyota.service.LIWebaccesslogService;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.service.SendMailService;
import com.leadics.Toyota.service.SendSMSService;
import com.leadics.Toyota.to.LIAuditlogRecord;
import com.leadics.Toyota.to.LIFailedloginRecord;
import com.leadics.Toyota.to.LIForgotpwdrequestRecord;
import com.leadics.Toyota.to.LIMaillogRecord;
import com.leadics.Toyota.to.LIOtplogRecord;
import com.leadics.Toyota.to.LIReportquiresRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.LIUserverifyRecord;
import com.leadics.Toyota.to.LIWebaccesslogRecord;
import com.leadics.Toyota.to.LIWebsessionRecord;
import static com.leadics.Toyota.webactions.SystemForgotPassword.FORGOT_URL;
import com.leadics.utils.LISecurityUtil;
import com.leadics.utils.LIStatusConstants;
import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.InetAddress;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.PropertyUtils;

/**
 *
 * @author varma.sagi
 */
public class SystemUserRegistartion extends LISystemuserController {

    static LogUtils logger = new LogUtils(SystemUserRegistartion.class.getName());
    public static final String VERIFICATION_URL = "http://localhost:4200/#/leadics/verify/";

    public static enum SystemUser {
        EMAILPENDING("EMAILPENDING"),
        ADMINPENDING("ADMINPENDING"),
        LIVE("LIVE"),
        BLOCKED("BLOCKED");

        private String value;

        SystemUser(String value) {
            this.value = value;

        }

        public String getValue() {
            return this.value;
        }
    }

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                super.processWebRequest(request, response, actionType);

                if (!StringUtils.isNullOrEmpty(actionType)) {

                    Method method = this.getClass().getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class);
                    method.invoke(this, request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void isUserNameAvailable(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String user_id = request.getParameter("user_id");
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = "select count(id) from system_user where user_id = \"" + user_id + "\"";
        String countString = dao.loadString(query);
        int count = Integer.parseInt(countString);
        HashMap jsonOutput = new HashMap();
        if (count > 0) {
            jsonOutput.put("status", LIStatusConstants.INVALID.getValue());
        } else {
            jsonOutput.put("status", LIStatusConstants.VALID.getValue());
        }

        service.writeOutput(response, jsonOutput);
    }

    public void isEmailRegistered(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String email = request.getParameter("email");
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = "select count(id) from system_user where email = \"" + email + "\"";
        String countString = dao.loadString(query);
        int count = Integer.parseInt(countString);
        HashMap jsonOutput = new HashMap();
        //System.out.println(count);
        if (count > 0) {
            jsonOutput.put("status", LIStatusConstants.INVALID.getValue());
        } else {
            jsonOutput.put("status", LIStatusConstants.VALID.getValue());
        }

        service.writeOutput(response, jsonOutput);

    }

    public void register(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ResourceBundle rb = ResourceBundle.getBundle("Toyota", Locale.getDefault());
        String adminUserVerfication = rb.getString("adminUserVerification");
        String adminEmail = rb.getString("adminEmail");

        HashMap jsonOutput = new HashMap();
        LIService service = new LIService();
        Gson gson = new Gson();
        LISystemuserService userService = new LISystemuserService();
        String userDetails = request.getParameter("userDetails");
        LISystemuserRecord userRecord = gson.fromJson(userDetails, LISystemuserRecord.class);
        userRecord.setCurrappstatus(SystemUser.EMAILPENDING.getValue());
        int status = userService.insertLISystemuserRecord(userRecord);
        //System.out.println("status is " + status);
        if (status > 0) {
            LIUserverifyRecord verifyRecord = new LIUserverifyRecord();
            LIUserverifyService verifyService = new LIUserverifyService();
            String unique = service.getUniqueString();
            String timeStamp = service.getCurrentTimeStamp();

            verifyRecord.setUserid(userRecord.getUserid());
            verifyRecord.setVerificationcode(unique);
            verifyRecord.setCreatedat(timeStamp);
            verifyRecord.setModifiedat(timeStamp);
            verifyService.insertLIUserverifyRecord(verifyRecord);
            this.sendVerificationMail(userRecord.getUserid(), userRecord.getEmail(), unique);

            jsonOutput.put("status", LIStatusConstants.SUCCESS.getValue());
        } else {

            jsonOutput.put("status", LIStatusConstants.FAIL.getValue());
        }

        service.writeOutput(response, jsonOutput);
    }

    private void sendVerificationMail(String user, String email, String unique) throws Exception {
        LIMaillogRecord mailLogRecord = new LIMaillogRecord();
        LIMaillogService mailLogService = new LIMaillogService();
        String body = "Please click the following link to acctivate your email<br>"
                + VERIFICATION_URL + user + "/" + unique;

        HashMap inputMap = new HashMap();

        inputMap.put("MailMessage", body);

        inputMap.put("ToAddress", email);
        inputMap.put("Subject", "Leadics Email Verification :  ");

        SendMailService mailService = new SendMailService();
        HashMap map = mailService.sendMail(inputMap);
        String response = map.get("message").toString();

        mailLogRecord.setToids(email);
        mailLogRecord.setMailmessage(body);
        mailLogRecord.setRespoinsemessage(response);
        mailLogService.insertLIMaillogRecord(mailLogRecord);
    }

    public void verifyEmail(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        HashMap jsonOutput = new HashMap();
        String user_id = request.getParameter("user_id");
        String verification_code = request.getParameter("token");
        String query = "select id from user_verify where user_id = \"" + user_id + "\" "
                + "and verification_code = \"" + verification_code + "\" ;";
        String id = dao.loadString(query);
        //System.out.println(id);
        if (id == null) {
            jsonOutput.put("status", LIStatusConstants.INVALID.getValue());
            service.writeOutput(response, jsonOutput);
            return;
        }

        query = " update system_user set CURR_APP_STATUS = \"" + SystemUser.LIVE + "\" where user_id = \"" + user_id + "\"";
        int status = dao.executeUpdateQuery(query);

        if (status > 0) {
            jsonOutput.put("status", LIStatusConstants.VALID.getValue());
        } else {
            jsonOutput.put("status", LIStatusConstants.ERROR.getValue());
        }
        service.writeOutput(response, jsonOutput);
    }

    public void verifyEmailByAdmin(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        HashMap jsonOutput = new HashMap();
        String user_id = request.getParameter("user_id");

        String query;

        query = " update system_user set CURR_APP_STATUS = \"" + SystemUser.LIVE + "\" where user_id = \"" + user_id + "\"";
        int status = dao.executeUpdateQuery(query);

        if (status > 0) {
            jsonOutput.put("status", LIStatusConstants.VALID.getValue());
        } else {
            jsonOutput.put("status", LIStatusConstants.ERROR.getValue());
        }
        service.writeOutput(response, jsonOutput);
    }
}

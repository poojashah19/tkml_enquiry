/*########### GENERATED AT 2018-05-24 10:29:31.441###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.google.gson.Gson;
import static com.leadics.Toyota.common.LIAction.AUTHORIZATION;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;

import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;

/**
 *
 * @author HARSHA
 */
public class OemKPIPerformance {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {

                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/
    public void getKPIPerformance(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiKPIPerformance-getKPIPerformance");
        // get all the filter groups and their fields
        String strDealer = request.getParameter("dealer");
        String authHeader = request.getHeader(AUTHORIZATION);
        LISystemuserRecord userRecord = service.getUserOfSession(authHeader);
        if (userRecord.getUserroleid().equalsIgnoreCase(PropertyUtil.getProperty("dealerRole"))) {
            strDealer = userRecord.getFirstname();
        }
        String strRegion = request.getParameter("region");
        String strZone = request.getParameter("zone");
        String strArea = request.getParameter("area");
        String dealergroup = request.getParameter("dealergroup");
        System.out.println("dealaergroup----------" + dealergroup);
        String regionName = "";
        String zoneName = "";
        String mainSelection = "Study Total";
        String strCompareTo = request.getParameter("kpi-benchmark");
        System.out.println("strCompareTo=-" + strCompareTo);
        if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            query = StringUtils.replaceString(query, "!region!", "region like \"" + strRegion + "\"", true);
            regionName = strRegion;
            mainSelection = strRegion;
            zoneName = dao.loadString("SELECT DISTINCT zone FROM kpi_aggregate WHERE region like \"" + strRegion + "\" limit 1");
        } else {
            query = StringUtils.replaceString(query, "!region!", " 1=1 ", true);
        }
        if (strArea != null && !strArea.equalsIgnoreCase("ALL") && !strArea.equalsIgnoreCase("Study Total")) {
            query = StringUtils.replaceString(query, "!area!", "area like \"" + strArea + "\"", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM kpi_aggregate WHERE area like '" + strArea + "' limit 1");
            mainSelection = strArea;
            zoneName = dao.loadString("SELECT DISTINCT zone FROM kpi_aggregate WHERE area like \"" + strArea + "\" limit 1");
        } else {
            query = StringUtils.replaceString(query, "!area!", " 1=1 ", true);
        }
        if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            query = StringUtils.replaceString(query, "!zone!", "zone like \"" + strZone + "\"", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM kpi_aggregate WHERE zone like '" + strZone + "' limit 1");
            zoneName = strZone;
            mainSelection = strZone;
        } else {
            query = StringUtils.replaceString(query, "!zone!", " 1=1 ", true);
        }
        if (dealergroup != null && !dealergroup.equalsIgnoreCase("ALL") && !dealergroup.equalsIgnoreCase("Study Total")) {
            query = StringUtils.replaceString(query, "!dealergroup!", "dealergroup like '" + dealergroup + "'", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM kpi_aggregate WHERE dealergroup like '" + dealergroup + "' limit 1");
            zoneName = dao.loadString("SELECT DISTINCT zone FROM kpi_aggregate WHERE dealergroup like \"" + dealergroup + "\" limit 1");
            mainSelection = dealergroup;
        } else {
            query = StringUtils.replaceString(query, "!dealergroup!", " 1=1 ", true);
        }
        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
            query = StringUtils.replaceString(query, "!dealer!", "dealer like '" + strDealer + "'", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM kpi_aggregate WHERE dealer like '" + strDealer + "' limit 1");
            zoneName = dao.loadString("SELECT DISTINCT zone FROM kpi_aggregate WHERE dealer like \"" + strDealer + "\" limit 1");
            mainSelection = strDealer;
        } else {
            query = StringUtils.replaceString(query, "!dealer!", " 1=1 ", true);
        }
        String average = " Average";
        String best = "Best in ";
        String avgString = "Study Total Average";
        String BestString = "Best Overall";

        if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            avgString = zoneName + average;
            BestString = best + zoneName;
            query = StringUtils.replaceString(query, "!regiondealer!", "zone like \"" + strZone + "\"", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            avgString = zoneName + average;
            BestString = best + zoneName;
            query = StringUtils.replaceString(query, "!regiondealer!", " zone like (SELECT DISTINCT zone FROM kpi_aggregate WHERE region like \"" + strRegion + "\" limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strArea != null && !strArea.equalsIgnoreCase("ALL") && !strArea.equalsIgnoreCase("Study Total")) {
            avgString = zoneName + average;
            BestString = best + zoneName;
            query = StringUtils.replaceString(query, "!regiondealer!", " zone like (SELECT DISTINCT zone FROM kpi_aggregate WHERE area like \"" + strArea + "\" limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && dealergroup != null && !dealergroup.equalsIgnoreCase("ALL") && !dealergroup.equalsIgnoreCase("Study Total")) {
            avgString = zoneName + average;
            BestString = best + zoneName;
            query = StringUtils.replaceString(query, "!regiondealer!", " zone like (SELECT DISTINCT zone FROM kpi_aggregate WHERE dealergroup like '" + dealergroup + "' limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
            avgString = zoneName + average;
            BestString = best + zoneName;
            query = StringUtils.replaceString(query, "!regiondealer!", " zone like (SELECT DISTINCT zone FROM kpi_aggregate WHERE dealer like '" + strDealer + "' limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            query = StringUtils.replaceString(query, "!regiondealer!", "region like(select distinct region from kpi_aggregate where zone like '" + strZone + "'limit 1)", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            query = StringUtils.replaceString(query, "!regiondealer!", "region like \"" + strRegion + "\"", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strArea != null && !strArea.equalsIgnoreCase("ALL") && !strArea.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            query = StringUtils.replaceString(query, "!regiondealer!", "region like(select distinct region from kpi_aggregate where area like '" + strArea + "'limit 1)", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && dealergroup != null && !dealergroup.equalsIgnoreCase("ALL") && !dealergroup.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            query = StringUtils.replaceString(query, "!regiondealer!", " region like (SELECT DISTINCT region FROM kpi_aggregate WHERE dealergroup like '" + dealergroup + "' limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            query = StringUtils.replaceString(query, "!regiondealer!", " region like (SELECT DISTINCT region FROM kpi_aggregate WHERE dealer like '" + strDealer + "' limit 1) ", true);
        } else {
            query = StringUtils.replaceString(query, "!regiondealer!", " 1=1 ", true);
        }

        //------------------------
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("KPI perforamcnae");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("attribute", "attribute");
        columnInfo.put("dealerscore", "dealerscore");
        columnInfo.put("regionavg", "regionavg");
        columnInfo.put("regionbest", "regionbest");
        columnInfo.put("firstseries", "firstseries");
        columnInfo.put("Impact", "Impact");
        LinkedList legendArray = new LinkedList<>();
        legendArray.add(mainSelection);
        legendArray.add(avgString);
        legendArray.add(BestString);
        Map<String, Collection> rows = dao.loadValues(query, columnInfo);
        rows.put("legends", legendArray);
        Map output = new HashMap();
        output.put("data", rows);
        service.writeOutput(response, output);
    }
    /*########### GENERATED ###########*/

}

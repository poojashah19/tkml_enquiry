/*########### GENERATED AT 2018-05-24 10:29:31.417###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.google.gson.Gson;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_FACTOR;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;

import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;

/**
 *
 * @author HARSHA
 */
public class OemSummary {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {

                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/
    public void getTopBottomDealerRanking(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiSummary-getTopBottomDealerRanking");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);

        System.out.println("top bottom dealer ranking ");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("Region", "Region");
        columnInfo.put("Dealer", "Dealer");
        columnInfo.put("score", "score");
        columnInfo.put("top_or_bottom", "top_or_bottom");

        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
        service.writeOutput(response, output);

    }

    public void getTopBottomAttributes(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiSummary-getTopBottomAttributes");

        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("top and bottom");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("Attribute", "attribute");
        columnInfo.put("score", "score");
        columnInfo.put("top_or_bottom", "top_or_bottom");

        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);

        service.writeOutput(response, output);

    }

    public void getRegionalCSI(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiSummary-getRegionalCSI");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("regional query");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("region", "region");
        columnInfo.put("csi_score", "value");
        columnInfo.put("count", "repondents");

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Map output = new HashMap();
        output.put("data", rows);

        service.writeOutput(response, output);

    }

    public void getFactorScores(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiSummary-getFactorScores");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        String bestString = "Best Overall";
        String worstString = "Worst Overall";
        bestString = service.getBestString(request);
        worstString = service.getWorstString(request);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("factor scores");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("factor", "factor");
        columnInfo.put("score", "score");
        columnInfo.put("minscore", "minscore");
        columnInfo.put("maxscore", "maxscore");
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();

        output.put("data", cols);
        output.put("best", bestString);
        output.put("worst", worstString);

        service.writeOutput(response, output);

    }

    public void getTopBottomKPIS(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiSummary-getTopBottomKPIS");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("kpi top 5 query");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("Attribute", "attribute");
        columnInfo.put("score", "score");
        columnInfo.put("top_or_bottom", "top_or_bottom");

        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();

        //added by krishna to iterate and adda percentage to the scores
        Map<String, Collection> finalMap = new HashMap<String, Collection>();
        for (Map.Entry<String, Collection> entry : cols.entrySet()) {
            LinkedList scoreList = new LinkedList<>();
            LinkedList scoreListFinal = new LinkedList<>();
            if (entry.getKey().equalsIgnoreCase("score")) {
                scoreList = (LinkedList) entry.getValue();
                for (Object str : scoreList) {
                    scoreListFinal.add(str + "%");
                }
                finalMap.put("bartitle", scoreListFinal);
                finalMap.put(entry.getKey(), entry.getValue());
            } else {

                finalMap.put(entry.getKey(), entry.getValue());
            }

        }

        output.put("data", finalMap);
        service.writeOutput(response, output);

    }

    public void getCSINPSScore(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiSummary-getCSINPSScore");
        System.out.println("-------------------------------------------");
        System.out.println("National Score Sumamry :: " + query);
        System.out.println("-------------------------------------------");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);

        System.out.println("csi nps query");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("wave", "wave");
        columnInfo.put("biannual", "biannual");
        columnInfo.put("score", "score");
        columnInfo.put("samplecount", "samplecount");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Map output = new HashMap();
        output.put("data", rows);

        service.writeOutput(response, output);
    }
    /*########### GENERATED ###########*/

}

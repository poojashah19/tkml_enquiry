/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.webactions.oem;

import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.utils.LogUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Krishna-Kanth;
 */
public class OemSample {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {

                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getSampleData(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-oemSample-getSampleData");
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);

        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("sample disposition");
        System.out.println(query);
        String region = "region";
        String samplecount = "samplecount";
        String dealcount = "dealcount";
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put(region, region);
        columnInfo.put(samplecount, samplecount);
        columnInfo.put(dealcount, dealcount);

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Iterator i = rows.iterator();
        Map<String, String> finalMap = new HashMap<String, String>();
        ArrayList TotalList = new ArrayList();
        while (i.hasNext()) {
            HashMap<String, String> insideMap = new HashMap();
            HashMap<String, String> row = (HashMap<String, String>) i.next();
            String getRegion = row.get("region");
            String getdealCount = row.get("dealcount");
            String getsamplecount = row.get("samplecount");
            if (getRegion.equalsIgnoreCase("Study Total")) {
                insideMap.put("region", getRegion + "(" + getdealCount + " Dealers)");
                insideMap.put("dealcount", getsamplecount);
                insideMap.put("color", "#607D8B");
                TotalList.add(insideMap);
            } else {
                String query1 = service.getQuery("-oemSample-getSampleDatadealers");
                String dealer = "dealer";
                String getSampleDealer = "samplecount";
                HashMap<String, String> columnInfos = new HashMap<>();
                columnInfos.put(dealer, dealer);
                columnInfos.put(getSampleDealer, getSampleDealer);
                query1=query1.replaceAll("!region!", " AND region='" + getRegion + "' ");
                Map<String, LIWhereconditionmapRecord> filtersMaps = service.getFiltersMap(query1);
                Map<String, String> acknowledgmentMaps = service.buildWherecondition(request, filtersMaps, query1);

                // query replacement with the new query 
                query1 = acknowledgmentMaps.get(QUERY_TEMPLATE);
                System.out.println("sample dispositioninner queryf");
                System.out.println(query1);
                List<HashMap<String, String>> rows1 = dao.loadValuesRowWise(query1, columnInfos);

                insideMap.put("region", getRegion + "(" + getdealCount + " Dealers)");
                insideMap.put("dealcount", getsamplecount);
                insideMap.put("color", "#607D8B");
                TotalList.add(insideMap);
                Iterator j = rows1.iterator();
                while (j.hasNext()) {
                    HashMap<String, String> rowmap = new HashMap();
                    HashMap<String, String> rowsInner = (HashMap<String, String>) j.next();
                    String dealerInner = rowsInner.get("dealer");
                    String samplecountInner = rowsInner.get("samplecount");

                    rowmap.put("region", dealerInner);
                    rowmap.put("dealcount", samplecountInner);
                    rowmap.put("color", "");
                    TotalList.add(rowmap);
                }

            }

        }

        System.out.println(rows);
        Map output = new HashMap();

        output.put("data", TotalList);
        service.writeOutput(response, output);
    }

}

/*########### GENERATED AT 2018-05-24 10:29:31.380###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.google.gson.Gson;
import com.leadics.Toyota.common.Constants;
import static com.leadics.Toyota.common.LIAction.AUTHORIZATION;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;

import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;
import java.util.Set;
import org.json.simple.JSONArray;

/**
 *
 * @author HARSHA
 */
public class OemSopTrend {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {

                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/
    public void getTrend(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiSopTrend-getTrend");
        String timeLine = request.getParameter("timeline");
        //added by kavya for timeline filter
        if (timeLine.equalsIgnoreCase("monthly")) {
            query = service.getQuery("-CsiSopTrend-getTrendmonthly");
        }  else if (timeLine.equalsIgnoreCase("Biannually")) {
            query = service.getQuery("-CsiSopTrend-getTrendBiannually");
        }
        String strRegion = request.getParameter("region");
        String strDealer = request.getParameter("dealer");
        String strDealergroup = request.getParameter("dealergroup");
        String strArea=request.getParameter("area");
        String authHeader = request.getHeader(AUTHORIZATION);
        LISystemuserRecord userRecord = service.getUserOfSession(authHeader);
        if (userRecord.getUserroleid().equalsIgnoreCase(PropertyUtil.getProperty("dealerRole"))) {
            strDealer = userRecord.getFirstname();
        }
        String strZone = request.getParameter("zone");
        String regionName = "";
        String zoneName = "";
        String mainSelection = "Study Total";
        String strCompareTo = request.getParameter("kpi-benchmark");
        String attibutename = request.getParameter("kpi-trend");
        query = StringUtils.replaceString(query, "!attribute!", "and attributename like \"" + attibutename + "\"", true);

//        if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
//            query = StringUtils.replaceString(query, "!region!", "region like \"" + strRegion + "\"", true);
//            regionName = strRegion;
//            mainSelection = strRegion;
//            zoneName = dao.loadString("SELECT DISTINCT zone FROM kpi_aggregate WHERE region like \"" + strRegion + "\" limit 1");
//        } else {
//            query = StringUtils.replaceString(query, "!region!", " 1=1 ", true);
//        }
//        if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
//            query = StringUtils.replaceString(query, "!zone!", "zone like \"" + strZone + "\"", true);
//            zoneName = strZone;
//            mainSelection = strZone;
//        } else {
//            query = StringUtils.replaceString(query, "!zone!", " 1=1 ", true);
//        }
//         if (strDealergroup != null && !strDealergroup.equalsIgnoreCase("ALL") && !strDealergroup.equalsIgnoreCase("Study Total")) {
//            query = StringUtils.replaceString(query, "!dealergroup!", "dealergroup like '" + strDealergroup + "'", true);
//            regionName = dao.loadString("SELECT DISTINCT region FROM kpi_aggregate WHERE dealergroup like '" + strDealergroup + "' limit 1");
//            zoneName = dao.loadString("SELECT DISTINCT zone FROM kpi_aggregate WHERE dealergroup like \"" + strDealergroup + "\" limit 1");
//            mainSelection = strDealergroup;
//        } else {
//            query = StringUtils.replaceString(query, "!dealergroup!", " 1=1 ", true);
//        }
//        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
//            query = StringUtils.replaceString(query, "!dealer!", "dealer like '" + strDealer + "'", true);
//            regionName = dao.loadString("SELECT DISTINCT region FROM kpi_aggregate WHERE dealer like '" + strDealer + "' limit 1");
//            zoneName = dao.loadString("SELECT DISTINCT zone FROM kpi_aggregate WHERE dealer like \"" + strDealer + "\" limit 1");
//            mainSelection = strDealer;
//        } else {
//            query = StringUtils.replaceString(query, "!dealer!", " 1=1 ", true);
//        }
        if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            query = StringUtils.replaceString(query, "!region!", "region like \"" + strRegion + "\"", true);
            regionName = strRegion;
            mainSelection = strRegion;
            zoneName = dao.loadString("SELECT DISTINCT zone FROM kpi_aggregate WHERE region like \"" + strRegion + "\" limit 1");
        } else {
            query = StringUtils.replaceString(query, "!region!", " 1=1 ", true);
        }
        if (strArea != null && !strArea.equalsIgnoreCase("ALL") && !strArea.equalsIgnoreCase("Study Total")) {
            query = StringUtils.replaceString(query, "!area!", "area like \"" + strArea + "\"", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM kpi_aggregate WHERE area like '" + strArea + "' limit 1");
            mainSelection = strArea;
            zoneName = dao.loadString("SELECT DISTINCT zone FROM kpi_aggregate WHERE area like \"" + strArea + "\" limit 1");
        } else {
            query = StringUtils.replaceString(query, "!area!", " 1=1 ", true);
        }
        if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            query = StringUtils.replaceString(query, "!zone!", "zone like \"" + strZone + "\"", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM kpi_aggregate WHERE zone like '" + strZone + "' limit 1");
            zoneName = strZone;
            mainSelection = strZone;
        } else {
            query = StringUtils.replaceString(query, "!zone!", " 1=1 ", true);
        }
        if (strDealergroup != null && !strDealergroup.equalsIgnoreCase("ALL") && !strDealergroup.equalsIgnoreCase("Study Total")) {
            query = StringUtils.replaceString(query, "!dealergroup!", "dealergroup like '" + strDealergroup + "'", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM kpi_aggregate WHERE dealergroup like '" + strDealergroup + "' limit 1");
            zoneName = dao.loadString("SELECT DISTINCT zone FROM kpi_aggregate WHERE dealergroup like \"" + strDealergroup + "\" limit 1");
            mainSelection = strDealergroup;
        } else {
            query = StringUtils.replaceString(query, "!dealergroup!", " 1=1 ", true);
        }
        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
            query = StringUtils.replaceString(query, "!dealer!", "dealer like '" + strDealer + "'", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM kpi_aggregate WHERE dealer like '" + strDealer + "' limit 1");
            zoneName = dao.loadString("SELECT DISTINCT zone FROM kpi_aggregate WHERE dealer like \"" + strDealer + "\" limit 1");
            mainSelection = strDealer;
        } else {
            query = StringUtils.replaceString(query, "!dealer!", " 1=1 ", true);
        }
        String average = " Average";
        String best = "Best in ";
        String avgString = "Study Total Average";
        String BestString = "Best Overall";

//        if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
//            avgString = zoneName + average;
//            BestString = best + zoneName;
//            query = StringUtils.replaceString(query, "!regiondealer!", "zone like \"" + strZone + "\"", true);
//        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
//            avgString = zoneName + average;
//            BestString = best + zoneName;
//            query = StringUtils.replaceString(query, "!regiondealer!", " zone like (SELECT DISTINCT zone FROM kpi_aggregate WHERE region like \"" + strRegion + "\" limit 1) ", true);
//        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
//            avgString = zoneName + average;
//            BestString = best + zoneName;
//            query = StringUtils.replaceString(query, "!regiondealer!", " zone like (SELECT DISTINCT zone FROM kpi_aggregate WHERE dealer like '" + strDealer + "' limit 1) ", true);
//        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
//            avgString = regionName + average;
//            BestString = best + regionName;
//            query = StringUtils.replaceString(query, "!regiondealer!", "region like \"" + strRegion + "\"", true);
//        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
//            avgString = regionName + average;
//            BestString = best + regionName;
//            query = StringUtils.replaceString(query, "!regiondealer!", " region like (SELECT DISTINCT region FROM kpi_aggregate WHERE dealer like '" + strDealer + "' limit 1) ", true);
//        } else {
//            query = StringUtils.replaceString(query, "!regiondealer!", " 1=1 ", true);
//        }
         if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            avgString = zoneName + average;
            BestString = best + zoneName;
            query = StringUtils.replaceString(query, "!regiondealer!", "zone like \"" + strZone + "\"", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            avgString = zoneName + average;
            BestString = best + zoneName;
            query = StringUtils.replaceString(query, "!regiondealer!", " zone like (SELECT DISTINCT zone FROM kpi_aggregate WHERE region like \"" + strRegion + "\" limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strArea != null && !strArea.equalsIgnoreCase("ALL") && !strArea.equalsIgnoreCase("Study Total")) {
            avgString = zoneName + average;
            BestString = best + zoneName;
            query = StringUtils.replaceString(query, "!regiondealer!", " zone like (SELECT DISTINCT zone FROM kpi_aggregate WHERE area like \"" + strArea + "\" limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strDealergroup != null && !strDealergroup.equalsIgnoreCase("ALL") && !strDealergroup.equalsIgnoreCase("Study Total")) {
            avgString = zoneName + average;
            BestString = best + zoneName;
            query = StringUtils.replaceString(query, "!regiondealer!", " zone like (SELECT DISTINCT zone FROM kpi_aggregate WHERE dealergroup like '" + strDealergroup + "' limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("zone") && strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
            avgString = zoneName + average;
            BestString = best + zoneName;
            query = StringUtils.replaceString(query, "!regiondealer!", " zone like (SELECT DISTINCT zone FROM kpi_aggregate WHERE dealer like '" + strDealer + "' limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            query = StringUtils.replaceString(query, "!regiondealer!", "region like(select distinct region from kpi_aggregate where zone like '" + strZone + "'limit 1)", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            query = StringUtils.replaceString(query, "!regiondealer!", "region like \"" + strRegion + "\"", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strArea != null && !strArea.equalsIgnoreCase("ALL") && !strArea.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            query = StringUtils.replaceString(query, "!regiondealer!", "region like(select distinct region from kpi_aggregate where area like '" + strArea + "'limit 1)", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strDealergroup != null && !strDealergroup.equalsIgnoreCase("ALL") && !strDealergroup.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            query = StringUtils.replaceString(query, "!regiondealer!", " region like (SELECT DISTINCT region FROM kpi_aggregate WHERE dealergroup like '" + strDealergroup + "' limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            query = StringUtils.replaceString(query, "!regiondealer!", " region like (SELECT DISTINCT region FROM kpi_aggregate WHERE dealer like '" + strDealer + "' limit 1) ", true);
        } else {
            query = StringUtils.replaceString(query, "!regiondealer!", " 1=1 ", true);
        }
        //------------------------
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("kpi trend");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("period", "period");
        columnInfo.put("score", "score");
        columnInfo.put("RegionAvg", "RegionAvg");
        columnInfo.put("RegionBest", "RegionBest");
        LinkedList legendArray = new LinkedList<>();
        legendArray.add(mainSelection);
        legendArray.add(avgString);
        legendArray.add(BestString);
        Map<String, Collection> rows = dao.loadValues(query, columnInfo);
        rows.put("legends", legendArray);
        Map output = new HashMap();
        output.put("data", rows);
        service.writeOutput(response, output);

    }

    public void getscoredistributionBenchmark(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiSopTrend-getscoredistributionBenchmark");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("becnhmark");
        System.out.println(query);
        String brand1 = "nissanscore";
        String brand2 = "datsunscore";
        String brand3 = "jdpascore";
        String value = "value";

        String brand1DisplayName = "Nissan Average";
        String brand2DisplayName = "Datsun Average";
        String brand3DisplayName = "Study Average";

        HashMap<String, String> columnInfo = new HashMap<>();

        columnInfo.put(value, value);
        columnInfo.put(brand1, brand1);
        columnInfo.put(brand2, brand2);
        columnInfo.put(brand3, brand3);

        JSONArray outputArray = new JSONArray();
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        Map<String, List> outputObject = new HashMap<>();
        Set<String> seriesSet = new LinkedHashSet<>();
        Iterator i = rows.iterator();
        while (i.hasNext()) {
            Map<String, String> row = (Map) i.next();
            Integer b1, b2, b3;
            String series = row.get(value);
            seriesSet.add(series);
            try {
                b1 = Integer.parseInt(row.get(brand1));
            } catch (Exception nfe) {
                b1 = 0;
            }

            try {
                b2 = Integer.parseInt(row.get(brand2));
            } catch (Exception nfe) {
                b2 = 0;
            }
            try {
                b3 = Integer.parseInt(row.get(brand3));
            } catch (Exception nfe) {
                b3 = 0;
            }
            HashMap<String, List> seriesMap = new LinkedHashMap<>();
            seriesMap.put(series, Arrays.asList(b1, b2, b3));
            outputObject.putAll(seriesMap);

        }

        outputObject.put(Constants.CATEGORIES, Arrays.asList(
                brand1DisplayName, brand2DisplayName, brand3DisplayName));

        Map<String, Object> metaData = new HashMap<>();
        metaData.put(Constants.CATEGORIES, Constants.CATEGORIES);
        metaData.put(Constants.SERIES, seriesSet);

        Map outPut = new HashMap();

        outPut.put(Constants.DATA, outputObject);
        outPut.put(Constants.MDATA, metaData);

        outPut.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, outPut);

    }

    public void getscoredistributionDealer(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiSopTrend-getscoredistributionDealer");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("dealer");
        System.out.println(query);
        LinkedHashMap<String, String> columnInfo = new LinkedHashMap<>();
        columnInfo.put("dealer", "dealer");
        columnInfo.put("value", "value");
        columnInfo.put("dealerscore", "dealerscore");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        Set<String> categories = new LinkedHashSet<>();
        Map<String, List> series = new LinkedHashMap<>();
        Iterator i = rows.iterator();
        while (i.hasNext()) {
            LinkedHashMap<String, String> row = (LinkedHashMap<String, String>) i.next();
            String value = row.get("dealerscore");
            Float score = Float.parseFloat(value);
            String attribute = row.get("value");
            String period = row.get("dealer");
            categories.add(period);

            List li = series.get(attribute);
            if (li == null) {
                li = new LinkedList<>();
            }
            li.add(score);
            series.put(attribute, li);
        };
        Map data = new HashMap<>();
        data.put("cat", categories);
        data.putAll(series);
        Map metaData = new HashMap();
        metaData.put("categories", "cat");
        metaData.put("seriesNames", series.keySet());
        Map output = new HashMap();
        output.put("data", data);
        output.put("metaData", metaData);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }
    /*########### GENERATED ###########*/

}

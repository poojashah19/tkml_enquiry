/*########### GENERATED AT 2018-05-24 10:29:31.404###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;

import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.List;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author HARSHA
 */
public class OemModelAnalysis {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {

                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/
    public void getFactorIndexByModel(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiModelAnalysis-getFactorIndexByModel");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("query:"+query);
        final String MODEL = "model";
        final String FACTOR = "factor";
        final String COLOR = "colour";
        final String SCORE = "score";
        String bestString = "Best Overall";
        String worstString = "Worst Overall";
        bestString = service.getBestString(request);
        worstString = service.getWorstString(request);

        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put(MODEL, MODEL);
        columnInfo.put(FACTOR, FACTOR);
        columnInfo.put(COLOR, COLOR);
        columnInfo.put(SCORE, SCORE);

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        Iterator i = rows.iterator();
        Map<String, ModelTable> modelTableMap = new LinkedHashMap<>();

        while (i.hasNext()) {
            Map row = (Map) i.next();
            String model = (String) row.get(MODEL);
            String colour = (String) row.get(COLOR);
            String factor = (String) row.get(FACTOR);
            String score = (String) row.get(SCORE);
            ModelTable table;
            table = modelTableMap.get(model);
            if (table == null) {
                table = new ModelTable(model);
            }
            table.setAttributeValue(factor, score);
            table.setAttributeColorValue(factor, colour);
            modelTableMap.put(model, table);
        }

        Map output = new HashMap();
        output.put("data", modelTableMap.values());
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        output.put("best", bestString);
        output.put("worst", worstString);
        service.writeOutput(response, output);

    }

    public void getFactorTrendScores(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiModelAnalysis-getFactorTrendScores");
        // get all the filter groups and their fields
        String timeLine = request.getParameter("timeline");
        //System.out.println("timeline");
        //System.out.println(timeLine);
        if (timeLine.equalsIgnoreCase("yearly")) {
            query = service.getQuery("-CsiModelAnalysis-getFactorTrendScores");
        } else if (timeLine.equalsIgnoreCase("monthly")) {
            query = service.getQuery("-CsiModelAnalysis-getFactorTrendByMonth");
        } else if (timeLine.equalsIgnoreCase("quarterly")) {
            query = service.getQuery("-CsiModelAnalysis-getFactorTrendByQuarter");
        } else if (timeLine.equalsIgnoreCase("Bi-Weekly")) {
            query = service.getQuery("-CsiModelAnalysis-getFactorTrendByBiWeekly");
        } else if (timeLine.equalsIgnoreCase("Biannually")) {
            query = service.getQuery("-CsiModelAnalysis-getFactorTrendByBiannually");
        };
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Set<String> whereMaps = filtersMap.keySet();
        Iterator whereMapIterator = whereMaps.iterator();
        while (whereMapIterator.hasNext()) {
            String whereTemplate = (String) whereMapIterator.next();
            //System.out.println("filter");
            //System.out.println(whereTemplate);
        }
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("trend querry");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("period", "period");
        columnInfo.put("factor_score", "factor_score");
        columnInfo.put("bestscore", "bestscore");
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }

    public void getCsiIndex(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiModelAnalysis-getCsiIndex");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("csi index");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("model", "model");
        columnInfo.put("csi_score", "csi_score");
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }
    /*########### GENERATED ###########*/

}

class ModelTable {

    public ModelTable(String model) {
        this.model = model;

    }

    public String ssi, dw, df, sc, deal, pw, dp, model;

    public String dwColor, dfColor, scColor, dealColor, pwColor, dpColor, ssiColor;

    public void setAttributeValue(String att, String score) {
        switch (att) {
            case "SSI":
                this.ssi = score;
                return;
            case "Dealership Website":
                this.dw = score;
                return;
            case "Dealership Facility":
                this.df = score;
                return;
            case "Dealer Sales Consultant":
                this.sc = score;
                return;
            case "Working out the Deal":
                this.deal = score;
                return;
            case "Paperwork Completion":
                this.pw = score;
                return;
            case "Delivery Process":
                this.dp = score;
                return;
        }
    }

    public void setAttributeColorValue(String att, String color) {
        switch (att) {
            case "SSI":
                this.ssiColor = color;
                return;
//            case "Dealership Website":
//                this.dwColor = color;
//                return;
            case "Dealership Facility":
                this.dfColor = color;
                return;
            case "Dealer Sales Consultant":
                this.scColor = color;
                return;
            case "Working out the Deal":
                this.dealColor = color;
                return;
            case "Paperwork Completion":
                this.pwColor = color;
                return;
            case "Delivery Process":
                this.dpColor = color;
                return;
        }
    }

}

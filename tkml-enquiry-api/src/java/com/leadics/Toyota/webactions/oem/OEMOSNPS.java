/*########### GENERATED AT 2018-05-24 10:29:31.392###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;


import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;

/**
 *
 * @author HARSHA
 */
public class OEMOSNPS {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;
    

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {
                String langCode = request.getParameter("languageCode");
                if (langCode != null) {
                    langCode = langCode;
                }
                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/

    public void getServiceosnps(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CSIOSNPS-getServiceosnps");
        // get all the filter groups and their fields
        String timeLine = request.getParameter("timeline");
        if (timeLine.equalsIgnoreCase("yearly")) {
            query = service.getQuery("-CSIOSNPS-getServiceosnps");
        } else if (timeLine.equalsIgnoreCase("monthly")) {
            query = service.getQuery("-CSIOSNPS-getServiceosnpsByMonth");
        } else if (timeLine.equalsIgnoreCase("quarterly")) {
            query = service.getQuery("-CSIOSNPS-getServiceosnpsByQuarter");
        } else if (timeLine.equalsIgnoreCase("Bi-Weekly")) {
            query = service.getQuery("-CSIOSNPS-getServiceosnpsBiWeekly");
        }

        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("mean", "mean");
        columnInfo.put("period", "period");

        Map<String, Collection> rows = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", rows);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }

    public void getTopBottomDealers(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CSIOSNPS-getTopBottomDealers");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("topandbottom");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("Region", "Region");
        columnInfo.put("Dealer", "Dealer");
        columnInfo.put("score", "score");
        columnInfo.put("top_or_bottom", "top_or_bottom");

        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
        service.writeOutput(response, output);

    }

    public void getregional(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CSIOSNPS-getregional");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);

        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("region", "region");
        columnInfo.put("csi_score", "value");
        columnInfo.put("count", "repondents");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Map output = new HashMap();
        output.put("data", rows);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }
    /*########### GENERATED ###########*/

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.webactions.oem;

import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.utils.LogUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Krishna-Kanth;
 */
public class OemRedalert {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {

                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getRedAlertData(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-oemRedalert-getRedalertData");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);

        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("red alert");
        System.out.println(query);
        String report_quarter = "report_quarter";
        String dealer = "dealer";
//        String product = "product";
        String model = "model";
        String responses = "response";
        String other_feedbacks = "other_feedback";
        String osat = "osat";

        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put(dealer, dealer);
        columnInfo.put(report_quarter, report_quarter);
//        columnInfo.put(product, product);
        columnInfo.put(responses, responses);
        columnInfo.put(other_feedbacks, other_feedbacks);
        columnInfo.put(osat, osat);
        columnInfo.put(model, model);

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        System.out.println( rows);
        Map output = new HashMap();
        output.put("data", rows);
        service.writeOutput(response, output);
    }
}

/*########### GENERATED AT 2018-05-24 10:29:31.349###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.google.gson.Gson;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_FACTOR;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_LIMIT;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_MAX_LIMIT;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;


import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;

/**
 *
 * @author HARSHA
 */
public class OemRegional {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;
    

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {
                
                
                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/
    public void getFactorScoresByRegion(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiRegional-getFactorScoresByRegion");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        String bestString = "Region Best";
        String worstString = "Region Worst";
        bestString = service.getBestStringDg(request);
        worstString = service.getWorstStringDg(request);
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("zone", "zone");
        columnInfo.put("ssi_score", "ssi_score");
        columnInfo.put("bestscore", "bestscore");
        columnInfo.put("worstscore", "worstscore");
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
          output.put("best", bestString);
        output.put("worst", worstString);
        service.writeOutput(response, output);

    }

    public void getFactorScores(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiRegional-getFactorScores");
        // get all the filter groups and their fields
         System.out.println("factor scores1:    "+ query);
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("factor scores:    "+ query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("factor", "factor");
        columnInfo.put("csi_score", "csi_score");
        Map<String, Collection> rows = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", rows);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }

    public void getFactorMeanAttributesScore(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiRegional-getFactorMeanAttributesScore");

        String factor = request.getParameter("factor");
        String factorReplacer = "", limitReplacer = "", orderLabel = "", order = "";
        if (factor == null || factor.equalsIgnoreCase(ATTRIBUTE_FACTOR) || factor.equalsIgnoreCase("ALL")) {
            factorReplacer = "%";
            limitReplacer = ATTRIBUTE_LIMIT;
            limitReplacer = ATTRIBUTE_LIMIT;
            orderLabel = "score";
            order = "desc";
        } else {
        factorReplacer = factor;
        limitReplacer = ATTRIBUTE_MAX_LIMIT;
        orderLabel = "AttributeIndex";
        order = "asc";
        }

        query = query.replaceAll("!FACTOR!", factorReplacer)
                .replaceAll("!LIMIT!", limitReplacer)
                .replaceAll("!ORDERLABEL!", orderLabel)
                .replaceAll("!ORDER!", order);

        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        String bestString = "Best Overall";
        String worstString = "Worst Overall";
        bestString = service.getBestStringDg(request);
        worstString = service.getWorstStringDg(request);
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("attribute", "attribute");
        columnInfo.put("score", "score");
        columnInfo.put("bestscore", "bestscore");
        columnInfo.put("worstscore", "worstscore");
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
          output.put("best", bestString);
        output.put("worst", worstString);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);
    }

    public void getFactorAttributeDelighted(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiRegional-getFactorAttributeDelighted");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        String factor = request.getParameter("factor");

        String factorReplacer = "", limitReplacer = "", orderLabel = "", order = "";
        if (factor == null || factor.equalsIgnoreCase(ATTRIBUTE_FACTOR) || factor.equalsIgnoreCase("ALL")) {
            factorReplacer = "%";
            limitReplacer = ATTRIBUTE_LIMIT;
            orderLabel = "a.score";
            order = "desc";
        } else {
        factorReplacer = factor;
        limitReplacer = ATTRIBUTE_MAX_LIMIT;
        orderLabel = "AttributeIndex";
        order = "asc";
        }

        query = query.replaceAll("!FACTOR!", factorReplacer)
                .replaceAll("!LIMIT!", limitReplacer)
                .replaceAll("!ORDERLABEL!", orderLabel)
                .replaceAll("!ORDER!", order);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        String bestString = "Best Overall";
        String worstString = "Worst Overall";
        bestString = service.getBestStringDg(request);
        worstString = service.getWorstStringDg(request);
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("delighted");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("attribute", "attribute");
        columnInfo.put("score", "score");
        columnInfo.put("bestscore", "bestscore");
        columnInfo.put("worstscore", "worstscore");

        Map<String, Collection> rows = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", rows);
          output.put("best", bestString);
        output.put("worst", worstString);

        service.writeOutput(response, output);

    }
    /*########### GENERATED ###########*/

}

/*########### GENERATED AT 2018-05-24 10:29:31.367###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;


import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.List;
import java.util.Set;

/**
 *
 * @author HARSHA
 */
public class OemPriority {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;
    

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {
                
                
                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/
    public void getComparison(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerPriority-getDealerComparison");
        // take a query based on the filter selection of region-dealer (comparison filter ) 
        try {
            String compare = request.getParameter("dealer-region");

            if (compare.trim().equalsIgnoreCase("dealer-compare")) {
                query = service.getQuery("-CsiPriority-getDealerComparison");
                String dealer1 = StringUtils.noNull(request.getParameter("dealer1"));
                String dealer2 = StringUtils.noNull(request.getParameter("dealer2"));
                query = query.replaceAll("<<DEALER-1>>", "\"" + dealer1 + "\"");
                query = query.replaceAll("<<DEALER-2>>", "\"" + dealer2 + "\"");
            } else if (compare.trim().equalsIgnoreCase("region-compare")) {
                query = service.getQuery("-CsiPriority-getRegionComparison");

                String region1 = StringUtils.noNull(request.getParameter("region1"));
                String region2 = StringUtils.noNull(request.getParameter("region2"));
                query = query.replaceAll("<<REGION-1>>", "\"" + region1 + "\"");
                query = query.replaceAll("<<REGION-2>>", "\"" + region2 + "\"");
            } else if (compare.trim().equalsIgnoreCase("dealergroup-compare")) {
                query = service.getQuery("-CsiPriority-getDealergroupComparision");

                String dgroup1 = StringUtils.noNull(request.getParameter("DealerGroup1"));
                String dgroup2 = StringUtils.noNull(request.getParameter("DealerGroup2"));
                query = query.replaceAll("<<DEALERGROUP-1>>", "\"" + dgroup1 + "\"");
                query = query.replaceAll("<<DEALERGROUP-2>>", "\"" + dgroup2 + "\"");
            }else if (compare.trim().equalsIgnoreCase("zone-compare")) {
                query = service.getQuery("-CsiPriority-getZoneComparision");

                String zone1 = StringUtils.noNull(request.getParameter("zone1"));
                String zone2 = StringUtils.noNull(request.getParameter("zone2"));
                query = query.replaceAll("<<ZONE-1>>", "\"" + zone1 + "\"");
                query = query.replaceAll("<<ZONE-2>>", "\"" + zone2 + "\"");
            }else {
                query = service.getQuery("-CsiPriority-getDealergroupComparision");
            }
        } catch (Exception e) {
            query = service.getQuery("-CsiDealerPriority-getDealerComparison");
        }
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("priority query");
        System.out.println(query);
        final String CAT_NAME = "attribute";
        final String SERIES_NAME = "series";
        final String STUDY_AVG = "studyavg";
        final String studyDisplayName = "Study Best";
        final String SCORE = "diffrance";

        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put(CAT_NAME, CAT_NAME);
        columnInfo.put(STUDY_AVG, STUDY_AVG);
        columnInfo.put(SCORE, SCORE);
        columnInfo.put(SERIES_NAME, SERIES_NAME);

        Map<String, Collection> columns = dao.loadValues(query, columnInfo);

        Collection catCollection = columns.get(CAT_NAME);
        // remove duplicate categories and maintain link
        Set<String> catSet = new LinkedHashSet<>(catCollection);

        // remove duplicate study average and maintain link
        Set studySet = new LinkedHashSet<>(columns.get(STUDY_AVG));

        Set seriesSet = new LinkedHashSet<>(columns.get(SERIES_NAME));

        Map<String, Collection> outPutObject = new LinkedHashMap<>();

        outPutObject.put(CAT_NAME, catSet);
        outPutObject.put(studyDisplayName, studySet);

        List scoreList = (List) columns.get(SCORE);
        Iterator i = seriesSet.iterator();

        while (i.hasNext()) {
            String series = i.next().toString();
            //System.out.println(series);
            List li = (List) columns.get(SERIES_NAME);
            int begin = li.indexOf(series);
            int end = li.lastIndexOf(series);

            outPutObject.put(series, scoreList.subList(begin, end + 1));

        }

        // metaData 
        seriesSet.add(studyDisplayName);

        Map<String, Object> metaData = new HashMap<>();
        metaData.put("categories", CAT_NAME);
        metaData.put("series", seriesSet);

        Map output = new HashMap();
        output.put("data", outPutObject);
        output.put("metaData", metaData);
        service.writeOutput(response, output);

    }
    /*########### GENERATED ###########*/

}

/*########### GENERATED AT 2018-05-24 10:29:31.329###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;


import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;
import java.util.Set;

/**
 *
 * @author HARSHA
 */
public class OEMNewQuestionsos {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;
    

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {
                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/

    public void getDelighted(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CSINewQuestions-getDelighted");
        String timeLine = request.getParameter("timeline");
        if (timeLine.equalsIgnoreCase("yearly")) {
            query = service.getQuery("-CSINewQuestions-getDelighted");
        } else if (timeLine.equalsIgnoreCase("monthly")) {
            query = service.getQuery("-CSINewQuestions-getDelightedByMonth");
        } else if (timeLine.equalsIgnoreCase("quarterly")) {
            query = service.getQuery("-CSINewQuestions-getDelightedByQuarter");
        } else if (timeLine.equalsIgnoreCase("Bi-Weekly")) {
            query = service.getQuery("-CSINewQuestions-getDelightedByBiWeekly");
        };
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("delighted query");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("period", "period");
        columnInfo.put("delighted", "delighted");
        columnInfo.put("AttributeName", "AttributeName");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Set<String> categories = new LinkedHashSet<>();
        Map<String, List> series = new LinkedHashMap<>();
        Iterator i = rows.iterator();
        while (i.hasNext()) {
            HashMap<String, String> row = (HashMap<String, String>) i.next();
            String value = row.get("delighted");
            Integer score = Integer.parseInt(value);
            String attribute = row.get("AttributeName");
            attribute = attribute.replaceAll("’", "\'");
            String period = row.get("period");
            categories.add(period);
            List li = series.get(attribute);
            if (li == null) {
                li = new LinkedList<>();
            };

            li.add(score);
            series.put(attribute, li);
        };
        Map data = new HashMap<>();
        data.put("cat", categories);
        data.putAll(series);
        Map metaData = new HashMap();
        metaData.put("categories", "cat");
        metaData.put("seriesNames", series.keySet());
        Map output = new HashMap();
        output.put("data", data);
        output.put("metaData", metaData);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);
    }

    public void getMean(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CSINewQuestions-getMean");

        String timeLine = request.getParameter("timeline");
        if (timeLine.equalsIgnoreCase("yearly")) {
            query = service.getQuery("-CSINewQuestions-getMean");
        } else if (timeLine.equalsIgnoreCase("monthly")) {
            query = service.getQuery("-CSINewQuestions-getMeanByMonth");
        } else if (timeLine.equalsIgnoreCase("quarterly")) {
            query = service.getQuery("-CSINewQuestions-getMeanByQuarter");
        } else if (timeLine.equalsIgnoreCase("Bi-Weekly")) {
            query = service.getQuery("-CSINewQuestions-getMeanByBiWeekly");
        };
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("mean query");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("mean", "mean");
        columnInfo.put("period", "period");
        columnInfo.put("AttributeName", "AttributeName");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Set<String> categories = new LinkedHashSet<>();
        Map<String, List> series = new LinkedHashMap<>();

        Iterator i = rows.iterator();
        while (i.hasNext()) {
            HashMap<String, String> row = (HashMap<String, String>) i.next();
            String value = row.get("mean");
            Float score = Float.parseFloat(value);
            String attribute = row.get("AttributeName");
            attribute = attribute.replace("’", "\'");
            String period = row.get("period");
            categories.add(period);
            List li = series.get(attribute);
            if (li == null) {
                li = new LinkedList<>();
            }
            li.add(score);
            series.put(attribute, li);
        };
        Map data = new HashMap<>();
        data.put("cat", categories);
        data.putAll(series);

        Map metaData = new HashMap();
        metaData.put("categories", "cat");
        metaData.put("seriesNames", series.keySet());

        Map output = new HashMap();
        output.put("data", data);
        output.put("metaData", metaData);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);
    }

    public void getDissatisfied(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CSINewQuestions-getDissatisfied");
        String timeLine = request.getParameter("timeline");
        if (timeLine.equalsIgnoreCase("yearly")) {
            query = service.getQuery("-CSINewQuestions-getDissatisfied");
        } else if (timeLine.equalsIgnoreCase("monthly")) {
            query = service.getQuery("-CSINewQuestions-getDissatisfiedByMonth");
        } else if (timeLine.equalsIgnoreCase("quarterly")) {
            query = service.getQuery("-CSINewQuestions-getDissatisfiedByQuarter");
        } else if (timeLine.equalsIgnoreCase("Bi-Weekly")) {
            query = service.getQuery("-CSINewQuestions-getDissatisfiedByBiWeekly");
        };
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("disatisfied");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("period", "period");
        columnInfo.put("disatisfied", "disatisfied");
        columnInfo.put("AttributeName", "AttributeName");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Set<String> categories = new LinkedHashSet<>();
        Map<String, List> series = new LinkedHashMap<>();

        Iterator i = rows.iterator();
        while (i.hasNext()) {
            HashMap<String, String> row = (HashMap<String, String>) i.next();
            String value = row.get("disatisfied");
            Integer score = Integer.parseInt(value);
            String attribute = row.get("AttributeName");

            attribute = attribute.replace("’", "\'");

            System.out.println("disatisfied------>" + attribute);
            String period = row.get("period");
            categories.add(period);

            List li = series.get(attribute);
            if (li == null) {
                li = new LinkedList<>();
            };
            System.out.println(score);
            System.out.println(attribute);
            li.add(score);
            series.put(attribute, li);
        };
        Map data = new HashMap<>();
        data.put("cat", categories);
        data.putAll(series);

        Map metaData = new HashMap();
        metaData.put("categories", "cat");
        metaData.put("seriesNames", series.keySet());

        Map output = new HashMap();
        output.put("data", data);
        output.put("metaData", metaData);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);
    }
    /*########### GENERATED ###########*/

}

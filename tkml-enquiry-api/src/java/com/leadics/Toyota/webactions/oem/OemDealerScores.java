/*########### GENERATED AT 2018-05-24 10:29:31.430###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;

import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.List;

/**
 *
 * @author HARSHA
 */
public class OemDealerScores {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {

                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/
    public void getScores(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerScores-getScores");

        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);

        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("ranking");
        System.out.println(query);
        String dealer = "dealer";
        String rank = "rank";
        String colour = "colour";
        String region = "region";
        String area = "area";
        String factor = "factor";
        String score = "score";
        String kpi="KPI";
        String bestString = "Best Overall";
        String worstString = "Worst Overall";
        bestString = service.getBestString(request);
        worstString = service.getWorstString(request);

        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put(dealer, dealer);
        columnInfo.put(rank, rank);
        columnInfo.put(colour, colour);
        columnInfo.put(region, region);
        columnInfo.put(area, area);
        columnInfo.put(factor, factor);
        columnInfo.put(score, score);
        columnInfo.put(kpi, kpi);
        System.out.println("test Kavya");
        System.out.println(columnInfo);

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        Iterator i = rows.iterator();
        Map<String, DealerTable> dealerTableMap = new LinkedHashMap<>();

        while (i.hasNext()) {
            Map<String, String> row = (Map) i.next();
            String _dealer = row.get(dealer);
            String _rank = row.get(rank);
            String _color = row.get(colour);
            String _region = row.get(region);
            String _area = row.get(area);
            String _factor = row.get(factor);
            String _kpi = row.get(kpi);
            String _score = row.get(score);

            if (_factor.equalsIgnoreCase("Dealership Website") && _score.equalsIgnoreCase("0")) {
                _score = "-";
            }
            // if score is 999
            _rank = _rank.equalsIgnoreCase("999") ? "" : _rank;
            _rank = _rank.equalsIgnoreCase("989") ? "" : _rank;

            DealerTable table;
            table = dealerTableMap.get(_dealer);
            if (table == null) {

                table = new DealerTable(_dealer, _color, _rank, _region,_area);

            }
            table.setAttributeValue(_factor, _score);
            table.setAttributeColorValue(_factor, _color);
            dealerTableMap.put(_dealer, table);
        }

        Map output = new HashMap();
        System.out.println("dealer table values");
        System.out.println(dealerTableMap.values());
        output.put("data", dealerTableMap.values());
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        output.put("best", bestString);
        output.put("worst", worstString);
        service.writeOutput(response, output);

    }
    /*########### GENERATED ###########*/

}

class DealerTable {

    public DealerTable(String dealer, String color, String rank, String region,String area) {
        this.dealer = dealer;
        this.color = color;
        this.rank = rank;
        this.region = region;
        this.area=area; 

    }

    public String ssi, dw, df, sc, deal, pw, dp, dealer, color, rank, region,area,KPI;

    public String dwColor, dfColor, scColor, dealColor, pwColor, dpColor, ssiColor,kpiColor;

    public void setAttributeValue(String att, String score) {
        switch (att) {
            case "SSI":
                this.ssi = score;
                return;
            case "Dealership Website":
                this.dw = score;
                return;
            case "Dealership Facility":
                this.df = score;
                return;
            case "Dealer Sales Consultant":
                this.sc = score;
                return;
            case "Working out the Deal":
                this.deal = score;
                return;
            case "Paperwork Completion":
                this.pw = score;
                return;
            case "Delivery Process":
                this.dp = score;
                return;
        }
    }

    public void setAttributeColorValue(String att, String color) {
        switch (att) {
            case "SSI":
                this.ssiColor = color;
                return;
            case "Dealership Website":
                this.dwColor = "white";
                return;
            case "Dealership Facility":
                this.dfColor = color;
                return;
            case "Dealer Sales Consultant":
                this.scColor = color;
                return;
            case "Working out the Deal":
                this.dealColor = color;
                return;
            case "Paperwork Completion":
                this.pwColor = color;
                return;
            case "Delivery Process":
                this.dpColor = color;
                return;
        }
    }

}

/*########### GENERATED AT 2018-05-24 10:29:31.299###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.google.gson.Gson;
import static com.leadics.Toyota.common.Constants.*;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;


import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;
import java.util.Set;

/**
 *
 * @author HARSHA
 */
public class OemLoyaltyAdvocacy {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;
    

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        System.out.println(actionType);
        System.out.println(actionType);
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {
                
                
                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/
    public void getLoyaltyAndAdvocacy(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiLoyaltyAdvocacy-getLoyaltyAndAdvocacy");

//        String query1 = service.getQuery("-CsiLoyaltyAdvocacy-getNPSScore");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
//        Map<String, String> acknowledgmentMaposnps = service.buildWherecondition(request, filtersMap, query1);

        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
//        query1 = acknowledgmentMaposnps.get(QUERY_TEMPLATE);
        System.out.println("loyality");
        System.out.println(query);
        final String mainQuestion = "attributeName";
        final String answer1 = "Definitely Will Not Count";
        final String answer2 = "Probably Will Not Count";
        final String answer3 = "Probably Will Count";
        final String answer4 = "Definitely Will Count";

        HashMap<String, String> columnInfoosnps = new HashMap<>();
        columnInfoosnps.put("AttributeName", "AttributeName");
//        columnInfoosnps.put("npsscore", "npsscore");
//        Map<String, Collection> npscols = dao.loadValues(query1, columnInfoosnps);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put(answer1, answer1);
        columnInfo.put(answer2, answer2);
        columnInfo.put(answer3, answer3);
        columnInfo.put(answer4, answer4);
        columnInfo.put(mainQuestion, mainQuestion);
        Map<String, Map<String, List>> questionAnswerMap = new LinkedHashMap<>();

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Set<String> categories = new LinkedHashSet<>();
        Map<String, List> series = new HashMap<>();
        Iterator i = rows.iterator();

        final List SERIES_LIST = Arrays.asList("Definitely Will Not", "Probably Will Not", "Probably Will", "Definitely Will");

        while (i.hasNext()) {
            HashMap<String, String> row = (HashMap<String, String>) i.next();
            String question = row.get(mainQuestion);
            String a1 = row.get(answer1);
            String a2 = row.get(answer2);
            String a3 = row.get(answer3);
            String a4 = row.get(answer4);

            Integer i1, i2, i3, i4;
            try {
                i1 = Integer.parseInt(a1);
            } catch (Exception e) {
                i1 = 0;
            }
            try {
                i2 = Integer.parseInt(a2);
            } catch (Exception e) {
                i2 = 0;
            }
            try {
                i3 = Integer.parseInt(a3);
            } catch (Exception e) {
                i3 = 0;
            }
            try {
                i4 = Integer.parseInt(a4);
            } catch (Exception e) {
                i4 = 0;
            }

            Map<String, List> answerMap = questionAnswerMap.get(question);
            if (answerMap == null) {
                answerMap = new LinkedHashMap<>();
            }
            List listOne = answerMap.get(answer1);
            List listTwo = answerMap.get(answer2);
            List listThree = answerMap.get(answer3);
            List listFour = answerMap.get(answer4);

            if (listOne == null) {
                listOne = new LinkedList<>();
            }
            if (listTwo == null) {
                listTwo = new LinkedList<>();
            }
            if (listThree == null) {
                listThree = new LinkedList<>();
            }
            if (listFour == null) {
                listFour = new LinkedList<>();
            }
            listOne.add(i1);
            listTwo.add(i2);
            listThree.add(i3);
            listFour.add(i4);

            answerMap.put("Definitely Will Not", listOne);
            answerMap.put("Probably Will Not", listTwo);
            answerMap.put("Probably Will", listThree);
            answerMap.put("Definitely Will", listFour);

            questionAnswerMap.put(question, answerMap);

        };

        Map<String, Object> metaDataMap = new HashMap<>();
        metaDataMap.put(SERIES, SERIES_LIST);

        Map output = new HashMap();
        output.put(DATA, questionAnswerMap);
        output.put(MDATA, metaDataMap);
//        output.put("OSNPS", npscols);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }

    public void getDealerLoyaltyAndAdvocacy(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerLoyaltyAdvocacy-getLoyaltyAndAdvocacy");

        String query1 = service.getQuery("-CsiDealerLoyaltyAdvocacy-getNPSScore");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        Map<String, String> acknowledgmentMaposnps = service.buildWherecondition(request, filtersMap, query1);

        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        query1 = acknowledgmentMaposnps.get(QUERY_TEMPLATE);
        System.out.println("loyality");
        System.out.println(query);
        final String mainQuestion = "attributeName";
        final String answer1 = "Definitely Will Not Count";
        final String answer2 = "Probably Will Not Count";
        final String answer3 = "Probably Will Count";
        final String answer4 = "Definitely Will Count";
        HashMap<String, String> columnInfoosnps = new HashMap<>();
        columnInfoosnps.put("AttributeName", "AttributeName");
        columnInfoosnps.put("npsscore", "npsscore");
        Map<String, Collection> npscols = dao.loadValues(query1, columnInfoosnps);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put(answer1, answer1);
        columnInfo.put(answer2, answer2);
        columnInfo.put(answer3, answer3);
        columnInfo.put(answer4, answer4);
        columnInfo.put(mainQuestion, mainQuestion);
        Map<String, Map<String, List>> questionAnswerMap = new LinkedHashMap<>();

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Set<String> categories = new LinkedHashSet<>();
        Map<String, List> series = new HashMap<>();
        Iterator i = rows.iterator();

        final List SERIES_LIST = Arrays.asList("Definitely Will Not", "Probably Will Not", "Probably Will", "Definitely Will");

        while (i.hasNext()) {
            HashMap<String, String> row = (HashMap<String, String>) i.next();
            String question = row.get(mainQuestion);
            String a1 = row.get(answer1);
            String a2 = row.get(answer2);
            String a3 = row.get(answer3);
            String a4 = row.get(answer4);

            Integer i1, i2, i3, i4;
            try {
                i1 = Integer.parseInt(a1);
            } catch (Exception e) {
                i1 = 0;
            }
            try {
                i2 = Integer.parseInt(a2);
            } catch (Exception e) {
                i2 = 0;
            }
            try {
                i3 = Integer.parseInt(a3);
            } catch (Exception e) {
                i3 = 0;
            }
            try {
                i4 = Integer.parseInt(a4);
            } catch (Exception e) {
                i4 = 0;
            }

            Map<String, List> answerMap = questionAnswerMap.get(question);
            if (answerMap == null) {
                answerMap = new LinkedHashMap<>();
            }
            List listOne = answerMap.get(answer1);
            List listTwo = answerMap.get(answer2);
            List listThree = answerMap.get(answer3);
            List listFour = answerMap.get(answer4);

            if (listOne == null) {
                listOne = new LinkedList<>();
            }
            if (listTwo == null) {
                listTwo = new LinkedList<>();
            }
            if (listThree == null) {
                listThree = new LinkedList<>();
            }
            if (listFour == null) {
                listFour = new LinkedList<>();
            }
            listOne.add(i1);
            listTwo.add(i2);
            listThree.add(i3);
            listFour.add(i4);

            answerMap.put("Definitely Will Not", listOne);
            answerMap.put("Probably Will Not", listTwo);
            answerMap.put("Probably Will", listThree);
            answerMap.put("Definitely Will", listFour);

            questionAnswerMap.put(question, answerMap);

        };

        Map<String, Object> metaDataMap = new HashMap<>();
        metaDataMap.put(SERIES, SERIES_LIST);

        Map output = new HashMap();
        output.put(DATA, questionAnswerMap);
        output.put(MDATA, metaDataMap);
        output.put("OSNPS", npscols);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }
    /*########### GENERATED ###########*/
}

/*########### GENERATED AT 2018-02-22 17:20:40.917###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.NameValuePair;
import com.google.gson.Gson;
import static com.leadics.Toyota.common.LIAction.AUTHORIZATION;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import com.leadics.Toyota.service.LISystemuserService;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;
import com.leadics.Toyota.to.LISystemuserRecord;

import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;

/**
 *
 * @author HARSHA
 */
public class OemFilters {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;
    

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {
                
                
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/
    public void getModel(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getModel");
        HashMap<String, String> columnInfo = new HashMap<>();
       
        final String column1 = "model";
        columnInfo.put(column1, column1);
      
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Iterator i = rows.iterator();
        ArrayList<modelbrand> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String model = map.get(column1);
          
            nvp = new NameValuePair(model, model);
            modelbrand mb = new modelbrand();
          
            mb.model = nvp;
            li.add(mb);
        }
        service.writeOutput(response, li);
    }

    ;
     public void getDealergroup(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getDealergroup");
        System.out.println("dealergroup filter  ::  " + query);
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column2 = "year";
        final String column3 = "biannual";
        final String column4 = "region";
        final String column5 = "area";
        final String column6 = "zone";
        final String column1 = "dealergroup";
        columnInfo.put(column1, column1);
        columnInfo.put(column2, column2);
        columnInfo.put(column3, column3);
        columnInfo.put(column4, column4);
        columnInfo.put(column5, column5);
        columnInfo.put(column6, column6);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Iterator i = rows.iterator();
        ArrayList<DealerGroupSelection> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String DealerGroup = map.get(column1);
            String year = map.get(column2);
            String biannual = map.get(column3);
            String region = map.get(column4);
            String area = map.get(column5);
            String zone = map.get(column6);
            nvp = new NameValuePair(DealerGroup, DealerGroup);
            DealerGroupSelection dg = new DealerGroupSelection();
            dg.year = year;
            dg.biannual = biannual;
            dg.region = region;
            dg.area = area;
            dg.zone = zone;
            dg.dealergroup = nvp;
            li.add(dg);
        }
        service.writeOutput(response, li);
    }

    ;
    public void getBrand(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getBrand");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "brand";
        columnInfo.put(column1, column1);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Iterator i = rows.iterator();
        ArrayList<NameValuePair> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String brand = map.get(column1);
            String label = brand.contains("Nissan") ? "Nissan" : "Datsun";
            nvp = new NameValuePair(label, brand);
            li.add(nvp);
        }
        service.writeOutput(response, li);
    }

    ;
    
    
    public void getYear(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getYear");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column = "year";
        columnInfo.put(column, column);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        Iterator i = rows.iterator();
        ArrayList<NameValuePair<String, String>> li = new ArrayList<>();
        NameValuePair<String, String> nvp;

        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String year = map.get(column);
            nvp = new NameValuePair(year, year);
            li.add(nvp);

        }

        service.writeOutput(response, li);

    }

    public void getKpi(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getKpi");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column = "kpi";
        columnInfo.put(column, column);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        System.out.println("factor ");
        System.out.println(query);
        Iterator i = rows.iterator();
        ArrayList<NameValuePair<String, String>> li = new ArrayList<>();
        NameValuePair<String, String> nvp;

        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String model = map.get(column);
            nvp = new NameValuePair(model, model);
            li.add(nvp);

        }

        service.writeOutput(response, li);

    }
    public void getBiannual(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getBiannual");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "biannual";
        final String column2 = "year";
        columnInfo.put(column1, column1);
        columnInfo.put(column2, column2);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Iterator i = rows.iterator();
        ArrayList<BiannualYear> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String biannual = map.get("biannual");
            String year = map.get("year");
            nvp = new NameValuePair(biannual, biannual);
            BiannualYear yq = new BiannualYear();
            yq.year = year;
            yq.biannual = nvp;
            li.add(yq);
        }
        service.writeOutput(response, li);

    }

    public void getQuarter(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getQuarter");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "quarter";
        final String column2 = "year";
        columnInfo.put(column1, column1);
        columnInfo.put(column2, column2);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Iterator i = rows.iterator();
        ArrayList<SingleParent> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String quarter = map.get("quarter");
            String year = map.get("year");
            nvp = new NameValuePair(quarter, quarter);
            SingleParent yq = new SingleParent();
            yq.year = year;
            yq.quarter = nvp;
            li.add(yq);
        }
        service.writeOutput(response, li);

    }

    public void getMonth(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getMonth");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column3 = "month";
        final String column1 = "biannual";
        final String column2 = "year";
        columnInfo.put(column1, column1);
        columnInfo.put(column2, column2);
        columnInfo.put(column3, column3);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Iterator i = rows.iterator();
        ArrayList<TwoParent> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String month = map.get("month");
            String biannual = map.get("biannual");
            String year = map.get("year");
            nvp = new NameValuePair(month, month);
            TwoParent yq = new TwoParent();
            yq.year = year;
            yq.biannual = biannual;
            yq.month = nvp;
            li.add(yq);
        }
        service.writeOutput(response, li);

    }

    public void getBiweekly(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getBiweekly");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column4 = "biweekly";
        final String column3 = "month";
        final String column1 = "quarter";
        final String column2 = "year";
        columnInfo.put(column1, column1);
        columnInfo.put(column2, column2);
        columnInfo.put(column3, column3);
        columnInfo.put(column4, column4);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        System.out.println("biweekly");
        System.out.println(query);
        Iterator i = rows.iterator();
        ArrayList<ThreeParent> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String biweekly = map.get("biweekly");
            String month = map.get("month");
            String quarter = map.get("quarter");
            String year = map.get("year");
            nvp = new NameValuePair(biweekly, biweekly);
            ThreeParent yq = new ThreeParent();
            yq.year = year;
            yq.quarter = quarter;
            yq.month = month;
            yq.biweekly = nvp;
            li.add(yq);
        }
        service.writeOutput(response, li);

    }

//    public void getRegion(HttpServletRequest request, HttpServletResponse response, String methodName)
//            throws Exception {
//
//        LIService service = new LIService();
//        LIDAO dao = new LIDAO();
//        String query = service.getQuery("-CsiFilters-getRegion");
//        System.out.println("getregion");
//        System.out.println(query);
//        HashMap<String, String> columnInfo = new HashMap<>();
//        final String column1 = "region";
//        final String column2 = "year";
//        columnInfo.put(column1, column1);
//        columnInfo.put(column2, column2);
//        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
//        Iterator i = rows.iterator();
//        ArrayList<RegionSelection> li = new ArrayList<>();
//        NameValuePair<String, String> nvp;
//        while (i.hasNext()) {
//            HashMap<String, String> map = (HashMap<String, String>) i.next();
//            String region = map.get(column1);
//            String year = map.get(column2);
//            nvp = new NameValuePair(region, region);
//            RegionSelection regionparent = new RegionSelection();
//            regionparent.year = year;
//            regionparent.region = nvp;
//            li.add(regionparent);
//        }
//        service.writeOutput(response, li);
//
//    }
        public void getRegion(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getRegion");
        System.out.println("getregion");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "region";
        final String column2 = "year";
        final String column3 = "biannual";
        
        columnInfo.put(column1, column1);
        columnInfo.put(column2, column2);
        columnInfo.put(column3, column3);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Iterator i = rows.iterator();
        ArrayList<RegionSelections> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String region = map.get(column1);
            String year = map.get(column2);
            String biannual = map.get(column3);
            nvp = new NameValuePair(region, region);
            RegionSelections regionparent = new RegionSelections();
            regionparent.year = year;
            regionparent.biannual = biannual;
            regionparent.region = nvp;
            li.add(regionparent);
        }
        service.writeOutput(response, li);

    }
        public void getArea(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getArea");
        System.out.println("getarea");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "area";
        final String column2 = "year";
        final String column3 = "biannual";
        final String column4 = "region";
        
        columnInfo.put(column1, column1);
        columnInfo.put(column2, column2);
        columnInfo.put(column3, column3);
        columnInfo.put(column4, column4);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Iterator i = rows.iterator();
        ArrayList<AreaSelection> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String area = map.get(column1);
             String year = map.get(column2);
              String biannual = map.get(column3);
            String region = map.get(column4);
            nvp = new NameValuePair(area, area);
            AreaSelection areaparent = new AreaSelection();
            areaparent.year = year;
            areaparent.biannual = biannual;
            areaparent.region = region;
            areaparent.area = nvp;
            li.add(areaparent);
        }
        service.writeOutput(response, li);

    }
        public void getZone(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getZone");
        System.out.println("getarea");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "zone";
        final String column2 = "year";
        final String column3 = "biannual";
        final String column4 = "region";
        final String column5 = "area";
        
        columnInfo.put(column1, column1);
        columnInfo.put(column2, column2);
        columnInfo.put(column3, column3);
        columnInfo.put(column4, column4);
        columnInfo.put(column5, column5);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Iterator i = rows.iterator();
        ArrayList<ZoneSelection> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String zone = map.get(column1);
             String year = map.get(column2);
              String biannual = map.get(column3);
            String region = map.get(column4);
            String area = map.get(column5);
            nvp = new NameValuePair(zone, zone);
            ZoneSelection zoneparent = new ZoneSelection();
            zoneparent.year = year;
            zoneparent.biannual = biannual;
            zoneparent.region = region;
            zoneparent.area = area;
            zoneparent.zone = nvp;
            li.add(zoneparent);
        }
        service.writeOutput(response, li);

    }

    public void getDealer(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getDealer");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "dealer";
        final String column2 = "year";
        final String column3 = "biannual";
        final String column4 = "region";
        final String column5 = "area";
        final String column6 = "zone";
        final String column7 = "dealergroup";
        
        columnInfo.put(column1, column1);
        columnInfo.put(column2, column2);
        columnInfo.put(column3, column3);
         columnInfo.put(column4, column4);
          columnInfo.put(column5, column5);
           columnInfo.put(column6, column6);
            columnInfo.put(column7, column7);
          

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        Iterator i = rows.iterator();
        ArrayList<DealerSelection> li = new ArrayList<>();
        NameValuePair<String, String> nvp;

        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String dealer = map.get(column1);
            String year = map.get(column2);
            String biannual = map.get(column3);
            String region = map.get(column4);
            String area = map.get(column5);
            String zone = map.get(column6);
            String dealergroup = map.get(column7);
            nvp = new NameValuePair(dealer, dealer);
            DealerSelection dealerparent = new DealerSelection();
            dealerparent.year = year;
            dealerparent.biannual = biannual;
            dealerparent.region = region;
            dealerparent.area = area;
            dealerparent.zone = zone;
            dealerparent.dealergroup = dealergroup;
            dealerparent.dealer = nvp;
            li.add(dealerparent);

        }
        System.out.println("city list");
        System.out.println(li);
        service.writeOutput(response, li);

    }

    public void getJDPAP(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getJDPAP");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "regiona";
        columnInfo.put(column1, column1);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        //System.out.println("query");
        //System.out.println(query);
        Iterator i = rows.iterator();
        ArrayList<NameValuePair> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String regiona = map.get(column1);
            String label = regiona.contains("Non") ? "Non-JDPAP" : "JDPAP";
            nvp = new NameValuePair(label, regiona);
            li.add(nvp);
        }
        service.writeOutput(response, li);

    }

    public void getTImeLine(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getTImeLine");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "value";
        columnInfo.put(column1, column1);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        //System.out.println("query");
        //System.out.println(query);
        Iterator i = rows.iterator();
        ArrayList<NameValuePair> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String regiona = map.get(column1);
            nvp = new NameValuePair(regiona, regiona);
            li.add(nvp);
        }
        service.writeOutput(response, li);

    }

    public void getFactor(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getFactor");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column = "factor";
        columnInfo.put(column, column);

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        System.out.println("factor ");
        System.out.println(query);
        Iterator i = rows.iterator();
        ArrayList<NameValuePair<String, String>> li = new ArrayList<>();
        NameValuePair<String, String> nvp;

        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String model = map.get(column);
            nvp = new NameValuePair(model, model);
            li.add(nvp);

        }

        service.writeOutput(response, li);

    }

    /*########### GENERATED ###########*/
    public void getCity(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getCity");
        System.out.println("getcity");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "city";
        final String column2 = "regiona";
        final String column3 = "region";
        columnInfo.put(column1, column1);
        columnInfo.put(column2, column2);
        columnInfo.put(column3, column3);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        //System.out.println(query + "::" + column2 + "::" + column1);
        Iterator i = rows.iterator();
        ArrayList<CitySelection> li = new ArrayList<>();
        NameValuePair<String, String> nvp;
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();
            String city = map.get(column1);
            String regiona = map.get(column2);
            String region = map.get(column3);
            nvp = new NameValuePair(city, city);
            CitySelection yq = new CitySelection();
            yq.regiona = regiona;
            yq.region = region;
            yq.city = nvp;
            li.add(yq);
        }
        service.writeOutput(response, li);

    }

    public void getDealerRegion(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        ArrayList<NameValuePair> li = new ArrayList<>();
        li.add(new NameValuePair("Region Comparison", "region-compare"));
        li.add(new NameValuePair("Zone Comparison", "zone-compare"));
        li.add(new NameValuePair("Dealer Comparison", "dealer-compare"));

        service.writeOutput(response, li);

    }

    public void getDealerRegionDealergroup(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        ArrayList<NameValuePair> li = new ArrayList<>();
        li.add(new NameValuePair("Region Comparison", "region-compare"));
        li.add(new NameValuePair("Dealer Comparison", "dealer-compare"));
        li.add(new NameValuePair("Dealer Group Comparison", "dealergroup-compare"));

        service.writeOutput(response, li);

    }

    public void getBenchmarks(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        ArrayList<NameValuePair> li = new ArrayList<>();
        li.add(new NameValuePair("Region Comparison", "region-compare"));
        li.add(new NameValuePair("Dealer Comparison", "dealer-compare"));
        li.add(new NameValuePair("Dealer Group Comparison", "dealergroup-compare"));

        service.writeOutput(response, li);

    }

    public void getDealerQuarter(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        ArrayList<NameValuePair> li = new ArrayList<>();
        li.add(new NameValuePair("Dealer Comparison", "dealer-compare"));
        //li.add(new NameValuePair("Quarter Comparison", "quarter-compare"));

        service.writeOutput(response, li);

    }

    public void getDealerRankComaprsionDealer(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        ArrayList<NameValuePair> li = new ArrayList<>();
        li.add(new NameValuePair("Study Total", "study-total"));
        li.add(new NameValuePair("Region Of The Dealer", "dealer-region"));
        //li.add(new NameValuePair("Quarter Comparison", "quarter-compare"));

        service.writeOutput(response, li);

    }

    public void getDealerRankComaprsionDealergroup(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        ArrayList<NameValuePair> li = new ArrayList<>();
        li.add(new NameValuePair("Study Total", "study-total"));
        li.add(new NameValuePair("Region Of The Dealer", "dealer-region"));
        li.add(new NameValuePair("Dealergroup", "Dealergroup"));
        //li.add(new NameValuePair("Quarter Comparison", "quarter-compare"));

        service.writeOutput(response, li);

    }

    public void getLoyalityComaprisionDealer(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        ArrayList<NameValuePair> li = new ArrayList<>();
        li.add(new NameValuePair("Study Total", "study-total"));
        li.add(new NameValuePair("Region Of The Dealer", "dealer-region"));
        li.add(new NameValuePair("Dealer", "dealer-dealer"));
        //li.add(new NameValuePair("Quarter Comparison", "quarter-compare"));

        service.writeOutput(response, li);

    }

    public void getLanguages(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        Map<String, String> map = new HashMap<String, String>();
        map.put("false", "Scope: Region");
        map.put("true", "Scope: Study");
        service.writeOutput(response, map);
        // service.writeOutput(response, li);

    }

    public void getScope(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        Map<String, String> map = new HashMap<String, String>();
        map.put("false", "Scope: Region");
        map.put("true", "Scope: Study");
        service.writeOutput(response, map);
        // service.writeOutput(response, li);

    }

    public void getBenchmark(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        ArrayList<NameValuePair> li = new ArrayList<>();
        li.add(new NameValuePair("Total", "Total"));
        li.add(new NameValuePair("Region", "Region"));
        service.writeOutput(response, li);
        // service.writeOutput(response, li);

    }

    public void getMeasure(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        ArrayList<NameValuePair> li = new ArrayList<>();

        Map<String, String> map = new HashMap<String, String>();
        map.put("false", "Measure: Best");
        map.put("true", "Measure: Average");
        service.writeOutput(response, map);
    }

    public void getDownloads(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiFilters-getDownloads");
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "period";
        final String column2 = "filename";
        columnInfo.put(column1, column1);
        columnInfo.put(column2, column2);

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        service.writeOutput(response, rows);

    }

    public void getDealershipWebisteCount(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String authHeader = request.getHeader(AUTHORIZATION);
        String query = service.getQuery("-CsiFilters-getDealershipWebisteCount");
        LISystemuserRecord userRecord = service.getUserOfSession(authHeader);
        String replace = "and dealer = \"" + userRecord.getFirstname() + "\"";
        query = query.replace("<<custom_dealer>>", replace);
        System.out.println("dealer ship count");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        final String column1 = "score";
        columnInfo.put(column1, column1);
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        service.writeOutput(response, cols);
    }
   

    class SingleParent {

        String year;
        NameValuePair quarter;

    }
  
    class TwoParent {

        String year;
        String biannual;
        NameValuePair month;
    }

    class ThreeParent {

        String year;
        String quarter;
        String month;
        NameValuePair biweekly;
    }

    class CitySelection {

        String regiona;
        String region;
        NameValuePair city;
    };

    class modelbrand {

       
        NameValuePair model;

    }

    class DealerSelection {

        String year;
         String biannual;
        String region;
        String area;
        String zone;
         String dealergroup;
        NameValuePair dealer;
    }

    class RegionSelection {

        String year;
        NameValuePair region;
    }

    class DealerGroupSelection {

        String year;//parent
        String biannual;
        String region; //parent
        String area;
        String zone;
        NameValuePair dealergroup; //year
    }
    class BiannualYear{

        String year;   
        NameValuePair biannual;

    }
    class RegionSelections {

        String year;
        String biannual;
        NameValuePair region;
    }
    class AreaSelection {
        String year; 
        String biannual;
        String region;
        NameValuePair area;
    }
    class ZoneSelection {
        String year;
        String biannual;
        String region;
        String area;
        NameValuePair zone;
    }
}

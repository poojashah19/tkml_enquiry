/*########### GENERATED AT 2018-05-24 10:29:31.478###########*/
package com.leadics.Toyota.webactions.oem;

import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.google.gson.Gson;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_FACTOR;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_LIMIT;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_MAX_LIMIT;
import static com.leadics.Toyota.common.Constants.ORDER_ASC;
import static com.leadics.Toyota.common.Constants.ORDER_DESC;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;

import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;
import java.util.Set;

/**
 *
 * @author HARSHA
 */
public class OemDealerSnapshot {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {

                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/
    public void getcsiandnps(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerSnapshot-getcsiandnps");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("biannual", "biannual");
        columnInfo.put("score", "score");
//        columnInfo.put("npsscore", "npsscore");
        System.out.println(query);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Map output = new HashMap();
        output.put("data", rows);

        service.writeOutput(response, output);

    }

    public void getDelighted(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerSnapshot-getDelighted");

        // get all the filter groups and their fields
        String factor = request.getParameter("factor");
        String factorReplacer = "", limitReplacer = "", orderReplacer = "", orderText = "";
        if (factor == null || factor.equalsIgnoreCase(ATTRIBUTE_FACTOR) || factor.equalsIgnoreCase("ALL")) {
            factorReplacer = "%";
            limitReplacer = ATTRIBUTE_LIMIT;
            orderReplacer = ORDER_DESC;
            orderText = "score";

        } else {
            factorReplacer = factor;
            orderReplacer = ORDER_ASC;
            limitReplacer = ATTRIBUTE_MAX_LIMIT;
            orderText = "AttributeIndex";
        }

        query = query.replaceAll("!FACTOR!", factorReplacer)
                .replaceAll("!LIMIT!", limitReplacer)
                .replaceAll("!ORDERLABEL!", orderText)
                .replaceAll("!ORDER!", orderReplacer);
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("delighted query");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("attribute", "attribute");
        columnInfo.put("score", "score");
        columnInfo.put("mean", "mean");
        columnInfo.put("second_score", "second_score");

        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }

    public void getTrendAnalysis(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerSnapshot-getTrendAnalysis");
        // get all the filter groups and their fields
        String timeLine = request.getParameter("timeline");
        if (timeLine.equalsIgnoreCase("yearly")) {
            query = service.getQuery("-CsiDealerSnapshot-getTrendAnalysis");
        } else if (timeLine.equalsIgnoreCase("monthly")) {
            query = service.getQuery("-CsiDealerSnapshot-getAttributeTrendByMonth");
        } else if (timeLine.equalsIgnoreCase("quarterly")) {
            query = service.getQuery("-CsiDealerSnapshot-getAttributeTrendByQuarter");
        } else if (timeLine.equalsIgnoreCase("Bi-Weekly")) {
            query = service.getQuery("-CsiDealerSnapshot-getAttributeTrendBiWeekly");
        } else if (timeLine.equalsIgnoreCase("Biannually")) {
            query = service.getQuery("-CsiDealerSnapshot-getAttributeTrendBiannually");
        }
        // get all the filter groups and their fields

        String factor = request.getParameter("factor");
        String factorReplacer = "", limitReplacer = "", orderReplacer = "";
        if (factor == null || factor.equalsIgnoreCase(ATTRIBUTE_FACTOR) || factor.equalsIgnoreCase("ALL")) {
            if (timeLine.equalsIgnoreCase("yearly")) {
                query = service.getQuery("-CsiDealerSnapshot-getIndexTrendAnalysis");
            } else if (timeLine.equalsIgnoreCase("monthly")) {
                query = service.getQuery("-CsiDealerSnapshot-getIndexAttributeTrendByMonth");
            } else if (timeLine.equalsIgnoreCase("quarterly")) {
                query = service.getQuery("-CsiDealerSnapshot-getIndexAttributeTrendByQuarter");
            } else if (timeLine.equalsIgnoreCase("Bi-Weekly")) {
                query = service.getQuery("-CsiDealerSnapshot-getIndexAttributeTrendBiWeekly");
            } else if (timeLine.equalsIgnoreCase("Biannually")) {
                query = service.getQuery("-CsiDealerSnapshot-getIndexAttributeTrendBiannually");
            }
            factorReplacer = "%";
            limitReplacer = ATTRIBUTE_LIMIT;
            limitReplacer = ATTRIBUTE_LIMIT;
            orderReplacer = ORDER_DESC;
        } else {
            factorReplacer = factor;
            limitReplacer = ATTRIBUTE_MAX_LIMIT;
            orderReplacer = ORDER_ASC;
        }
        System.out.println("orderReplacer");
        System.out.println(orderReplacer);
        query = query.replaceAll("!FACTOR!", factorReplacer)
                .replaceAll("!LIMIT!", limitReplacer)
                .replaceAll("!ORDER!", orderReplacer);
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("attribute");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("period", "period");
        columnInfo.put("attribute", "attribute");
        columnInfo.put("bestscore", "bestscore");
        columnInfo.put("score", "score");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        Set<String> categories = new LinkedHashSet<>();
        Map<String, List> series = new LinkedHashMap<>();

        Iterator i = rows.iterator();
        while (i.hasNext()) {
            HashMap<String, String> row = (HashMap<String, String>) i.next();
            String value = row.get("score");
            if (value == null) {
                value = "0.0";
            }
            Float score = Float.parseFloat(value);

            String attribute = row.get("attribute");
            String period = row.get("period");
            categories.add(period);
            List li = series.get(attribute);
            if (li == null) {
                li = new LinkedList<>();
            }
            li.add(score);
            series.put(attribute, li);
        };
        Map data = new HashMap<>();
        data.put("cat", categories);
        data.putAll(series);

        Map metaData = new HashMap();
        metaData.put("categories", "cat");
        metaData.put("seriesNames", series.keySet());

        Map output = new HashMap();
        output.put("data", data);
        output.put("metaData", metaData);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);
    }

    public void getTop5Bot5Kpis(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerSnapshot-getTop5Bot5Kpis");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("kpi top 5 query");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
//        columnInfo.put("Region", "Region");
        columnInfo.put("Attribute", "attribute");
        columnInfo.put("score", "score");
        columnInfo.put("top_or_bottom", "top_or_bottom");

        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
        service.writeOutput(response, output);

    }

    public void getFactorScores(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerSnapshot-getFactorScores");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        String bestString = "Best Overall";
        String worstString = "Worst Overall";
        bestString = service.getBestString(request);
        worstString = service.getWorstString(request);

        System.out.println("factor query ::");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("factor", "factor");
        columnInfo.put("score", "score");
        columnInfo.put("best", "best");
        columnInfo.put("worst", "worst");
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
        output.put("best", bestString);
        output.put("worst", worstString);

        service.writeOutput(response, output);

    }
    /*########### GENERATED ###########*/

}

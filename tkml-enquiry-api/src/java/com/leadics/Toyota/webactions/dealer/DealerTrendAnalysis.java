/*########### GENERATED AT 2018-06-15 12:32:17.219###########*/
package com.leadics.Toyota.webactions.dealer;

import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.google.gson.Gson;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_FACTOR;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_LIMIT;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_MAX_LIMIT;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;
import static com.leadics.Toyota.common.LIService.strWhereCluse;


import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;
import java.util.Set;

/**
 *
 * @author HARSHA
 */
public class DealerTrendAnalysis {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;
    

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {
                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/

    public void getFactorTrend(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerTrendAnalysis-getFactorTrend");
        // get all the filter groups and their fields
        String timeLine = request.getParameter("timeline");
        if (timeLine.equalsIgnoreCase("yearly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-getFactorTrend");
        } else if (timeLine.equalsIgnoreCase("monthly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-getFactorTrendByMonth");
        } else if (timeLine.equalsIgnoreCase("quarterly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-getFactorTrendByQuarter");
        } else if (timeLine.equalsIgnoreCase("Bi-Weekly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-getFactorTrendByBiWeekly");
        };
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Set<String> whereMaps = filtersMap.keySet();
        Iterator whereMapIterator = whereMaps.iterator();
        while (whereMapIterator.hasNext()) {
            String whereTemplate = (String) whereMapIterator.next();
            //System.out.println("filter");
            //System.out.println(whereTemplate);
        }
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("score trend");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("period", "period");
        columnInfo.put("csi_score", "csi_score");
        columnInfo.put("bestscore", "bestscore");
        columnInfo.put("worstscore", "worstscore");
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }

    public void getAttributeTrend(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerTrendAnalysis-getAttributeTrend");

        String timeLine = request.getParameter("timeline");

        if (timeLine.equalsIgnoreCase("yearly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-getAttributeTrend");
        } else if (timeLine.equalsIgnoreCase("monthly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-getAttributeTrendByMonth");
        } else if (timeLine.equalsIgnoreCase("quarterly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-getAttributeTrendByQuarter");
        } else if (timeLine.equalsIgnoreCase("Bi-Weekly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-getAttributeTrendBiWeekly");
        }
        // replace % is it is csi/ssi with limit or pass factor with max limit
        String factor = request.getParameter("factor");

        String factorReplacer = "", limitReplacer = "", dateOrder = "";
        if (factor == null || factor.equalsIgnoreCase(ATTRIBUTE_FACTOR) || factor.equalsIgnoreCase("ALL")) {

            if (timeLine.equalsIgnoreCase("yearly")) {
                query = service.getQuery("-CsiDealerTrendAnalysis-getIndexAttributeTrend");
            } else if (timeLine.equalsIgnoreCase("monthly")) {
                query = service.getQuery("-CsiDealerTrendAnalysis-getIndexAttributeTrendByMonth");
            } else if (timeLine.equalsIgnoreCase("quarterly")) {
                query = service.getQuery("-CsiDealerTrendAnalysis-getIndexAttributeTrendByQuarter");
            } else if (timeLine.equalsIgnoreCase("Bi-Weekly")) {
                query = service.getQuery("-CsiDealerTrendAnalysis-getIndexAttributeTrendBiWeekly");
            };
            factorReplacer = "%";
            limitReplacer = ATTRIBUTE_LIMIT;
            dateOrder = "DESC";

        } else {
            factorReplacer = factor;
            limitReplacer = ATTRIBUTE_MAX_LIMIT;
            dateOrder = "ASC";
        }

        query = query.replaceAll("!FACTOR!", factorReplacer)
                .replaceAll("!LIMIT!", limitReplacer)
                .replaceAll("!DATE_ORDER!", dateOrder);

        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("attribute");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("period", "period");
        columnInfo.put("attribute", "attribute");
        columnInfo.put("bestscore", "bestscore");
        columnInfo.put("score", "score");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        Set<String> categories = new LinkedHashSet<>();
        Map<String, List> series = new LinkedHashMap<>();

        Iterator i = rows.iterator();
        while (i.hasNext()) {
            LinkedHashMap<String, String> row = (LinkedHashMap<String, String>) i.next();

            String value = row.get("score");
            Float score = Float.parseFloat(value);
            String attribute = row.get("attribute");
            String period = row.get("period");
            categories.add(period);
            List li = series.get(attribute);
            if (li == null) {
                li = new LinkedList<>();
            }
            li.add(score);
            series.put(attribute, li);
        };
        Map data = new LinkedHashMap<>();
        data.put("cat", categories);
        data.putAll(series);

        Map metaData = new HashMap();
        metaData.put("categories", "cat");
        metaData.put("seriesNames", series.keySet());

        Map output = new HashMap();
        output.put("data", data);
        output.put("metaData", metaData);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

        ////////////////////////////////////
//        // get all the filter groups and their fields
//        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
//
//        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
//        // query replacement with the new query 
//        query = acknowledgmentMap.get(QUERY_TEMPLATE);
//        System.out.println("attribute");
//        System.out.println(query);
//        LinkedHashMap<String, String> columnInfo = new LinkedHashMap<>();
//        columnInfo.put("period", "period");
//        columnInfo.put("attribute", "attribute");
//        columnInfo.put("score", "score");
//        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
//        Set<String> categories = new LinkedHashSet<>();
//        Map<String, List> series = new LinkedHashMap<>();
//        Iterator i = rows.iterator();
//        while (i.hasNext()) {
//            LinkedHashMap<String, String> row = (LinkedHashMap<String, String>) i.next();
//            String datavalue = row.get("score");
//            System.out.println(datavalue);
//            if(datavalue==null){
//               datavalue =  "0.0";
//            }
//            Float score = Float.parseFloat(datavalue);
//            String attribute = row.get("attribute");
//            String period = row.get("period");
//            categories.add(period);
//            List li = series.get(attribute);
//            if (li == null) {
//                li = new LinkedList<>();
//            }
//            li.add(score);
//            series.put(attribute, li);
//        };
//        Map data = new LinkedHashMap<>();
//        data.put("cat", categories);
//        data.putAll(series);
//
//        Map metaData = new HashMap();
//        metaData.put("categories", "cat");
//        metaData.put("seriesNames", series.keySet());
//
//        Map output = new HashMap();
//        output.put("data", data);
//        output.put("metaData", metaData);
//        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
//        service.writeOutput(response, output);
    }

    ;

public void getScoreTrend(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerTrendAnalysis-getScoreTrend");
        // get all the filter groups and their fields
        String timeLine = request.getParameter("timeline");
        if (timeLine.equalsIgnoreCase("yearly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-getScoreTrend");
        } else if (timeLine.equalsIgnoreCase("monthly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-gietScoreTrendByMonth");
        } else if (timeLine.equalsIgnoreCase("quarterly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-getScoreTrendByQuarter");
        } else if (timeLine.equalsIgnoreCase("Bi-Weekly")) {
            query = service.getQuery("-CsiDealerTrendAnalysis-getScoreTrendByBiWeekly");
        }
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("trend score");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("period", "period");
        columnInfo.put("csi_score", "csi_score");
        columnInfo.put("bestscore", "bestscore");
        columnInfo.put("worstscore", "worstscore");
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }
    /*########### GENERATED ###########*/

}

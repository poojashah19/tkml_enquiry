/*########### GENERATED AT 2018-06-15 12:32:17.179###########*/
package com.leadics.Toyota.webactions.dealer;

import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.google.gson.Gson;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_FACTOR;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_LIMIT;
import static com.leadics.Toyota.common.Constants.ATTRIBUTE_MAX_LIMIT;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.strWhereCluse;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;


import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;

/**
 *
 * @author HARSHA
 */
public class DealerSummary {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;
    

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {
                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/
    public void getBenchMarkScores(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
//          String scope = request.getParameter("scope");
//          String measure = request.getParameter("measure");
//          System.out.println(scope);
        String query = service.getQuery("-CsiDealerSummary-getRegionBenchMarkScores");
        // get all the filter groups and their fields
//          if (scope.equalsIgnoreCase("false")&&measure.equalsIgnoreCase("false")) {
//                query = service.getQuery("-CsiDealerSummary-getRegionBestBenchMarkScores");
//            } 
//          else
//              if(scope.equalsIgnoreCase("true")&&measure.equalsIgnoreCase("false"))
//              {
//                 query = service.getQuery("-CsiDealerSummary-getBestBenchMarkScores");  
//              }
//          else
//              if(scope.equalsIgnoreCase("false")&&measure.equalsIgnoreCase("true")){
//               query = service.getQuery("-CsiDealerSummary-getRegionBenchMarkScores");
//          }
//          if(scope.equalsIgnoreCase("true")&&measure.equalsIgnoreCase("true")){
//        query = service.getQuery("-CsiDealerSummary-getBenchMarkScores");
//          }

        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("bench mark");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("biannual", "biannual");
        columnInfo.put("score", "score");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Map output = new HashMap();
        output.put("data", rows);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }

    public void getFactorScore(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerSummary-getFactorScore");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("factor scores");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("factor", "factor");
        columnInfo.put("score", "score");
        columnInfo.put("minscore", "minscore");
        columnInfo.put("maxscore", "maxscore");
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);

        service.writeOutput(response, output);

    }

    public void getTopBottomAttributes(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerSummary-getTopBottomAttributes");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("top 5 bottom 5");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("Attribute", "attribute");
        columnInfo.put("score", "score");
        columnInfo.put("top_or_bottom", "top_or_bottom");
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);

        service.writeOutput(response, output);
    }

    public void getCSIScore(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerSummary-getCSIScore");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("biannual", "biannual");
        columnInfo.put("score", "score");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Map output = new HashMap();
        output.put("data", rows);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);
    }

    public void getAttributeMeanScores(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerSummary-getAttributeMeanScores");
        // get all the filter groups and their fields

        String factor = request.getParameter("factor");
        String factorReplacer = "", limitReplacer = "";
         String bestString = "Best Overall";
        String worstString = "Worst Overall";
        bestString = service.getBestString(request);
        worstString = service.getWorstString(request);
        if (factor == null || factor.equalsIgnoreCase(ATTRIBUTE_FACTOR) || factor.equalsIgnoreCase("ALL")) {
            factorReplacer = "%";
            limitReplacer = ATTRIBUTE_LIMIT;
            limitReplacer = ATTRIBUTE_LIMIT;
        } else {
            factorReplacer = factor;
            limitReplacer = ATTRIBUTE_MAX_LIMIT;
        }
        query = query.replaceAll("!FACTOR!", factorReplacer)
                .replaceAll("!LIMIT!", limitReplacer);

        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("attribute", "attribute");
        columnInfo.put("score", "score");
        columnInfo.put("bestscore", "bestscore");
        columnInfo.put("worstscore", "worstscore");
        Map<String, Collection> cols = dao.loadValues(query, columnInfo);
        Map output = new HashMap();
        output.put("data", cols);
         output.put("best", bestString);
        output.put("worst", worstString);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }

    public void getTopBottomKPIS(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerSummary-getTopBottomKPIS");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);

        HashMap<String, String> columnInfo = new HashMap<>();
        //columnInfo.put("column-name-from-sql", "display-name-in-json-output");
        //columnInfo.put("column-name-from-sql", "display-name-in-json-output");

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Map output = new HashMap();
        output.put("data", rows);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }
    /*########### GENERATED ###########*/

}

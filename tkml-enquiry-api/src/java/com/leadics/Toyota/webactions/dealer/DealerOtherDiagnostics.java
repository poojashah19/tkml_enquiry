/*########### GENERATED AT 2018-06-15 12:32:17.230###########*/
package com.leadics.Toyota.webactions.dealer;

import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.NameValuePair;
import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIService;
import static com.leadics.Toyota.common.LIService.ACKNOWLEDGMENT;
import static com.leadics.Toyota.common.LIService.QUERY_TEMPLATE;
import static com.leadics.Toyota.common.LIService.strWhereCluse;


import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;
import java.util.Set;

/**
 *
 * @author HARSHA
 */
public class DealerOtherDiagnostics {

    LogUtils logger = new LogUtils(this.getClass().getName());
    LISystemuserRecord user;
    Gson gson;
    

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {
                user = gson.fromJson(request.getParameter("user"), LISystemuserRecord.class);
                LIWebsessionService service = new LIWebsessionService();
//                if (!service.isValidSession(user)) {
//                    out.println("-2");
//                    return;
//                }

                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);

                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    /*########### GENERATED ###########*/

    public void getTableData(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerOtherDiagnostics-getTableData");
        String dealer = request.getParameter("dealer");
        String city = request.getParameter("city");
        String region = request.getParameter("region");
        String heading1 = "Dealer Average";
        String heading2 = "Region Average";
        String heading3 = "Study Average";
        String heading4 = "Study Best";
        if (dealer != null && !dealer.equalsIgnoreCase("Study Total") && !dealer.equalsIgnoreCase("All")) {
            heading1 = "Dealer Average";
            heading2 = "Region Average";
            heading3 = "Study Average";
            heading4 = "Study Best";
        } else if (city != null && !city.equalsIgnoreCase("Study Total") && !city.equalsIgnoreCase("All")) {
            heading1 = "City Average";
            heading2 = "Region Average";
            heading3 = "Study Average";
            heading4 = "Study Best";
        } else if (region != null && !region.equalsIgnoreCase("Study Total") && !region.equalsIgnoreCase("All")) {
            heading1 = "Region Average";
            heading2 = "Region Average";
            heading3 = "Study Average";
            heading4 = "Study Best";;
        }
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("otherss table data");
        System.out.println(query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("attributename", "attributename");
        columnInfo.put("dealeraverage", "dealeraverage");
        columnInfo.put("regionaverage", "regionaverage");
        columnInfo.put("regionbest", "regionbest");
        columnInfo.put("studybest", "studybest");

        List<String> categories = Arrays.asList(heading1, heading2, heading3, heading4);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        NameValuePair<String, String> nvp;
        Map output = new HashMap();
        Iterator i = rows.iterator();
        while (i.hasNext()) {
            HashMap<String, String> map = (HashMap<String, String>) i.next();

            String attributename = map.get("attributename");
            attributename = attributename.replaceAll("’", "\'");
            attributename = attributename.replaceAll("’", "\'");
            attributename = attributename.replaceAll("“", "\"");
            attributename = attributename.replaceAll("”", "\"");
            // attributename = attributename.replaceAll("?","");
            map.put("attributename", attributename);
            String dealeraverage = map.get("dealeraverage");
            dealeraverage = dealeraverage.contains(".") || dealeraverage.equalsIgnoreCase("NA") ? dealeraverage : dealeraverage + '%';
            map.put("dealeraverage", dealeraverage);
            String regionaverage = map.get("regionaverage");
            regionaverage = regionaverage.contains(".") || regionaverage.equalsIgnoreCase("NA") ? regionaverage : regionaverage + '%';
            map.put("regionaverage", regionaverage);
            String regionbest = map.get("regionbest");
            regionbest = regionbest.contains(".") || regionbest.equalsIgnoreCase("NA") ? regionbest : regionbest + '%';
            map.put("regionbest", regionbest);
            String studybest = map.get("studybest");
            studybest = studybest.contains(".") || studybest.equalsIgnoreCase("NA") ? studybest : studybest + '%';
            map.put("studybest", studybest);
        }
        output.put("data", rows);
        output.put("categories", categories);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }

    public void getMostRecentServicePerformed(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerOtherDiagnostics-getMostRecentServicePerformed");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        System.out.println("get dealer questions");
        System.out.println(query);

        System.out.println("other charts");
        System.out.println(query);
        String dealer = request.getParameter("dealer");
        String city = request.getParameter("city");
        String region = request.getParameter("region");
        String heading1 = "Dealer Average";
        String heading2 = "Region Average";
        String heading3 = "Study Average";
        if (dealer != null && !dealer.equalsIgnoreCase("Study Total") && !dealer.equalsIgnoreCase("All")) {
            heading1 = "Dealer Average";
            heading2 = "Region Average";
            heading3 = "Study Average";
        } else if (city != null && !city.equalsIgnoreCase("Study Total") && !city.equalsIgnoreCase("All")) {
            heading1 = "City Average";
            heading2 = "Region Average";
            heading3 = "Study Average";
        } else if (region != null && !region.equalsIgnoreCase("Study Total") && !region.equalsIgnoreCase("All")) {
            heading1 = "Region Average";
            heading2 = "Region Average";
            heading3 = "Study Average";
        }
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("value", "value");
        columnInfo.put("dealerscore", "dealerscore");
        columnInfo.put("regionscore", "regionscore");
        columnInfo.put("studyscore", "studyscore");
        columnInfo.put("attributeName", "attributeName");
        Map<String, Object> finalOutputMap = new LinkedHashMap<>();
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Iterator i = rows.iterator();
        List<String> categories = Arrays.asList(heading1, heading2, heading3);
        Map<String, List> series = new HashMap<>();
        Map<String, List> attributeNameMap = new HashMap<>();

        while (i.hasNext()) {
            HashMap<String, String> row = (HashMap<String, String>) i.next();

            String value = row.get("dealerscore");

            Float dealerscore = Float.parseFloat(value);
            String regionscorevalue = row.get("regionscore");
            Float regionscore = Float.parseFloat(regionscorevalue);
            String studyscorevalue = row.get("studyscore");
            Float studyscore = Float.parseFloat(studyscorevalue);
            String attribute = row.get("value");
            String attributeName = row.get("attributeName");
            //String period = row.get("period");

            final String slugQuestionAttribute = attributeName + "<<>>" + attribute;

            series.put(slugQuestionAttribute, Arrays.asList(dealerscore, regionscore, studyscore));

            List attributeNameList = attributeNameMap.get(attributeName);
            if (attributeNameList == null) {
                attributeNameList = new LinkedList<>();
            }
            attributeNameList.add(attribute);

            attributeNameMap.put(attributeName, attributeNameList);

        }
        Set<String> keys = attributeNameMap.keySet();
        Iterator itr = keys.iterator();
        while (itr.hasNext()) {

            String attributeValue = (String) itr.next();
            Map metaData = new HashMap();
            metaData.put("categories", "cat");

            metaData.put("seriesNames", attributeNameMap.get(attributeValue));
            Map data = new HashMap();
            List attributes = attributeNameMap.get(attributeValue);
            Iterator listItr = attributes.iterator();

            HashMap attributeScore = new HashMap();
            while (listItr.hasNext()) {
                String seriesattributes = (String) listItr.next();
                String slugQuestionAttribute = attributeValue + "<<>>" + seriesattributes;
                attributeScore.put(seriesattributes, series.get(slugQuestionAttribute));
            }
            data.putAll(attributeScore);
            data.put("cat", categories);
            Map attributeMap = new HashMap();
            attributeMap.put("data", data);
            attributeMap.put("metaData", metaData);
            attributeMap.put("name", attributeValue);
            finalOutputMap.put(attributeValue, attributeMap);

        }
        Map outPut = new HashMap();

        outPut.put("data", finalOutputMap);

        outPut.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, outPut);

    }

    public void getReasonForNotDoing(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {

        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query = service.getQuery("-CsiDealerOtherDiagnostics-getReasonForNotDoing");
        // get all the filter groups and their fields
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);

        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        // query replacement with the new query 
        query = acknowledgmentMap.get(QUERY_TEMPLATE);

        HashMap<String, String> columnInfo = new HashMap<>();
        //columnInfo.put("column-name-from-sql", "display-name-in-json-output");
        //columnInfo.put("column-name-from-sql", "display-name-in-json-output");

        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Map output = new HashMap();
        output.put("data", rows);
        output.put(ACKNOWLEDGMENT, acknowledgmentMap);
        service.writeOutput(response, output);

    }
    /*########### GENERATED ###########*/

}

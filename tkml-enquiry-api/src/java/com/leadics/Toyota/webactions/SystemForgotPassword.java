/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.webactions;

import com.google.gson.Gson;
import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.common.LIService;
import com.leadics.Toyota.controller.LISystemuserController;
import com.leadics.Toyota.dao.LIReportquiresDAO;
import com.leadics.Toyota.service.LIAuditlogService;
import com.leadics.Toyota.service.LIFailedloginService;
import com.leadics.Toyota.service.LIForgotpwdrequestService;
import com.leadics.Toyota.service.LIMaillogService;
import com.leadics.Toyota.service.LIOtplogService;
import com.leadics.Toyota.service.LISystemuserService;
import com.leadics.Toyota.service.LIWebaccesslogService;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.service.SendMailService;
import com.leadics.Toyota.service.SendSMSService;
import com.leadics.Toyota.to.LIAuditlogRecord;
import com.leadics.Toyota.to.LIFailedloginRecord;
import com.leadics.Toyota.to.LIForgotpwdrequestRecord;
import com.leadics.Toyota.to.LIMaillogRecord;
import com.leadics.Toyota.to.LIOtplogRecord;
import com.leadics.Toyota.to.LIReportquiresRecord;
import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.to.LIWebaccesslogRecord;
import com.leadics.Toyota.to.LIWebsessionRecord;
import com.leadics.utils.LISecurityUtil;
import com.leadics.utils.LIStatusConstants;
import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.InetAddress;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.PropertyUtils;

/**
 *
 * @author varma.sagi
 */
public class SystemForgotPassword extends LISystemuserController {

    static LogUtils logger = new LogUtils(SystemForgotPassword.class.getName());
    public static final String FORGOT_URL = "http://localhost:4200/#/leadics/forgot/";
    

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                super.processWebRequest(request, response, actionType);

                if (!StringUtils.isNullOrEmpty(actionType)) {

                    Method method = this.getClass().getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class);
                    method.invoke(this, request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void changePassword(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ResourceBundle rb = ResourceBundle.getBundle("Toyota", Locale.getDefault());
        LISystemuserRecord userRecord = new LISystemuserRecord();
        LISystemuserService userService = new LISystemuserService();
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String newPassword = request.getParameter("newPassword");
        String user_id = request.getParameter("user_id");
        String token = request.getParameter("token");

        //System.out.println(newPassword);
        userRecord.setUserpassword(newPassword);
        userRecord.setUserid(user_id);

        String updateTokenStatusQuery = " update forgot_pwd_request set use_status=1 where user_id = \"" + user_id + "\" and "
                + "request_id =\"" + token + "\" ; ";
        dao.executeUpdateQuery(updateTokenStatusQuery);

        boolean result = userService.updateLISystemuserRecord(userRecord);
        HashMap jsonOutput = new HashMap();
        if (!result) {
            jsonOutput.put("status", LIStatusConstants.SUCCESS.getValue());
        } else {
            jsonOutput.put("status", LIStatusConstants.FAIL.getValue());

        }

        service.writeOutput(response, jsonOutput);

    }

    private boolean isBlocked(HttpServletRequest request, HttpServletResponse response) throws Exception {
        boolean flag = false;
        String userID = request.getParameter("user_id");
        String query = "select RSTATUS FROM `system_user` where user_id = \"" + userID + "\"";
        LIDAO dao = new LIDAO();
        String result = dao.loadString(query);
        if (result == null) {

            return false;
        }
        return result.contentEquals("0");
    }

    public void requestPasswordChange(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String user_id = request.getParameter("user_id");
        LISystemuserService userService = new LISystemuserService();
        LIService service = new LIService();
        LIForgotpwdrequestRecord forgotRecord = new LIForgotpwdrequestRecord();
        LIForgotpwdrequestService forgotService = new LIForgotpwdrequestService();

        LISystemuserRecord userRecord = userService.loadLISystemuserRecord(user_id);
        HashMap jsonOutput = new HashMap();

        if (isBlocked(request, response)) {
            jsonOutput.put("status", LIStatusConstants.USER_BLOCKED.getValue());
            service.writeOutput(response, jsonOutput);
            return;
        }
        String unique = service.getUniqueString();
        forgotRecord.setUserid(user_id);
        forgotRecord.setCreatedat(service.getCurrentTimeStamp());
        forgotRecord.setRequestid(unique);
        int status = forgotService.insertLIForgotpwdrequestRecord(forgotRecord);
        if (status > 0) {
            this.sendMail(userRecord, unique);
            jsonOutput.put("status", LIStatusConstants.SUCCESS.getValue());
        } else {
            jsonOutput.put("status", LIStatusConstants.ERROR.getValue());

        }
        service.writeOutput(response, jsonOutput);

    }

    private void sendMail(LISystemuserRecord record, String unique) throws Exception {
        LIMaillogRecord mailLogRecord = new LIMaillogRecord();
        LIMaillogService mailLogService = new LIMaillogService();
        String body = "Please click the following link to rest your password<br>"
                + FORGOT_URL + unique;
        String email = record.getEmail();
        HashMap inputMap = new HashMap();

        inputMap.put("MailMessage", body);

        inputMap.put("ToAddress", email);
        inputMap.put("Subject", "Leadics Password change request :  ");

        SendMailService mailService = new SendMailService();
        HashMap map = mailService.sendMail(inputMap);
        String response = map.get("message").toString();

        mailLogRecord.setToids(email);
        mailLogRecord.setMailmessage(body);
        mailLogRecord.setRespoinsemessage(response);
        mailLogService.insertLIMaillogRecord(mailLogRecord);

    }

    public void isTokenValid(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String token = request.getParameter("token");
        String user_id = request.getParameter("user_id");
        ResourceBundle rb = ResourceBundle.getBundle("Toyota");
        LIService service = new LIService();
        HashMap jsonOutput = new HashMap();
        String forgotExpireSeconds = rb.getString("forgotExpireSeconds");
        String query = "select id , timestampdiff(second,created_at,now()) as timeDiff from forgot_pwd_request where user_id = \"" + user_id + "\" "
                + "and request_id = \"" + token + "\" and use_status is null ;";
        LIDAO dao = new LIDAO();
        HashMap keys = new HashMap();
        keys.put("id", "id");
        keys.put("timeDiff", "timeDiff");
        List<HashMap<String, String>> result = dao.loadValuesRowWise(query, keys);
        try {
            String id = result.get(0).get("id").toString();
            String timeDiff = result.get(0).get("timeDiff").toString();

            if (!StringUtils.isNullOrEmpty(id) && Long.parseLong(timeDiff) <= Long.parseLong(forgotExpireSeconds)) {
                jsonOutput.put("status", LIStatusConstants.VALID.getValue());
            } else if (!StringUtils.isNullOrEmpty(id)) {
                jsonOutput.put("status", LIStatusConstants.EXPIRED.getValue());
            } else {
                jsonOutput.put("status", LIStatusConstants.INVALID.getValue());
            }
        } catch (Exception e) {
            jsonOutput.put("status", LIStatusConstants.INVALID.getValue());
        }
        service.writeOutput(response, jsonOutput);
    }
}


/*
 * LIOthersdistributionaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIOthersdistributionaggregateRecord;
import com.leadics.Toyota.dao.LIOthersdistributionaggregateDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIOthersdistributionaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LIOthersdistributionaggregateService.class.getName());


	public LIOthersdistributionaggregateRecord[] loadLIOthersdistributionaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOthersdistributionaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOthersdistributionaggregateRecords", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			LIOthersdistributionaggregateRecord[] results = dao.loadLIOthersdistributionaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIOthersdistributionaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOthersdistributionaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIOthersdistributionaggregateRecord loadFirstLIOthersdistributionaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIOthersdistributionaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIOthersdistributionaggregateRecord", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			LIOthersdistributionaggregateRecord result = dao.loadFirstLIOthersdistributionaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIOthersdistributionaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOthersdistributionaggregateRecord searchFirstLIOthersdistributionaggregateRecord(LIOthersdistributionaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIOthersdistributionaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIOthersdistributionaggregateRecord", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			LIOthersdistributionaggregateRecord[] records = dao.searchLIOthersdistributionaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIOthersdistributionaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIOthersdistributionaggregateRecord searchFirstLIOthersdistributionaggregateRecordExactUpper(LIOthersdistributionaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIOthersdistributionaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIOthersdistributionaggregateRecordsExactUpper", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			LIOthersdistributionaggregateRecord[] records = dao.searchLIOthersdistributionaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIOthersdistributionaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOthersdistributionaggregateRecord[] searchLIOthersdistributionaggregateRecords(LIOthersdistributionaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIOthersdistributionaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOthersdistributionaggregateRecords", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			LIOthersdistributionaggregateRecord[] records = dao.searchLIOthersdistributionaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIOthersdistributionaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIOthersdistributionaggregateRecordCount(LIOthersdistributionaggregateRecord record)
	throws Exception
	{
		return loadLIOthersdistributionaggregateRecordCount(record, null);
	}


	public int loadLIOthersdistributionaggregateRecordCount(LIOthersdistributionaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOthersdistributionaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOthersdistributionaggregateRecordCount", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIOthersdistributionaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOthersdistributionaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOthersdistributionaggregateRecord loadLIOthersdistributionaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOthersdistributionaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOthersdistributionaggregateRecord", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			LIOthersdistributionaggregateRecord result = dao.loadLIOthersdistributionaggregateRecord(key);
			logger.trace("loadLIOthersdistributionaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOthersdistributionaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIOthersdistributionaggregateRecordSearchResultByPage(LIOthersdistributionaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIOthersdistributionaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIOthersdistributionaggregateRecordSearchResultByPage(LIOthersdistributionaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIOthersdistributionaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIOthersdistributionaggregateRecordSearchResult", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIOthersdistributionaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIOthersdistributionaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIOthersdistributionaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIOthersdistributionaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIOthersdistributionaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIOthersdistributionaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIOthersdistributionaggregateRecordSearchResultByPageQuery", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIOthersdistributionaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIOthersdistributionaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIOthersdistributionaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIOthersdistributionaggregateRecord(LIOthersdistributionaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIOthersdistributionaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIOthersdistributionaggregateRecord", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			int result = dao.insertLIOthersdistributionaggregateRecord(record);
			logger.trace("insertLIOthersdistributionaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIOthersdistributionaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIOthersdistributionaggregateRecord(LIOthersdistributionaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIOthersdistributionaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIOthersdistributionaggregateRecord", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			boolean result = dao.updateLIOthersdistributionaggregateRecord(record);
			logger.trace("updateLIOthersdistributionaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIOthersdistributionaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIOthersdistributionaggregateRecordNonNull(LIOthersdistributionaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIOthersdistributionaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIOthersdistributionaggregateRecordNonNull", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			LIOthersdistributionaggregateRecord dbRecord = dao.loadLIOthersdistributionaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIOthersdistributionaggregateRecord(dbRecord);
			logger.trace("updateLIOthersdistributionaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIOthersdistributionaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIOthersdistributionaggregateRecord(LIOthersdistributionaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIOthersdistributionaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIOthersdistributionaggregateRecord", null);
			LIOthersdistributionaggregateDAO dao = new LIOthersdistributionaggregateDAO();
			boolean result = dao.deleteLIOthersdistributionaggregateRecord(record);
			logger.trace("deleteLIOthersdistributionaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIOthersdistributionaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}


/*
 * LIDrilldownmainaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIDrilldownmainaggregateRecord;
import com.leadics.Toyota.dao.LIDrilldownmainaggregateDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIDrilldownmainaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LIDrilldownmainaggregateService.class.getName());


	public LIDrilldownmainaggregateRecord[] loadLIDrilldownmainaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIDrilldownmainaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIDrilldownmainaggregateRecords", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			LIDrilldownmainaggregateRecord[] results = dao.loadLIDrilldownmainaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIDrilldownmainaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIDrilldownmainaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIDrilldownmainaggregateRecord loadFirstLIDrilldownmainaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIDrilldownmainaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIDrilldownmainaggregateRecord", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			LIDrilldownmainaggregateRecord result = dao.loadFirstLIDrilldownmainaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIDrilldownmainaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIDrilldownmainaggregateRecord searchFirstLIDrilldownmainaggregateRecord(LIDrilldownmainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIDrilldownmainaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIDrilldownmainaggregateRecord", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			LIDrilldownmainaggregateRecord[] records = dao.searchLIDrilldownmainaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIDrilldownmainaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIDrilldownmainaggregateRecord searchFirstLIDrilldownmainaggregateRecordExactUpper(LIDrilldownmainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIDrilldownmainaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIDrilldownmainaggregateRecordsExactUpper", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			LIDrilldownmainaggregateRecord[] records = dao.searchLIDrilldownmainaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIDrilldownmainaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIDrilldownmainaggregateRecord[] searchLIDrilldownmainaggregateRecords(LIDrilldownmainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIDrilldownmainaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIDrilldownmainaggregateRecords", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			LIDrilldownmainaggregateRecord[] records = dao.searchLIDrilldownmainaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIDrilldownmainaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIDrilldownmainaggregateRecordCount(LIDrilldownmainaggregateRecord record)
	throws Exception
	{
		return loadLIDrilldownmainaggregateRecordCount(record, null);
	}


	public int loadLIDrilldownmainaggregateRecordCount(LIDrilldownmainaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIDrilldownmainaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIDrilldownmainaggregateRecordCount", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIDrilldownmainaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIDrilldownmainaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIDrilldownmainaggregateRecord loadLIDrilldownmainaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIDrilldownmainaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIDrilldownmainaggregateRecord", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			LIDrilldownmainaggregateRecord result = dao.loadLIDrilldownmainaggregateRecord(key);
			logger.trace("loadLIDrilldownmainaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIDrilldownmainaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIDrilldownmainaggregateRecordSearchResultByPage(LIDrilldownmainaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIDrilldownmainaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIDrilldownmainaggregateRecordSearchResultByPage(LIDrilldownmainaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIDrilldownmainaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIDrilldownmainaggregateRecordSearchResult", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIDrilldownmainaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIDrilldownmainaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIDrilldownmainaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIDrilldownmainaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIDrilldownmainaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIDrilldownmainaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIDrilldownmainaggregateRecordSearchResultByPageQuery", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIDrilldownmainaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIDrilldownmainaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIDrilldownmainaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIDrilldownmainaggregateRecord(LIDrilldownmainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIDrilldownmainaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIDrilldownmainaggregateRecord", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			int result = dao.insertLIDrilldownmainaggregateRecord(record);
			logger.trace("insertLIDrilldownmainaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIDrilldownmainaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIDrilldownmainaggregateRecord(LIDrilldownmainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIDrilldownmainaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIDrilldownmainaggregateRecord", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			boolean result = dao.updateLIDrilldownmainaggregateRecord(record);
			logger.trace("updateLIDrilldownmainaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIDrilldownmainaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIDrilldownmainaggregateRecordNonNull(LIDrilldownmainaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIDrilldownmainaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIDrilldownmainaggregateRecordNonNull", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			LIDrilldownmainaggregateRecord dbRecord = dao.loadLIDrilldownmainaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIDrilldownmainaggregateRecord(dbRecord);
			logger.trace("updateLIDrilldownmainaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIDrilldownmainaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIDrilldownmainaggregateRecord(LIDrilldownmainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIDrilldownmainaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIDrilldownmainaggregateRecord", null);
			LIDrilldownmainaggregateDAO dao = new LIDrilldownmainaggregateDAO();
			boolean result = dao.deleteLIDrilldownmainaggregateRecord(record);
			logger.trace("deleteLIDrilldownmainaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIDrilldownmainaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}


/*
 * LILipermissionsService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LILipermissionsRecord;
import com.leadics.Toyota.dao.LILipermissionsDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LILipermissionsService extends LIService
{
	static LogUtils logger = new LogUtils(LILipermissionsService.class.getName());


	public LILipermissionsRecord[] loadLILipermissionsRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLILipermissionsRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILipermissionsRecords", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord[] results = dao.loadLILipermissionsRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLILipermissionsRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLILipermissionsRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LILipermissionsRecord loadFirstLILipermissionsRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLILipermissionsRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLILipermissionsRecord", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord result = dao.loadFirstLILipermissionsRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLILipermissionsRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LILipermissionsRecord searchFirstLILipermissionsRecord(LILipermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLILipermissionsRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLILipermissionsRecord", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord[] records = dao.searchLILipermissionsRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLILipermissionsRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LILipermissionsRecord searchFirstLILipermissionsRecordExactUpper(LILipermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLILipermissionsRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLILipermissionsRecordsExactUpper", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord[] records = dao.searchLILipermissionsRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLILipermissionsRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LILipermissionsRecord[] searchLILipermissionsRecords(LILipermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLILipermissionsRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILipermissionsRecords", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord[] records = dao.searchLILipermissionsRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLILipermissionsRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLILipermissionsRecordCount(LILipermissionsRecord record)
	throws Exception
	{
		return loadLILipermissionsRecordCount(record, null);
	}


	public int loadLILipermissionsRecordCount(LILipermissionsRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLILipermissionsRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILipermissionsRecordCount", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLILipermissionsRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLILipermissionsRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LILipermissionsRecord loadLILipermissionsRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLILipermissionsRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILipermissionsRecord", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord result = dao.loadLILipermissionsRecord(key);
			logger.trace("loadLILipermissionsRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLILipermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLILipermissionsRecordSearchResultByPage(LILipermissionsRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLILipermissionsRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLILipermissionsRecordSearchResultByPage(LILipermissionsRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLILipermissionsRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLILipermissionsRecordSearchResult", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLILipermissionsRecordCount(record);
			dao.setLimits(offset, maxrows);
			LILipermissionsRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLILipermissionsRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLILipermissionsRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLILipermissionsRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLILipermissionsRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLILipermissionsRecordSearchResultByPageQuery", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LILipermissionsRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLILipermissionsRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLILipermissionsRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLILipermissionsRecord(LILipermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLILipermissionsRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLILipermissionsRecord", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			int result = dao.insertLILipermissionsRecord(record);
			logger.trace("insertLILipermissionsRecord:Result:" + result);			
			createMakerCheckerAuditEntry("li_permissions",result + "", "Create",record.getCreatedby(),"Created");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLILipermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLILipermissionsRecord(LILipermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLILipermissionsRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLILipermissionsRecord", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			boolean result = dao.updateLILipermissionsRecord(record);
			createMakerCheckerAuditEntry("li_permissions",record.getId(), "Update",record.getModifiedby(),"Update");
			logger.trace("updateLILipermissionsRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLILipermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLILipermissionsRecordNonNull(LILipermissionsRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLILipermissionsRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLILipermissionsRecordNonNull", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord dbRecord = dao.loadLILipermissionsRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLILipermissionsRecord(dbRecord);
			createMakerCheckerAuditEntry("li_permissions",inputRecord.getId(), "Update",dbRecord.getModifiedby(),"Update");
			logger.trace("updateLILipermissionsRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLILipermissionsRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLILipermissionsRecord(LILipermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLILipermissionsRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLILipermissionsRecord", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			boolean result = dao.deleteLILipermissionsRecord(record);
			logger.trace("deleteLILipermissionsRecord:Result:" + result);			
			createMakerCheckerAuditEntry("li_permissions",record.getId(), "Delete",null,"Deleted");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLILipermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean approveLILipermissionsRecord(LILipermissionsRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("approveLILipermissionsRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("approveLILipermissionsRecord", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord updateRecord = dao.loadLILipermissionsRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CA-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CA-IS101:Cannot approve - Invalid Status for Approval");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CA-MCS102:Cannot Approve. Maker Checker cannot be same");
			}
			updateRecord.setRstatus("1");
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("1");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLILipermissionsRecord(updateRecord);
			logger.trace("approveLILipermissionsRecord:Result:" + result);			
			createMakerCheckerAuditEntry("li_permissions",updateRecord.getId(), "Approve",updateId,comment);
			HashMap approveEventMap = callActionEvent("Event_OnApprovalOf_LILipermissionsRecord", updateRecord.getId());
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("approveLILipermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean submitLILipermissionsRecord(LILipermissionsRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("submitLILipermissionsRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("submitLILipermissionsRecordNonNull", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord updateRecord = dao.loadLILipermissionsRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CS-RNFA104:Record not found");
			}
			if (!isValidStatusForSubmission(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CS-IS101:Cannot submit - Invalid Status for Submission");
			}
			updateRecord.setRstatus("1");
			updateRecord.setMakerlastcmt(comment);
			updateRecord.setCurrappstatus("4");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLILipermissionsRecord(updateRecord);
			createMakerCheckerAuditEntry("li_permissions",updateRecord.getId(),"Submit",updateId,comment);
			HashMap submitEventMap = callActionEvent("Event_OnSubmitOf_LILipermissionsRecord", updateRecord.getId());
			logger.trace("submitLILipermissionsRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("submitLILipermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean denyLILipermissionsRecord(LILipermissionsRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyLILipermissionsRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyLILipermissionsRecord", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord updateRecord = dao.loadLILipermissionsRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFA104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-IS101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("5");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLILipermissionsRecord(updateRecord);
			createMakerCheckerAuditEntry("li_permissions",updateRecord.getId(),"Deny",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyOf_LILipermissionsRecord", updateRecord.getId());
			logger.trace("denyLILipermissionsRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyLILipermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}
	public boolean denyPermanantlyLILipermissionsRecord(LILipermissionsRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyPermanantlyLILipermissionsRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyPermanantlyLILipermissionsRecord", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord updateRecord = dao.loadLILipermissionsRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFADP104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-ISDP101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101DP:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("58");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLILipermissionsRecord(updateRecord);
			createMakerCheckerAuditEntry("li_permissions",updateRecord.getId(),"DenyP",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyPOf_LILipermissionsRecord", updateRecord.getId());
			logger.trace("denyPLILipermissionsRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyPLILipermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean remindApprovalLILipermissionsRecord(LILipermissionsRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("remindApprovalLILipermissionsRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("remindApprovalLILipermissionsRecord", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord updateRecord = dao.loadLILipermissionsRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CR-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CR-IS101:Cannot Remind - Invalid Status for Reminder");
			}
			createMakerCheckerAuditEntry("li_permissions",updateRecord.getId(), "Approval Reminder",updateId,comment);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return true;
		}
		catch(Exception exception)
		{
			logger.error("remindApprovalLILipermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean resetApprovalLILipermissionsRecord(LILipermissionsRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("resetApprovalLILipermissionsRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("resetApprovalLILipermissionsRecordNonNull", null);
			LILipermissionsDAO dao = new LILipermissionsDAO();
			LILipermissionsRecord updateRecord = dao.loadLILipermissionsRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-RA-RNFA104:Record not found");
			}
			if (!isValidStatusForReset(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-RA-IS101:Cannot submit - Invalid Status for Reset");
			}
			updateRecord.setRstatus("0");
			updateRecord.setAdminlastcmt(comment);
			updateRecord.setCurrappstatus("0");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLILipermissionsRecord(updateRecord);
			createMakerCheckerAuditEntry("li_permissions",updateRecord.getId(),"Reset Approval",updateId,comment);
			logger.trace("resetApprovalLILipermissionsRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("resetApprovalLILipermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}

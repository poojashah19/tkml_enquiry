
/*
 * LISystemuserService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;

import com.leadics.Toyota.to.LISystemuserRecord;
import com.leadics.Toyota.dao.LISystemuserDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;

public class LISystemuserService extends LIService {

    static LogUtils logger = new LogUtils(LISystemuserService.class.getName());

    public LISystemuserRecord[] loadLISystemuserRecords(String query)
            throws Exception {
        try {
            logger.trace("loadLISystemuserRecords:" + query);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISystemuserRecords", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord[] results = dao.loadLISystemuserRecords(query);
            int resultRecordCount = 0;
            if (results != null) {
                resultRecordCount = results.length;
            }
            logger.trace("loadLISystemuserRecords:Fetched" + resultRecordCount);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return results;
        } catch (Exception exception) {
            logger.error("loadLISystemuserRecords" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LISystemuserRecord loadFirstLISystemuserRecord(String query)
            throws Exception {
        try {
            logger.trace("loadFirstLISystemuserRecord:" + query);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLISystemuserRecord", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord result = dao.loadFirstLISystemuserRecord(query);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("loadFirstLISystemuserRecord" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LISystemuserRecord searchFirstLISystemuserRecord(LISystemuserRecord record)
            throws Exception {
        try {
            logger.trace("searchFirstLISystemuserRecords:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISystemuserRecord", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord[] records = dao.searchLISystemuserRecords(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            if (records == null) {
                return null;
            }
            if (records.length < 1) {
                return null;
            }
            return records[0];
        } catch (Exception exception) {
            logger.error("searchFirstLISystemuserRecords" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LISystemuserRecord searchFirstLISystemuserRecordExactUpper(LISystemuserRecord record)
            throws Exception {
        try {
            logger.trace("searchFirstLISystemuserRecordsExactUpper:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISystemuserRecordsExactUpper", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord[] records = dao.searchLISystemuserRecordsExactUpper(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            if (records == null) {
                return null;
            }
            if (records.length < 1) {
                return null;
            }
            return records[0];
        } catch (Exception exception) {
            logger.error("searchFirstLISystemuserRecordsExactUpper" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LISystemuserRecord[] searchLISystemuserRecords(LISystemuserRecord record)
            throws Exception {
        try {
            logger.trace("searchLISystemuserRecords:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISystemuserRecords", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord[] records = dao.searchLISystemuserRecords(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return records;
        } catch (Exception exception) {
            logger.error("searchLISystemuserRecords" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public int loadLISystemuserRecordCount(LISystemuserRecord record)
            throws Exception {
        return loadLISystemuserRecordCount(record, null);
    }

    public int loadLISystemuserRecordCount(LISystemuserRecord record, String customCondition)
            throws Exception {
        try {
            logger.trace("loadLISystemuserRecordCount:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISystemuserRecordCount", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            dao.setCustomCondition(customCondition);
            int resultcount = dao.loadLISystemuserRecordCount(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return resultcount;
        } catch (Exception exception) {
            logger.error("loadLISystemuserRecordCount" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LISystemuserRecord loadLISystemuserRecord(String key)
            throws Exception {
        try {
            logger.trace("loadLISystemuserRecord:" + key);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISystemuserRecord", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord result = dao.loadLISystemuserRecord(key);
            logger.trace("loadLISystemuserRecord:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("loadLISystemuserRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public JSONObject getJSONLISystemuserRecordSearchResultByPage(LISystemuserRecord record, String offset, String maxrows, String orderBy)
            throws Exception {
        return getJSONLISystemuserRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
    }

    public JSONObject getJSONLISystemuserRecordSearchResultByPage(LISystemuserRecord record, String offset, String maxrows, String orderBy, String customCondition)
            throws Exception {
        try {
            logger.trace("getJSONLISystemuserRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISystemuserRecordSearchResult", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            dao.setCustomCondition(customCondition);
            int totalCount = dao.loadLISystemuserRecordCount(record);
            dao.setLimits(offset, maxrows);
            LISystemuserRecord[] records = null;
            if (totalCount > 0) {
                dao.setOrderBy(orderBy);
                records = dao.searchLISystemuserRecords(record);
            }
            JSONObject resultObject = new JSONObject();
            resultObject.put("total", totalCount + "");
            JSONArray dataArray = new JSONArray();
            int recordCount = 0;
            if (records != null) {
                recordCount = records.length;
            }
            for (int index = 0; index < recordCount; index++) {
                dataArray.add(records[index].getJSONObjectUI());
            }
            resultObject.put("rows", dataArray);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return resultObject;
        } catch (Exception exception) {
            logger.error("getJSONLISystemuserRecordSearchResult" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public JSONObject getJSONLISystemuserRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
            throws Exception {
        try {
            logger.trace("getJSONLISystemuserRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISystemuserRecordSearchResultByPageQuery", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            int totalCount = dao.loadRecordCount(con, countQuery);
            dao.setLimits(offset, maxrows);
            LISystemuserRecord[] records = null;
            if (totalCount > 0) {
                records = dao.loadLISystemuserRecords(query, con, true);
            }
            JSONObject resultObject = new JSONObject();
            resultObject.put("total", totalCount + "");
            JSONArray dataArray = new JSONArray();
            int recordCount = 0;
            if (records != null) {
                recordCount = records.length;
            }
            for (int index = 0; index < recordCount; index++) {
                dataArray.add(records[index].getJSONObjectUI());
            }
            resultObject.put("rows", dataArray);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return resultObject;
        } catch (Exception exception) {
            logger.error("getJSONLISystemuserRecordSearchResultByPageQuery" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public int insertLISystemuserRecord(LISystemuserRecord record)
            throws Exception {
        try {
            logger.trace("insertLISystemuserRecord:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLISystemuserRecord", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            int result = dao.insertLISystemuserRecord(record);
            logger.trace("insertLISystemuserRecord:Result:" + result);
            createMakerCheckerAuditEntry("system_user", result + "", "Create", record.getCreatedby(), "Created");
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("insertLISystemuserRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean updateLISystemuserRecord(LISystemuserRecord record)
            throws Exception {
        try {
            logger.trace("updateLISystemuserRecord:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISystemuserRecord", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            boolean result = dao.updateLISystemuserRecord(record);
            createMakerCheckerAuditEntry("system_user", record.getId(), "Update", record.getModifiedby(), "Update");
            logger.trace("updateLISystemuserRecord:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("updateLISystemuserRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean updateLISystemuserRecordNonNull(LISystemuserRecord inputRecord)
            throws Exception {
        try {
            logger.trace("updateLISystemuserRecordNonNull:" + inputRecord);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISystemuserRecordNonNull", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord dbRecord = dao.loadLISystemuserRecord(inputRecord.getId());
            if (dbRecord == null) {
                throw new Exception("Record not found");
            }
            dbRecord.loadNonNullContent(inputRecord);
            dbRecord.setActionSource(inputRecord.getActionSource());
            boolean result = dao.updateLISystemuserRecord(dbRecord);
            createMakerCheckerAuditEntry("system_user", inputRecord.getId(), "Update", dbRecord.getModifiedby(), "Update");
            logger.trace("updateLISystemuserRecordNonNull:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("updateLISystemuserRecordNonNull" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean deleteLISystemuserRecord(LISystemuserRecord record)
            throws Exception {
        try {
            logger.trace("deleteLISystemuserRecord:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLISystemuserRecord", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            boolean result = dao.deleteLISystemuserRecord(record);
            logger.trace("deleteLISystemuserRecord:Result:" + result);
            createMakerCheckerAuditEntry("system_user", record.getId(), "Delete", null, "Deleted");
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("deleteLISystemuserRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean approveLISystemuserRecord(LISystemuserRecord inputRecord, String updateId, String comment)
            throws Exception {
        try {
            logger.trace("approveLISystemuserRecord:" + inputRecord);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("approveLISystemuserRecord", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord updateRecord = dao.loadLISystemuserRecord(inputRecord.getId());
            if (updateRecord == null) {
                throw new Exception("MF-ERR-CA-RNFA104:Record not found");
            }
            if (!isValidStatusForApproval(updateRecord.getCurrappstatus())) {
                throw new Exception("MF-ERR-CA-IS101:Cannot approve - Invalid Status for Approval");
            }
            if (StringUtils.isSame(updateRecord.getMadeby(), updateId)) {
                throw new Exception("MF-ERR-CA-MCS102:Cannot Approve. Maker Checker cannot be same");
            }
            updateRecord.setRstatus("1");
            updateRecord.setCheckerlastcmt(comment);
            updateRecord.setCurrappstatus("1");
            updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
            updateRecord.setModifiedby(updateId);
            updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
            updateRecord.setCheckedby(updateId);
            boolean result = dao.updateLISystemuserRecord(updateRecord);
            logger.trace("approveLISystemuserRecord:Result:" + result);
            createMakerCheckerAuditEntry("system_user", updateRecord.getId(), "Approve", updateId, comment);
            HashMap approveEventMap = callActionEvent("Event_OnApprovalOf_LISystemuserRecord", updateRecord.getId());
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("approveLISystemuserRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean submitLISystemuserRecord(LISystemuserRecord inputRecord, String updateId, String comment)
            throws Exception {
        try {
            logger.trace("submitLISystemuserRecordNonNull:" + inputRecord);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("submitLISystemuserRecordNonNull", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord updateRecord = dao.loadLISystemuserRecord(inputRecord.getId());
            if (updateRecord == null) {
                throw new Exception("MF-ERR-CS-RNFA104:Record not found");
            }
            if (!isValidStatusForSubmission(updateRecord.getCurrappstatus())) {
                throw new Exception("MF-ERR-CS-IS101:Cannot submit - Invalid Status for Submission");
            }
            updateRecord.setRstatus("1");
            updateRecord.setMakerlastcmt(comment);
            updateRecord.setCurrappstatus("4");
            updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
            updateRecord.setModifiedby(updateId);
            updateRecord.setMadeat(DateUtils.getCurrentDateTime());
            updateRecord.setMadeby(updateId);
            boolean result = dao.updateLISystemuserRecord(updateRecord);
            createMakerCheckerAuditEntry("system_user", updateRecord.getId(), "Submit", updateId, comment);
            HashMap submitEventMap = callActionEvent("Event_OnSubmitOf_LISystemuserRecord", updateRecord.getId());
            logger.trace("submitLISystemuserRecord:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("submitLISystemuserRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean denyLISystemuserRecord(LISystemuserRecord inputRecord, String updateId, String comment)
            throws Exception {
        try {
            logger.trace("denyLISystemuserRecord:" + inputRecord);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("denyLISystemuserRecord", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord updateRecord = dao.loadLISystemuserRecord(inputRecord.getId());
            if (updateRecord == null) {
                throw new Exception("MF-ERR-CD-RNFA104:Record not found");
            }
            if (!isValidStatusForDeny(updateRecord.getCurrappstatus())) {
                throw new Exception("MF-ERR-CD-IS101:Cannot deny - Invalid Status for Deny");
            }
            if (StringUtils.isSame(updateRecord.getMadeby(), updateId)) {
                throw new Exception("MF-ERR-CD-MCS101:Cannot Deny. Maker Checker cannot be same");
            }
            updateRecord.setCheckerlastcmt(comment);
            updateRecord.setCurrappstatus("5");
            updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
            updateRecord.setModifiedby(updateId);
            updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
            updateRecord.setCheckedby(updateId);
            boolean result = dao.updateLISystemuserRecord(updateRecord);
            createMakerCheckerAuditEntry("system_user", updateRecord.getId(), "Deny", updateId, comment);
            HashMap denyEventMap = callActionEvent("Event_OnDenyOf_LISystemuserRecord", updateRecord.getId());
            logger.trace("denyLISystemuserRecord:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("denyLISystemuserRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean denyPermanantlyLISystemuserRecord(LISystemuserRecord inputRecord, String updateId, String comment)
            throws Exception {
        try {
            logger.trace("denyPermanantlyLISystemuserRecord:" + inputRecord);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("denyPermanantlyLISystemuserRecord", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord updateRecord = dao.loadLISystemuserRecord(inputRecord.getId());
            if (updateRecord == null) {
                throw new Exception("MF-ERR-CD-RNFADP104:Record not found");
            }
            if (!isValidStatusForDeny(updateRecord.getCurrappstatus())) {
                throw new Exception("MF-ERR-CD-ISDP101:Cannot deny - Invalid Status for Deny");
            }
            if (StringUtils.isSame(updateRecord.getMadeby(), updateId)) {
                throw new Exception("MF-ERR-CD-MCS101DP:Cannot Deny. Maker Checker cannot be same");
            }
            updateRecord.setCheckerlastcmt(comment);
            updateRecord.setCurrappstatus("58");
            updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
            updateRecord.setModifiedby(updateId);
            updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
            updateRecord.setCheckedby(updateId);
            boolean result = dao.updateLISystemuserRecord(updateRecord);
            createMakerCheckerAuditEntry("system_user", updateRecord.getId(), "DenyP", updateId, comment);
            HashMap denyEventMap = callActionEvent("Event_OnDenyPOf_LISystemuserRecord", updateRecord.getId());
            logger.trace("denyPLISystemuserRecord:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("denyPLISystemuserRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean remindApprovalLISystemuserRecord(LISystemuserRecord inputRecord, String updateId, String comment)
            throws Exception {
        try {
            logger.trace("remindApprovalLISystemuserRecord:" + inputRecord);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("remindApprovalLISystemuserRecord", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord updateRecord = dao.loadLISystemuserRecord(inputRecord.getId());
            if (updateRecord == null) {
                throw new Exception("MF-ERR-CR-RNFA104:Record not found");
            }
            if (!isValidStatusForApproval(updateRecord.getCurrappstatus())) {
                throw new Exception("MF-ERR-CR-IS101:Cannot Remind - Invalid Status for Reminder");
            }
            createMakerCheckerAuditEntry("system_user", updateRecord.getId(), "Approval Reminder", updateId, comment);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return true;
        } catch (Exception exception) {
            logger.error("remindApprovalLISystemuserRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean resetApprovalLISystemuserRecord(LISystemuserRecord inputRecord, String updateId, String comment)
            throws Exception {
        try {
            logger.trace("resetApprovalLISystemuserRecordNonNull:" + inputRecord);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("resetApprovalLISystemuserRecordNonNull", null);
            LISystemuserDAO dao = new LISystemuserDAO();
            LISystemuserRecord updateRecord = dao.loadLISystemuserRecord(inputRecord.getId());
            if (updateRecord == null) {
                throw new Exception("MF-ERR-RA-RNFA104:Record not found");
            }
            if (!isValidStatusForReset(updateRecord.getCurrappstatus())) {
                throw new Exception("MF-ERR-RA-IS101:Cannot submit - Invalid Status for Reset");
            }
            updateRecord.setRstatus("0");
            updateRecord.setAdminlastcmt(comment);
            updateRecord.setCurrappstatus("0");
            updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
            updateRecord.setModifiedby(updateId);
            updateRecord.setMadeat(DateUtils.getCurrentDateTime());
            updateRecord.setMadeby(updateId);
            boolean result = dao.updateLISystemuserRecord(updateRecord);
            createMakerCheckerAuditEntry("system_user", updateRecord.getId(), "Reset Approval", updateId, comment);
            logger.trace("resetApprovalLISystemuserRecord:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("resetApprovalLISystemuserRecord" + getStackTrace(exception));
            throw exception;
        }
    }
}

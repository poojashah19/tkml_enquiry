
/*
 * LIConfigdivService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIConfigdivRecord;
import com.leadics.Toyota.dao.LIConfigdivDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIConfigdivService extends LIService
{
	static LogUtils logger = new LogUtils(LIConfigdivService.class.getName());


	public LIConfigdivRecord[] loadLIConfigdivRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConfigdivRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigdivRecords", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			LIConfigdivRecord[] results = dao.loadLIConfigdivRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIConfigdivRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConfigdivRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIConfigdivRecord loadFirstLIConfigdivRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIConfigdivRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIConfigdivRecord", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			LIConfigdivRecord result = dao.loadFirstLIConfigdivRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIConfigdivRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConfigdivRecord searchFirstLIConfigdivRecord(LIConfigdivRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIConfigdivRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConfigdivRecord", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			LIConfigdivRecord[] records = dao.searchLIConfigdivRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIConfigdivRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIConfigdivRecord searchFirstLIConfigdivRecordExactUpper(LIConfigdivRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIConfigdivRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConfigdivRecordsExactUpper", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			LIConfigdivRecord[] records = dao.searchLIConfigdivRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIConfigdivRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConfigdivRecord[] searchLIConfigdivRecords(LIConfigdivRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIConfigdivRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigdivRecords", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			LIConfigdivRecord[] records = dao.searchLIConfigdivRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIConfigdivRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIConfigdivRecordCount(LIConfigdivRecord record)
	throws Exception
	{
		return loadLIConfigdivRecordCount(record, null);
	}


	public int loadLIConfigdivRecordCount(LIConfigdivRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConfigdivRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigdivRecordCount", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIConfigdivRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConfigdivRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConfigdivRecord loadLIConfigdivRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConfigdivRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigdivRecord", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			LIConfigdivRecord result = dao.loadLIConfigdivRecord(key);
			logger.trace("loadLIConfigdivRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConfigdivRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIConfigdivRecordSearchResultByPage(LIConfigdivRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIConfigdivRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIConfigdivRecordSearchResultByPage(LIConfigdivRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIConfigdivRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConfigdivRecordSearchResult", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIConfigdivRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIConfigdivRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIConfigdivRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIConfigdivRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIConfigdivRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIConfigdivRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConfigdivRecordSearchResultByPageQuery", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIConfigdivRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIConfigdivRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIConfigdivRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIConfigdivRecord(LIConfigdivRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIConfigdivRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIConfigdivRecord", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			int result = dao.insertLIConfigdivRecord(record);
			logger.trace("insertLIConfigdivRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIConfigdivRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIConfigdivRecord(LIConfigdivRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIConfigdivRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConfigdivRecord", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			boolean result = dao.updateLIConfigdivRecord(record);
			logger.trace("updateLIConfigdivRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIConfigdivRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIConfigdivRecordNonNull(LIConfigdivRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIConfigdivRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConfigdivRecordNonNull", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			LIConfigdivRecord dbRecord = dao.loadLIConfigdivRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIConfigdivRecord(dbRecord);
			logger.trace("updateLIConfigdivRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIConfigdivRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIConfigdivRecord(LIConfigdivRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIConfigdivRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIConfigdivRecord", null);
			LIConfigdivDAO dao = new LIConfigdivDAO();
			boolean result = dao.deleteLIConfigdivRecord(record);
			logger.trace("deleteLIConfigdivRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIConfigdivRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}

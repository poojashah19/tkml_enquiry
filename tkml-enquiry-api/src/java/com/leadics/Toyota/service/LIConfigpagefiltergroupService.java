
/*
 * LIConfigpagefiltergroupService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;

import com.leadics.Toyota.to.LIConfigpagefiltergroupRecord;
import com.leadics.Toyota.dao.LIConfigpagefiltergroupDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;

public class LIConfigpagefiltergroupService extends LIService {

    static LogUtils logger = new LogUtils(LIConfigpagefiltergroupService.class.getName());

    public LIConfigpagefiltergroupRecord[] loadLIConfigpagefiltergroupRecords(String query)
            throws Exception {
        try {
            logger.trace("loadLIConfigpagefiltergroupRecords:" + query);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagefiltergroupRecords", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            LIConfigpagefiltergroupRecord[] results = dao.loadLIConfigpagefiltergroupRecords(query);
            int resultRecordCount = 0;
            if (results != null) {
                resultRecordCount = results.length;
            }
            logger.trace("loadLIConfigpagefiltergroupRecords:Fetched" + resultRecordCount);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return results;
        } catch (Exception exception) {
            logger.error("loadLIConfigpagefiltergroupRecords" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LIConfigpagefiltergroupRecord loadFirstLIConfigpagefiltergroupRecord(String query)
            throws Exception {
        try {
            logger.trace("loadFirstLIConfigpagefiltergroupRecord:" + query);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIConfigpagefiltergroupRecord", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            LIConfigpagefiltergroupRecord result = dao.loadFirstLIConfigpagefiltergroupRecord(query);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("loadFirstLIConfigpagefiltergroupRecord" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LIConfigpagefiltergroupRecord searchFirstLIConfigpagefiltergroupRecord(LIConfigpagefiltergroupRecord record)
            throws Exception {
        try {
            logger.trace("searchFirstLIConfigpagefiltergroupRecords:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConfigpagefiltergroupRecord", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            LIConfigpagefiltergroupRecord[] records = dao.searchLIConfigpagefiltergroupRecords(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            if (records == null) {
                return null;
            }
            if (records.length < 1) {
                return null;
            }
            return records[0];
        } catch (Exception exception) {
            logger.error("searchFirstLIConfigpagefiltergroupRecords" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LIConfigpagefiltergroupRecord searchFirstLIConfigpagefiltergroupRecordExactUpper(LIConfigpagefiltergroupRecord record)
            throws Exception {
        try {
            logger.trace("searchFirstLIConfigpagefiltergroupRecordsExactUpper:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConfigpagefiltergroupRecordsExactUpper", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            LIConfigpagefiltergroupRecord[] records = dao.searchLIConfigpagefiltergroupRecordsExactUpper(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            if (records == null) {
                return null;
            }
            if (records.length < 1) {
                return null;
            }
            return records[0];
        } catch (Exception exception) {
            logger.error("searchFirstLIConfigpagefiltergroupRecordsExactUpper" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LIConfigpagefiltergroupRecord[] searchLIConfigpagefiltergroupRecords(LIConfigpagefiltergroupRecord record)
            throws Exception {
        try {
            logger.trace("searchLIConfigpagefiltergroupRecords:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagefiltergroupRecords", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            LIConfigpagefiltergroupRecord[] records = dao.searchLIConfigpagefiltergroupRecords(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return records;
        } catch (Exception exception) {
            logger.error("searchLIConfigpagefiltergroupRecords" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public int loadLIConfigpagefiltergroupRecordCount(LIConfigpagefiltergroupRecord record)
            throws Exception {
        return loadLIConfigpagefiltergroupRecordCount(record, null);
    }

    public int loadLIConfigpagefiltergroupRecordCount(LIConfigpagefiltergroupRecord record, String customCondition)
            throws Exception {
        try {
            logger.trace("loadLIConfigpagefiltergroupRecordCount:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagefiltergroupRecordCount", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            dao.setCustomCondition(customCondition);
            int resultcount = dao.loadLIConfigpagefiltergroupRecordCount(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return resultcount;
        } catch (Exception exception) {
            logger.error("loadLIConfigpagefiltergroupRecordCount" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LIConfigpagefiltergroupRecord loadLIConfigpagefiltergroupRecord(String key)
            throws Exception {
        try {
            logger.trace("loadLIConfigpagefiltergroupRecord:" + key);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagefiltergroupRecord", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            LIConfigpagefiltergroupRecord result = dao.loadLIConfigpagefiltergroupRecord(key);
            logger.trace("loadLIConfigpagefiltergroupRecord:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("loadLIConfigpagefiltergroupRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public JSONObject getJSONLIConfigpagefiltergroupRecordSearchResultByPage(LIConfigpagefiltergroupRecord record, String offset, String maxrows, String orderBy)
            throws Exception {
        return getJSONLIConfigpagefiltergroupRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
    }

    public JSONObject getJSONLIConfigpagefiltergroupRecordSearchResultByPage(LIConfigpagefiltergroupRecord record, String offset, String maxrows, String orderBy, String customCondition)
            throws Exception {
        try {
            logger.trace("getJSONLIConfigpagefiltergroupRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConfigpagefiltergroupRecordSearchResult", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            dao.setCustomCondition(customCondition);
            int totalCount = dao.loadLIConfigpagefiltergroupRecordCount(record);
            dao.setLimits(offset, maxrows);
            LIConfigpagefiltergroupRecord[] records = null;
            if (totalCount > 0) {
                dao.setOrderBy(orderBy);
                records = dao.searchLIConfigpagefiltergroupRecords(record);
            }
            JSONObject resultObject = new JSONObject();
            resultObject.put("total", totalCount + "");
            JSONArray dataArray = new JSONArray();
            int recordCount = 0;
            if (records != null) {
                recordCount = records.length;
            }
            for (int index = 0; index < recordCount; index++) {
//                dataArray.add(records[index].getJSONObjectUI());
            }
            resultObject.put("rows", dataArray);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return resultObject;
        } catch (Exception exception) {
            logger.error("getJSONLIConfigpagefiltergroupRecordSearchResult" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public JSONObject getJSONLIConfigpagefiltergroupRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
            throws Exception {
        try {
            logger.trace("getJSONLIConfigpagefiltergroupRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConfigpagefiltergroupRecordSearchResultByPageQuery", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            int totalCount = dao.loadRecordCount(con, countQuery);
            dao.setLimits(offset, maxrows);
            LIConfigpagefiltergroupRecord[] records = null;
            if (totalCount > 0) {
                records = dao.loadLIConfigpagefiltergroupRecords(query, con, true);
            }
            JSONObject resultObject = new JSONObject();
            resultObject.put("total", totalCount + "");
            JSONArray dataArray = new JSONArray();
            int recordCount = 0;
            if (records != null) {
                recordCount = records.length;
            }
            for (int index = 0; index < recordCount; index++) {
//                dataArray.add(records[index].getJSONObjectUI());
            }
            resultObject.put("rows", dataArray);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return resultObject;
        } catch (Exception exception) {
            logger.error("getJSONLIConfigpagefiltergroupRecordSearchResultByPageQuery" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public int insertLIConfigpagefiltergroupRecord(LIConfigpagefiltergroupRecord record)
            throws Exception {
        try {
            logger.trace("insertLIConfigpagefiltergroupRecord:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIConfigpagefiltergroupRecord", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            int result = dao.insertLIConfigpagefiltergroupRecord(record);
            logger.trace("insertLIConfigpagefiltergroupRecord:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("insertLIConfigpagefiltergroupRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean updateLIConfigpagefiltergroupRecord(LIConfigpagefiltergroupRecord record)
            throws Exception {
        try {
            logger.trace("updateLIConfigpagefiltergroupRecord:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConfigpagefiltergroupRecord", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            boolean result = dao.updateLIConfigpagefiltergroupRecord(record);
            logger.trace("updateLIConfigpagefiltergroupRecord:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("updateLIConfigpagefiltergroupRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean updateLIConfigpagefiltergroupRecordNonNull(LIConfigpagefiltergroupRecord inputRecord)
            throws Exception {
        try {
            logger.trace("updateLIConfigpagefiltergroupRecordNonNull:" + inputRecord);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConfigpagefiltergroupRecordNonNull", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            LIConfigpagefiltergroupRecord dbRecord = dao.loadLIConfigpagefiltergroupRecord(inputRecord.getId());
            if (dbRecord == null) {
                throw new Exception("Record not found");
            }
//            dbRecord.loadNonNullContent(inputRecord);
            dbRecord.setActionSource(inputRecord.getActionSource());
            boolean result = dao.updateLIConfigpagefiltergroupRecord(dbRecord);
            logger.trace("updateLIConfigpagefiltergroupRecordNonNull:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("updateLIConfigpagefiltergroupRecordNonNull" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean deleteLIConfigpagefiltergroupRecord(LIConfigpagefiltergroupRecord record)
            throws Exception {
        try {
            logger.trace("deleteLIConfigpagefiltergroupRecord:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIConfigpagefiltergroupRecord", null);
            LIConfigpagefiltergroupDAO dao = new LIConfigpagefiltergroupDAO();
            boolean result = dao.deleteLIConfigpagefiltergroupRecord(record);
            logger.trace("deleteLIConfigpagefiltergroupRecord:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("deleteLIConfigpagefiltergroupRecord" + getStackTrace(exception));
            throw exception;
        }
    }
}

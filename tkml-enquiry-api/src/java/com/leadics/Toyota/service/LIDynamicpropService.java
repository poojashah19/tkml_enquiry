
/*
 * LIDynamicpropService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIDynamicpropRecord;
import com.leadics.Toyota.dao.LIDynamicpropDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIDynamicpropService extends LIService
{
	static LogUtils logger = new LogUtils(LIDynamicpropService.class.getName());


	public LIDynamicpropRecord[] loadLIDynamicpropRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIDynamicpropRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIDynamicpropRecords", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord[] results = dao.loadLIDynamicpropRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIDynamicpropRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIDynamicpropRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIDynamicpropRecord loadFirstLIDynamicpropRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIDynamicpropRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIDynamicpropRecord", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord result = dao.loadFirstLIDynamicpropRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIDynamicpropRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIDynamicpropRecord searchFirstLIDynamicpropRecord(LIDynamicpropRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIDynamicpropRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIDynamicpropRecord", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord[] records = dao.searchLIDynamicpropRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIDynamicpropRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIDynamicpropRecord searchFirstLIDynamicpropRecordExactUpper(LIDynamicpropRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIDynamicpropRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIDynamicpropRecordsExactUpper", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord[] records = dao.searchLIDynamicpropRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIDynamicpropRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIDynamicpropRecord[] searchLIDynamicpropRecords(LIDynamicpropRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIDynamicpropRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIDynamicpropRecords", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord[] records = dao.searchLIDynamicpropRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIDynamicpropRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIDynamicpropRecordCount(LIDynamicpropRecord record)
	throws Exception
	{
		return loadLIDynamicpropRecordCount(record, null);
	}


	public int loadLIDynamicpropRecordCount(LIDynamicpropRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIDynamicpropRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIDynamicpropRecordCount", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIDynamicpropRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIDynamicpropRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIDynamicpropRecord loadLIDynamicpropRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIDynamicpropRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIDynamicpropRecord", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord result = dao.loadLIDynamicpropRecord(key);
			logger.trace("loadLIDynamicpropRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIDynamicpropRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIDynamicpropRecordSearchResultByPage(LIDynamicpropRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIDynamicpropRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIDynamicpropRecordSearchResultByPage(LIDynamicpropRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIDynamicpropRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIDynamicpropRecordSearchResult", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIDynamicpropRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIDynamicpropRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIDynamicpropRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIDynamicpropRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIDynamicpropRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIDynamicpropRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIDynamicpropRecordSearchResultByPageQuery", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIDynamicpropRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIDynamicpropRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIDynamicpropRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIDynamicpropRecord(LIDynamicpropRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIDynamicpropRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIDynamicpropRecord", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			int result = dao.insertLIDynamicpropRecord(record);
			logger.trace("insertLIDynamicpropRecord:Result:" + result);			
			createMakerCheckerAuditEntry("dynamic_prop",result + "", "Create",record.getCreatedby(),"Created");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIDynamicpropRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIDynamicpropRecord(LIDynamicpropRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIDynamicpropRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIDynamicpropRecord", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			boolean result = dao.updateLIDynamicpropRecord(record);
			createMakerCheckerAuditEntry("dynamic_prop",record.getId(), "Update",record.getModifiedby(),"Update");
			logger.trace("updateLIDynamicpropRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIDynamicpropRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIDynamicpropRecordNonNull(LIDynamicpropRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIDynamicpropRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIDynamicpropRecordNonNull", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord dbRecord = dao.loadLIDynamicpropRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIDynamicpropRecord(dbRecord);
			createMakerCheckerAuditEntry("dynamic_prop",inputRecord.getId(), "Update",dbRecord.getModifiedby(),"Update");
			logger.trace("updateLIDynamicpropRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIDynamicpropRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIDynamicpropRecord(LIDynamicpropRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIDynamicpropRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIDynamicpropRecord", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			boolean result = dao.deleteLIDynamicpropRecord(record);
			logger.trace("deleteLIDynamicpropRecord:Result:" + result);			
			createMakerCheckerAuditEntry("dynamic_prop",record.getId(), "Delete",null,"Deleted");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIDynamicpropRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean approveLIDynamicpropRecord(LIDynamicpropRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("approveLIDynamicpropRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("approveLIDynamicpropRecord", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord updateRecord = dao.loadLIDynamicpropRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CA-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CA-IS101:Cannot approve - Invalid Status for Approval");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CA-MCS102:Cannot Approve. Maker Checker cannot be same");
			}
			updateRecord.setRstatus("1");
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("1");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIDynamicpropRecord(updateRecord);
			logger.trace("approveLIDynamicpropRecord:Result:" + result);			
			createMakerCheckerAuditEntry("dynamic_prop",updateRecord.getId(), "Approve",updateId,comment);
			HashMap approveEventMap = callActionEvent("Event_OnApprovalOf_LIDynamicpropRecord", updateRecord.getId());
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("approveLIDynamicpropRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean submitLIDynamicpropRecord(LIDynamicpropRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("submitLIDynamicpropRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("submitLIDynamicpropRecordNonNull", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord updateRecord = dao.loadLIDynamicpropRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CS-RNFA104:Record not found");
			}
			if (!isValidStatusForSubmission(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CS-IS101:Cannot submit - Invalid Status for Submission");
			}
			updateRecord.setRstatus("1");
			updateRecord.setMakerlastcmt(comment);
			updateRecord.setCurrappstatus("4");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLIDynamicpropRecord(updateRecord);
			createMakerCheckerAuditEntry("dynamic_prop",updateRecord.getId(),"Submit",updateId,comment);
			HashMap submitEventMap = callActionEvent("Event_OnSubmitOf_LIDynamicpropRecord", updateRecord.getId());
			logger.trace("submitLIDynamicpropRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("submitLIDynamicpropRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean denyLIDynamicpropRecord(LIDynamicpropRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyLIDynamicpropRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyLIDynamicpropRecord", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord updateRecord = dao.loadLIDynamicpropRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFA104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-IS101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("5");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIDynamicpropRecord(updateRecord);
			createMakerCheckerAuditEntry("dynamic_prop",updateRecord.getId(),"Deny",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyOf_LIDynamicpropRecord", updateRecord.getId());
			logger.trace("denyLIDynamicpropRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyLIDynamicpropRecord" + getStackTrace(exception));
			throw exception;
		}
	}
	public boolean denyPermanantlyLIDynamicpropRecord(LIDynamicpropRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyPermanantlyLIDynamicpropRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyPermanantlyLIDynamicpropRecord", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord updateRecord = dao.loadLIDynamicpropRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFADP104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-ISDP101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101DP:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("58");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIDynamicpropRecord(updateRecord);
			createMakerCheckerAuditEntry("dynamic_prop",updateRecord.getId(),"DenyP",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyPOf_LIDynamicpropRecord", updateRecord.getId());
			logger.trace("denyPLIDynamicpropRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyPLIDynamicpropRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean remindApprovalLIDynamicpropRecord(LIDynamicpropRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("remindApprovalLIDynamicpropRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("remindApprovalLIDynamicpropRecord", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord updateRecord = dao.loadLIDynamicpropRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CR-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CR-IS101:Cannot Remind - Invalid Status for Reminder");
			}
			createMakerCheckerAuditEntry("dynamic_prop",updateRecord.getId(), "Approval Reminder",updateId,comment);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return true;
		}
		catch(Exception exception)
		{
			logger.error("remindApprovalLIDynamicpropRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean resetApprovalLIDynamicpropRecord(LIDynamicpropRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("resetApprovalLIDynamicpropRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("resetApprovalLIDynamicpropRecordNonNull", null);
			LIDynamicpropDAO dao = new LIDynamicpropDAO();
			LIDynamicpropRecord updateRecord = dao.loadLIDynamicpropRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-RA-RNFA104:Record not found");
			}
			if (!isValidStatusForReset(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-RA-IS101:Cannot submit - Invalid Status for Reset");
			}
			updateRecord.setRstatus("0");
			updateRecord.setAdminlastcmt(comment);
			updateRecord.setCurrappstatus("0");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLIDynamicpropRecord(updateRecord);
			createMakerCheckerAuditEntry("dynamic_prop",updateRecord.getId(),"Reset Approval",updateId,comment);
			logger.trace("resetApprovalLIDynamicpropRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("resetApprovalLIDynamicpropRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}

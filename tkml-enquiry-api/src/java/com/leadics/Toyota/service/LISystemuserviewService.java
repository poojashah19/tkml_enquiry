
/*
 * LISystemuserviewService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LISystemuserviewRecord;
import com.leadics.Toyota.dao.LISystemuserviewDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LISystemuserviewService extends LIService
{
	static LogUtils logger = new LogUtils(LISystemuserviewService.class.getName());


	public LISystemuserviewRecord[] loadLISystemuserviewRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLISystemuserviewRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISystemuserviewRecords", null);
			LISystemuserviewDAO dao = new LISystemuserviewDAO();
			LISystemuserviewRecord[] results = dao.loadLISystemuserviewRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLISystemuserviewRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLISystemuserviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISystemuserviewRecord loadFirstLISystemuserviewRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLISystemuserviewRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLISystemuserviewRecord", null);
			LISystemuserviewDAO dao = new LISystemuserviewDAO();
			LISystemuserviewRecord result = dao.loadFirstLISystemuserviewRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLISystemuserviewRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISystemuserviewRecord searchFirstLISystemuserviewRecord(LISystemuserviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISystemuserviewRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISystemuserviewRecord", null);
			LISystemuserviewDAO dao = new LISystemuserviewDAO();
			LISystemuserviewRecord[] records = dao.searchLISystemuserviewRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISystemuserviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISystemuserviewRecord searchFirstLISystemuserviewRecordExactUpper(LISystemuserviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISystemuserviewRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISystemuserviewRecordsExactUpper", null);
			LISystemuserviewDAO dao = new LISystemuserviewDAO();
			LISystemuserviewRecord[] records = dao.searchLISystemuserviewRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISystemuserviewRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISystemuserviewRecord[] searchLISystemuserviewRecords(LISystemuserviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLISystemuserviewRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISystemuserviewRecords", null);
			LISystemuserviewDAO dao = new LISystemuserviewDAO();
			LISystemuserviewRecord[] records = dao.searchLISystemuserviewRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLISystemuserviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLISystemuserviewRecordCount(LISystemuserviewRecord record)
	throws Exception
	{
		return loadLISystemuserviewRecordCount(record, null);
	}


	public int loadLISystemuserviewRecordCount(LISystemuserviewRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLISystemuserviewRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISystemuserviewRecordCount", null);
			LISystemuserviewDAO dao = new LISystemuserviewDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLISystemuserviewRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLISystemuserviewRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISystemuserviewRecord loadLISystemuserviewRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLISystemuserviewRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISystemuserviewRecord", null);
			LISystemuserviewDAO dao = new LISystemuserviewDAO();
			LISystemuserviewRecord result = dao.loadLISystemuserviewRecord(key);
			logger.trace("loadLISystemuserviewRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLISystemuserviewRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLISystemuserviewRecordSearchResultByPage(LISystemuserviewRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLISystemuserviewRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLISystemuserviewRecordSearchResultByPage(LISystemuserviewRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISystemuserviewRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISystemuserviewRecordSearchResult", null);
			LISystemuserviewDAO dao = new LISystemuserviewDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLISystemuserviewRecordCount(record);
			dao.setLimits(offset, maxrows);
			LISystemuserviewRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLISystemuserviewRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISystemuserviewRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLISystemuserviewRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISystemuserviewRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISystemuserviewRecordSearchResultByPageQuery", null);
			LISystemuserviewDAO dao = new LISystemuserviewDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LISystemuserviewRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLISystemuserviewRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISystemuserviewRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
}

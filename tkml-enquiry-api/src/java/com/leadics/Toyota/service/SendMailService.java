package com.leadics.Toyota.service;

import com.leadics.Toyota.common.LIDAO;
import com.leadics.Toyota.dao.ICSmaillogDAO;
import com.leadics.Toyota.to.ICSmaillogRecord;
import com.leadics.utils.StringUtils;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sun.mail.smtp.SMTPAddressFailedException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class SendMailService {

    public HashMap sendMail(HashMap inputMap)
            throws Exception {
        HashMap outputMap = new HashMap(inputMap);

        Properties props = new Properties();
        String secureMailFlag = "Y";//PropertyUtil.getProperty("SecureMail");
        String secureMailFlagTLSEnable = "N"; //StringUtils.noNull(PropertyUtil.getProperty("SecureMailTLSENable"));

        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        props.put("mail.smtp.auth", "true");

        if (secureMailFlagTLSEnable.equals("Y")) {
            props.put("mail.smtp.starttls.enable", "true");
        }

        props.put("mail.smtp.host", "smtp.gmail.com");//PropertyUtil.getProperty("smtpserver")
        props.put("mail.smtp.port", "465");//PropertyUtil.getProperty("smtpport")

        if (secureMailFlag.equals("Y")) {
            props.put("mail.smtp.socketFactory.port", "465");//PropertyUtil.getProperty("smtpport")
            props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
            props.put("mail.smtp.socketFactory.fallback", "false");
        }

        //System.out.println("UID:" + "varma.sagi@leadics.com"); //PropertyUtil.getProperty("smtpuid")
        //System.out.println("PWD:" + "Lohitha@123");//PropertyUtil.getProperty("smtppwd")

        props.put("mail.default.sender", "varma.sagi@leadics.com");//PropertyUtil.getProperty("smtpfromaddr")

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        "varma.sagi@leadics.com", //PropertyUtil.getProperty("smtpfromaddr")
                        "Lohitha@123"); //PropertyUtil.getProperty("smtppwd")
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("varma.sagi@leadics.com"));//PropertyUtil.getProperty("smtpfromaddr")

            String aliasName = "varma.sagi@leadics.com";//PropertyUtil.getProperty("smtpfromaddralias")
            if (!StringUtils.isNullOrEmpty(aliasName)) {

                message.setFrom(new InternetAddress("varma.sagi@leadics.com", aliasName));//PropertyUtil.getProperty("smtpfromaddr")
            }

            String toAddress = StringUtils.getMapValue(inputMap, "ToAddress");
            if (toAddress.startsWith("555")) {
                toAddress = StringUtils.replaceString(toAddress, "555", "", false);
            }

            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));
            message.setSubject(StringUtils.getMapValue(inputMap, "Subject"));

            BodyPart messageBodyPart = new MimeBodyPart();

            ArrayList attachmentsList = getAttachmentList(StringUtils.getMapValue(inputMap, "MailMessage"));
            String messageToBeSent = clearAttachMessages(StringUtils.getMapValue(inputMap, "MailMessage"));

            messageBodyPart.setText(messageToBeSent);

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            Iterator attachmentIterator = attachmentsList.iterator();
            while (attachmentIterator.hasNext()) {
                String attachmentFile = attachmentIterator.next().toString();
                addAttachment(multipart, attachmentFile);
            }
            message.setContent(messageToBeSent, "text/html");

            Transport.send(message);

            outputMap.put("message", "Message delivered successfully");
            return outputMap;
        } catch (SMTPAddressFailedException addressFailed) {

            outputMap.put("message", "Message delivery failed " + addressFailed);
            outputMap.put("DoNotAutoRetry", "Y");
            return outputMap;
        } catch (Exception e) {

            outputMap.put("message", "Message delivery failed " + e);
            return outputMap;
        }
    }

    private static void addAttachment(Multipart multipart, String filename)
            throws Exception {
        MimeBodyPart attachment = new MimeBodyPart();
        DataSource source = new FileDataSource(filename);
        attachment.setDataHandler(new DataHandler(source));
        attachment.setFileName(source.getName());
        multipart.addBodyPart(attachment);
    }

    public String clearAttachMessages(String inputMessage)
            throws Exception {
        InputStream is = new ByteArrayInputStream(inputMessage.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String line;
        String resultMessage = "";
        while ((line = br.readLine()) != null) {
            if (line.toUpperCase().startsWith("#ATTACH-")) {
                continue;
            }
            if (line.toUpperCase().startsWith("#ATTACHA-")) {
                continue;
            }
            resultMessage = resultMessage + "\n" + line;
        }
        br.close();
        return resultMessage;
    }

    public String getAttachCode(String inputMessage)
            throws Exception {
        InputStream is = new ByteArrayInputStream(inputMessage.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String line;
        String resultMessage = "";
        while ((line = br.readLine()) != null) {
            if (line.toUpperCase().startsWith("#ATTACH-")) {
                return line.substring(8);
            }
            if (line.toUpperCase().startsWith("#ATTACHA-")) {
                return line.substring(9);
            }
        }
        br.close();
        return "";
    }

    public ArrayList getAttachmentList(String inputMessage)
            throws Exception {
        ArrayList resultList = new ArrayList();
        String attachmentLocation = "";//PropertyUtil.getProperty("MailAttachmentsLocation")

        InputStream is = new ByteArrayInputStream(inputMessage.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String line;
        String resultMessage = "";
        while ((line = br.readLine()) != null) {
            if (line.toUpperCase().startsWith("#ATTACH-")) {
                resultList.add(attachmentLocation + line.substring(8));
            }

            if (line.toUpperCase().startsWith("#ATTACHA-")) {
                resultList.add(line.substring(9));
            }
        }
        br.close();
        return resultList;
    }

    public static void main(String[] args)
            throws Exception {
        String emailID = "harsha.ans@leadics.com";

        SimpleDateFormat dfwithtime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        String datetime = dfwithtime.format(new Date());

        HashMap inputMap = new HashMap();

        inputMap.put("MailMessage", "<HTML> Dear  <br><br> As of  there are  s overdue for more than 3 days.<br><br>Please log in to  http://webapps.wheelmonk.com/SalesTML/  to update the status of these commitments.<br><br>Here is a summary of the overdue commitments<br>"
                + " <br> <Table  BORDER=\"1\" style='width:100%;'>"
                + "<th style=\"width:40%\">Counter Measure</th>"
                + "<th style=\"width:13%\">Responsibility</th>"
                + "<th style=\"width:34%\">KPI</th>"
                + "<th style=\"width:13%;white-space: nowrap ;\">Due Date</th>"
                + "</table>  </HTML><br><br><br>regards,<br>Admin<br> TML Dealer Audit Program (Sales)");

        inputMap.put("ToAddress", emailID);
        inputMap.put("Subject", "Reminder:  Commitments Overdue - JDPAP Sales Consistency Program");

        SendMailService mail = new SendMailService();

        HashMap outputMap = mail.sendMail(inputMap);
        ICSmaillogRecord logrecord = new ICSmaillogRecord();
        ICSmaillogDAO logdao = new ICSmaillogDAO();
        logrecord.setFromID("varma.sagi@leadics.com");
        logrecord.setMailMessage((String) inputMap.get("MailMessage"));
        logrecord.setToIDs((String) inputMap.get("ToAddress"));
        logrecord.setDatetime(datetime);
        logrecord.setRespoinseMessage(outputMap.toString());
        logdao.insertICSmaillogRecord(logrecord);

        //System.out.println(outputMap);

    }
}

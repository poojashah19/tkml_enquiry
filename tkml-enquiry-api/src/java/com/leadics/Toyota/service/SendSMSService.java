/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.service;


import com.leadics.Toyota.common.LIDAO;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.HttpClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.leadics.Toyota.dao.ICSmaillogDAO;
import com.leadics.Toyota.dao.ICSsmslogDAO;
import com.leadics.Toyota.to.ICSmaillogRecord;
import com.leadics.Toyota.to.ICSsmslogRecord;
/**
 *
 * @author varma.sagi
 */
public class SendSMSService {

    public HashMap<String, String> sendSMS(String msgText, String repNos) {
        HttpClient httpclient = new HttpClient();
        HashMap<String, String> map = new HashMap<>();
        //String serviceURL = "http://api.mVaayoo.com/mvaayooapi/MessageCompose?user=varmasagi@gmail.com:googleone&senderID=VARMAS&receipientno=9390977778&dcs=0&msgtxt=This is Test message&state=4";
        String serviceURL = "http://api.mVaayoo.com/mvaayooapi/MessageCompose?";
        String serviceUID = "varmasagi@gmail.com:googleone";
        String serviceSID = "LEDICS";
        String Message = msgText;
        String receptentNO = repNos;

        try {
            serviceUID = URLEncoder.encode(serviceUID, "UTF-8");
            serviceSID = URLEncoder.encode(serviceSID, "UTF-8");

            Message = URLEncoder.encode(Message, "UTF-8");
            receptentNO = URLEncoder.encode(receptentNO, "UTF-8");

            serviceURL = serviceURL + "user=" + serviceUID + "&";
            serviceURL = serviceURL + "senderID=" + serviceSID + "&";
            serviceURL = serviceURL + "msgtxt=" + Message + "&";
            serviceURL = serviceURL + "receipientno=" + receptentNO + "&";
            serviceURL = serviceURL + "dcs=0&";
            serviceURL = serviceURL + "state=4";
            //System.out.println("SU1:" + serviceURL);

            GetMethod httpget = new GetMethod(serviceURL);
            // Execute the request
            int responseCode = httpclient.executeMethod(httpget);

            if (responseCode != 200) {
                //System.out.println("Failed to deliver SMS");
                return null;
            }

            byte[] responseBody = httpget.getResponseBody();

            String responseMessage = new String(responseBody);
            map.put("serviceURL", serviceURL);
            map.put("responseMessage", responseMessage);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static void main(String[] args) throws Exception {
        LIDAO dao = new LIDAO();
        JSONArray jsonArray = dao.load3Vals("SELECT Dealername,dealerphoneno,COUNT(1) AS cnt FROM phase1master a,dealermaster b WHERE a.Dealerid=b.Dealer_id AND Proposedduedate < DATE(NOW()-INTERVAL 3 DAY) AND (DealerPhase1status IS NULL OR DealerPhase1status IN ('NOT Implemented','Somewhat Implemented/Need More TO Be Implemented')) GROUP BY 1", "Dealername", "Dealername", "dealerphoneno", "dealerphoneno", "cnt", "cnt");

        Iterator i = jsonArray.iterator();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat dfwithtime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String todayDate = df.format(new Date());
        String datetime = dfwithtime.format(new Date());
        while (i.hasNext()) {
            HashMap inputMap = new HashMap();
            JSONObject obj = (JSONObject) i.next();
            String dealer = (String) obj.get("Dealername");
            String cnt = (String) obj.get("cnt");
            String dealeremailid = (String) obj.get("dealeremailid");
            String dealerMobileNos = (String) obj.get("dealerphoneno");
            String smsMessage = "Dear " + dealer + " ,\r\n"
                    + "As of " + todayDate + ", there are the " + cnt + " commitments overdue for more than 3 days.\r\n"
                    + "regards,\r\n"
                    + "Admin\r\n"
                    + "TML Dealer Audit Program (Sales)";

            SendSMSService objSendSMS = new SendSMSService();
           objSendSMS.sendSMS(smsMessage, dealerMobileNos);
           
           
        }
    }
}

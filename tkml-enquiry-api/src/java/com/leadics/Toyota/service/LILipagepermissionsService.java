
/*
 * LILipagepermissionsService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LILipagepermissionsRecord;
import com.leadics.Toyota.dao.LILipagepermissionsDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LILipagepermissionsService extends LIService
{
	static LogUtils logger = new LogUtils(LILipagepermissionsService.class.getName());


	public LILipagepermissionsRecord[] loadLILipagepermissionsRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLILipagepermissionsRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILipagepermissionsRecords", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			LILipagepermissionsRecord[] results = dao.loadLILipagepermissionsRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLILipagepermissionsRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLILipagepermissionsRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LILipagepermissionsRecord loadFirstLILipagepermissionsRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLILipagepermissionsRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLILipagepermissionsRecord", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			LILipagepermissionsRecord result = dao.loadFirstLILipagepermissionsRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLILipagepermissionsRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LILipagepermissionsRecord searchFirstLILipagepermissionsRecord(LILipagepermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLILipagepermissionsRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLILipagepermissionsRecord", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			LILipagepermissionsRecord[] records = dao.searchLILipagepermissionsRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLILipagepermissionsRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LILipagepermissionsRecord searchFirstLILipagepermissionsRecordExactUpper(LILipagepermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLILipagepermissionsRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLILipagepermissionsRecordsExactUpper", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			LILipagepermissionsRecord[] records = dao.searchLILipagepermissionsRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLILipagepermissionsRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LILipagepermissionsRecord[] searchLILipagepermissionsRecords(LILipagepermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLILipagepermissionsRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILipagepermissionsRecords", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			LILipagepermissionsRecord[] records = dao.searchLILipagepermissionsRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLILipagepermissionsRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLILipagepermissionsRecordCount(LILipagepermissionsRecord record)
	throws Exception
	{
		return loadLILipagepermissionsRecordCount(record, null);
	}


	public int loadLILipagepermissionsRecordCount(LILipagepermissionsRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLILipagepermissionsRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILipagepermissionsRecordCount", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLILipagepermissionsRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLILipagepermissionsRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LILipagepermissionsRecord loadLILipagepermissionsRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLILipagepermissionsRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILipagepermissionsRecord", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			LILipagepermissionsRecord result = dao.loadLILipagepermissionsRecord(key);
			logger.trace("loadLILipagepermissionsRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLILipagepermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLILipagepermissionsRecordSearchResultByPage(LILipagepermissionsRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLILipagepermissionsRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLILipagepermissionsRecordSearchResultByPage(LILipagepermissionsRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLILipagepermissionsRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLILipagepermissionsRecordSearchResult", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLILipagepermissionsRecordCount(record);
			dao.setLimits(offset, maxrows);
			LILipagepermissionsRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLILipagepermissionsRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLILipagepermissionsRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLILipagepermissionsRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLILipagepermissionsRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLILipagepermissionsRecordSearchResultByPageQuery", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LILipagepermissionsRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLILipagepermissionsRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLILipagepermissionsRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLILipagepermissionsRecord(LILipagepermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLILipagepermissionsRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLILipagepermissionsRecord", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			int result = dao.insertLILipagepermissionsRecord(record);
			logger.trace("insertLILipagepermissionsRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLILipagepermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLILipagepermissionsRecord(LILipagepermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLILipagepermissionsRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLILipagepermissionsRecord", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			boolean result = dao.updateLILipagepermissionsRecord(record);
			logger.trace("updateLILipagepermissionsRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLILipagepermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLILipagepermissionsRecordNonNull(LILipagepermissionsRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLILipagepermissionsRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLILipagepermissionsRecordNonNull", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			LILipagepermissionsRecord dbRecord = dao.loadLILipagepermissionsRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLILipagepermissionsRecord(dbRecord);
			logger.trace("updateLILipagepermissionsRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLILipagepermissionsRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLILipagepermissionsRecord(LILipagepermissionsRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLILipagepermissionsRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLILipagepermissionsRecord", null);
			LILipagepermissionsDAO dao = new LILipagepermissionsDAO();
			boolean result = dao.deleteLILipagepermissionsRecord(record);
			logger.trace("deleteLILipagepermissionsRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLILipagepermissionsRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}

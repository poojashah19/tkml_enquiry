
/*
 * LIControllermapService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIControllermapRecord;
import com.leadics.Toyota.dao.LIControllermapDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIControllermapService extends LIService
{
	static LogUtils logger = new LogUtils(LIControllermapService.class.getName());


	public LIControllermapRecord[] loadLIControllermapRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIControllermapRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIControllermapRecords", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			LIControllermapRecord[] results = dao.loadLIControllermapRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIControllermapRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIControllermapRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIControllermapRecord loadFirstLIControllermapRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIControllermapRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIControllermapRecord", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			LIControllermapRecord result = dao.loadFirstLIControllermapRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIControllermapRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIControllermapRecord searchFirstLIControllermapRecord(LIControllermapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIControllermapRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIControllermapRecord", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			LIControllermapRecord[] records = dao.searchLIControllermapRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIControllermapRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIControllermapRecord searchFirstLIControllermapRecordExactUpper(LIControllermapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIControllermapRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIControllermapRecordsExactUpper", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			LIControllermapRecord[] records = dao.searchLIControllermapRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIControllermapRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIControllermapRecord[] searchLIControllermapRecords(LIControllermapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIControllermapRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIControllermapRecords", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			LIControllermapRecord[] records = dao.searchLIControllermapRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIControllermapRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIControllermapRecordCount(LIControllermapRecord record)
	throws Exception
	{
		return loadLIControllermapRecordCount(record, null);
	}


	public int loadLIControllermapRecordCount(LIControllermapRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIControllermapRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIControllermapRecordCount", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIControllermapRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIControllermapRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIControllermapRecord loadLIControllermapRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIControllermapRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIControllermapRecord", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			LIControllermapRecord result = dao.loadLIControllermapRecord(key);
			logger.trace("loadLIControllermapRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIControllermapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIControllermapRecordSearchResultByPage(LIControllermapRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIControllermapRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIControllermapRecordSearchResultByPage(LIControllermapRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIControllermapRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIControllermapRecordSearchResult", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIControllermapRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIControllermapRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIControllermapRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIControllermapRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIControllermapRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIControllermapRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIControllermapRecordSearchResultByPageQuery", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIControllermapRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIControllermapRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIControllermapRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIControllermapRecord(LIControllermapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIControllermapRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIControllermapRecord", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			int result = dao.insertLIControllermapRecord(record);
			logger.trace("insertLIControllermapRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIControllermapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIControllermapRecord(LIControllermapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIControllermapRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIControllermapRecord", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			boolean result = dao.updateLIControllermapRecord(record);
			logger.trace("updateLIControllermapRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIControllermapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIControllermapRecordNonNull(LIControllermapRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIControllermapRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIControllermapRecordNonNull", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			LIControllermapRecord dbRecord = dao.loadLIControllermapRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIControllermapRecord(dbRecord);
			logger.trace("updateLIControllermapRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIControllermapRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIControllermapRecord(LIControllermapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIControllermapRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIControllermapRecord", null);
			LIControllermapDAO dao = new LIControllermapDAO();
			boolean result = dao.deleteLIControllermapRecord(record);
			logger.trace("deleteLIControllermapRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIControllermapRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}

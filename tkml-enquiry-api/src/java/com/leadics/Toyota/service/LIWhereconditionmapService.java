
/*
 * LIWhereconditionmapService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIWhereconditionmapRecord;
import com.leadics.Toyota.dao.LIWhereconditionmapDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIWhereconditionmapService extends LIService
{
	static LogUtils logger = new LogUtils(LIWhereconditionmapService.class.getName());


	public LIWhereconditionmapRecord[] loadLIWhereconditionmapRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIWhereconditionmapRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWhereconditionmapRecords", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			LIWhereconditionmapRecord[] results = dao.loadLIWhereconditionmapRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIWhereconditionmapRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIWhereconditionmapRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIWhereconditionmapRecord loadFirstLIWhereconditionmapRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIWhereconditionmapRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIWhereconditionmapRecord", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			LIWhereconditionmapRecord result = dao.loadFirstLIWhereconditionmapRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIWhereconditionmapRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIWhereconditionmapRecord searchFirstLIWhereconditionmapRecord(LIWhereconditionmapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIWhereconditionmapRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIWhereconditionmapRecord", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			LIWhereconditionmapRecord[] records = dao.searchLIWhereconditionmapRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIWhereconditionmapRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIWhereconditionmapRecord searchFirstLIWhereconditionmapRecordExactUpper(LIWhereconditionmapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIWhereconditionmapRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIWhereconditionmapRecordsExactUpper", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			LIWhereconditionmapRecord[] records = dao.searchLIWhereconditionmapRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIWhereconditionmapRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIWhereconditionmapRecord[] searchLIWhereconditionmapRecords(LIWhereconditionmapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIWhereconditionmapRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWhereconditionmapRecords", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			LIWhereconditionmapRecord[] records = dao.searchLIWhereconditionmapRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIWhereconditionmapRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIWhereconditionmapRecordCount(LIWhereconditionmapRecord record)
	throws Exception
	{
		return loadLIWhereconditionmapRecordCount(record, null);
	}


	public int loadLIWhereconditionmapRecordCount(LIWhereconditionmapRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIWhereconditionmapRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWhereconditionmapRecordCount", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIWhereconditionmapRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIWhereconditionmapRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIWhereconditionmapRecord loadLIWhereconditionmapRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIWhereconditionmapRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWhereconditionmapRecord", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			LIWhereconditionmapRecord result = dao.loadFirstLIWhereconditionmapRecord(key);
			logger.trace("loadLIWhereconditionmapRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIWhereconditionmapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIWhereconditionmapRecordSearchResultByPage(LIWhereconditionmapRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIWhereconditionmapRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIWhereconditionmapRecordSearchResultByPage(LIWhereconditionmapRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIWhereconditionmapRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIWhereconditionmapRecordSearchResult", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIWhereconditionmapRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIWhereconditionmapRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIWhereconditionmapRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIWhereconditionmapRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIWhereconditionmapRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIWhereconditionmapRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIWhereconditionmapRecordSearchResultByPageQuery", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIWhereconditionmapRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIWhereconditionmapRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIWhereconditionmapRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIWhereconditionmapRecord(LIWhereconditionmapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIWhereconditionmapRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIWhereconditionmapRecord", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			int result = dao.insertLIWhereconditionmapRecord(record);
			logger.trace("insertLIWhereconditionmapRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIWhereconditionmapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIWhereconditionmapRecord(LIWhereconditionmapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIWhereconditionmapRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIWhereconditionmapRecord", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			boolean result = dao.updateLIWhereconditionmapRecord(record);
			logger.trace("updateLIWhereconditionmapRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIWhereconditionmapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIWhereconditionmapRecordNonNull(LIWhereconditionmapRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIWhereconditionmapRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIWhereconditionmapRecordNonNull", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			LIWhereconditionmapRecord dbRecord = dao.loadLIWhereconditionmapRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIWhereconditionmapRecord(dbRecord);
			logger.trace("updateLIWhereconditionmapRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIWhereconditionmapRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIWhereconditionmapRecord(LIWhereconditionmapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIWhereconditionmapRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIWhereconditionmapRecord", null);
			LIWhereconditionmapDAO dao = new LIWhereconditionmapDAO();
			boolean result = dao.deleteLIWhereconditionmapRecord(record);
			logger.trace("deleteLIWhereconditionmapRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIWhereconditionmapRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}

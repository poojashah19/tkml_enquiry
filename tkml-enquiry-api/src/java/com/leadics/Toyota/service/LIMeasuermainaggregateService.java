
/*
 * LIMeasuermainaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIMeasuermainaggregateRecord;
import com.leadics.Toyota.dao.LIMeasuermainaggregateDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIMeasuermainaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LIMeasuermainaggregateService.class.getName());


	public LIMeasuermainaggregateRecord[] loadLIMeasuermainaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIMeasuermainaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIMeasuermainaggregateRecords", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			LIMeasuermainaggregateRecord[] results = dao.loadLIMeasuermainaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIMeasuermainaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIMeasuermainaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIMeasuermainaggregateRecord loadFirstLIMeasuermainaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIMeasuermainaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIMeasuermainaggregateRecord", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			LIMeasuermainaggregateRecord result = dao.loadFirstLIMeasuermainaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIMeasuermainaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIMeasuermainaggregateRecord searchFirstLIMeasuermainaggregateRecord(LIMeasuermainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIMeasuermainaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIMeasuermainaggregateRecord", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			LIMeasuermainaggregateRecord[] records = dao.searchLIMeasuermainaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIMeasuermainaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIMeasuermainaggregateRecord searchFirstLIMeasuermainaggregateRecordExactUpper(LIMeasuermainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIMeasuermainaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIMeasuermainaggregateRecordsExactUpper", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			LIMeasuermainaggregateRecord[] records = dao.searchLIMeasuermainaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIMeasuermainaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIMeasuermainaggregateRecord[] searchLIMeasuermainaggregateRecords(LIMeasuermainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIMeasuermainaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIMeasuermainaggregateRecords", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			LIMeasuermainaggregateRecord[] records = dao.searchLIMeasuermainaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIMeasuermainaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIMeasuermainaggregateRecordCount(LIMeasuermainaggregateRecord record)
	throws Exception
	{
		return loadLIMeasuermainaggregateRecordCount(record, null);
	}


	public int loadLIMeasuermainaggregateRecordCount(LIMeasuermainaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIMeasuermainaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIMeasuermainaggregateRecordCount", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIMeasuermainaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIMeasuermainaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIMeasuermainaggregateRecord loadLIMeasuermainaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIMeasuermainaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIMeasuermainaggregateRecord", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			LIMeasuermainaggregateRecord result = dao.loadLIMeasuermainaggregateRecord(key);
			logger.trace("loadLIMeasuermainaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIMeasuermainaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIMeasuermainaggregateRecordSearchResultByPage(LIMeasuermainaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIMeasuermainaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIMeasuermainaggregateRecordSearchResultByPage(LIMeasuermainaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIMeasuermainaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIMeasuermainaggregateRecordSearchResult", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIMeasuermainaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIMeasuermainaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIMeasuermainaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIMeasuermainaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIMeasuermainaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIMeasuermainaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIMeasuermainaggregateRecordSearchResultByPageQuery", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIMeasuermainaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIMeasuermainaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIMeasuermainaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIMeasuermainaggregateRecord(LIMeasuermainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIMeasuermainaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIMeasuermainaggregateRecord", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			int result = dao.insertLIMeasuermainaggregateRecord(record);
			logger.trace("insertLIMeasuermainaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIMeasuermainaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIMeasuermainaggregateRecord(LIMeasuermainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIMeasuermainaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIMeasuermainaggregateRecord", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			boolean result = dao.updateLIMeasuermainaggregateRecord(record);
			logger.trace("updateLIMeasuermainaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIMeasuermainaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIMeasuermainaggregateRecordNonNull(LIMeasuermainaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIMeasuermainaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIMeasuermainaggregateRecordNonNull", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			LIMeasuermainaggregateRecord dbRecord = dao.loadLIMeasuermainaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIMeasuermainaggregateRecord(dbRecord);
			logger.trace("updateLIMeasuermainaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIMeasuermainaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIMeasuermainaggregateRecord(LIMeasuermainaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIMeasuermainaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIMeasuermainaggregateRecord", null);
			LIMeasuermainaggregateDAO dao = new LIMeasuermainaggregateDAO();
			boolean result = dao.deleteLIMeasuermainaggregateRecord(record);
			logger.trace("deleteLIMeasuermainaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIMeasuermainaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}


/*
 * LILimodulesService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LILimodulesRecord;
import com.leadics.Toyota.dao.LILimodulesDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LILimodulesService extends LIService
{
	static LogUtils logger = new LogUtils(LILimodulesService.class.getName());


	public LILimodulesRecord[] loadLILimodulesRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLILimodulesRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILimodulesRecords", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			LILimodulesRecord[] results = dao.loadLILimodulesRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLILimodulesRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLILimodulesRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LILimodulesRecord loadFirstLILimodulesRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLILimodulesRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLILimodulesRecord", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			LILimodulesRecord result = dao.loadFirstLILimodulesRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLILimodulesRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LILimodulesRecord searchFirstLILimodulesRecord(LILimodulesRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLILimodulesRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLILimodulesRecord", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			LILimodulesRecord[] records = dao.searchLILimodulesRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLILimodulesRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LILimodulesRecord searchFirstLILimodulesRecordExactUpper(LILimodulesRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLILimodulesRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLILimodulesRecordsExactUpper", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			LILimodulesRecord[] records = dao.searchLILimodulesRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLILimodulesRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LILimodulesRecord[] searchLILimodulesRecords(LILimodulesRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLILimodulesRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILimodulesRecords", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			LILimodulesRecord[] records = dao.searchLILimodulesRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLILimodulesRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLILimodulesRecordCount(LILimodulesRecord record)
	throws Exception
	{
		return loadLILimodulesRecordCount(record, null);
	}


	public int loadLILimodulesRecordCount(LILimodulesRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLILimodulesRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILimodulesRecordCount", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLILimodulesRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLILimodulesRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LILimodulesRecord loadLILimodulesRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLILimodulesRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLILimodulesRecord", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			LILimodulesRecord result = dao.loadLILimodulesRecord(key);
			logger.trace("loadLILimodulesRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLILimodulesRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLILimodulesRecordSearchResultByPage(LILimodulesRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLILimodulesRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLILimodulesRecordSearchResultByPage(LILimodulesRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLILimodulesRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLILimodulesRecordSearchResult", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLILimodulesRecordCount(record);
			dao.setLimits(offset, maxrows);
			LILimodulesRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLILimodulesRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLILimodulesRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLILimodulesRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLILimodulesRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLILimodulesRecordSearchResultByPageQuery", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LILimodulesRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLILimodulesRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLILimodulesRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLILimodulesRecord(LILimodulesRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLILimodulesRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLILimodulesRecord", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			int result = dao.insertLILimodulesRecord(record);
			logger.trace("insertLILimodulesRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLILimodulesRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLILimodulesRecord(LILimodulesRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLILimodulesRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLILimodulesRecord", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			boolean result = dao.updateLILimodulesRecord(record);
			logger.trace("updateLILimodulesRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLILimodulesRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLILimodulesRecordNonNull(LILimodulesRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLILimodulesRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLILimodulesRecordNonNull", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			LILimodulesRecord dbRecord = dao.loadLILimodulesRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLILimodulesRecord(dbRecord);
			logger.trace("updateLILimodulesRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLILimodulesRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLILimodulesRecord(LILimodulesRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLILimodulesRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLILimodulesRecord", null);
			LILimodulesDAO dao = new LILimodulesDAO();
			boolean result = dao.deleteLILimodulesRecord(record);
			logger.trace("deleteLILimodulesRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLILimodulesRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}


/*
 * LIOthersmeanaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIOthersmeanaggregateRecord;
import com.leadics.Toyota.dao.LIOthersmeanaggregateDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIOthersmeanaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LIOthersmeanaggregateService.class.getName());


	public LIOthersmeanaggregateRecord[] loadLIOthersmeanaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOthersmeanaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOthersmeanaggregateRecords", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			LIOthersmeanaggregateRecord[] results = dao.loadLIOthersmeanaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIOthersmeanaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOthersmeanaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIOthersmeanaggregateRecord loadFirstLIOthersmeanaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIOthersmeanaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIOthersmeanaggregateRecord", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			LIOthersmeanaggregateRecord result = dao.loadFirstLIOthersmeanaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIOthersmeanaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOthersmeanaggregateRecord searchFirstLIOthersmeanaggregateRecord(LIOthersmeanaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIOthersmeanaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIOthersmeanaggregateRecord", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			LIOthersmeanaggregateRecord[] records = dao.searchLIOthersmeanaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIOthersmeanaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIOthersmeanaggregateRecord searchFirstLIOthersmeanaggregateRecordExactUpper(LIOthersmeanaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIOthersmeanaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIOthersmeanaggregateRecordsExactUpper", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			LIOthersmeanaggregateRecord[] records = dao.searchLIOthersmeanaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIOthersmeanaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOthersmeanaggregateRecord[] searchLIOthersmeanaggregateRecords(LIOthersmeanaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIOthersmeanaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOthersmeanaggregateRecords", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			LIOthersmeanaggregateRecord[] records = dao.searchLIOthersmeanaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIOthersmeanaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIOthersmeanaggregateRecordCount(LIOthersmeanaggregateRecord record)
	throws Exception
	{
		return loadLIOthersmeanaggregateRecordCount(record, null);
	}


	public int loadLIOthersmeanaggregateRecordCount(LIOthersmeanaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOthersmeanaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOthersmeanaggregateRecordCount", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIOthersmeanaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOthersmeanaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOthersmeanaggregateRecord loadLIOthersmeanaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOthersmeanaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOthersmeanaggregateRecord", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			LIOthersmeanaggregateRecord result = dao.loadLIOthersmeanaggregateRecord(key);
			logger.trace("loadLIOthersmeanaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOthersmeanaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIOthersmeanaggregateRecordSearchResultByPage(LIOthersmeanaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIOthersmeanaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIOthersmeanaggregateRecordSearchResultByPage(LIOthersmeanaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIOthersmeanaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIOthersmeanaggregateRecordSearchResult", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIOthersmeanaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIOthersmeanaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIOthersmeanaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIOthersmeanaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIOthersmeanaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIOthersmeanaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIOthersmeanaggregateRecordSearchResultByPageQuery", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIOthersmeanaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIOthersmeanaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIOthersmeanaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIOthersmeanaggregateRecord(LIOthersmeanaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIOthersmeanaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIOthersmeanaggregateRecord", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			int result = dao.insertLIOthersmeanaggregateRecord(record);
			logger.trace("insertLIOthersmeanaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIOthersmeanaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIOthersmeanaggregateRecord(LIOthersmeanaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIOthersmeanaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIOthersmeanaggregateRecord", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			boolean result = dao.updateLIOthersmeanaggregateRecord(record);
			logger.trace("updateLIOthersmeanaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIOthersmeanaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIOthersmeanaggregateRecordNonNull(LIOthersmeanaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIOthersmeanaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIOthersmeanaggregateRecordNonNull", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			LIOthersmeanaggregateRecord dbRecord = dao.loadLIOthersmeanaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIOthersmeanaggregateRecord(dbRecord);
			logger.trace("updateLIOthersmeanaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIOthersmeanaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIOthersmeanaggregateRecord(LIOthersmeanaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIOthersmeanaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIOthersmeanaggregateRecord", null);
			LIOthersmeanaggregateDAO dao = new LIOthersmeanaggregateDAO();
			boolean result = dao.deleteLIOthersmeanaggregateRecord(record);
			logger.trace("deleteLIOthersmeanaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIOthersmeanaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}

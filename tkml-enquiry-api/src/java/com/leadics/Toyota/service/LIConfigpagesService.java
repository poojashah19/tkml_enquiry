
/*
 * LIConfigpagesService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;

import com.leadics.Toyota.to.LIConfigpagesRecord;
import com.leadics.Toyota.dao.LIConfigpagesDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;

public class LIConfigpagesService extends LIService {

    static LogUtils logger = new LogUtils(LIConfigpagesService.class.getName());

    public LIConfigpagesRecord[] loadLIConfigpagesRecords(String query)
            throws Exception {
        try {
            logger.trace("loadLIConfigpagesRecords:" + query);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagesRecords", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            LIConfigpagesRecord[] results = dao.loadLIConfigpagesRecords(query);
            int resultRecordCount = 0;
            if (results != null) {
                resultRecordCount = results.length;
            }
            logger.trace("loadLIConfigpagesRecords:Fetched" + resultRecordCount);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return results;
        } catch (Exception exception) {
            logger.error("loadLIConfigpagesRecords" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LIConfigpagesRecord loadFirstLIConfigpagesRecord(String query)
            throws Exception {
        try {
            logger.trace("loadFirstLIConfigpagesRecord:" + query);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIConfigpagesRecord", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            LIConfigpagesRecord result = dao.loadFirstLIConfigpagesRecord(query);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("loadFirstLIConfigpagesRecord" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LIConfigpagesRecord searchFirstLIConfigpagesRecord(LIConfigpagesRecord record)
            throws Exception {
        try {
            logger.trace("searchFirstLIConfigpagesRecords:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConfigpagesRecord", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            LIConfigpagesRecord[] records = dao.searchLIConfigpagesRecords(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            if (records == null) {
                return null;
            }
            if (records.length < 1) {
                return null;
            }
            return records[0];
        } catch (Exception exception) {
            logger.error("searchFirstLIConfigpagesRecords" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LIConfigpagesRecord searchFirstLIConfigpagesRecordExactUpper(LIConfigpagesRecord record)
            throws Exception {
        try {
            logger.trace("searchFirstLIConfigpagesRecordsExactUpper:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConfigpagesRecordsExactUpper", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            LIConfigpagesRecord[] records = dao.searchLIConfigpagesRecordsExactUpper(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            if (records == null) {
                return null;
            }
            if (records.length < 1) {
                return null;
            }
            return records[0];
        } catch (Exception exception) {
            logger.error("searchFirstLIConfigpagesRecordsExactUpper" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LIConfigpagesRecord[] searchLIConfigpagesRecords(LIConfigpagesRecord record)
            throws Exception {
        try {
            logger.trace("searchLIConfigpagesRecords:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagesRecords", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            LIConfigpagesRecord[] records = dao.searchLIConfigpagesRecords(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return records;
        } catch (Exception exception) {
            logger.error("searchLIConfigpagesRecords" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public int loadLIConfigpagesRecordCount(LIConfigpagesRecord record)
            throws Exception {
        return loadLIConfigpagesRecordCount(record, null);
    }

    public int loadLIConfigpagesRecordCount(LIConfigpagesRecord record, String customCondition)
            throws Exception {
        try {
            logger.trace("loadLIConfigpagesRecordCount:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagesRecordCount", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            dao.setCustomCondition(customCondition);
            int resultcount = dao.loadLIConfigpagesRecordCount(record);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return resultcount;
        } catch (Exception exception) {
            logger.error("loadLIConfigpagesRecordCount" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public LIConfigpagesRecord loadLIConfigpagesRecord(String key)
            throws Exception {
        try {
            logger.trace("loadLIConfigpagesRecord:" + key);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagesRecord", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            LIConfigpagesRecord result = dao.loadLIConfigpagesRecord(key);
            logger.trace("loadLIConfigpagesRecord:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("loadLIConfigpagesRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public JSONObject getJSONLIConfigpagesRecordSearchResultByPage(LIConfigpagesRecord record, String offset, String maxrows, String orderBy)
            throws Exception {
        return getJSONLIConfigpagesRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
    }

    public JSONObject getJSONLIConfigpagesRecordSearchResultByPage(LIConfigpagesRecord record, String offset, String maxrows, String orderBy, String customCondition)
            throws Exception {
        try {
            logger.trace("getJSONLIConfigpagesRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConfigpagesRecordSearchResult", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            dao.setCustomCondition(customCondition);
            int totalCount = dao.loadLIConfigpagesRecordCount(record);
            dao.setLimits(offset, maxrows);
            LIConfigpagesRecord[] records = null;
            if (totalCount > 0) {
                dao.setOrderBy(orderBy);
                records = dao.searchLIConfigpagesRecords(record);
            }
            JSONObject resultObject = new JSONObject();
            resultObject.put("total", totalCount + "");
            JSONArray dataArray = new JSONArray();
            int recordCount = 0;
            if (records != null) {
                recordCount = records.length;
            }
            for (int index = 0; index < recordCount; index++) {
//                dataArray.add(records[index].getJSONObjectUI());
            }
            resultObject.put("rows", dataArray);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return resultObject;
        } catch (Exception exception) {
            logger.error("getJSONLIConfigpagesRecordSearchResult" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public JSONObject getJSONLIConfigpagesRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
            throws Exception {
        try {
            logger.trace("getJSONLIConfigpagesRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConfigpagesRecordSearchResultByPageQuery", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            int totalCount = dao.loadRecordCount(con, countQuery);
            dao.setLimits(offset, maxrows);
            LIConfigpagesRecord[] records = null;
            if (totalCount > 0) {
                records = dao.loadLIConfigpagesRecords(query, con, true);
            }
            JSONObject resultObject = new JSONObject();
            resultObject.put("total", totalCount + "");
            JSONArray dataArray = new JSONArray();
            int recordCount = 0;
            if (records != null) {
                recordCount = records.length;
            }
            for (int index = 0; index < recordCount; index++) {
//                dataArray.add(records[index].getJSONObjectUI());
            }
            resultObject.put("rows", dataArray);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return resultObject;
        } catch (Exception exception) {
            logger.error("getJSONLIConfigpagesRecordSearchResultByPageQuery" + getStackTrace(exception));
            throw new Exception("MFException:" + getErrorMessage(exception));
        }
    }

    public int insertLIConfigpagesRecord(LIConfigpagesRecord record)
            throws Exception {
        try {
            logger.trace("insertLIConfigpagesRecord:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIConfigpagesRecord", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            int result = dao.insertLIConfigpagesRecord(record);
            logger.trace("insertLIConfigpagesRecord:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("insertLIConfigpagesRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean updateLIConfigpagesRecord(LIConfigpagesRecord record)
            throws Exception {
        try {
            logger.trace("updateLIConfigpagesRecord:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConfigpagesRecord", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            boolean result = dao.updateLIConfigpagesRecord(record);
            logger.trace("updateLIConfigpagesRecord:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("updateLIConfigpagesRecord" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean updateLIConfigpagesRecordNonNull(LIConfigpagesRecord inputRecord)
            throws Exception {
        try {
            logger.trace("updateLIConfigpagesRecordNonNull:" + inputRecord);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConfigpagesRecordNonNull", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            LIConfigpagesRecord dbRecord = dao.loadLIConfigpagesRecord(inputRecord.getId());
            if (dbRecord == null) {
                throw new Exception("Record not found");
            }
//            dbRecord.loadNonNullContent(inputRecord);
            dbRecord.setActionSource(inputRecord.getActionSource());
            boolean result = dao.updateLIConfigpagesRecord(dbRecord);
            logger.trace("updateLIConfigpagesRecordNonNull:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("updateLIConfigpagesRecordNonNull" + getStackTrace(exception));
            throw exception;
        }
    }

    public boolean deleteLIConfigpagesRecord(LIConfigpagesRecord record)
            throws Exception {
        try {
            logger.trace("deleteLIConfigpagesRecord:" + record);
            String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIConfigpagesRecord", null);
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            boolean result = dao.deleteLIConfigpagesRecord(record);
            logger.trace("deleteLIConfigpagesRecord:Result:" + result);
            logger.generateFunctionEndTimerMessage(beginMesssage);
            return result;
        } catch (Exception exception) {
            logger.error("deleteLIConfigpagesRecord" + getStackTrace(exception));
            throw exception;
        }
    }
}


/*
 * LIMaillogService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIMaillogRecord;
import com.leadics.Toyota.dao.LIMaillogDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIMaillogService extends LIService
{
	static LogUtils logger = new LogUtils(LIMaillogService.class.getName());


	public LIMaillogRecord[] loadLIMaillogRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIMaillogRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIMaillogRecords", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			LIMaillogRecord[] results = dao.loadLIMaillogRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIMaillogRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIMaillogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIMaillogRecord loadFirstLIMaillogRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIMaillogRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIMaillogRecord", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			LIMaillogRecord result = dao.loadFirstLIMaillogRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIMaillogRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIMaillogRecord searchFirstLIMaillogRecord(LIMaillogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIMaillogRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIMaillogRecord", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			LIMaillogRecord[] records = dao.searchLIMaillogRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIMaillogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIMaillogRecord searchFirstLIMaillogRecordExactUpper(LIMaillogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIMaillogRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIMaillogRecordsExactUpper", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			LIMaillogRecord[] records = dao.searchLIMaillogRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIMaillogRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIMaillogRecord[] searchLIMaillogRecords(LIMaillogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIMaillogRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIMaillogRecords", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			LIMaillogRecord[] records = dao.searchLIMaillogRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIMaillogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIMaillogRecordCount(LIMaillogRecord record)
	throws Exception
	{
		return loadLIMaillogRecordCount(record, null);
	}


	public int loadLIMaillogRecordCount(LIMaillogRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIMaillogRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIMaillogRecordCount", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIMaillogRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIMaillogRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIMaillogRecord loadLIMaillogRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIMaillogRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIMaillogRecord", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			LIMaillogRecord result = dao.loadLIMaillogRecord(key);
			logger.trace("loadLIMaillogRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIMaillogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIMaillogRecordSearchResultByPage(LIMaillogRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIMaillogRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIMaillogRecordSearchResultByPage(LIMaillogRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIMaillogRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIMaillogRecordSearchResult", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIMaillogRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIMaillogRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIMaillogRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIMaillogRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIMaillogRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIMaillogRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIMaillogRecordSearchResultByPageQuery", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIMaillogRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIMaillogRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIMaillogRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIMaillogRecord(LIMaillogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIMaillogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIMaillogRecord", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			int result = dao.insertLIMaillogRecord(record);
			logger.trace("insertLIMaillogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIMaillogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIMaillogRecord(LIMaillogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIMaillogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIMaillogRecord", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			boolean result = dao.updateLIMaillogRecord(record);
			logger.trace("updateLIMaillogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIMaillogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIMaillogRecordNonNull(LIMaillogRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIMaillogRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIMaillogRecordNonNull", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			LIMaillogRecord dbRecord = dao.loadLIMaillogRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIMaillogRecord(dbRecord);
			logger.trace("updateLIMaillogRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIMaillogRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIMaillogRecord(LIMaillogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIMaillogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIMaillogRecord", null);
			LIMaillogDAO dao = new LIMaillogDAO();
			boolean result = dao.deleteLIMaillogRecord(record);
			logger.trace("deleteLIMaillogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIMaillogRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}

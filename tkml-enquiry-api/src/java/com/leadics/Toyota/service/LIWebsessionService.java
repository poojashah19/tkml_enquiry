
/*
 * LIWebsessionService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIWebsessionRecord;
import com.leadics.Toyota.dao.LIWebsessionDAO;
import com.leadics.Toyota.common.LIService;
import com.leadics.Toyota.to.LISystemuserRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
import java.sql.Timestamp;
public class LIWebsessionService extends LIService
{
	static LogUtils logger = new LogUtils(LIWebsessionService.class.getName());


	public LIWebsessionRecord[] loadLIWebsessionRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIWebsessionRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWebsessionRecords", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			LIWebsessionRecord[] results = dao.loadLIWebsessionRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIWebsessionRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIWebsessionRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIWebsessionRecord loadFirstLIWebsessionRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIWebsessionRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIWebsessionRecord", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			LIWebsessionRecord result = dao.loadFirstLIWebsessionRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIWebsessionRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIWebsessionRecord searchFirstLIWebsessionRecord(LIWebsessionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIWebsessionRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIWebsessionRecord", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			LIWebsessionRecord[] records = dao.searchLIWebsessionRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIWebsessionRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIWebsessionRecord searchFirstLIWebsessionRecordExactUpper(LIWebsessionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIWebsessionRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIWebsessionRecordsExactUpper", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			LIWebsessionRecord[] records = dao.searchLIWebsessionRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIWebsessionRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIWebsessionRecord[] searchLIWebsessionRecords(LIWebsessionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIWebsessionRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWebsessionRecords", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			LIWebsessionRecord[] records = dao.searchLIWebsessionRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIWebsessionRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIWebsessionRecordCount(LIWebsessionRecord record)
	throws Exception
	{
		return loadLIWebsessionRecordCount(record, null);
	}


	public int loadLIWebsessionRecordCount(LIWebsessionRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIWebsessionRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWebsessionRecordCount", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIWebsessionRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIWebsessionRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIWebsessionRecord loadLIWebsessionRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIWebsessionRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWebsessionRecord", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			LIWebsessionRecord result = dao.loadLIWebsessionRecord(key);
			logger.trace("loadLIWebsessionRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIWebsessionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIWebsessionRecordSearchResultByPage(LIWebsessionRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIWebsessionRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIWebsessionRecordSearchResultByPage(LIWebsessionRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIWebsessionRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIWebsessionRecordSearchResult", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIWebsessionRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIWebsessionRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIWebsessionRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIWebsessionRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIWebsessionRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIWebsessionRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIWebsessionRecordSearchResultByPageQuery", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIWebsessionRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIWebsessionRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIWebsessionRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIWebsessionRecord(LIWebsessionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIWebsessionRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIWebsessionRecord", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			int result = dao.insertLIWebsessionRecord(record);
			logger.trace("insertLIWebsessionRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIWebsessionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIWebsessionRecord(LIWebsessionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIWebsessionRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIWebsessionRecord", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			boolean result = dao.updateLIWebsessionRecord(record);
			logger.trace("updateLIWebsessionRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIWebsessionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIWebsessionRecordNonNull(LIWebsessionRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIWebsessionRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIWebsessionRecordNonNull", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			LIWebsessionRecord dbRecord = dao.loadLIWebsessionRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIWebsessionRecord(dbRecord);
			logger.trace("updateLIWebsessionRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIWebsessionRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIWebsessionRecord(LIWebsessionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIWebsessionRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIWebsessionRecord", null);
			LIWebsessionDAO dao = new LIWebsessionDAO();
			boolean result = dao.deleteLIWebsessionRecord(record);
			logger.trace("deleteLIWebsessionRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIWebsessionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

        
        public boolean isValidSession(LISystemuserRecord userRecord) throws Exception {
            String userId = userRecord.getUserid();
            String sessionId = userRecord.getSessionId();
            String id = userRecord.getId();
            boolean isValid = super.isValidSession(userId, sessionId);
            LIWebsessionRecord record = searchByUser(userId, sessionId);
            if(record == null ) return false;
            record.setId(id);
            record.setUserid(userId);
            record.setSessionid(sessionId);
            
            if(isValid) {
                record.setLastacc(""+ new DateUtils().getCurrentSQLDateObject());
                new LIWebsessionDAO().updateLIWebsessionLastAccessedRecord(record);
            } else {
                record.setClosedat(""+ new DateUtils().getCurrentSQLDateObject());
                record.setClosetype("EXPIRE");
                 new LIWebsessionDAO().updateLIWebsessionClosedRecord(record);
                 
                new LIService().eventLogger( "N.A.","session expire","logout",sessionId );
                 
            }
           
            
            return isValid;
        }
        public void closeSession(LISystemuserRecord userRecord) throws Exception {
            LIWebsessionRecord record = getSessionRecord(userRecord);
            record.setClosetype("Logout");
             new LIWebsessionDAO().updateLIWebsessionClosedRecord(record);
        }
        private LIWebsessionRecord searchByUser(String userName, String sessionId) throws Exception {
            String query = "select * from web_session where USER_ID =\""+ userName+ "\" "
                    + "and SESSIONID =\""+sessionId+"\" and CLOSED_AT is null order by id desc limit 1;" ;
            
            return loadFirstLIWebsessionRecord(query);
            
        }

        private LIWebsessionRecord getSessionRecord(LISystemuserRecord userRecord) {
            LIWebsessionRecord sessionRecord = new LIWebsessionRecord();
            sessionRecord.setId(userRecord.getId());
            sessionRecord.setSessionid(userRecord.getSessionId());
            //System.out.println("TEST"+userRecord.getSessionId());
            sessionRecord.setUserid(userRecord.getUserid());;
            return sessionRecord;
        }



}


/*
 * LIOtplogService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIOtplogRecord;
import com.leadics.Toyota.dao.LIOtplogDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIOtplogService extends LIService
{
	static LogUtils logger = new LogUtils(LIOtplogService.class.getName());


	public LIOtplogRecord[] loadLIOtplogRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOtplogRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOtplogRecords", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			LIOtplogRecord[] results = dao.loadLIOtplogRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIOtplogRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOtplogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIOtplogRecord loadFirstLIOtplogRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIOtplogRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIOtplogRecord", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			LIOtplogRecord result = dao.loadFirstLIOtplogRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIOtplogRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOtplogRecord searchFirstLIOtplogRecord(LIOtplogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIOtplogRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIOtplogRecord", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			LIOtplogRecord[] records = dao.searchLIOtplogRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIOtplogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIOtplogRecord searchFirstLIOtplogRecordExactUpper(LIOtplogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIOtplogRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIOtplogRecordsExactUpper", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			LIOtplogRecord[] records = dao.searchLIOtplogRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIOtplogRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOtplogRecord[] searchLIOtplogRecords(LIOtplogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIOtplogRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOtplogRecords", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			LIOtplogRecord[] records = dao.searchLIOtplogRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIOtplogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIOtplogRecordCount(LIOtplogRecord record)
	throws Exception
	{
		return loadLIOtplogRecordCount(record, null);
	}


	public int loadLIOtplogRecordCount(LIOtplogRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOtplogRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOtplogRecordCount", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIOtplogRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOtplogRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOtplogRecord loadLIOtplogRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOtplogRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOtplogRecord", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			LIOtplogRecord result = dao.loadLIOtplogRecord(key);
			logger.trace("loadLIOtplogRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOtplogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIOtplogRecordSearchResultByPage(LIOtplogRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIOtplogRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIOtplogRecordSearchResultByPage(LIOtplogRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIOtplogRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIOtplogRecordSearchResult", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIOtplogRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIOtplogRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIOtplogRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIOtplogRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIOtplogRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIOtplogRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIOtplogRecordSearchResultByPageQuery", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIOtplogRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIOtplogRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIOtplogRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIOtplogRecord(LIOtplogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIOtplogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIOtplogRecord", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			int result = dao.insertLIOtplogRecord(record);
			logger.trace("insertLIOtplogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIOtplogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIOtplogRecord(LIOtplogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIOtplogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIOtplogRecord", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			boolean result = dao.updateLIOtplogRecord(record);
			logger.trace("updateLIOtplogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIOtplogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIOtplogRecordNonNull(LIOtplogRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIOtplogRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIOtplogRecordNonNull", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			LIOtplogRecord dbRecord = dao.loadLIOtplogRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIOtplogRecord(dbRecord);
			logger.trace("updateLIOtplogRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIOtplogRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIOtplogRecord(LIOtplogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIOtplogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIOtplogRecord", null);
			LIOtplogDAO dao = new LIOtplogDAO();
			boolean result = dao.deleteLIOtplogRecord(record);
			logger.trace("deleteLIOtplogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIOtplogRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}

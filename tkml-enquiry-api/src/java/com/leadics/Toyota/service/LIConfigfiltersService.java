
/*
 * LIConfigfiltersService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIConfigfiltersRecord;
import com.leadics.Toyota.dao.LIConfigfiltersDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIConfigfiltersService extends LIService
{
	static LogUtils logger = new LogUtils(LIConfigfiltersService.class.getName());


	public LIConfigfiltersRecord[] loadLIConfigfiltersRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConfigfiltersRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigfiltersRecords", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			LIConfigfiltersRecord[] results = dao.loadLIConfigfiltersRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIConfigfiltersRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConfigfiltersRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIConfigfiltersRecord loadFirstLIConfigfiltersRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIConfigfiltersRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIConfigfiltersRecord", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			LIConfigfiltersRecord result = dao.loadFirstLIConfigfiltersRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIConfigfiltersRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConfigfiltersRecord searchFirstLIConfigfiltersRecord(LIConfigfiltersRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIConfigfiltersRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConfigfiltersRecord", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			LIConfigfiltersRecord[] records = dao.searchLIConfigfiltersRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIConfigfiltersRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIConfigfiltersRecord searchFirstLIConfigfiltersRecordExactUpper(LIConfigfiltersRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIConfigfiltersRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConfigfiltersRecordsExactUpper", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			LIConfigfiltersRecord[] records = dao.searchLIConfigfiltersRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIConfigfiltersRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConfigfiltersRecord[] searchLIConfigfiltersRecords(LIConfigfiltersRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIConfigfiltersRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigfiltersRecords", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			LIConfigfiltersRecord[] records = dao.searchLIConfigfiltersRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIConfigfiltersRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIConfigfiltersRecordCount(LIConfigfiltersRecord record)
	throws Exception
	{
		return loadLIConfigfiltersRecordCount(record, null);
	}


	public int loadLIConfigfiltersRecordCount(LIConfigfiltersRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConfigfiltersRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigfiltersRecordCount", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIConfigfiltersRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConfigfiltersRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConfigfiltersRecord loadLIConfigfiltersRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConfigfiltersRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigfiltersRecord", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			LIConfigfiltersRecord result = dao.loadLIConfigfiltersRecord(key);
			logger.trace("loadLIConfigfiltersRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConfigfiltersRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIConfigfiltersRecordSearchResultByPage(LIConfigfiltersRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIConfigfiltersRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIConfigfiltersRecordSearchResultByPage(LIConfigfiltersRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIConfigfiltersRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConfigfiltersRecordSearchResult", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIConfigfiltersRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIConfigfiltersRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIConfigfiltersRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIConfigfiltersRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIConfigfiltersRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIConfigfiltersRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConfigfiltersRecordSearchResultByPageQuery", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIConfigfiltersRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIConfigfiltersRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIConfigfiltersRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIConfigfiltersRecord(LIConfigfiltersRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIConfigfiltersRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIConfigfiltersRecord", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			int result = dao.insertLIConfigfiltersRecord(record);
			logger.trace("insertLIConfigfiltersRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIConfigfiltersRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIConfigfiltersRecord(LIConfigfiltersRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIConfigfiltersRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConfigfiltersRecord", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			boolean result = dao.updateLIConfigfiltersRecord(record);
			logger.trace("updateLIConfigfiltersRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIConfigfiltersRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIConfigfiltersRecordNonNull(LIConfigfiltersRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIConfigfiltersRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConfigfiltersRecordNonNull", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			LIConfigfiltersRecord dbRecord = dao.loadLIConfigfiltersRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIConfigfiltersRecord(dbRecord);
			logger.trace("updateLIConfigfiltersRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIConfigfiltersRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIConfigfiltersRecord(LIConfigfiltersRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIConfigfiltersRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIConfigfiltersRecord", null);
			LIConfigfiltersDAO dao = new LIConfigfiltersDAO();
			boolean result = dao.deleteLIConfigfiltersRecord(record);
			logger.trace("deleteLIConfigfiltersRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIConfigfiltersRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}


/*
 * LIForgotpwdrequestService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIForgotpwdrequestRecord;
import com.leadics.Toyota.dao.LIForgotpwdrequestDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIForgotpwdrequestService extends LIService
{
	static LogUtils logger = new LogUtils(LIForgotpwdrequestService.class.getName());


	public LIForgotpwdrequestRecord[] loadLIForgotpwdrequestRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIForgotpwdrequestRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIForgotpwdrequestRecords", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			LIForgotpwdrequestRecord[] results = dao.loadLIForgotpwdrequestRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIForgotpwdrequestRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIForgotpwdrequestRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIForgotpwdrequestRecord loadFirstLIForgotpwdrequestRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIForgotpwdrequestRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIForgotpwdrequestRecord", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			LIForgotpwdrequestRecord result = dao.loadFirstLIForgotpwdrequestRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIForgotpwdrequestRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIForgotpwdrequestRecord searchFirstLIForgotpwdrequestRecord(LIForgotpwdrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIForgotpwdrequestRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIForgotpwdrequestRecord", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			LIForgotpwdrequestRecord[] records = dao.searchLIForgotpwdrequestRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIForgotpwdrequestRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIForgotpwdrequestRecord searchFirstLIForgotpwdrequestRecordExactUpper(LIForgotpwdrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIForgotpwdrequestRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIForgotpwdrequestRecordsExactUpper", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			LIForgotpwdrequestRecord[] records = dao.searchLIForgotpwdrequestRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIForgotpwdrequestRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIForgotpwdrequestRecord[] searchLIForgotpwdrequestRecords(LIForgotpwdrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIForgotpwdrequestRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIForgotpwdrequestRecords", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			LIForgotpwdrequestRecord[] records = dao.searchLIForgotpwdrequestRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIForgotpwdrequestRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIForgotpwdrequestRecordCount(LIForgotpwdrequestRecord record)
	throws Exception
	{
		return loadLIForgotpwdrequestRecordCount(record, null);
	}


	public int loadLIForgotpwdrequestRecordCount(LIForgotpwdrequestRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIForgotpwdrequestRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIForgotpwdrequestRecordCount", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIForgotpwdrequestRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIForgotpwdrequestRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIForgotpwdrequestRecord loadLIForgotpwdrequestRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIForgotpwdrequestRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIForgotpwdrequestRecord", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			LIForgotpwdrequestRecord result = dao.loadLIForgotpwdrequestRecord(key);
			logger.trace("loadLIForgotpwdrequestRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIForgotpwdrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIForgotpwdrequestRecordSearchResultByPage(LIForgotpwdrequestRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIForgotpwdrequestRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIForgotpwdrequestRecordSearchResultByPage(LIForgotpwdrequestRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIForgotpwdrequestRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIForgotpwdrequestRecordSearchResult", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIForgotpwdrequestRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIForgotpwdrequestRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIForgotpwdrequestRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIForgotpwdrequestRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIForgotpwdrequestRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIForgotpwdrequestRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIForgotpwdrequestRecordSearchResultByPageQuery", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIForgotpwdrequestRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIForgotpwdrequestRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIForgotpwdrequestRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIForgotpwdrequestRecord(LIForgotpwdrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIForgotpwdrequestRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIForgotpwdrequestRecord", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			int result = dao.insertLIForgotpwdrequestRecord(record);
			logger.trace("insertLIForgotpwdrequestRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIForgotpwdrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIForgotpwdrequestRecord(LIForgotpwdrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIForgotpwdrequestRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIForgotpwdrequestRecord", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			boolean result = dao.updateLIForgotpwdrequestRecord(record);
			logger.trace("updateLIForgotpwdrequestRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIForgotpwdrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIForgotpwdrequestRecordNonNull(LIForgotpwdrequestRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIForgotpwdrequestRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIForgotpwdrequestRecordNonNull", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			LIForgotpwdrequestRecord dbRecord = dao.loadLIForgotpwdrequestRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIForgotpwdrequestRecord(dbRecord);
			logger.trace("updateLIForgotpwdrequestRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIForgotpwdrequestRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIForgotpwdrequestRecord(LIForgotpwdrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIForgotpwdrequestRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIForgotpwdrequestRecord", null);
			LIForgotpwdrequestDAO dao = new LIForgotpwdrequestDAO();
			boolean result = dao.deleteLIForgotpwdrequestRecord(record);
			logger.trace("deleteLIForgotpwdrequestRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIForgotpwdrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}


/*
 * LIAuditlogService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIAuditlogRecord;
import com.leadics.Toyota.dao.LIAuditlogDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIAuditlogService extends LIService
{
	static LogUtils logger = new LogUtils(LIAuditlogService.class.getName());


	public LIAuditlogRecord[] loadLIAuditlogRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIAuditlogRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIAuditlogRecords", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			LIAuditlogRecord[] results = dao.loadLIAuditlogRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIAuditlogRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIAuditlogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIAuditlogRecord loadFirstLIAuditlogRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIAuditlogRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIAuditlogRecord", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			LIAuditlogRecord result = dao.loadFirstLIAuditlogRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIAuditlogRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIAuditlogRecord searchFirstLIAuditlogRecord(LIAuditlogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIAuditlogRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIAuditlogRecord", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			LIAuditlogRecord[] records = dao.searchLIAuditlogRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIAuditlogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIAuditlogRecord searchFirstLIAuditlogRecordExactUpper(LIAuditlogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIAuditlogRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIAuditlogRecordsExactUpper", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			LIAuditlogRecord[] records = dao.searchLIAuditlogRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIAuditlogRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIAuditlogRecord[] searchLIAuditlogRecords(LIAuditlogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIAuditlogRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIAuditlogRecords", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			LIAuditlogRecord[] records = dao.searchLIAuditlogRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIAuditlogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIAuditlogRecordCount(LIAuditlogRecord record)
	throws Exception
	{
		return loadLIAuditlogRecordCount(record, null);
	}


	public int loadLIAuditlogRecordCount(LIAuditlogRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIAuditlogRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIAuditlogRecordCount", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIAuditlogRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIAuditlogRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIAuditlogRecord loadLIAuditlogRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIAuditlogRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIAuditlogRecord", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			LIAuditlogRecord result = dao.loadLIAuditlogRecord(key);
			logger.trace("loadLIAuditlogRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIAuditlogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIAuditlogRecordSearchResultByPage(LIAuditlogRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIAuditlogRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIAuditlogRecordSearchResultByPage(LIAuditlogRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIAuditlogRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIAuditlogRecordSearchResult", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIAuditlogRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIAuditlogRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIAuditlogRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIAuditlogRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIAuditlogRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIAuditlogRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIAuditlogRecordSearchResultByPageQuery", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIAuditlogRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIAuditlogRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIAuditlogRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIAuditlogRecord(LIAuditlogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIAuditlogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIAuditlogRecord", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			int result = dao.insertLIAuditlogRecord(record);
			logger.trace("insertLIAuditlogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIAuditlogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIAuditlogRecord(LIAuditlogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIAuditlogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIAuditlogRecord", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			boolean result = dao.updateLIAuditlogRecord(record);
			logger.trace("updateLIAuditlogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIAuditlogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIAuditlogRecordNonNull(LIAuditlogRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIAuditlogRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIAuditlogRecordNonNull", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			LIAuditlogRecord dbRecord = dao.loadLIAuditlogRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIAuditlogRecord(dbRecord);
			logger.trace("updateLIAuditlogRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIAuditlogRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIAuditlogRecord(LIAuditlogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIAuditlogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIAuditlogRecord", null);
			LIAuditlogDAO dao = new LIAuditlogDAO();
			boolean result = dao.deleteLIAuditlogRecord(record);
			logger.trace("deleteLIAuditlogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIAuditlogRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}


/*
 * LIUserverifyService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIUserverifyRecord;
import com.leadics.Toyota.dao.LIUserverifyDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIUserverifyService extends LIService
{
	static LogUtils logger = new LogUtils(LIUserverifyService.class.getName());


	public LIUserverifyRecord[] loadLIUserverifyRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserverifyRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserverifyRecords", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			LIUserverifyRecord[] results = dao.loadLIUserverifyRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIUserverifyRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserverifyRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIUserverifyRecord loadFirstLIUserverifyRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIUserverifyRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIUserverifyRecord", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			LIUserverifyRecord result = dao.loadFirstLIUserverifyRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIUserverifyRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserverifyRecord searchFirstLIUserverifyRecord(LIUserverifyRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIUserverifyRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIUserverifyRecord", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			LIUserverifyRecord[] records = dao.searchLIUserverifyRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIUserverifyRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIUserverifyRecord searchFirstLIUserverifyRecordExactUpper(LIUserverifyRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIUserverifyRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIUserverifyRecordsExactUpper", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			LIUserverifyRecord[] records = dao.searchLIUserverifyRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIUserverifyRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserverifyRecord[] searchLIUserverifyRecords(LIUserverifyRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIUserverifyRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserverifyRecords", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			LIUserverifyRecord[] records = dao.searchLIUserverifyRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIUserverifyRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIUserverifyRecordCount(LIUserverifyRecord record)
	throws Exception
	{
		return loadLIUserverifyRecordCount(record, null);
	}


	public int loadLIUserverifyRecordCount(LIUserverifyRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserverifyRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserverifyRecordCount", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIUserverifyRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserverifyRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserverifyRecord loadLIUserverifyRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserverifyRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserverifyRecord", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			LIUserverifyRecord result = dao.loadLIUserverifyRecord(key);
			logger.trace("loadLIUserverifyRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserverifyRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIUserverifyRecordSearchResultByPage(LIUserverifyRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIUserverifyRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIUserverifyRecordSearchResultByPage(LIUserverifyRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIUserverifyRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIUserverifyRecordSearchResult", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIUserverifyRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIUserverifyRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIUserverifyRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIUserverifyRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIUserverifyRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIUserverifyRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIUserverifyRecordSearchResultByPageQuery", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIUserverifyRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIUserverifyRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIUserverifyRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIUserverifyRecord(LIUserverifyRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIUserverifyRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIUserverifyRecord", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			int result = dao.insertLIUserverifyRecord(record);
			logger.trace("insertLIUserverifyRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIUserverifyRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIUserverifyRecord(LIUserverifyRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIUserverifyRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIUserverifyRecord", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			boolean result = dao.updateLIUserverifyRecord(record);
			logger.trace("updateLIUserverifyRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIUserverifyRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIUserverifyRecordNonNull(LIUserverifyRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIUserverifyRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIUserverifyRecordNonNull", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			LIUserverifyRecord dbRecord = dao.loadLIUserverifyRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIUserverifyRecord(dbRecord);
			logger.trace("updateLIUserverifyRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIUserverifyRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIUserverifyRecord(LIUserverifyRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIUserverifyRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIUserverifyRecord", null);
			LIUserverifyDAO dao = new LIUserverifyDAO();
			boolean result = dao.deleteLIUserverifyRecord(record);
			logger.trace("deleteLIUserverifyRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIUserverifyRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}


/*
 * LICalenderfilterService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LICalenderfilterRecord;
import com.leadics.Toyota.dao.LICalenderfilterDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LICalenderfilterService extends LIService
{
	static LogUtils logger = new LogUtils(LICalenderfilterService.class.getName());


	public LICalenderfilterRecord[] loadLICalenderfilterRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLICalenderfilterRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICalenderfilterRecords", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			LICalenderfilterRecord[] results = dao.loadLICalenderfilterRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLICalenderfilterRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLICalenderfilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICalenderfilterRecord loadFirstLICalenderfilterRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLICalenderfilterRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLICalenderfilterRecord", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			LICalenderfilterRecord result = dao.loadFirstLICalenderfilterRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLICalenderfilterRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICalenderfilterRecord searchFirstLICalenderfilterRecord(LICalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICalenderfilterRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICalenderfilterRecord", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			LICalenderfilterRecord[] records = dao.searchLICalenderfilterRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICalenderfilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICalenderfilterRecord searchFirstLICalenderfilterRecordExactUpper(LICalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICalenderfilterRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICalenderfilterRecordsExactUpper", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			LICalenderfilterRecord[] records = dao.searchLICalenderfilterRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICalenderfilterRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICalenderfilterRecord[] searchLICalenderfilterRecords(LICalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLICalenderfilterRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICalenderfilterRecords", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			LICalenderfilterRecord[] records = dao.searchLICalenderfilterRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLICalenderfilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLICalenderfilterRecordCount(LICalenderfilterRecord record)
	throws Exception
	{
		return loadLICalenderfilterRecordCount(record, null);
	}


	public int loadLICalenderfilterRecordCount(LICalenderfilterRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLICalenderfilterRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICalenderfilterRecordCount", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLICalenderfilterRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLICalenderfilterRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICalenderfilterRecord loadLICalenderfilterRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLICalenderfilterRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICalenderfilterRecord", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			LICalenderfilterRecord result = dao.loadLICalenderfilterRecord(key);
			logger.trace("loadLICalenderfilterRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLICalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLICalenderfilterRecordSearchResultByPage(LICalenderfilterRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLICalenderfilterRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLICalenderfilterRecordSearchResultByPage(LICalenderfilterRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICalenderfilterRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICalenderfilterRecordSearchResult", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLICalenderfilterRecordCount(record);
			dao.setLimits(offset, maxrows);
			LICalenderfilterRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLICalenderfilterRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICalenderfilterRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLICalenderfilterRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICalenderfilterRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICalenderfilterRecordSearchResultByPageQuery", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LICalenderfilterRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLICalenderfilterRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICalenderfilterRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLICalenderfilterRecord(LICalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLICalenderfilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLICalenderfilterRecord", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			int result = dao.insertLICalenderfilterRecord(record);
			logger.trace("insertLICalenderfilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLICalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICalenderfilterRecord(LICalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLICalenderfilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICalenderfilterRecord", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			boolean result = dao.updateLICalenderfilterRecord(record);
			logger.trace("updateLICalenderfilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICalenderfilterRecordNonNull(LICalenderfilterRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLICalenderfilterRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICalenderfilterRecordNonNull", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			LICalenderfilterRecord dbRecord = dao.loadLICalenderfilterRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLICalenderfilterRecord(dbRecord);
			logger.trace("updateLICalenderfilterRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICalenderfilterRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLICalenderfilterRecord(LICalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLICalenderfilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLICalenderfilterRecord", null);
			LICalenderfilterDAO dao = new LICalenderfilterDAO();
			boolean result = dao.deleteLICalenderfilterRecord(record);
			logger.trace("deleteLICalenderfilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLICalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}

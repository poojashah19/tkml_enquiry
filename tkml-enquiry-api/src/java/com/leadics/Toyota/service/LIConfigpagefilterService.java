
/*
 * LIConfigpagefilterService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.service;
import com.leadics.Toyota.to.LIConfigpagefilterRecord;
import com.leadics.Toyota.dao.LIConfigpagefilterDAO;
import com.leadics.Toyota.common.LIService;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIConfigpagefilterService extends LIService
{
	static LogUtils logger = new LogUtils(LIConfigpagefilterService.class.getName());


	public LIConfigpagefilterRecord[] loadLIConfigpagefilterRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConfigpagefilterRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagefilterRecords", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			LIConfigpagefilterRecord[] results = dao.loadLIConfigpagefilterRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIConfigpagefilterRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConfigpagefilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIConfigpagefilterRecord loadFirstLIConfigpagefilterRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIConfigpagefilterRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIConfigpagefilterRecord", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			LIConfigpagefilterRecord result = dao.loadFirstLIConfigpagefilterRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIConfigpagefilterRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConfigpagefilterRecord searchFirstLIConfigpagefilterRecord(LIConfigpagefilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIConfigpagefilterRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConfigpagefilterRecord", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			LIConfigpagefilterRecord[] records = dao.searchLIConfigpagefilterRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIConfigpagefilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIConfigpagefilterRecord searchFirstLIConfigpagefilterRecordExactUpper(LIConfigpagefilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIConfigpagefilterRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConfigpagefilterRecordsExactUpper", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			LIConfigpagefilterRecord[] records = dao.searchLIConfigpagefilterRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIConfigpagefilterRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConfigpagefilterRecord[] searchLIConfigpagefilterRecords(LIConfigpagefilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIConfigpagefilterRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagefilterRecords", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			LIConfigpagefilterRecord[] records = dao.searchLIConfigpagefilterRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIConfigpagefilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIConfigpagefilterRecordCount(LIConfigpagefilterRecord record)
	throws Exception
	{
		return loadLIConfigpagefilterRecordCount(record, null);
	}


	public int loadLIConfigpagefilterRecordCount(LIConfigpagefilterRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConfigpagefilterRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagefilterRecordCount", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIConfigpagefilterRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConfigpagefilterRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConfigpagefilterRecord loadLIConfigpagefilterRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConfigpagefilterRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConfigpagefilterRecord", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			LIConfigpagefilterRecord result = dao.loadLIConfigpagefilterRecord(key);
			logger.trace("loadLIConfigpagefilterRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConfigpagefilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIConfigpagefilterRecordSearchResultByPage(LIConfigpagefilterRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIConfigpagefilterRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIConfigpagefilterRecordSearchResultByPage(LIConfigpagefilterRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIConfigpagefilterRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConfigpagefilterRecordSearchResult", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIConfigpagefilterRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIConfigpagefilterRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIConfigpagefilterRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
//				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIConfigpagefilterRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIConfigpagefilterRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIConfigpagefilterRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConfigpagefilterRecordSearchResultByPageQuery", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIConfigpagefilterRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIConfigpagefilterRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
//				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIConfigpagefilterRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIConfigpagefilterRecord(LIConfigpagefilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIConfigpagefilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIConfigpagefilterRecord", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			int result = dao.insertLIConfigpagefilterRecord(record);
			logger.trace("insertLIConfigpagefilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIConfigpagefilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIConfigpagefilterRecord(LIConfigpagefilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIConfigpagefilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConfigpagefilterRecord", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			boolean result = dao.updateLIConfigpagefilterRecord(record);
			logger.trace("updateLIConfigpagefilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIConfigpagefilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIConfigpagefilterRecordNonNull(LIConfigpagefilterRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIConfigpagefilterRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConfigpagefilterRecordNonNull", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			LIConfigpagefilterRecord dbRecord = dao.loadLIConfigpagefilterRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
//			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIConfigpagefilterRecord(dbRecord);
			logger.trace("updateLIConfigpagefilterRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIConfigpagefilterRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIConfigpagefilterRecord(LIConfigpagefilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIConfigpagefilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIConfigpagefilterRecord", null);
			LIConfigpagefilterDAO dao = new LIConfigpagefilterDAO();
			boolean result = dao.deleteLIConfigpagefilterRecord(record);
			logger.trace("deleteLIConfigpagefilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIConfigpagefilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}

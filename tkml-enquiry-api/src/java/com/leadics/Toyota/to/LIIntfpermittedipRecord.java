
/*
 * LIIntfpermittedipRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.to;
import com.leadics.Toyota.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIIntfpermittedipRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIIntfpermittedipRecord.class.getName());

	private String id;
	private String intfcode;
	private String permittedip;
	private String rstatus;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getIntfcode()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intfcode);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intfcode);
		}
		else
		{
			return intfcode;
		}
	}

	public String getPermittedip()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(permittedip);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(permittedip);
		}
		else
		{
			return permittedip;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setIntfcode(String value)
	{
		intfcode = value;
	}

	public void setPermittedip(String value)
	{
		permittedip = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nintfcode:" + intfcode +
				"\npermittedip:" + permittedip +
				"\nrstatus:" + rstatus +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\n";
	}

	public void loadContent(LIIntfpermittedipRecord inputRecord)
	{
		setId(inputRecord.getId());
		setIntfcode(inputRecord.getIntfcode());
		setPermittedip(inputRecord.getPermittedip());
		setRstatus(inputRecord.getRstatus());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
	}

	public void loadNonNullContent(LIIntfpermittedipRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getIntfcode(), inputRecord.getIntfcode()))
		{
			setIntfcode(StringUtils.noNull(inputRecord.getIntfcode()));
		}
		if (StringUtils.hasChanged(getPermittedip(), inputRecord.getPermittedip()))
		{
			setPermittedip(StringUtils.noNull(inputRecord.getPermittedip()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("intfcode",StringUtils.noNull(intfcode));				
		obj.put("permittedip",StringUtils.noNull(permittedip));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		intfcode = StringUtils.getValueFromJSONObject(obj, "intfcode");				
		permittedip = StringUtils.getValueFromJSONObject(obj, "permittedip");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("intf_code",StringUtils.noNull(intfcode));				
		obj.put("permitted_ip",StringUtils.noNull(permittedip));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "intf_permitted_ip");

		columnList.add("id");				
		columnList.add("intf_code");				
		columnList.add("permitted_ip");				
		columnList.add("rstatus");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}

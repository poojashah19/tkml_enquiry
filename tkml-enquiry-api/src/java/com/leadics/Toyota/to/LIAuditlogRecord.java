
/*
 * LIAuditlogRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.to;
import com.leadics.Toyota.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIAuditlogRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIAuditlogRecord.class.getName());

	private String id;
	private String entityname;
	private String institutionid;
	private String message;
	private String actiontype;
	private String userid;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;
	private String sourceid;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getEntityname()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(entityname);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(entityname);
		}
		else
		{
			return entityname;
		}
	}

	public String getInstitutionid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(institutionid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(institutionid);
		}
		else
		{       
			return institutionid ;
		}
	}

	public String getMessage()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(message);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(message);
		}
		else
		{
			return message;
		}
	}

	public String getActiontype()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(actiontype);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(actiontype);
		}
		else
		{
			return actiontype;
		}
	}

	public String getUserid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(userid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(userid);
		}
		else
		{
			return userid;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}

	public String getSourceid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sourceid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sourceid);
		}
		else
		{
			return sourceid;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setEntityname(String value)
	{
		entityname = value;
	}

	public void setInstitutionid(String value)
	{
		institutionid = value;
	}

	public void setMessage(String value)
	{
		message = value;
	}

	public void setActiontype(String value)
	{
		actiontype = value;
	}

	public void setUserid(String value)
	{
		userid = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}

	public void setSourceid(String value)
	{
		sourceid = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nentityname:" + entityname +
				"\ninstitutionid:" + institutionid +
				"\nmessage:" + message +
				"\nactiontype:" + actiontype +
				"\nuserid:" + userid +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\nsourceid:" + sourceid +
				"\n";
	}

	public void loadContent(LIAuditlogRecord inputRecord)
	{
		setId(inputRecord.getId());
		setEntityname(inputRecord.getEntityname());
		setInstitutionid(inputRecord.getInstitutionid());
		setMessage(inputRecord.getMessage());
		setActiontype(inputRecord.getActiontype());
		setUserid(inputRecord.getUserid());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
		setSourceid(inputRecord.getSourceid());
	}

	public void loadNonNullContent(LIAuditlogRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getEntityname(), inputRecord.getEntityname()))
		{
			setEntityname(StringUtils.noNull(inputRecord.getEntityname()));
		}
		if (StringUtils.hasChanged(getInstitutionid(), inputRecord.getInstitutionid()))
		{
			setInstitutionid(StringUtils.noNull(inputRecord.getInstitutionid()));
		}
		if (StringUtils.hasChanged(getMessage(), inputRecord.getMessage()))
		{
			setMessage(StringUtils.noNull(inputRecord.getMessage()));
		}
		if (StringUtils.hasChanged(getActiontype(), inputRecord.getActiontype()))
		{
			setActiontype(StringUtils.noNull(inputRecord.getActiontype()));
		}
		if (StringUtils.hasChanged(getUserid(), inputRecord.getUserid()))
		{
			setUserid(StringUtils.noNull(inputRecord.getUserid()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
		if (StringUtils.hasChanged(getSourceid(), inputRecord.getSourceid()))
		{
			setSourceid(StringUtils.noNull(inputRecord.getSourceid()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("entityname",StringUtils.noNull(entityname));				
		obj.put("institutionid",StringUtils.noNull(institutionid));				
		obj.put("message",StringUtils.noNull(message));				
		obj.put("actiontype",StringUtils.noNull(actiontype));				
		obj.put("userid",StringUtils.noNull(userid));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));				
		obj.put("sourceid",StringUtils.noNull(sourceid));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		entityname = StringUtils.getValueFromJSONObject(obj, "entityname");				
		institutionid = StringUtils.getValueFromJSONObject(obj, "institutionid");				
		message = StringUtils.getValueFromJSONObject(obj, "message");				
		actiontype = StringUtils.getValueFromJSONObject(obj, "actiontype");				
		userid = StringUtils.getValueFromJSONObject(obj, "userid");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");				
		sourceid = StringUtils.getValueFromJSONObject(obj, "sourceid");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("entity_name",StringUtils.noNull(entityname));				
		obj.put("institution_id",StringUtils.noNull(institutionid));				
		obj.put("message",StringUtils.noNull(message));				
		obj.put("action_type",StringUtils.noNull(actiontype));				
		obj.put("user_id",StringUtils.noNull(userid));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));				
		obj.put("source_id",StringUtils.noNull(sourceid));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "audit_log");

		columnList.add("id");				
		columnList.add("entity_name");				
		columnList.add("institution_id");				
		columnList.add("message");				
		columnList.add("action_type");				
		columnList.add("user_id");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");				
		columnList.add("source_id");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}

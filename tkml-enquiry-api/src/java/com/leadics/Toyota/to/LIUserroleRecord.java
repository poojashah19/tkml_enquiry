
/*
 * LIUserroleRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.to;
import com.leadics.Toyota.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIUserroleRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIUserroleRecord.class.getName());

	private String id;
	private String rolename;
	private String defaulturl;
	private String adminperm;
	private String makerperm;
	private String checkerperm;
	private String globaladminperm;
	private String reportperm;
	private String auditperm;
	private String madeby;
	private String madeat;
	private String checkedby;
	private String checkedat;
	private String makerlastcmt;
	private String checkerlastcmt;
	private String currappstatus;
	private String adminlastcmt;
	private String rstatus;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;
	private String institutionid;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getRolename()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rolename);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rolename);
		}
		else
		{
			return rolename;
		}
	}

	public String getDefaulturl()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(defaulturl);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(defaulturl);
		}
		else
		{
			return defaulturl;
		}
	}

	public String getAdminperm()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adminperm);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adminperm);
		}
		else
		{
			return adminperm;
		}
	}

	public String getMakerperm()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(makerperm);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(makerperm);
		}
		else
		{
			return makerperm;
		}
	}

	public String getCheckerperm()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkerperm);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkerperm);
		}
		else
		{
			return checkerperm;
		}
	}

	public String getGlobaladminperm()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(globaladminperm);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(globaladminperm);
		}
		else
		{
			return globaladminperm;
		}
	}

	public String getReportperm()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(reportperm);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(reportperm);
		}
		else
		{
			return reportperm;
		}
	}

	public String getAuditperm()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(auditperm);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(auditperm);
		}
		else
		{
			return auditperm;
		}
	}

	public String getMadeby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(madeby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(madeby);
		}
		else
		{
			return madeby;
		}
	}

	public String getMadeat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(madeat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(madeat);
		}
		else
		{
			return madeat;
		}
	}

	public String getCheckedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkedby);
		}
		else
		{
			return checkedby;
		}
	}

	public String getCheckedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkedat);
		}
		else
		{
			return checkedat;
		}
	}

	public String getMakerlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(makerlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(makerlastcmt);
		}
		else
		{
			return makerlastcmt;
		}
	}

	public String getCheckerlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkerlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkerlastcmt);
		}
		else
		{
			return checkerlastcmt;
		}
	}

	public String getCurrappstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(currappstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(currappstatus);
		}
		else
		{
			return currappstatus;
		}
	}

	public String getAdminlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adminlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adminlastcmt);
		}
		else
		{
			return adminlastcmt;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}

	public String getInstitutionid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(institutionid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(institutionid);
		}
		else
		{
			return institutionid;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setRolename(String value)
	{
		rolename = value;
	}

	public void setDefaulturl(String value)
	{
		defaulturl = value;
	}

	public void setAdminperm(String value)
	{
		adminperm = value;
	}

	public void setMakerperm(String value)
	{
		makerperm = value;
	}

	public void setCheckerperm(String value)
	{
		checkerperm = value;
	}

	public void setGlobaladminperm(String value)
	{
		globaladminperm = value;
	}

	public void setReportperm(String value)
	{
		reportperm = value;
	}

	public void setAuditperm(String value)
	{
		auditperm = value;
	}

	public void setMadeby(String value)
	{
		madeby = value;
	}

	public void setMadeat(String value)
	{
		madeat = value;
	}

	public void setCheckedby(String value)
	{
		checkedby = value;
	}

	public void setCheckedat(String value)
	{
		checkedat = value;
	}

	public void setMakerlastcmt(String value)
	{
		makerlastcmt = value;
	}

	public void setCheckerlastcmt(String value)
	{
		checkerlastcmt = value;
	}

	public void setCurrappstatus(String value)
	{
		currappstatus = value;
	}

	public void setAdminlastcmt(String value)
	{
		adminlastcmt = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}

	public void setInstitutionid(String value)
	{
		institutionid = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nrolename:" + rolename +
				"\ndefaulturl:" + defaulturl +
				"\nadminperm:" + adminperm +
				"\nmakerperm:" + makerperm +
				"\ncheckerperm:" + checkerperm +
				"\nglobaladminperm:" + globaladminperm +
				"\nreportperm:" + reportperm +
				"\nauditperm:" + auditperm +
				"\nmadeby:" + madeby +
				"\nmadeat:" + madeat +
				"\ncheckedby:" + checkedby +
				"\ncheckedat:" + checkedat +
				"\nmakerlastcmt:" + makerlastcmt +
				"\ncheckerlastcmt:" + checkerlastcmt +
				"\ncurrappstatus:" + currappstatus +
				"\nadminlastcmt:" + adminlastcmt +
				"\nrstatus:" + rstatus +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\ninstitutionid:" + institutionid +
				"\n";
	}

	public void loadContent(LIUserroleRecord inputRecord)
	{
		setId(inputRecord.getId());
		setRolename(inputRecord.getRolename());
		setDefaulturl(inputRecord.getDefaulturl());
		setAdminperm(inputRecord.getAdminperm());
		setMakerperm(inputRecord.getMakerperm());
		setCheckerperm(inputRecord.getCheckerperm());
		setGlobaladminperm(inputRecord.getGlobaladminperm());
		setReportperm(inputRecord.getReportperm());
		setAuditperm(inputRecord.getAuditperm());
		setMadeby(inputRecord.getMadeby());
		setMadeat(inputRecord.getMadeat());
		setCheckedby(inputRecord.getCheckedby());
		setCheckedat(inputRecord.getCheckedat());
		setMakerlastcmt(inputRecord.getMakerlastcmt());
		setCheckerlastcmt(inputRecord.getCheckerlastcmt());
		setCurrappstatus(inputRecord.getCurrappstatus());
		setAdminlastcmt(inputRecord.getAdminlastcmt());
		setRstatus(inputRecord.getRstatus());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
		setInstitutionid(inputRecord.getInstitutionid());
	}

	public void loadNonNullContent(LIUserroleRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getRolename(), inputRecord.getRolename()))
		{
			setRolename(StringUtils.noNull(inputRecord.getRolename()));
		}
		if (StringUtils.hasChanged(getDefaulturl(), inputRecord.getDefaulturl()))
		{
			setDefaulturl(StringUtils.noNull(inputRecord.getDefaulturl()));
		}
		if (StringUtils.hasChanged(getAdminperm(), inputRecord.getAdminperm()))
		{
			setAdminperm(StringUtils.noNull(inputRecord.getAdminperm()));
		}
		if (StringUtils.hasChanged(getMakerperm(), inputRecord.getMakerperm()))
		{
			setMakerperm(StringUtils.noNull(inputRecord.getMakerperm()));
		}
		if (StringUtils.hasChanged(getCheckerperm(), inputRecord.getCheckerperm()))
		{
			setCheckerperm(StringUtils.noNull(inputRecord.getCheckerperm()));
		}
		if (StringUtils.hasChanged(getGlobaladminperm(), inputRecord.getGlobaladminperm()))
		{
			setGlobaladminperm(StringUtils.noNull(inputRecord.getGlobaladminperm()));
		}
		if (StringUtils.hasChanged(getReportperm(), inputRecord.getReportperm()))
		{
			setReportperm(StringUtils.noNull(inputRecord.getReportperm()));
		}
		if (StringUtils.hasChanged(getAuditperm(), inputRecord.getAuditperm()))
		{
			setAuditperm(StringUtils.noNull(inputRecord.getAuditperm()));
		}
		if (StringUtils.hasChanged(getMadeby(), inputRecord.getMadeby()))
		{
			setMadeby(StringUtils.noNull(inputRecord.getMadeby()));
		}
		if (StringUtils.hasChanged(getMadeat(), inputRecord.getMadeat()))
		{
			setMadeat(StringUtils.noNull(inputRecord.getMadeat()));
		}
		if (StringUtils.hasChanged(getCheckedby(), inputRecord.getCheckedby()))
		{
			setCheckedby(StringUtils.noNull(inputRecord.getCheckedby()));
		}
		if (StringUtils.hasChanged(getCheckedat(), inputRecord.getCheckedat()))
		{
			setCheckedat(StringUtils.noNull(inputRecord.getCheckedat()));
		}
		if (StringUtils.hasChanged(getMakerlastcmt(), inputRecord.getMakerlastcmt()))
		{
			setMakerlastcmt(StringUtils.noNull(inputRecord.getMakerlastcmt()));
		}
		if (StringUtils.hasChanged(getCheckerlastcmt(), inputRecord.getCheckerlastcmt()))
		{
			setCheckerlastcmt(StringUtils.noNull(inputRecord.getCheckerlastcmt()));
		}
		if (StringUtils.hasChanged(getCurrappstatus(), inputRecord.getCurrappstatus()))
		{
			setCurrappstatus(StringUtils.noNull(inputRecord.getCurrappstatus()));
		}
		if (StringUtils.hasChanged(getAdminlastcmt(), inputRecord.getAdminlastcmt()))
		{
			setAdminlastcmt(StringUtils.noNull(inputRecord.getAdminlastcmt()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
		if (StringUtils.hasChanged(getInstitutionid(), inputRecord.getInstitutionid()))
		{
			setInstitutionid(StringUtils.noNull(inputRecord.getInstitutionid()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("rolename",StringUtils.noNull(rolename));				
		obj.put("defaulturl",StringUtils.noNull(defaulturl));				
		obj.put("adminperm",StringUtils.noNull(adminperm));				
		obj.put("makerperm",StringUtils.noNull(makerperm));				
		obj.put("checkerperm",StringUtils.noNull(checkerperm));				
		obj.put("globaladminperm",StringUtils.noNull(globaladminperm));				
		obj.put("reportperm",StringUtils.noNull(reportperm));				
		obj.put("auditperm",StringUtils.noNull(auditperm));				
		obj.put("madeby",StringUtils.noNull(madeby));				
		obj.put("madeat",StringUtils.noNull(madeat));				
		obj.put("checkedby",StringUtils.noNull(checkedby));				
		obj.put("checkedat",StringUtils.noNull(checkedat));				
		obj.put("makerlastcmt",StringUtils.noNull(makerlastcmt));				
		obj.put("checkerlastcmt",StringUtils.noNull(checkerlastcmt));				
		obj.put("currappstatus",StringUtils.noNull(currappstatus));				
		obj.put("adminlastcmt",StringUtils.noNull(adminlastcmt));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));				
		obj.put("institutionid",StringUtils.noNull(institutionid));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		rolename = StringUtils.getValueFromJSONObject(obj, "rolename");				
		defaulturl = StringUtils.getValueFromJSONObject(obj, "defaulturl");				
		adminperm = StringUtils.getValueFromJSONObject(obj, "adminperm");				
		makerperm = StringUtils.getValueFromJSONObject(obj, "makerperm");				
		checkerperm = StringUtils.getValueFromJSONObject(obj, "checkerperm");				
		globaladminperm = StringUtils.getValueFromJSONObject(obj, "globaladminperm");				
		reportperm = StringUtils.getValueFromJSONObject(obj, "reportperm");				
		auditperm = StringUtils.getValueFromJSONObject(obj, "auditperm");				
		madeby = StringUtils.getValueFromJSONObject(obj, "madeby");				
		madeat = StringUtils.getValueFromJSONObject(obj, "madeat");				
		checkedby = StringUtils.getValueFromJSONObject(obj, "checkedby");				
		checkedat = StringUtils.getValueFromJSONObject(obj, "checkedat");				
		makerlastcmt = StringUtils.getValueFromJSONObject(obj, "makerlastcmt");				
		checkerlastcmt = StringUtils.getValueFromJSONObject(obj, "checkerlastcmt");				
		currappstatus = StringUtils.getValueFromJSONObject(obj, "currappstatus");				
		adminlastcmt = StringUtils.getValueFromJSONObject(obj, "adminlastcmt");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");				
		institutionid = StringUtils.getValueFromJSONObject(obj, "institutionid");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("rolename",StringUtils.noNull(rolename));				
		obj.put("default_url",StringUtils.noNull(defaulturl));				
		obj.put("admin_perm",StringUtils.noNull(adminperm));				
		obj.put("maker_perm",StringUtils.noNull(makerperm));				
		obj.put("checker_perm",StringUtils.noNull(checkerperm));				
		obj.put("global_admin_perm",StringUtils.noNull(globaladminperm));				
		obj.put("report_perm",StringUtils.noNull(reportperm));				
		obj.put("audit_perm",StringUtils.noNull(auditperm));				
		obj.put("made_by",StringUtils.noNull(madeby));				
		obj.put("made_at",StringUtils.noNull(madeat));				
		obj.put("checked_by",StringUtils.noNull(checkedby));				
		obj.put("checked_at",StringUtils.noNull(checkedat));				
		obj.put("maker_last_cmt",StringUtils.noNull(makerlastcmt));				
		obj.put("checker_last_cmt",StringUtils.noNull(checkerlastcmt));				
		obj.put("curr_app_status",StringUtils.noNull(currappstatus));				
		obj.put("admin_last_cmt",StringUtils.noNull(adminlastcmt));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));				
		obj.put("institution_id",StringUtils.noNull(institutionid));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "user_role");

		columnList.add("id");				
		columnList.add("rolename");				
		columnList.add("default_url");				
		columnList.add("admin_perm");				
		columnList.add("maker_perm");				
		columnList.add("checker_perm");				
		columnList.add("global_admin_perm");				
		columnList.add("report_perm");				
		columnList.add("audit_perm");				
		columnList.add("made_by");				
		columnList.add("made_at");				
		columnList.add("checked_by");				
		columnList.add("checked_at");				
		columnList.add("maker_last_cmt");				
		columnList.add("checker_last_cmt");				
		columnList.add("curr_app_status");				
		columnList.add("admin_last_cmt");				
		columnList.add("rstatus");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");				
		columnList.add("institution_id");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}

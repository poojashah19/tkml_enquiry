
/*
 * LIMaillogRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.to;
import com.leadics.Toyota.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIMaillogRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIMaillogRecord.class.getName());

	private String id;
	private String fromid;
	private String toids;
	private String mailmessage;
	private String datetime;
	private String respoinsemessage;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getFromid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(fromid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(fromid);
		}
		else
		{
			return fromid;
		}
	}

	public String getToids()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(toids);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(toids);
		}
		else
		{
			return toids;
		}
	}

	public String getMailmessage()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(mailmessage);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(mailmessage);
		}
		else
		{
			return mailmessage;
		}
	}

	public String getDatetime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(datetime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(datetime);
		}
		else
		{
			return datetime;
		}
	}

	public String getRespoinsemessage()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(respoinsemessage);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(respoinsemessage);
		}
		else
		{
			return respoinsemessage;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setFromid(String value)
	{
		fromid = value;
	}

	public void setToids(String value)
	{
		toids = value;
	}

	public void setMailmessage(String value)
	{
		mailmessage = value;
	}

	public void setDatetime(String value)
	{
		datetime = value;
	}

	public void setRespoinsemessage(String value)
	{
		respoinsemessage = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nfromid:" + fromid +
				"\ntoids:" + toids +
				"\nmailmessage:" + mailmessage +
				"\ndatetime:" + datetime +
				"\nrespoinsemessage:" + respoinsemessage +
				"\n";
	}

	public void loadContent(LIMaillogRecord inputRecord)
	{
		setId(inputRecord.getId());
		setFromid(inputRecord.getFromid());
		setToids(inputRecord.getToids());
		setMailmessage(inputRecord.getMailmessage());
		setDatetime(inputRecord.getDatetime());
		setRespoinsemessage(inputRecord.getRespoinsemessage());
	}

	public void loadNonNullContent(LIMaillogRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getFromid(), inputRecord.getFromid()))
		{
			setFromid(StringUtils.noNull(inputRecord.getFromid()));
		}
		if (StringUtils.hasChanged(getToids(), inputRecord.getToids()))
		{
			setToids(StringUtils.noNull(inputRecord.getToids()));
		}
		if (StringUtils.hasChanged(getMailmessage(), inputRecord.getMailmessage()))
		{
			setMailmessage(StringUtils.noNull(inputRecord.getMailmessage()));
		}
		if (StringUtils.hasChanged(getDatetime(), inputRecord.getDatetime()))
		{
			setDatetime(StringUtils.noNull(inputRecord.getDatetime()));
		}
		if (StringUtils.hasChanged(getRespoinsemessage(), inputRecord.getRespoinsemessage()))
		{
			setRespoinsemessage(StringUtils.noNull(inputRecord.getRespoinsemessage()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("fromid",StringUtils.noNull(fromid));				
		obj.put("toids",StringUtils.noNull(toids));				
		obj.put("mailmessage",StringUtils.noNull(mailmessage));				
		obj.put("datetime",StringUtils.noNull(datetime));				
		obj.put("respoinsemessage",StringUtils.noNull(respoinsemessage));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		fromid = StringUtils.getValueFromJSONObject(obj, "fromid");				
		toids = StringUtils.getValueFromJSONObject(obj, "toids");				
		mailmessage = StringUtils.getValueFromJSONObject(obj, "mailmessage");				
		datetime = StringUtils.getValueFromJSONObject(obj, "datetime");				
		respoinsemessage = StringUtils.getValueFromJSONObject(obj, "respoinsemessage");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("fromid",StringUtils.noNull(fromid));				
		obj.put("toids",StringUtils.noNull(toids));				
		obj.put("mailmessage",StringUtils.noNull(mailmessage));				
		obj.put("datetime",StringUtils.noNull(datetime));				
		obj.put("respoinsemessage",StringUtils.noNull(respoinsemessage));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "maillog");

		columnList.add("id");				
		columnList.add("fromid");				
		columnList.add("toids");				
		columnList.add("mailmessage");				
		columnList.add("datetime");				
		columnList.add("respoinsemessage");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}

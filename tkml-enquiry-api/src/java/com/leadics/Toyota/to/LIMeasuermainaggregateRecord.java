
/*
 * LIMeasuermainaggregateRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.to;
import com.leadics.Toyota.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIMeasuermainaggregateRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIMeasuermainaggregateRecord.class.getName());

	private String id;
	private String sheetcolomname;
	private String rawcolumnname;
	private String displayname;
	private String drilldownof;
	private String servicecity30jdp;
	private String month;
	private String modeldb;
	private String fuel;
	private String iqssum;
	private String iqscount;
	private String rstatus;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getSheetcolomname()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sheetcolomname);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sheetcolomname);
		}
		else
		{
			return sheetcolomname;
		}
	}

	public String getRawcolumnname()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rawcolumnname);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rawcolumnname);
		}
		else
		{
			return rawcolumnname;
		}
	}

	public String getDisplayname()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(displayname);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(displayname);
		}
		else
		{
			return displayname;
		}
	}

	public String getDrilldownof()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(drilldownof);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(drilldownof);
		}
		else
		{
			return drilldownof;
		}
	}

	public String getServicecity30jdp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(servicecity30jdp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(servicecity30jdp);
		}
		else
		{
			return servicecity30jdp;
		}
	}

	public String getMonth()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(month);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(month);
		}
		else
		{
			return month;
		}
	}

	public String getModeldb()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modeldb);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modeldb);
		}
		else
		{
			return modeldb;
		}
	}

	public String getFuel()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(fuel);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(fuel);
		}
		else
		{
			return fuel;
		}
	}

	public String getIqssum()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(iqssum);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(iqssum);
		}
		else
		{
			return iqssum;
		}
	}

	public String getIqscount()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(iqscount);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(iqscount);
		}
		else
		{
			return iqscount;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setSheetcolomname(String value)
	{
		sheetcolomname = value;
	}

	public void setRawcolumnname(String value)
	{
		rawcolumnname = value;
	}

	public void setDisplayname(String value)
	{
		displayname = value;
	}

	public void setDrilldownof(String value)
	{
		drilldownof = value;
	}

	public void setServicecity30jdp(String value)
	{
		servicecity30jdp = value;
	}

	public void setMonth(String value)
	{
		month = value;
	}

	public void setModeldb(String value)
	{
		modeldb = value;
	}

	public void setFuel(String value)
	{
		fuel = value;
	}

	public void setIqssum(String value)
	{
		iqssum = value;
	}

	public void setIqscount(String value)
	{
		iqscount = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nsheetcolomname:" + sheetcolomname +
				"\nrawcolumnname:" + rawcolumnname +
				"\ndisplayname:" + displayname +
				"\ndrilldownof:" + drilldownof +
				"\nservicecity30jdp:" + servicecity30jdp +
				"\nmonth:" + month +
				"\nmodeldb:" + modeldb +
				"\nfuel:" + fuel +
				"\niqssum:" + iqssum +
				"\niqscount:" + iqscount +
				"\nrstatus:" + rstatus +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\n";
	}

	public void loadContent(LIMeasuermainaggregateRecord inputRecord)
	{
		setId(inputRecord.getId());
		setSheetcolomname(inputRecord.getSheetcolomname());
		setRawcolumnname(inputRecord.getRawcolumnname());
		setDisplayname(inputRecord.getDisplayname());
		setDrilldownof(inputRecord.getDrilldownof());
		setServicecity30jdp(inputRecord.getServicecity30jdp());
		setMonth(inputRecord.getMonth());
		setModeldb(inputRecord.getModeldb());
		setFuel(inputRecord.getFuel());
		setIqssum(inputRecord.getIqssum());
		setIqscount(inputRecord.getIqscount());
		setRstatus(inputRecord.getRstatus());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
	}

	public void loadNonNullContent(LIMeasuermainaggregateRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getSheetcolomname(), inputRecord.getSheetcolomname()))
		{
			setSheetcolomname(StringUtils.noNull(inputRecord.getSheetcolomname()));
		}
		if (StringUtils.hasChanged(getRawcolumnname(), inputRecord.getRawcolumnname()))
		{
			setRawcolumnname(StringUtils.noNull(inputRecord.getRawcolumnname()));
		}
		if (StringUtils.hasChanged(getDisplayname(), inputRecord.getDisplayname()))
		{
			setDisplayname(StringUtils.noNull(inputRecord.getDisplayname()));
		}
		if (StringUtils.hasChanged(getDrilldownof(), inputRecord.getDrilldownof()))
		{
			setDrilldownof(StringUtils.noNull(inputRecord.getDrilldownof()));
		}
		if (StringUtils.hasChanged(getServicecity30jdp(), inputRecord.getServicecity30jdp()))
		{
			setServicecity30jdp(StringUtils.noNull(inputRecord.getServicecity30jdp()));
		}
		if (StringUtils.hasChanged(getMonth(), inputRecord.getMonth()))
		{
			setMonth(StringUtils.noNull(inputRecord.getMonth()));
		}
		if (StringUtils.hasChanged(getModeldb(), inputRecord.getModeldb()))
		{
			setModeldb(StringUtils.noNull(inputRecord.getModeldb()));
		}
		if (StringUtils.hasChanged(getFuel(), inputRecord.getFuel()))
		{
			setFuel(StringUtils.noNull(inputRecord.getFuel()));
		}
		if (StringUtils.hasChanged(getIqssum(), inputRecord.getIqssum()))
		{
			setIqssum(StringUtils.noNull(inputRecord.getIqssum()));
		}
		if (StringUtils.hasChanged(getIqscount(), inputRecord.getIqscount()))
		{
			setIqscount(StringUtils.noNull(inputRecord.getIqscount()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("sheetcolomname",StringUtils.noNull(sheetcolomname));				
		obj.put("rawcolumnname",StringUtils.noNull(rawcolumnname));				
		obj.put("displayname",StringUtils.noNull(displayname));				
		obj.put("drilldownof",StringUtils.noNull(drilldownof));				
		obj.put("servicecity30jdp",StringUtils.noNull(servicecity30jdp));				
		obj.put("month",StringUtils.noNull(month));				
		obj.put("modeldb",StringUtils.noNull(modeldb));				
		obj.put("fuel",StringUtils.noNull(fuel));				
		obj.put("iqssum",StringUtils.noNull(iqssum));				
		obj.put("iqscount",StringUtils.noNull(iqscount));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		sheetcolomname = StringUtils.getValueFromJSONObject(obj, "sheetcolomname");				
		rawcolumnname = StringUtils.getValueFromJSONObject(obj, "rawcolumnname");				
		displayname = StringUtils.getValueFromJSONObject(obj, "displayname");				
		drilldownof = StringUtils.getValueFromJSONObject(obj, "drilldownof");				
		servicecity30jdp = StringUtils.getValueFromJSONObject(obj, "servicecity30jdp");				
		month = StringUtils.getValueFromJSONObject(obj, "month");				
		modeldb = StringUtils.getValueFromJSONObject(obj, "modeldb");				
		fuel = StringUtils.getValueFromJSONObject(obj, "fuel");				
		iqssum = StringUtils.getValueFromJSONObject(obj, "iqssum");				
		iqscount = StringUtils.getValueFromJSONObject(obj, "iqscount");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("sheetcolomname",StringUtils.noNull(sheetcolomname));				
		obj.put("rawcolumnname",StringUtils.noNull(rawcolumnname));				
		obj.put("displayname",StringUtils.noNull(displayname));				
		obj.put("drilldownof",StringUtils.noNull(drilldownof));				
		obj.put("servicecity_30jdp",StringUtils.noNull(servicecity30jdp));				
		obj.put("month",StringUtils.noNull(month));				
		obj.put("modeldb",StringUtils.noNull(modeldb));				
		obj.put("fuel",StringUtils.noNull(fuel));				
		obj.put("iqssum",StringUtils.noNull(iqssum));				
		obj.put("iqscount",StringUtils.noNull(iqscount));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "measuer_main_aggregate");

		columnList.add("id");				
		columnList.add("sheetcolomname");				
		columnList.add("rawcolumnname");				
		columnList.add("displayname");				
		columnList.add("drilldownof");				
		columnList.add("servicecity_30jdp");				
		columnList.add("month");				
		columnList.add("modeldb");				
		columnList.add("fuel");				
		columnList.add("iqssum");				
		columnList.add("iqscount");				
		columnList.add("rstatus");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}

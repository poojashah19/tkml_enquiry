/*
* ICSmaillogRecord.java 
* Copyright (c) LeadICS Private Limited
* This software is the confidential and proprietary information of 
* LeadICS Private Limited You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with LeadICS Private Limited
* Project Name             : dealeraudit
* Module                   : Record beans
* Author                   : Subrahmanya Varma Sagi, LeadICS Private Limited
* Date                     : Mon Sep 19 09:41:34 IST 2016
* Change Revision
* ----------------------------------------------------------------
* Date            Author         Version#    Remarks/Description
*-----------------------------------------------------------------
*/
package com.leadics.Toyota.to;

 

 

 
 import com.google.gson.Gson;
 

 
public class ICSmaillogRecord 
 {
 	private int id;
 	private String FromID;
 	private String ToIDs;
 	private String MailMessage;
 	private String Datetime;
 	private String RespoinseMessage;
 

 

 
 	 public static ICSmaillogRecord getObject(String json) throws Exception
 
 	 {
 
 	 Gson gson = new Gson();
 
 	 return gson.fromJson(json, ICSmaillogRecord.class);
 
 	 }
 

 

 
 	public void setid(int id) 
 	{
 	 	 this.id= id;
 	 }
 
 	public void setFromID(String strFromID) 
 	{
 	 	 this.FromID= strFromID;
 	 }
 
 	public void setToIDs(String strToIDs) 
 	{
 	 	 this.ToIDs= strToIDs;
 	 }
 
 	public void setMailMessage(String strMailMessage) 
 	{
 	 	 this.MailMessage= strMailMessage;
 	 }
 
 	public void setDatetime(String strDatetime) 
 	{
 	 	 this.Datetime= strDatetime;
 	 }
 
 	public void setRespoinseMessage(String strRespoinseMessage) 
 	{
 	 	 this.RespoinseMessage= strRespoinseMessage;
 	 }
 

 

 

 

 
 	public int getid() 
 	 {
 	 	 return this.id;
 	 }
 
 	public String getFromID() 
 	 {
 	 	 return this.FromID;
 	 }
 
 	public String getToIDs() 
 	 {
 	 	 return this.ToIDs;
 	 }
 
 	public String getMailMessage() 
 	 {
 	 	 return this.MailMessage;
 	 }
 
 	public String getDatetime() 
 	 {
 	 	 return this.Datetime;
 	 }
 
 	public String getRespoinseMessage() 
 	 {
 	 	 return this.RespoinseMessage;
 	 }
 

 

 
 	 public String getJson() throws Exception
 
 	 {
 
 	 return new Gson().toJson(this);
 
 	 }
 

 
}

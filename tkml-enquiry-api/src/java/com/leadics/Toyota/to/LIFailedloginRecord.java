
/*
 * LIFailedloginRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.to;
import com.leadics.Toyota.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIFailedloginRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIFailedloginRecord.class.getName());

	private String id;
	private String username;
	private String email;
	private String source;
	private String createdat;
	private String createdby;
	private String modifiedat;
	private String modifiedby;
	private String failreason;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getUsername()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(username);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(username);
		}
		else
		{
			return username;
		}
	}

	public String getEmail()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(email);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(email);
		}
		else
		{
			return email;
		}
	}

	public String getSource()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(source);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(source);
		}
		else
		{
			return source;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}

	public String getFailreason()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(failreason);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(failreason);
		}
		else
		{
			return failreason;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setUsername(String value)
	{
		username = value;
	}

	public void setEmail(String value)
	{
		email = value;
	}

	public void setSource(String value)
	{
		source = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}

	public void setFailreason(String value)
	{
		failreason = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nusername:" + username +
				"\nemail:" + email +
				"\nsource:" + source +
				"\ncreatedat:" + createdat +
				"\ncreatedby:" + createdby +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\nfailreason:" + failreason +
				"\n";
	}

	public void loadContent(LIFailedloginRecord inputRecord)
	{
		setId(inputRecord.getId());
		setUsername(inputRecord.getUsername());
		setEmail(inputRecord.getEmail());
		setSource(inputRecord.getSource());
		setCreatedat(inputRecord.getCreatedat());
		setCreatedby(inputRecord.getCreatedby());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
		setFailreason(inputRecord.getFailreason());
	}

	public void loadNonNullContent(LIFailedloginRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getUsername(), inputRecord.getUsername()))
		{
			setUsername(StringUtils.noNull(inputRecord.getUsername()));
		}
		if (StringUtils.hasChanged(getEmail(), inputRecord.getEmail()))
		{
			setEmail(StringUtils.noNull(inputRecord.getEmail()));
		}
		if (StringUtils.hasChanged(getSource(), inputRecord.getSource()))
		{
			setSource(StringUtils.noNull(inputRecord.getSource()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
		if (StringUtils.hasChanged(getFailreason(), inputRecord.getFailreason()))
		{
			setFailreason(StringUtils.noNull(inputRecord.getFailreason()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("username",StringUtils.noNull(username));				
		obj.put("email",StringUtils.noNull(email));				
		obj.put("source",StringUtils.noNull(source));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));				
		obj.put("failreason",StringUtils.noNull(failreason));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		username = StringUtils.getValueFromJSONObject(obj, "username");				
		email = StringUtils.getValueFromJSONObject(obj, "email");				
		source = StringUtils.getValueFromJSONObject(obj, "source");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");				
		failreason = StringUtils.getValueFromJSONObject(obj, "failreason");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("username",StringUtils.noNull(username));				
		obj.put("email",StringUtils.noNull(email));				
		obj.put("source",StringUtils.noNull(source));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));				
		obj.put("failreason",StringUtils.noNull(failreason));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "failed_login");

		columnList.add("id");				
		columnList.add("username");				
		columnList.add("email");				
		columnList.add("source");				
		columnList.add("created_at");				
		columnList.add("created_by");				
		columnList.add("modified_at");				
		columnList.add("modified_by");				
		columnList.add("failreason");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}

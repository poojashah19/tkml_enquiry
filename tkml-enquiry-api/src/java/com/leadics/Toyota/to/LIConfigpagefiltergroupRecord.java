
/*
 * LIConfigpagefiltergroupRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.to;

import com.leadics.Toyota.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;

public class LIConfigpagefiltergroupRecord extends LIRecord {

    static LogUtils logger = new LogUtils(LIConfigpagefiltergroupRecord.class.getName());

    private String id;
    private String groupfiltername;
    private String slugName;

    private String filtername;
    private String filtertype;
    private String filterplacehodler;
    private int filterpriority;
    private String conditionvalue;
    private String parents;
    private String childs;
    private int defaultindex;
    private boolean containsall;
    private boolean containsnone;
    private String createdat;
    private String createdby;
    private String modifiedat;
    private String modifiedby;

    public String getId() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(id);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(id);
        } else {
            return id;
        }
    }

    public String getGroupfiltername() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(groupfiltername);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(groupfiltername);
        } else {
            return groupfiltername;
        }
    }

    public String getSlugName() {
        return slugName;
    }

    public void setSlugName(String slugName) {
        this.slugName = slugName;
    }

    public String getFiltername() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(filtername);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(filtername);
        } else {
            return filtername;
        }
    }

    public String getFiltertype() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(filtertype);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(filtertype);
        } else {
            return filtertype;
        }
    }

    public String getFilterplacehodler() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(filterplacehodler);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(filterplacehodler);
        } else {
            return filterplacehodler;
        }
    }

    public int getFilterpriority() {
        return filterpriority;
    }

    public String getConditionvalue() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(conditionvalue);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(conditionvalue);
        } else {
            return conditionvalue;
        }
    }

    public String getParents() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(parents);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(parents);
        } else {
            return parents;
        }
    }

    public String getChilds() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(childs);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(childs);
        } else {
            return childs;
        }
    }

    public int getDefaultindex() {
        return defaultindex;
    }

    public boolean getContainsall() {
        return containsall;
    }

    public boolean getContainsnone() {
        return containsnone;
    }

    public String getCreatedat() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(createdat);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(createdat);
        } else {
            return createdat;
        }
    }

    public String getCreatedby() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(createdby);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(createdby);
        } else {
            return createdby;
        }
    }

    public String getModifiedat() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(modifiedat);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(modifiedat);
        } else {
            return modifiedat;
        }
    }

    public String getModifiedby() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(modifiedby);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(modifiedby);
        } else {
            return modifiedby;
        }
    }

    public void setId(String value) {
        id = value;
    }

    public void setGroupfiltername(String value) {
        groupfiltername = value;
    }

    public void setFiltername(String value) {
        filtername = value;
    }

    public void setFiltertype(String value) {
        filtertype = value;
    }

    public void setFilterplacehodler(String value) {
        filterplacehodler = value;
    }

    public void setFilterpriority(int value) {
        filterpriority = value;
    }

    public void setConditionvalue(String value) {
        conditionvalue = value;
    }

    public void setParents(String value) {
        parents = value;
    }

    public void setChilds(String value) {
        childs = value;
    }

    public void setDefaultindex(int value) {
        defaultindex = value;
    }

    public void setContainsall(boolean value) {
        containsall = value;
    }

    public void setContainsnone(boolean value) {
        containsnone = value;
    }

    public void setCreatedat(String value) {
        createdat = value;
    }

    public void setCreatedby(String value) {
        createdby = value;
    }

    public void setModifiedat(String value) {
        modifiedat = value;
    }

    public void setModifiedby(String value) {
        modifiedby = value;
    }

    public String toString() {
        return "\nid:" + id
                + "\ngroupfiltername:" + groupfiltername
                + "\nfiltername:" + filtername
                + "\nfiltertype:" + filtertype
                + "\nfilterplacehodler:" + filterplacehodler
                + "\nfilterpriority:" + filterpriority
                + "\nconditionvalue:" + conditionvalue
                + "\nparents:" + parents
                + "\nchilds:" + childs
                + "\ndefaultindex:" + defaultindex
                + "\ncontainsall:" + containsall
                + "\ncontainsnone:" + containsnone
                + "\ncreatedat:" + createdat
                + "\ncreatedby:" + createdby
                + "\nmodifiedat:" + modifiedat
                + "\nmodifiedby:" + modifiedby
                + "\n";
    }

}

/*
* ICSsmslogRecord.java 
* Copyright (c) LeadICS Private Limited
* This software is the confidential and proprietary information of 
* LeadICS Private Limited You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with LeadICS Private Limited
* Project Name             : dealeraudit
* Module                   : Record beans
* Author                   : Subrahmanya Varma Sagi, LeadICS Private Limited
* Date                     : Mon Sep 19 09:41:35 IST 2016
* Change Revision
* ----------------------------------------------------------------
* Date            Author         Version#    Remarks/Description
*-----------------------------------------------------------------
*/
package com.leadics.Toyota.to;
 

 

 
 import com.google.gson.Gson;
 

 
public class ICSsmslogRecord 
 {
 	private int ID;
 	private String URL;
 	private String ResponseMessge;
 	private String Sentat;
 

 

 
 	 public static ICSsmslogRecord getObject(String json) throws Exception
 
 	 {
 
 	 Gson gson = new Gson();
 
 	 return gson.fromJson(json, ICSsmslogRecord.class);
 
 	 }
 

 

 
 	public void setID(int ID) 
 	{
 	 	 this.ID= ID;
 	 }
 
 	public void setURL(String strURL) 
 	{
 	 	 this.URL= strURL;
 	 }
 
 	public void setResponseMessge(String strResponseMessge) 
 	{
 	 	 this.ResponseMessge= strResponseMessge;
 	 }
 
 	public void setSentat(String strSentat) 
 	{
 	 	 this.Sentat= strSentat;
 	 }
 

 

 

 

 
 	public int getID() 
 	 {
 	 	 return this.ID;
 	 }
 
 	public String getURL() 
 	 {
 	 	 return this.URL;
 	 }
 
 	public String getResponseMessge() 
 	 {
 	 	 return this.ResponseMessge;
 	 }
 
 	public String getSentat() 
 	 {
 	 	 return this.Sentat;
 	 }
 

 

 
 	 public String getJson() throws Exception
 
 	 {
 
 	 return new Gson().toJson(this);
 
 	 }
 

 
}

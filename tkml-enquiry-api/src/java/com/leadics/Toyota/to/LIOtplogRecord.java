
/*
 * LIOtplogRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.to;
import com.leadics.Toyota.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIOtplogRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIOtplogRecord.class.getName());

	private String id;
	private String userid;
	private String usermobile;
	private String useremail;
	private String otp;
	private String otpstatus;
	private String otpid;
	private String request;
	private String response;
	private String createdon;
	private String createdby;
	private String modifiedon;
	private String modifiedby;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getUserid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(userid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(userid);
		}
		else
		{
			return userid;
		}
	}

	public String getUsermobile()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(usermobile);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(usermobile);
		}
		else
		{
			return usermobile;
		}
	}

	public String getUseremail()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(useremail);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(useremail);
		}
		else
		{
			return useremail;
		}
	}

	public String getOtp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(otp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(otp);
		}
		else
		{
			return otp;
		}
	}

	public String getOtpstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(otpstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(otpstatus);
		}
		else
		{
			return otpstatus;
		}
	}

	public String getOtpid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(otpid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(otpid);
		}
		else
		{
			return otpid;
		}
	}

	public String getRequest()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(request);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(request);
		}
		else
		{
			return request;
		}
	}

	public String getResponse()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(response);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(response);
		}
		else
		{
			return response;
		}
	}

	public String getCreatedon()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdon);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdon);
		}
		else
		{
			return createdon;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getModifiedon()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedon);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedon);
		}
		else
		{
			return modifiedon;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setUserid(String value)
	{
		userid = value;
	}

	public void setUsermobile(String value)
	{
		usermobile = value;
	}

	public void setUseremail(String value)
	{
		useremail = value;
	}

	public void setOtp(String value)
	{
		otp = value;
	}

	public void setOtpstatus(String value)
	{
		otpstatus = value;
	}

	public void setOtpid(String value)
	{
		otpid = value;
	}

	public void setRequest(String value)
	{
		request = value;
	}

	public void setResponse(String value)
	{
		response = value;
	}

	public void setCreatedon(String value)
	{
		createdon = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setModifiedon(String value)
	{
		modifiedon = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nuserid:" + userid +
				"\nusermobile:" + usermobile +
				"\nuseremail:" + useremail +
				"\notp:" + otp +
				"\notpstatus:" + otpstatus +
				"\notpid:" + otpid +
				"\nrequest:" + request +
				"\nresponse:" + response +
				"\ncreatedon:" + createdon +
				"\ncreatedby:" + createdby +
				"\nmodifiedon:" + modifiedon +
				"\nmodifiedby:" + modifiedby +
				"\n";
	}

	public void loadContent(LIOtplogRecord inputRecord)
	{
		setId(inputRecord.getId());
		setUserid(inputRecord.getUserid());
		setUsermobile(inputRecord.getUsermobile());
		setUseremail(inputRecord.getUseremail());
		setOtp(inputRecord.getOtp());
		setOtpstatus(inputRecord.getOtpstatus());
		setOtpid(inputRecord.getOtpid());
		setRequest(inputRecord.getRequest());
		setResponse(inputRecord.getResponse());
		setCreatedon(inputRecord.getCreatedon());
		setCreatedby(inputRecord.getCreatedby());
		setModifiedon(inputRecord.getModifiedon());
		setModifiedby(inputRecord.getModifiedby());
	}

	public void loadNonNullContent(LIOtplogRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getUserid(), inputRecord.getUserid()))
		{
			setUserid(StringUtils.noNull(inputRecord.getUserid()));
		}
		if (StringUtils.hasChanged(getUsermobile(), inputRecord.getUsermobile()))
		{
			setUsermobile(StringUtils.noNull(inputRecord.getUsermobile()));
		}
		if (StringUtils.hasChanged(getUseremail(), inputRecord.getUseremail()))
		{
			setUseremail(StringUtils.noNull(inputRecord.getUseremail()));
		}
		if (StringUtils.hasChanged(getOtp(), inputRecord.getOtp()))
		{
			setOtp(StringUtils.noNull(inputRecord.getOtp()));
		}
		if (StringUtils.hasChanged(getOtpstatus(), inputRecord.getOtpstatus()))
		{
			setOtpstatus(StringUtils.noNull(inputRecord.getOtpstatus()));
		}
		if (StringUtils.hasChanged(getOtpid(), inputRecord.getOtpid()))
		{
			setOtpid(StringUtils.noNull(inputRecord.getOtpid()));
		}
		if (StringUtils.hasChanged(getRequest(), inputRecord.getRequest()))
		{
			setRequest(StringUtils.noNull(inputRecord.getRequest()));
		}
		if (StringUtils.hasChanged(getResponse(), inputRecord.getResponse()))
		{
			setResponse(StringUtils.noNull(inputRecord.getResponse()));
		}
		if (StringUtils.hasChanged(getCreatedon(), inputRecord.getCreatedon()))
		{
			setCreatedon(StringUtils.noNull(inputRecord.getCreatedon()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getModifiedon(), inputRecord.getModifiedon()))
		{
			setModifiedon(StringUtils.noNull(inputRecord.getModifiedon()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("userid",StringUtils.noNull(userid));				
		obj.put("usermobile",StringUtils.noNull(usermobile));				
		obj.put("useremail",StringUtils.noNull(useremail));				
		obj.put("otp",StringUtils.noNull(otp));				
		obj.put("otpstatus",StringUtils.noNull(otpstatus));				
		obj.put("otpid",StringUtils.noNull(otpid));				
		obj.put("request",StringUtils.noNull(request));				
		obj.put("response",StringUtils.noNull(response));				
		obj.put("createdon",StringUtils.noNull(createdon));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("modifiedon",StringUtils.noNull(modifiedon));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		userid = StringUtils.getValueFromJSONObject(obj, "userid");				
		usermobile = StringUtils.getValueFromJSONObject(obj, "usermobile");				
		useremail = StringUtils.getValueFromJSONObject(obj, "useremail");				
		otp = StringUtils.getValueFromJSONObject(obj, "otp");				
		otpstatus = StringUtils.getValueFromJSONObject(obj, "otpstatus");				
		otpid = StringUtils.getValueFromJSONObject(obj, "otpid");				
		request = StringUtils.getValueFromJSONObject(obj, "request");				
		response = StringUtils.getValueFromJSONObject(obj, "response");				
		createdon = StringUtils.getValueFromJSONObject(obj, "createdon");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		modifiedon = StringUtils.getValueFromJSONObject(obj, "modifiedon");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("user_id",StringUtils.noNull(userid));				
		obj.put("user_mobile",StringUtils.noNull(usermobile));				
		obj.put("user_email",StringUtils.noNull(useremail));				
		obj.put("otp",StringUtils.noNull(otp));				
		obj.put("otp_status",StringUtils.noNull(otpstatus));				
		obj.put("otp_id",StringUtils.noNull(otpid));				
		obj.put("request",StringUtils.noNull(request));				
		obj.put("response",StringUtils.noNull(response));				
		obj.put("created_on",StringUtils.noNull(createdon));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("modified_on",StringUtils.noNull(modifiedon));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "otp_log");

		columnList.add("id");				
		columnList.add("user_id");				
		columnList.add("user_mobile");				
		columnList.add("user_email");				
		columnList.add("otp");				
		columnList.add("otp_status");				
		columnList.add("otp_id");				
		columnList.add("request");				
		columnList.add("response");				
		columnList.add("created_on");				
		columnList.add("created_by");				
		columnList.add("modified_on");				
		columnList.add("modified_by");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}

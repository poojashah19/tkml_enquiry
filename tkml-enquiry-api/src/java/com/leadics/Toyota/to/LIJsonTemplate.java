/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.to;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author HARSHA
 */
public class LIJsonTemplate <T> {
    
      private final Map <String, Object> values = new HashMap<>();
      
      public <T> void put( String key, List<T> value, Class<T> valueType ) {
        values.put( key, value );
      }

      public <T> T get( String key, Class<T> valueType ) {
        return ( T )values.get( key );
      } 

    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.to;

import java.util.List;

/**
 *
 * @author HARSHA
 */
public class FilterProperties {

    public String name, type, placeholder;
    boolean containsAll = false, containsNone = false;
    public double priority;
    int defaultIndex = 0;
    String conditionValue;
    String slugName;

    public List<String> parents, childs;
    public List<FilterProperties> filterGroups;

    public List<FilterProperties> getFilterGroups() {
        return filterGroups;
    }

    public int getDefaultIndex() {
        return defaultIndex;
    }

    public List<String> getParents() {
        return parents;
    }

    public String getConditionValue() {
        return conditionValue;
    }

    public void setConditionValue(String conditionValue) {
        this.conditionValue = conditionValue;
    }

    public List<String> getChilds() {
        return childs;
    }

    public boolean isContainsAll() {
        return containsAll;
    }

    public boolean isContainsNone() {
        return containsNone;
    }

    public String getName() {

        return name;
    }

    public String getSlugName() {
        return slugName;
    }

    public void setSlugName(String slugName) {
        this.slugName = slugName;
    }

    public void setName(String name) {
        this.name = name;

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }

    public void setContainsAll(boolean containsAll) {
        this.containsAll = containsAll;
    }

    public void setContainsNone(boolean containsNone) {
        this.containsNone = containsNone;
    }

    public void setDefaultIndex(int defaultIndex) {
        this.defaultIndex = defaultIndex;
    }

    public void setParents(List<String> parents) {
        this.parents = parents;
    }

    public void setChilds(List<String> childs) {
        this.childs = childs;
    }

    public void setFilterGroups(List<FilterProperties> filterGroups) {
        this.filterGroups = filterGroups;
    }

}

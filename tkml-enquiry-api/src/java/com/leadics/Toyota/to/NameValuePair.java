/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.Toyota.to;

/**
 *
 * @author HARSHA
 */
public class NameValuePair<K,V> {

    K name;
    V value;

    public NameValuePair(K name, V value) {
        this.name = name;
        this.value = value;
    }

}


/*
 * LIConsolefailedloginController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LIConsolefailedloginRecord;
import com.leadics.Toyota.service.LIConsolefailedloginService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIConsolefailedloginController extends LIController
{
	static LogUtils logger = new LogUtils(LIConsolefailedloginController.class.getName());

	public LIConsolefailedloginRecord loadFormLIConsolefailedloginRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIConsolefailedloginRecord", null);
		LIConsolefailedloginRecord record = new LIConsolefailedloginRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setUsername(getFormFieldValue(req, res, "tfUsername"));
		record.setChannel(getFormFieldValue(req, res, "tfChannel"));
		record.setSource(getFormFieldValue(req, res, "tfSource"));
		record.setReason(getFormFieldValue(req, res, "tfReason"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIConsolefailedloginRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIConsolefailedloginRecord loadJSONFormLIConsolefailedloginRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIConsolefailedloginRecord", null);
		LIConsolefailedloginRecord record = new LIConsolefailedloginRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setUsername(getFormFieldValue(req, res, "username"));
		record.setChannel(getFormFieldValue(req, res, "channel"));
		record.setSource(getFormFieldValue(req, res, "source"));
		record.setReason(getFormFieldValue(req, res, "reason"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIConsolefailedloginRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIConsolefailedloginRecord loadJSONFormLIConsolefailedloginRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIConsolefailedloginRecord", null);
		LIConsolefailedloginRecord record = new LIConsolefailedloginRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setUsername(getFormFieldValueEncode(req, res, "username"));
		record.setChannel(getFormFieldValueEncode(req, res, "channel"));
		record.setSource(getFormFieldValueEncode(req, res, "source"));
		record.setReason(getFormFieldValueEncode(req, res, "reason"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIConsolefailedloginRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIConsolefailedloginRecord loadMapLIConsolefailedloginRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIConsolefailedloginRecord", null);
		LIConsolefailedloginRecord record = new LIConsolefailedloginRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setUsername(getMapValue(inputMap, "username"));
		record.setChannel(getMapValue(inputMap, "channel"));
		record.setSource(getMapValue(inputMap, "source"));
		record.setReason(getMapValue(inputMap, "reason"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		logger.trace("loadMapLIConsolefailedloginRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIConsolefailedloginRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIConsolefailedloginRecord", null);
		LIConsolefailedloginService service = new LIConsolefailedloginService();

		try
		{
			LIConsolefailedloginRecord record = loadFormLIConsolefailedloginRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIConsolefailedloginRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consolefailedlogin.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Consolefailedlogin");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Consolefailedlogin " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consolefailedlogin.jsp");
		}
	}

	public void processUpdateLIConsolefailedloginRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIConsolefailedloginRecord", null);
		LIConsolefailedloginService service = new LIConsolefailedloginService();

		try
		{
			LIConsolefailedloginRecord record = loadFormLIConsolefailedloginRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIConsolefailedloginRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Consolefailedlogin data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consolefailedlogin.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Consolefailedlogin");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Consolefailedlogin " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consolefailedlogin.jsp");
		}
	}

	public void processDeleteLIConsolefailedloginRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIConsolefailedloginRecord", null);
		LIConsolefailedloginService service = new LIConsolefailedloginService();

		try
		{
			LIConsolefailedloginRecord record = loadFormLIConsolefailedloginRecord(req, res);
			service.deleteLIConsolefailedloginRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Consolefailedlogin deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consolefailedlogin.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Consolefailedlogin");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Consolefailedlogin " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consolefailedlogin.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertConsolefailedloginRecord"))
		{
			processInsertLIConsolefailedloginRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateConsolefailedloginRecord"))
		{
			processUpdateLIConsolefailedloginRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteConsolefailedloginRecord"))
		{
			processDeleteLIConsolefailedloginRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}

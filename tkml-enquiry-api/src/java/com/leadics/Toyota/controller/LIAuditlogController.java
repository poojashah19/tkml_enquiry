
/*
 * LIAuditlogController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LIAuditlogRecord;
import com.leadics.Toyota.service.LIAuditlogService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIAuditlogController extends LIController
{
	static LogUtils logger = new LogUtils(LIAuditlogController.class.getName());

	public LIAuditlogRecord loadFormLIAuditlogRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIAuditlogRecord", null);
		LIAuditlogRecord record = new LIAuditlogRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setEntityname(getFormFieldValue(req, res, "tfEntityname"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setMessage(getFormFieldValue(req, res, "tfMessage"));
		record.setActiontype(getFormFieldValue(req, res, "tfActiontype"));
		record.setUserid(getFormFieldValue(req, res, "tfUserid"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setSourceid(getFormFieldValue(req, res, "tfSourceid"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIAuditlogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIAuditlogRecord loadJSONFormLIAuditlogRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIAuditlogRecord", null);
		LIAuditlogRecord record = new LIAuditlogRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setEntityname(getFormFieldValue(req, res, "entity_name"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setMessage(getFormFieldValue(req, res, "message"));
		record.setActiontype(getFormFieldValue(req, res, "action_type"));
		record.setUserid(getFormFieldValue(req, res, "user_id"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setSourceid(getFormFieldValue(req, res, "source_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIAuditlogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIAuditlogRecord loadJSONFormLIAuditlogRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIAuditlogRecord", null);
		LIAuditlogRecord record = new LIAuditlogRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setEntityname(getFormFieldValueEncode(req, res, "entity_name"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setMessage(getFormFieldValueEncode(req, res, "message"));
		record.setActiontype(getFormFieldValueEncode(req, res, "action_type"));
		record.setUserid(getFormFieldValueEncode(req, res, "user_id"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setSourceid(getFormFieldValueEncode(req, res, "source_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIAuditlogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIAuditlogRecord loadMapLIAuditlogRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIAuditlogRecord", null);
		LIAuditlogRecord record = new LIAuditlogRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setEntityname(getMapValue(inputMap, "entity_name"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		record.setMessage(getMapValue(inputMap, "message"));
		record.setActiontype(getMapValue(inputMap, "action_type"));
		record.setUserid(getMapValue(inputMap, "user_id"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setSourceid(getMapValue(inputMap, "source_id"));
		logger.trace("loadMapLIAuditlogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIAuditlogRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIAuditlogRecord", null);
		LIAuditlogService service = new LIAuditlogService();

		try
		{
			LIAuditlogRecord record = loadFormLIAuditlogRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIAuditlogRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("auditlog.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Auditlog");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Auditlog " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("auditlog.jsp");
		}
	}

	public void processUpdateLIAuditlogRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIAuditlogRecord", null);
		LIAuditlogService service = new LIAuditlogService();

		try
		{
			LIAuditlogRecord record = loadFormLIAuditlogRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIAuditlogRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Auditlog data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("auditlog.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Auditlog");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Auditlog " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("auditlog.jsp");
		}
	}

	public void processDeleteLIAuditlogRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIAuditlogRecord", null);
		LIAuditlogService service = new LIAuditlogService();

		try
		{
			LIAuditlogRecord record = loadFormLIAuditlogRecord(req, res);
			service.deleteLIAuditlogRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Auditlog deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("auditlog.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Auditlog");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Auditlog " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("auditlog.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertAuditlogRecord"))
		{
			processInsertLIAuditlogRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateAuditlogRecord"))
		{
			processUpdateLIAuditlogRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteAuditlogRecord"))
		{
			processDeleteLIAuditlogRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}

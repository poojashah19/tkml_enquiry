
/*
 * LIWebaccesslogController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LIWebaccesslogRecord;
import com.leadics.Toyota.service.LIWebaccesslogService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIWebaccesslogController extends LIController
{
	static LogUtils logger = new LogUtils(LIWebaccesslogController.class.getName());

	public LIWebaccesslogRecord loadFormLIWebaccesslogRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIWebaccesslogRecord", null);
		LIWebaccesslogRecord record = new LIWebaccesslogRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setUserid(getFormFieldValue(req, res, "tfUserid"));
		record.setSource(getFormFieldValue(req, res, "tfSource"));
		record.setService(getFormFieldValue(req, res, "tfService"));
		record.setResult(getFormFieldValue(req, res, "tfResult"));
		record.setSessionid(getFormFieldValue(req, res, "tfSessionid"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIWebaccesslogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIWebaccesslogRecord loadJSONFormLIWebaccesslogRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIWebaccesslogRecord", null);
		LIWebaccesslogRecord record = new LIWebaccesslogRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setUserid(getFormFieldValue(req, res, "user_id"));
		record.setSource(getFormFieldValue(req, res, "source"));
		record.setService(getFormFieldValue(req, res, "service"));
		record.setResult(getFormFieldValue(req, res, "result"));
		record.setSessionid(getFormFieldValue(req, res, "sessionid"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIWebaccesslogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIWebaccesslogRecord loadJSONFormLIWebaccesslogRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIWebaccesslogRecord", null);
		LIWebaccesslogRecord record = new LIWebaccesslogRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setUserid(getFormFieldValueEncode(req, res, "user_id"));
		record.setSource(getFormFieldValueEncode(req, res, "source"));
		record.setService(getFormFieldValueEncode(req, res, "service"));
		record.setResult(getFormFieldValueEncode(req, res, "result"));
		record.setSessionid(getFormFieldValueEncode(req, res, "sessionid"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIWebaccesslogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIWebaccesslogRecord loadMapLIWebaccesslogRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIWebaccesslogRecord", null);
		LIWebaccesslogRecord record = new LIWebaccesslogRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setUserid(getMapValue(inputMap, "user_id"));
		record.setSource(getMapValue(inputMap, "source"));
		record.setService(getMapValue(inputMap, "service"));
		record.setResult(getMapValue(inputMap, "result"));
		record.setSessionid(getMapValue(inputMap, "sessionid"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		logger.trace("loadMapLIWebaccesslogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIWebaccesslogRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIWebaccesslogRecord", null);
		LIWebaccesslogService service = new LIWebaccesslogService();

		try
		{
			LIWebaccesslogRecord record = loadFormLIWebaccesslogRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIWebaccesslogRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("webaccesslog.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Webaccesslog");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Webaccesslog " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("webaccesslog.jsp");
		}
	}

	public void processUpdateLIWebaccesslogRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIWebaccesslogRecord", null);
		LIWebaccesslogService service = new LIWebaccesslogService();

		try
		{
			LIWebaccesslogRecord record = loadFormLIWebaccesslogRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIWebaccesslogRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Webaccesslog data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("webaccesslog.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Webaccesslog");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Webaccesslog " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("webaccesslog.jsp");
		}
	}

	public void processDeleteLIWebaccesslogRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIWebaccesslogRecord", null);
		LIWebaccesslogService service = new LIWebaccesslogService();

		try
		{
			LIWebaccesslogRecord record = loadFormLIWebaccesslogRecord(req, res);
			service.deleteLIWebaccesslogRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Webaccesslog deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("webaccesslog.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Webaccesslog");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Webaccesslog " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("webaccesslog.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertWebaccesslogRecord"))
		{
			processInsertLIWebaccesslogRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateWebaccesslogRecord"))
		{
			processUpdateLIWebaccesslogRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteWebaccesslogRecord"))
		{
			processDeleteLIWebaccesslogRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}

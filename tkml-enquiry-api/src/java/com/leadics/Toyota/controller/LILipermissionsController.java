
/*
 * LILipermissionsController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LILipermissionsRecord;
import com.leadics.Toyota.service.LILipermissionsService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LILipermissionsController extends LIController
{
	static LogUtils logger = new LogUtils(LILipermissionsController.class.getName());

	public LILipermissionsRecord loadFormLILipermissionsRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLILipermissionsRecord", null);
		LILipermissionsRecord record = new LILipermissionsRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setCategory(getFormFieldValue(req, res, "tfCategory"));
		record.setRolecode(getFormFieldValue(req, res, "tfRolecode"));
		record.setFeaturecode(getFormFieldValue(req, res, "tfFeaturecode"));
		record.setPermit(getFormFieldValue(req, res, "tfPermit"));
		record.setBlock(getFormFieldValue(req, res, "tfBlock"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setMadeby(getFormFieldValue(req, res, "tfMadeby"));
		record.setMadeat(getFormFieldValue(req, res, "tfMadeat"));
		record.setCheckedby(getFormFieldValue(req, res, "tfCheckedby"));
		record.setCheckedat(getFormFieldValue(req, res, "tfCheckedat"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "tfMakerlastcmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "tfCheckerlastcmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "tfCurrappstatus"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "tfAdminlastcmt"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLILipermissionsRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LILipermissionsRecord loadJSONFormLILipermissionsRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLILipermissionsRecord", null);
		LILipermissionsRecord record = new LILipermissionsRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setCategory(getFormFieldValue(req, res, "category"));
		record.setRolecode(getFormFieldValue(req, res, "role_code"));
		record.setFeaturecode(getFormFieldValue(req, res, "feature_code"));
		record.setPermit(getFormFieldValue(req, res, "permit"));
		record.setBlock(getFormFieldValue(req, res, "block"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setMadeby(getFormFieldValue(req, res, "made_by"));
		record.setMadeat(getFormFieldValue(req, res, "made_at"));
		record.setCheckedby(getFormFieldValue(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValue(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "admin_last_cmt"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLILipermissionsRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LILipermissionsRecord loadJSONFormLILipermissionsRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLILipermissionsRecord", null);
		LILipermissionsRecord record = new LILipermissionsRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setCategory(getFormFieldValueEncode(req, res, "category"));
		record.setRolecode(getFormFieldValueEncode(req, res, "role_code"));
		record.setFeaturecode(getFormFieldValueEncode(req, res, "feature_code"));
		record.setPermit(getFormFieldValueEncode(req, res, "permit"));
		record.setBlock(getFormFieldValueEncode(req, res, "block"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setMadeby(getFormFieldValueEncode(req, res, "made_by"));
		record.setMadeat(getFormFieldValueEncode(req, res, "made_at"));
		record.setCheckedby(getFormFieldValueEncode(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValueEncode(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValueEncode(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValueEncode(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValueEncode(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValueEncode(req, res, "admin_last_cmt"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLILipermissionsRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LILipermissionsRecord loadMapLILipermissionsRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLILipermissionsRecord", null);
		LILipermissionsRecord record = new LILipermissionsRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setCategory(getMapValue(inputMap, "category"));
		record.setRolecode(getMapValue(inputMap, "role_code"));
		record.setFeaturecode(getMapValue(inputMap, "feature_code"));
		record.setPermit(getMapValue(inputMap, "permit"));
		record.setBlock(getMapValue(inputMap, "block"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		record.setMadeby(getMapValue(inputMap, "made_by"));
		record.setMadeat(getMapValue(inputMap, "made_at"));
		record.setCheckedby(getMapValue(inputMap, "checked_by"));
		record.setCheckedat(getMapValue(inputMap, "checked_at"));
		record.setMakerlastcmt(getMapValue(inputMap, "maker_last_cmt"));
		record.setCheckerlastcmt(getMapValue(inputMap, "checker_last_cmt"));
		record.setCurrappstatus(getMapValue(inputMap, "curr_app_status"));
		record.setAdminlastcmt(getMapValue(inputMap, "admin_last_cmt"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		logger.trace("loadMapLILipermissionsRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLILipermissionsRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLILipermissionsRecord", null);
		LILipermissionsService service = new LILipermissionsService();

		try
		{
			LILipermissionsRecord record = loadFormLILipermissionsRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLILipermissionsRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("lipermissions.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Lipermissions");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Lipermissions " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("lipermissions.jsp");
		}
	}

	public void processUpdateLILipermissionsRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLILipermissionsRecord", null);
		LILipermissionsService service = new LILipermissionsService();

		try
		{
			LILipermissionsRecord record = loadFormLILipermissionsRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLILipermissionsRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Lipermissions data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("lipermissions.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Lipermissions");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Lipermissions " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("lipermissions.jsp");
		}
	}

	public void processDeleteLILipermissionsRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLILipermissionsRecord", null);
		LILipermissionsService service = new LILipermissionsService();

		try
		{
			LILipermissionsRecord record = loadFormLILipermissionsRecord(req, res);
			service.deleteLILipermissionsRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Lipermissions deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("lipermissions.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Lipermissions");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Lipermissions " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("lipermissions.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertLipermissionsRecord"))
		{
			processInsertLILipermissionsRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateLipermissionsRecord"))
		{
			processUpdateLILipermissionsRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteLipermissionsRecord"))
		{
			processDeleteLILipermissionsRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}

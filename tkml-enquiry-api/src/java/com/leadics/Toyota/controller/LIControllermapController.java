
/*
 * LIControllermapController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LIControllermapRecord;
import com.leadics.Toyota.service.LIControllermapService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIControllermapController extends LIController
{
	static LogUtils logger = new LogUtils(LIControllermapController.class.getName());

	public LIControllermapRecord loadFormLIControllermapRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIControllermapRecord", null);
		LIControllermapRecord record = new LIControllermapRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setCode(getFormFieldValue(req, res, "tfCode"));
		record.setClassname(getFormFieldValue(req, res, "tfClassname"));
		record.setAuthflag(getFormFieldValue(req, res, "tfAuthflag"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIControllermapRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIControllermapRecord loadJSONFormLIControllermapRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIControllermapRecord", null);
		LIControllermapRecord record = new LIControllermapRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setCode(getFormFieldValue(req, res, "code"));
		record.setClassname(getFormFieldValue(req, res, "classname"));
		record.setAuthflag(getFormFieldValue(req, res, "auth_flag"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIControllermapRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIControllermapRecord loadJSONFormLIControllermapRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIControllermapRecord", null);
		LIControllermapRecord record = new LIControllermapRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setCode(getFormFieldValueEncode(req, res, "code"));
		record.setClassname(getFormFieldValueEncode(req, res, "classname"));
		record.setAuthflag(getFormFieldValueEncode(req, res, "auth_flag"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIControllermapRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIControllermapRecord loadMapLIControllermapRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIControllermapRecord", null);
		LIControllermapRecord record = new LIControllermapRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setCode(getMapValue(inputMap, "code"));
		record.setClassname(getMapValue(inputMap, "classname"));
		record.setAuthflag(getMapValue(inputMap, "auth_flag"));
		logger.trace("loadMapLIControllermapRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIControllermapRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIControllermapRecord", null);
		LIControllermapService service = new LIControllermapService();

		try
		{
			LIControllermapRecord record = loadFormLIControllermapRecord(req, res);
			int resultId = service.insertLIControllermapRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("controllermap.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Controllermap");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Controllermap " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("controllermap.jsp");
		}
	}

	public void processUpdateLIControllermapRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIControllermapRecord", null);
		LIControllermapService service = new LIControllermapService();

		try
		{
			LIControllermapRecord record = loadFormLIControllermapRecord(req, res);
			service.updateLIControllermapRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Controllermap data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("controllermap.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Controllermap");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Controllermap " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("controllermap.jsp");
		}
	}

	public void processDeleteLIControllermapRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIControllermapRecord", null);
		LIControllermapService service = new LIControllermapService();

		try
		{
			LIControllermapRecord record = loadFormLIControllermapRecord(req, res);
			service.deleteLIControllermapRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Controllermap deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("controllermap.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Controllermap");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Controllermap " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("controllermap.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertControllermapRecord"))
		{
			processInsertLIControllermapRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateControllermapRecord"))
		{
			processUpdateLIControllermapRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteControllermapRecord"))
		{
			processDeleteLIControllermapRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}


/*
 * LIOthersmeanaggregateController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LIOthersmeanaggregateRecord;
import com.leadics.Toyota.service.LIOthersmeanaggregateService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIOthersmeanaggregateController extends LIController
{
	static LogUtils logger = new LogUtils(LIOthersmeanaggregateController.class.getName());

	public LIOthersmeanaggregateRecord loadFormLIOthersmeanaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIOthersmeanaggregateRecord", null);
		LIOthersmeanaggregateRecord record = new LIOthersmeanaggregateRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setSheetcolomname(getFormFieldValue(req, res, "tfSheetcolomname"));
		record.setRawcolumnname(getFormFieldValue(req, res, "tfRawcolumnname"));
		record.setDisplayname(getFormFieldValue(req, res, "tfDisplayname"));
		record.setDrilldownof(getFormFieldValue(req, res, "tfDrilldownof"));
		record.setServicecity30jdp(getFormFieldValue(req, res, "tfServicecity30jdp"));
		record.setMonth(getFormFieldValue(req, res, "tfMonth"));
		record.setModeldb(getFormFieldValue(req, res, "tfModeldb"));
		record.setFuel(getFormFieldValue(req, res, "tfFuel"));
		record.setIqssum(getFormFieldValue(req, res, "tfIqssum"));
		record.setIqscount(getFormFieldValue(req, res, "tfIqscount"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIOthersmeanaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIOthersmeanaggregateRecord loadJSONFormLIOthersmeanaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIOthersmeanaggregateRecord", null);
		LIOthersmeanaggregateRecord record = new LIOthersmeanaggregateRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setSheetcolomname(getFormFieldValue(req, res, "sheetcolomname"));
		record.setRawcolumnname(getFormFieldValue(req, res, "rawcolumnname"));
		record.setDisplayname(getFormFieldValue(req, res, "displayname"));
		record.setDrilldownof(getFormFieldValue(req, res, "drilldownof"));
		record.setServicecity30jdp(getFormFieldValue(req, res, "servicecity_30jdp"));
		record.setMonth(getFormFieldValue(req, res, "month"));
		record.setModeldb(getFormFieldValue(req, res, "modeldb"));
		record.setFuel(getFormFieldValue(req, res, "fuel"));
		record.setIqssum(getFormFieldValue(req, res, "iqssum"));
		record.setIqscount(getFormFieldValue(req, res, "iqscount"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIOthersmeanaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIOthersmeanaggregateRecord loadJSONFormLIOthersmeanaggregateRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIOthersmeanaggregateRecord", null);
		LIOthersmeanaggregateRecord record = new LIOthersmeanaggregateRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setSheetcolomname(getFormFieldValueEncode(req, res, "sheetcolomname"));
		record.setRawcolumnname(getFormFieldValueEncode(req, res, "rawcolumnname"));
		record.setDisplayname(getFormFieldValueEncode(req, res, "displayname"));
		record.setDrilldownof(getFormFieldValueEncode(req, res, "drilldownof"));
		record.setServicecity30jdp(getFormFieldValueEncode(req, res, "servicecity_30jdp"));
		record.setMonth(getFormFieldValueEncode(req, res, "month"));
		record.setModeldb(getFormFieldValueEncode(req, res, "modeldb"));
		record.setFuel(getFormFieldValueEncode(req, res, "fuel"));
		record.setIqssum(getFormFieldValueEncode(req, res, "iqssum"));
		record.setIqscount(getFormFieldValueEncode(req, res, "iqscount"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIOthersmeanaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIOthersmeanaggregateRecord loadMapLIOthersmeanaggregateRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIOthersmeanaggregateRecord", null);
		LIOthersmeanaggregateRecord record = new LIOthersmeanaggregateRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setSheetcolomname(getMapValue(inputMap, "sheetcolomname"));
		record.setRawcolumnname(getMapValue(inputMap, "rawcolumnname"));
		record.setDisplayname(getMapValue(inputMap, "displayname"));
		record.setDrilldownof(getMapValue(inputMap, "drilldownof"));
		record.setServicecity30jdp(getMapValue(inputMap, "servicecity_30jdp"));
		record.setMonth(getMapValue(inputMap, "month"));
		record.setModeldb(getMapValue(inputMap, "modeldb"));
		record.setFuel(getMapValue(inputMap, "fuel"));
		record.setIqssum(getMapValue(inputMap, "iqssum"));
		record.setIqscount(getMapValue(inputMap, "iqscount"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		logger.trace("loadMapLIOthersmeanaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIOthersmeanaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIOthersmeanaggregateRecord", null);
		LIOthersmeanaggregateService service = new LIOthersmeanaggregateService();

		try
		{
			LIOthersmeanaggregateRecord record = loadFormLIOthersmeanaggregateRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIOthersmeanaggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("othersmeanaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Othersmeanaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Othersmeanaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("othersmeanaggregate.jsp");
		}
	}

	public void processUpdateLIOthersmeanaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIOthersmeanaggregateRecord", null);
		LIOthersmeanaggregateService service = new LIOthersmeanaggregateService();

		try
		{
			LIOthersmeanaggregateRecord record = loadFormLIOthersmeanaggregateRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIOthersmeanaggregateRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Othersmeanaggregate data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("othersmeanaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Othersmeanaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Othersmeanaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("othersmeanaggregate.jsp");
		}
	}

	public void processDeleteLIOthersmeanaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIOthersmeanaggregateRecord", null);
		LIOthersmeanaggregateService service = new LIOthersmeanaggregateService();

		try
		{
			LIOthersmeanaggregateRecord record = loadFormLIOthersmeanaggregateRecord(req, res);
			service.deleteLIOthersmeanaggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Othersmeanaggregate deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("othersmeanaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Othersmeanaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Othersmeanaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("othersmeanaggregate.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertOthersmeanaggregateRecord"))
		{
			processInsertLIOthersmeanaggregateRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateOthersmeanaggregateRecord"))
		{
			processUpdateLIOthersmeanaggregateRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteOthersmeanaggregateRecord"))
		{
			processDeleteLIOthersmeanaggregateRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}

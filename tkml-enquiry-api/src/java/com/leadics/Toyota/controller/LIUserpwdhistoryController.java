
/*
 * LIUserpwdhistoryController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LIUserpwdhistoryRecord;
import com.leadics.Toyota.service.LIUserpwdhistoryService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIUserpwdhistoryController extends LIController
{
	static LogUtils logger = new LogUtils(LIUserpwdhistoryController.class.getName());

	public LIUserpwdhistoryRecord loadFormLIUserpwdhistoryRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIUserpwdhistoryRecord", null);
		LIUserpwdhistoryRecord record = new LIUserpwdhistoryRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setUserid(getFormFieldValue(req, res, "tfUserid"));
		record.setPassword(getFormFieldValue(req, res, "tfPassword"));
		record.setSetts(getFormFieldValue(req, res, "tfSetts"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIUserpwdhistoryRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIUserpwdhistoryRecord loadJSONFormLIUserpwdhistoryRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIUserpwdhistoryRecord", null);
		LIUserpwdhistoryRecord record = new LIUserpwdhistoryRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setUserid(getFormFieldValue(req, res, "user_id"));
		record.setPassword(getFormFieldValue(req, res, "password"));
		record.setSetts(getFormFieldValue(req, res, "setts"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIUserpwdhistoryRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIUserpwdhistoryRecord loadJSONFormLIUserpwdhistoryRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIUserpwdhistoryRecord", null);
		LIUserpwdhistoryRecord record = new LIUserpwdhistoryRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setUserid(getFormFieldValueEncode(req, res, "user_id"));
		record.setPassword(getFormFieldValueEncode(req, res, "password"));
		record.setSetts(getFormFieldValueEncode(req, res, "setts"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIUserpwdhistoryRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIUserpwdhistoryRecord loadMapLIUserpwdhistoryRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIUserpwdhistoryRecord", null);
		LIUserpwdhistoryRecord record = new LIUserpwdhistoryRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setUserid(getMapValue(inputMap, "user_id"));
		record.setPassword(getMapValue(inputMap, "password"));
		record.setSetts(getMapValue(inputMap, "setts"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		logger.trace("loadMapLIUserpwdhistoryRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIUserpwdhistoryRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIUserpwdhistoryRecord", null);
		LIUserpwdhistoryService service = new LIUserpwdhistoryService();

		try
		{
			LIUserpwdhistoryRecord record = loadFormLIUserpwdhistoryRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIUserpwdhistoryRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userpwdhistory.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Userpwdhistory");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Userpwdhistory " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userpwdhistory.jsp");
		}
	}

	public void processUpdateLIUserpwdhistoryRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIUserpwdhistoryRecord", null);
		LIUserpwdhistoryService service = new LIUserpwdhistoryService();

		try
		{
			LIUserpwdhistoryRecord record = loadFormLIUserpwdhistoryRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIUserpwdhistoryRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Userpwdhistory data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userpwdhistory.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Userpwdhistory");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Userpwdhistory " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userpwdhistory.jsp");
		}
	}

	public void processDeleteLIUserpwdhistoryRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIUserpwdhistoryRecord", null);
		LIUserpwdhistoryService service = new LIUserpwdhistoryService();

		try
		{
			LIUserpwdhistoryRecord record = loadFormLIUserpwdhistoryRecord(req, res);
			service.deleteLIUserpwdhistoryRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Userpwdhistory deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userpwdhistory.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Userpwdhistory");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Userpwdhistory " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userpwdhistory.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertUserpwdhistoryRecord"))
		{
			processInsertLIUserpwdhistoryRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateUserpwdhistoryRecord"))
		{
			processUpdateLIUserpwdhistoryRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteUserpwdhistoryRecord"))
		{
			processDeleteLIUserpwdhistoryRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}


/*
 * LIConsoleaccesslogController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LIConsoleaccesslogRecord;
import com.leadics.Toyota.service.LIConsoleaccesslogService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIConsoleaccesslogController extends LIController
{
	static LogUtils logger = new LogUtils(LIConsoleaccesslogController.class.getName());

	public LIConsoleaccesslogRecord loadFormLIConsoleaccesslogRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIConsoleaccesslogRecord", null);
		LIConsoleaccesslogRecord record = new LIConsoleaccesslogRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setUserid(getFormFieldValue(req, res, "tfUserid"));
		record.setChannel(getFormFieldValue(req, res, "tfChannel"));
		record.setSource(getFormFieldValue(req, res, "tfSource"));
		record.setPagepath(getFormFieldValue(req, res, "tfPagepath"));
		record.setSessionid(getFormFieldValue(req, res, "tfSessionid"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIConsoleaccesslogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIConsoleaccesslogRecord loadJSONFormLIConsoleaccesslogRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIConsoleaccesslogRecord", null);
		LIConsoleaccesslogRecord record = new LIConsoleaccesslogRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setUserid(getFormFieldValue(req, res, "user_id"));
		record.setChannel(getFormFieldValue(req, res, "channel"));
		record.setSource(getFormFieldValue(req, res, "source"));
		record.setPagepath(getFormFieldValue(req, res, "pagepath"));
		record.setSessionid(getFormFieldValue(req, res, "sessionid"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIConsoleaccesslogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIConsoleaccesslogRecord loadJSONFormLIConsoleaccesslogRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIConsoleaccesslogRecord", null);
		LIConsoleaccesslogRecord record = new LIConsoleaccesslogRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setUserid(getFormFieldValueEncode(req, res, "user_id"));
		record.setChannel(getFormFieldValueEncode(req, res, "channel"));
		record.setSource(getFormFieldValueEncode(req, res, "source"));
		record.setPagepath(getFormFieldValueEncode(req, res, "pagepath"));
		record.setSessionid(getFormFieldValueEncode(req, res, "sessionid"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIConsoleaccesslogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIConsoleaccesslogRecord loadMapLIConsoleaccesslogRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIConsoleaccesslogRecord", null);
		LIConsoleaccesslogRecord record = new LIConsoleaccesslogRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setUserid(getMapValue(inputMap, "user_id"));
		record.setChannel(getMapValue(inputMap, "channel"));
		record.setSource(getMapValue(inputMap, "source"));
		record.setPagepath(getMapValue(inputMap, "pagepath"));
		record.setSessionid(getMapValue(inputMap, "sessionid"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		logger.trace("loadMapLIConsoleaccesslogRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIConsoleaccesslogRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIConsoleaccesslogRecord", null);
		LIConsoleaccesslogService service = new LIConsoleaccesslogService();

		try
		{
			LIConsoleaccesslogRecord record = loadFormLIConsoleaccesslogRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIConsoleaccesslogRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consoleaccesslog.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Consoleaccesslog");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Consoleaccesslog " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consoleaccesslog.jsp");
		}
	}

	public void processUpdateLIConsoleaccesslogRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIConsoleaccesslogRecord", null);
		LIConsoleaccesslogService service = new LIConsoleaccesslogService();

		try
		{
			LIConsoleaccesslogRecord record = loadFormLIConsoleaccesslogRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIConsoleaccesslogRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Consoleaccesslog data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consoleaccesslog.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Consoleaccesslog");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Consoleaccesslog " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consoleaccesslog.jsp");
		}
	}

	public void processDeleteLIConsoleaccesslogRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIConsoleaccesslogRecord", null);
		LIConsoleaccesslogService service = new LIConsoleaccesslogService();

		try
		{
			LIConsoleaccesslogRecord record = loadFormLIConsoleaccesslogRecord(req, res);
			service.deleteLIConsoleaccesslogRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Consoleaccesslog deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consoleaccesslog.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Consoleaccesslog");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Consoleaccesslog " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("consoleaccesslog.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertConsoleaccesslogRecord"))
		{
			processInsertLIConsoleaccesslogRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateConsoleaccesslogRecord"))
		{
			processUpdateLIConsoleaccesslogRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteConsoleaccesslogRecord"))
		{
			processDeleteLIConsoleaccesslogRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}

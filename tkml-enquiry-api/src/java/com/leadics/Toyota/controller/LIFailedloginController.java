
/*
 * LIFailedloginController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LIFailedloginRecord;
import com.leadics.Toyota.service.LIFailedloginService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIFailedloginController extends LIController
{
	static LogUtils logger = new LogUtils(LIFailedloginController.class.getName());

	public LIFailedloginRecord loadFormLIFailedloginRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIFailedloginRecord", null);
		LIFailedloginRecord record = new LIFailedloginRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setUsername(getFormFieldValue(req, res, "tfUsername"));
		record.setEmail(getFormFieldValue(req, res, "tfEmail"));
		record.setSource(getFormFieldValue(req, res, "tfSource"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setFailreason(getFormFieldValue(req, res, "tfFailreason"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIFailedloginRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIFailedloginRecord loadJSONFormLIFailedloginRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIFailedloginRecord", null);
		LIFailedloginRecord record = new LIFailedloginRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setUsername(getFormFieldValue(req, res, "username"));
		record.setEmail(getFormFieldValue(req, res, "email"));
		record.setSource(getFormFieldValue(req, res, "source"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setFailreason(getFormFieldValue(req, res, "failreason"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIFailedloginRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIFailedloginRecord loadJSONFormLIFailedloginRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIFailedloginRecord", null);
		LIFailedloginRecord record = new LIFailedloginRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setUsername(getFormFieldValueEncode(req, res, "username"));
		record.setEmail(getFormFieldValueEncode(req, res, "email"));
		record.setSource(getFormFieldValueEncode(req, res, "source"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setFailreason(getFormFieldValueEncode(req, res, "failreason"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIFailedloginRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIFailedloginRecord loadMapLIFailedloginRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIFailedloginRecord", null);
		LIFailedloginRecord record = new LIFailedloginRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setUsername(getMapValue(inputMap, "username"));
		record.setEmail(getMapValue(inputMap, "email"));
		record.setSource(getMapValue(inputMap, "source"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setFailreason(getMapValue(inputMap, "failreason"));
		logger.trace("loadMapLIFailedloginRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIFailedloginRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIFailedloginRecord", null);
		LIFailedloginService service = new LIFailedloginService();

		try
		{
			LIFailedloginRecord record = loadFormLIFailedloginRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIFailedloginRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("failedlogin.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Failedlogin");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Failedlogin " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("failedlogin.jsp");
		}
	}

	public void processUpdateLIFailedloginRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIFailedloginRecord", null);
		LIFailedloginService service = new LIFailedloginService();

		try
		{
			LIFailedloginRecord record = loadFormLIFailedloginRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIFailedloginRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Failedlogin data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("failedlogin.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Failedlogin");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Failedlogin " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("failedlogin.jsp");
		}
	}

	public void processDeleteLIFailedloginRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIFailedloginRecord", null);
		LIFailedloginService service = new LIFailedloginService();

		try
		{
			LIFailedloginRecord record = loadFormLIFailedloginRecord(req, res);
			service.deleteLIFailedloginRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Failedlogin deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("failedlogin.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Failedlogin");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Failedlogin " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("failedlogin.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertFailedloginRecord"))
		{
			processInsertLIFailedloginRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateFailedloginRecord"))
		{
			processUpdateLIFailedloginRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteFailedloginRecord"))
		{
			processDeleteLIFailedloginRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}

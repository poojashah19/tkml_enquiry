
/*
 * LIUserrequestController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LIUserrequestRecord;
import com.leadics.Toyota.service.LIUserrequestService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIUserrequestController extends LIController
{
	static LogUtils logger = new LogUtils(LIUserrequestController.class.getName());

	public LIUserrequestRecord loadFormLIUserrequestRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIUserrequestRecord", null);
		LIUserrequestRecord record = new LIUserrequestRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setUserrecid(getFormFieldValue(req, res, "tfUserrecid"));
		record.setUserid(getFormFieldValue(req, res, "tfUserid"));
		record.setUname(getFormFieldValue(req, res, "tfUname"));
		record.setRequesttype(getFormFieldValue(req, res, "tfRequesttype"));
		record.setComments(getFormFieldValue(req, res, "tfComments"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setMadeby(getFormFieldValue(req, res, "tfMadeby"));
		record.setMadeat(getFormFieldValue(req, res, "tfMadeat"));
		record.setCheckedby(getFormFieldValue(req, res, "tfCheckedby"));
		record.setCheckedat(getFormFieldValue(req, res, "tfCheckedat"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "tfMakerlastcmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "tfCheckerlastcmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "tfCurrappstatus"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "tfAdminlastcmt"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIUserrequestRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIUserrequestRecord loadJSONFormLIUserrequestRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIUserrequestRecord", null);
		LIUserrequestRecord record = new LIUserrequestRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setUserrecid(getFormFieldValue(req, res, "userrecid"));
		record.setUserid(getFormFieldValue(req, res, "userid"));
		record.setUname(getFormFieldValue(req, res, "uname"));
		record.setRequesttype(getFormFieldValue(req, res, "request_type"));
		record.setComments(getFormFieldValue(req, res, "comments"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setMadeby(getFormFieldValue(req, res, "made_by"));
		record.setMadeat(getFormFieldValue(req, res, "made_at"));
		record.setCheckedby(getFormFieldValue(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValue(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "admin_last_cmt"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIUserrequestRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIUserrequestRecord loadJSONFormLIUserrequestRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIUserrequestRecord", null);
		LIUserrequestRecord record = new LIUserrequestRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setUserrecid(getFormFieldValueEncode(req, res, "userrecid"));
		record.setUserid(getFormFieldValueEncode(req, res, "userid"));
		record.setUname(getFormFieldValueEncode(req, res, "uname"));
		record.setRequesttype(getFormFieldValueEncode(req, res, "request_type"));
		record.setComments(getFormFieldValueEncode(req, res, "comments"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setMadeby(getFormFieldValueEncode(req, res, "made_by"));
		record.setMadeat(getFormFieldValueEncode(req, res, "made_at"));
		record.setCheckedby(getFormFieldValueEncode(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValueEncode(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValueEncode(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValueEncode(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValueEncode(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValueEncode(req, res, "admin_last_cmt"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIUserrequestRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIUserrequestRecord loadMapLIUserrequestRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIUserrequestRecord", null);
		LIUserrequestRecord record = new LIUserrequestRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setUserrecid(getMapValue(inputMap, "userrecid"));
		record.setUserid(getMapValue(inputMap, "userid"));
		record.setUname(getMapValue(inputMap, "uname"));
		record.setRequesttype(getMapValue(inputMap, "request_type"));
		record.setComments(getMapValue(inputMap, "comments"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		record.setMadeby(getMapValue(inputMap, "made_by"));
		record.setMadeat(getMapValue(inputMap, "made_at"));
		record.setCheckedby(getMapValue(inputMap, "checked_by"));
		record.setCheckedat(getMapValue(inputMap, "checked_at"));
		record.setMakerlastcmt(getMapValue(inputMap, "maker_last_cmt"));
		record.setCheckerlastcmt(getMapValue(inputMap, "checker_last_cmt"));
		record.setCurrappstatus(getMapValue(inputMap, "curr_app_status"));
		record.setAdminlastcmt(getMapValue(inputMap, "admin_last_cmt"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		logger.trace("loadMapLIUserrequestRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIUserrequestRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIUserrequestRecord", null);
		LIUserrequestService service = new LIUserrequestService();

		try
		{
			LIUserrequestRecord record = loadFormLIUserrequestRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIUserrequestRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrequest.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Userrequest");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Userrequest " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrequest.jsp");
		}
	}

	public void processUpdateLIUserrequestRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIUserrequestRecord", null);
		LIUserrequestService service = new LIUserrequestService();

		try
		{
			LIUserrequestRecord record = loadFormLIUserrequestRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIUserrequestRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Userrequest data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrequest.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Userrequest");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Userrequest " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrequest.jsp");
		}
	}

	public void processDeleteLIUserrequestRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIUserrequestRecord", null);
		LIUserrequestService service = new LIUserrequestService();

		try
		{
			LIUserrequestRecord record = loadFormLIUserrequestRecord(req, res);
			service.deleteLIUserrequestRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Userrequest deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrequest.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Userrequest");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Userrequest " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrequest.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertUserrequestRecord"))
		{
			processInsertLIUserrequestRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateUserrequestRecord"))
		{
			processUpdateLIUserrequestRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteUserrequestRecord"))
		{
			processDeleteLIUserrequestRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}

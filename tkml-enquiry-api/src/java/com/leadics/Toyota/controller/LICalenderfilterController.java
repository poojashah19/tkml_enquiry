
/*
 * LICalenderfilterController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LICalenderfilterRecord;
import com.leadics.Toyota.service.LICalenderfilterService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LICalenderfilterController extends LIController
{
	static LogUtils logger = new LogUtils(LICalenderfilterController.class.getName());

	public LICalenderfilterRecord loadFormLICalenderfilterRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLICalenderfilterRecord", null);
		LICalenderfilterRecord record = new LICalenderfilterRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setMonth(getFormFieldValue(req, res, "tfMonth"));
		record.setIqseligibility(getFormFieldValue(req, res, "tfIqseligibility"));
		record.setDate(getFormFieldValue(req, res, "tfDate"));
		record.setYear(getFormFieldValue(req, res, "tfYear"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setLastacc(getFormFieldValue(req, res, "tfLastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setClosetype(getFormFieldValue(req, res, "tfClosetype"));
		record.setClosedat(getFormFieldValue(req, res, "tfClosedat"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLICalenderfilterRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICalenderfilterRecord loadJSONFormLICalenderfilterRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICalenderfilterRecord", null);
		LICalenderfilterRecord record = new LICalenderfilterRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setMonth(getFormFieldValue(req, res, "month"));
		record.setIqseligibility(getFormFieldValue(req, res, "iqseligibility"));
		record.setDate(getFormFieldValue(req, res, "date"));
		record.setYear(getFormFieldValue(req, res, "year"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setLastacc(getFormFieldValue(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setClosetype(getFormFieldValue(req, res, "close_type"));
		record.setClosedat(getFormFieldValue(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICalenderfilterRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICalenderfilterRecord loadJSONFormLICalenderfilterRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICalenderfilterRecord", null);
		LICalenderfilterRecord record = new LICalenderfilterRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setMonth(getFormFieldValueEncode(req, res, "month"));
		record.setIqseligibility(getFormFieldValueEncode(req, res, "iqseligibility"));
		record.setDate(getFormFieldValueEncode(req, res, "date"));
		record.setYear(getFormFieldValueEncode(req, res, "year"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setLastacc(getFormFieldValueEncode(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setClosetype(getFormFieldValueEncode(req, res, "close_type"));
		record.setClosedat(getFormFieldValueEncode(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICalenderfilterRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICalenderfilterRecord loadMapLICalenderfilterRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLICalenderfilterRecord", null);
		LICalenderfilterRecord record = new LICalenderfilterRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setMonth(getMapValue(inputMap, "month"));
		record.setIqseligibility(getMapValue(inputMap, "iqseligibility"));
		record.setDate(getMapValue(inputMap, "date"));
		record.setYear(getMapValue(inputMap, "year"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setLastacc(getMapValue(inputMap, "lastacc"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setClosetype(getMapValue(inputMap, "close_type"));
		record.setClosedat(getMapValue(inputMap, "closed_at"));
		logger.trace("loadMapLICalenderfilterRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLICalenderfilterRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICalenderfilterRecord", null);
		LICalenderfilterService service = new LICalenderfilterService();

		try
		{
			LICalenderfilterRecord record = loadFormLICalenderfilterRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLICalenderfilterRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Calenderfilter");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Calenderfilter " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
	}

	public void processUpdateLICalenderfilterRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICalenderfilterRecord", null);
		LICalenderfilterService service = new LICalenderfilterService();

		try
		{
			LICalenderfilterRecord record = loadFormLICalenderfilterRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLICalenderfilterRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Calenderfilter data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Calenderfilter");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Calenderfilter " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
	}

	public void processDeleteLICalenderfilterRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICalenderfilterRecord", null);
		LICalenderfilterService service = new LICalenderfilterService();

		try
		{
			LICalenderfilterRecord record = loadFormLICalenderfilterRecord(req, res);
			service.deleteLICalenderfilterRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Calenderfilter deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Calenderfilter");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Calenderfilter " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertCalenderfilterRecord"))
		{
			processInsertLICalenderfilterRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateCalenderfilterRecord"))
		{
			processUpdateLICalenderfilterRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteCalenderfilterRecord"))
		{
			processDeleteLICalenderfilterRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}

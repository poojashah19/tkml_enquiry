
/*
 * LIWebsessionController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.Toyota.controller;
import com.leadics.Toyota.to.LIWebsessionRecord;
import com.leadics.Toyota.service.LIWebsessionService;
import com.leadics.Toyota.common.LIController;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIWebsessionController extends LIController
{
	static LogUtils logger = new LogUtils(LIWebsessionController.class.getName());

	public LIWebsessionRecord loadFormLIWebsessionRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIWebsessionRecord", null);
		LIWebsessionRecord record = new LIWebsessionRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setUserid(getFormFieldValue(req, res, "tfUserid"));
		record.setSessionid(getFormFieldValue(req, res, "tfSessionid"));
		record.setSource(getFormFieldValue(req, res, "tfSource"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setLastacc(getFormFieldValue(req, res, "tfLastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setClosetype(getFormFieldValue(req, res, "tfClosetype"));
		record.setClosedat(getFormFieldValue(req, res, "tfClosedat"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIWebsessionRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIWebsessionRecord loadJSONFormLIWebsessionRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIWebsessionRecord", null);
		LIWebsessionRecord record = new LIWebsessionRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setUserid(getFormFieldValue(req, res, "user_id"));
		record.setSessionid(getFormFieldValue(req, res, "sessionid"));
		record.setSource(getFormFieldValue(req, res, "source"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setLastacc(getFormFieldValue(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setClosetype(getFormFieldValue(req, res, "close_type"));
		record.setClosedat(getFormFieldValue(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIWebsessionRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIWebsessionRecord loadJSONFormLIWebsessionRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIWebsessionRecord", null);
		LIWebsessionRecord record = new LIWebsessionRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setUserid(getFormFieldValueEncode(req, res, "user_id"));
		record.setSessionid(getFormFieldValueEncode(req, res, "sessionid"));
		record.setSource(getFormFieldValueEncode(req, res, "source"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setLastacc(getFormFieldValueEncode(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setClosetype(getFormFieldValueEncode(req, res, "close_type"));
		record.setClosedat(getFormFieldValueEncode(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIWebsessionRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIWebsessionRecord loadMapLIWebsessionRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIWebsessionRecord", null);
		LIWebsessionRecord record = new LIWebsessionRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setUserid(getMapValue(inputMap, "user_id"));
		record.setSessionid(getMapValue(inputMap, "sessionid"));
		record.setSource(getMapValue(inputMap, "source"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setLastacc(getMapValue(inputMap, "lastacc"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setClosetype(getMapValue(inputMap, "close_type"));
		record.setClosedat(getMapValue(inputMap, "closed_at"));
		logger.trace("loadMapLIWebsessionRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIWebsessionRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIWebsessionRecord", null);
		LIWebsessionService service = new LIWebsessionService();

		try
		{
			LIWebsessionRecord record = loadFormLIWebsessionRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIWebsessionRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("websession.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Websession");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Websession " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("websession.jsp");
		}
	}

	public void processUpdateLIWebsessionRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIWebsessionRecord", null);
		LIWebsessionService service = new LIWebsessionService();

		try
		{
			LIWebsessionRecord record = loadFormLIWebsessionRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIWebsessionRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Websession data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("websession.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Websession");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Websession " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("websession.jsp");
		}
	}

	public void processDeleteLIWebsessionRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIWebsessionRecord", null);
		LIWebsessionService service = new LIWebsessionService();

		try
		{
			LIWebsessionRecord record = loadFormLIWebsessionRecord(req, res);
			service.deleteLIWebsessionRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Websession deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("websession.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Websession");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Websession " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("websession.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertWebsessionRecord"))
		{
			processInsertLIWebsessionRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateWebsessionRecord"))
		{
			processUpdateLIWebsessionRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteWebsessionRecord"))
		{
			processDeleteLIWebsessionRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}

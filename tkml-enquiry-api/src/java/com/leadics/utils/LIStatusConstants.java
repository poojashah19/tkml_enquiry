/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.utils;

/**
 *
 * @author HARSHA
 */
public enum LIStatusConstants {
    ERROR(-1),
    FAIL(0),
    SUCCESS(1),
    EXPIRED(2),
    LINK_USED(3),
    VALID(4),
    USER_BLOCKED(5),
    USER_NOT_FOUND(6),
    INVALID(7),
    LIVE(8),
    SUSPENDED(9),
    UNKNOWN(10),
    VALIDATE_OTP(11),
    INVALID_OTP(12);
    private int no;

    LIStatusConstants(int no) {
        this.no = no;
    }

    public int getValue() {
        return this.no;
    }

}

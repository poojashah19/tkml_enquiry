package com.leadics.utils;
//Java imports

import java.io.*;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class StringUtils {

    /**
     * Returns boolean Y/N Code
     *
     * @param flag input boolean value
     * @return Y if input is true else returns N
     */
    public static String getStringValue(boolean flag) {
        if (flag) {
            return "Y";
        } else {
            return "N";
        }
    }

    public static boolean stringEquals(String oldS, String newS) {
        oldS = StringUtils.noNull(oldS);
        newS = StringUtils.noNull(newS);
        return oldS.equals(newS);
    }

    public static boolean stringEqualsIC(String oldS, String newS) {
        oldS = StringUtils.noNull(oldS);
        newS = StringUtils.noNull(newS);
        return oldS.equalsIgnoreCase(newS);
    }

    public static boolean isNonNullStringInRange(String value, int min, int max)
            throws Exception {
        if (StringUtils.isNullOrEmpty(value)) {
            return false;
        }
        if (value.length() < min) {
            return false;
        }
        if (value.length() > max) {
            return false;
        }
        return true;
    }

    public static boolean hasDigits(String message) {
        message = noNull(message);
        boolean hasDigitInOutput = false;
        char[] messageChars = message.toCharArray();

        for (int index = 0; index < messageChars.length; index++) {
            if (Character.isDigit(messageChars[index])) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method converts given value(Y/N) to boolean. This method throws
     * exception if input value is not Y or N.
     *
     * @param inputValue Input Value(Valid values are Y and N)
     * @param nullOK Is null accepted? if null is accepted and and if input
     * value is null, this method returns false
     * @return Equivalent boolean value of input
     * @throws Exception incase of any exception
     */
    public static boolean getBooleanValue(String inputValue,
            boolean nullOK)
            throws Exception {
        //Is input value null
        if (StringUtils.isNullOrEmpty(inputValue)) {
            //If null not accepted throw exception
            if (!nullOK) {
                throw new Exception("Input value cannot be empty "
                        + "in StringUtils.getBooleanValue");
            } else {
                //Return default value if null
                return false;
            }
        }

        //Convert input value to upper case for comparision
        inputValue = inputValue.toUpperCase();

        //If input value is Yes return true
        if (StringUtils.isSame(inputValue, "Y")) {
            return true;
        } //If input value if No return false
        else if (StringUtils.isSame(inputValue, "N")) {
            return false;
        } //If input valud is invalid throws exception
        else {
            throw new Exception("Invalid input value:" + inputValue
                    + "in StringUtils.getBooleanValue");
        }
    }

    /**
     * This method compares two strings with equals method. This method
     * considers null as empty string.
     *
     * @param firstValue First String to compare; Considers null as empty string
     * for comparision
     * @param secondValue Second String to compare; Considers null as empty
     * string for comparision
     * @return true - if equals; else - false
     */
    public static boolean isSame(String firstValue,
            String secondValue) {
        if (firstValue == null) {
            firstValue = "";
        }

        if (secondValue == null) {
            secondValue = "";
        }

        return firstValue.equals(secondValue);
    }

    /**
     * This method converts object as a string value. If input object is null
     * this method then returns default value
     *
     * @param object Object
     * @param defaultValue Default Value
     * @return Returns object as string; if object is null returns defaultValue
     */
    public static String getStringValue(Object object,
            String defaultValue) {
        //If input object is null return default value
        if (object == null) {
            return defaultValue;
        }
        return object.toString();
    }

    /**
     * This method converts object as an integer value. If input object is null
     * or invalid this method then returns default value
     *
     * @param object Object
     * @param defaultValue Default Value
     * @return Returns object as integer; if object is null or invalid returns
     * defaultValue
     */
    public static int getIntegerValue(Object object,
            int defaultValue) {
        String stringValue = getStringValue(object, "");
        try {
            return Integer.parseInt(stringValue);
        } catch (Exception exception) {
            return defaultValue;
        }
    }

    /**
     * Appends spaces to given string to meet maximum length
     *
     * @param inputValue Input string to which spaces needs to be added.
     * @param maxLength Maximum Length of string.
     * @return input value with spaces appended
     * @throws IllegalArgumentException for Invalid input
     */
    public static String appendSpaces(String inputValue,
            int maxLength)
            throws IllegalArgumentException {
        int spacesToAdd = 0;

        //Validate input value
        if (inputValue == null) {
            throw new IllegalArgumentException("Input value cannot "
                    + "be null in StringUtils.appendSpace method");
        }

        //Validate Maximum length
        if (maxLength < 0) {
            throw new IllegalArgumentException("Invalid maximum "
                    + "length be null in StringUtils.appendSpace method");
        }

        spacesToAdd = maxLength - inputValue.length();

        if (spacesToAdd <= 0) {
            return inputValue;
        }

        for (int spaceIndex = 0; spaceIndex < spacesToAdd;
                spaceIndex++) {
            //append space
            inputValue = inputValue + " ";
        }

        return inputValue;
    }

    /**
     * Checks if input value is null or empty string
     *
     * @param inputValue Input Value
     * @return true - if null or empty string; false - otherwise
     */
    public static boolean isNullOrEmpty(String inputValue) {
        if (inputValue == null) {
            return true;
        }
        if (inputValue.trim().length() < 1) {
            return true;
        }
        return false;
    }

    /**
     * Converts object to string. Returns null if object is null else casts
     * object as String
     *
     * @param inputObject Input Object
     * @return Returns null if input object is null; else casts object as String
     */
    public static String getStringValue(Object inputObject) {
        if (inputObject == null) {
            return null;
        }

        return (String) inputObject;
    }

    /**
     * Parses input value based on delimiter and returns desired token
     *
     * @param inputValue String to be parsed
     * @param delimiter Delimiter for parsing
     * @param tokenNumber Desired token number(0 for First token, 1 for Second
     * token etc)
     * @return Returns desired token
     * @throws IllegalArgumentException for Invalid input
     * @throws Exception for all others
     */
    public static String getToken(
            String inputValue,
            String delimiter,
            int tokenNumber)
            throws IllegalArgumentException, Exception {
        String resultValue = null;

        //Validate Input Value
        if (inputValue == null) {
            throw new IllegalArgumentException("Input value cannot"
                    + "be null in StringUtils.getToken method");
        }

        //Validate Delimiter
        if (delimiter == null) {
            throw new IllegalArgumentException("Delimiter cannot be"
                    + " null in StringUtils.getToken method");
        }

        //Validate token number
        if (tokenNumber < 0) {
            throw new IllegalArgumentException("Invalid token number"
                    + " in StringUtils.getToken method");
        }

        StringTokenizer st = new StringTokenizer(inputValue, delimiter);
        for (int tokenIndex = 0; tokenIndex < tokenNumber;
                tokenIndex++) {
            st.nextToken();
        }

        if (st.hasMoreTokens()) {
            resultValue = st.nextToken();
        }

        return resultValue;
    }

    /**
     * This method is used to capitalize the first character of a String to
     * upper case.
     *
     * @param inputValue The string whose first character has to be capitalized
     * @return The input String with the first character capitalized.
     */

    public static String convertFirstCharacterToUpperCase(String inputValue) {
        //Get the character representation of the 
        char[] inputValueCharArray = inputValue.toCharArray();
        //Convert the first character to Upper Case
        inputValueCharArray[0] = Character.toUpperCase(inputValueCharArray[0]);
        //return the new String
        return String.copyValueOf(inputValueCharArray);

    }

    /**
     * Parses given string and returns array of tokens
     *
     * @param inputValue String to be parsed
     * @param delimiter Delimiter for parsing
     * @return Array of tokens
     * @throws IllegalArgumentException for Invalid input
     * @throws Exception for all others
     */
    public static String[] parseString(String inputValue,
            String delimiter)
            throws IllegalArgumentException, Exception {
        //Validate Input Value
        if (inputValue == null) {
            throw new IllegalArgumentException("Input value cannot"
                    + "be null in StringUtils.parseString method");
        }

        //Validate delimiter
        if (delimiter == null) {
            throw new IllegalArgumentException("Delimiter cannot"
                    + "be null in StringUtils.parseString method");
        }

        //Validate delimiter
        if (delimiter.length() < 1) {
            throw new IllegalArgumentException("Invalid Delimiter "
                    + " in StringUtils.parseString method");
        }

        //Parse
        StringTokenizer tokenizer
                = new StringTokenizer(inputValue, delimiter);

        //Create an array for tokens
        String[] result = new String[tokenizer.countTokens()];

        int tokenIndex = 0;
        while (tokenizer.hasMoreTokens()) {
            result[tokenIndex] = tokenizer.nextToken();
            tokenIndex++;
        }

        //Return result array
        return result;
    }

    /**
     * A method that uses a stringtokenizer to parse a NameValue pair and
     * returns a map. A NameValue Pair is in the following format:
     * OrderNo=Y100089&EnterpriseCode=Bestbuy&ShiptoID=493848234
     *
     * @param inputValue String to be parsed
     * @return Map object that contains name value pairs
     * @throws IllegalArgumentException for Invalid input
     * @throws Exception for all others
     */
    public static java.util.Map getMap(String inputValue)
            throws IllegalArgumentException, Exception {
        //Validate Input Value
        if (inputValue == null) {
            throw new IllegalArgumentException("Input value cannot"
                    + "be null in StringUtils.getMap method");
        }

        //Validate Input Value
        if (inputValue.indexOf("&") <= -1) {
            throw new IllegalArgumentException("Invalid Input value"
                    + " in StringUtils.getMap method");
        }

        //Validate Input Value
        {
            if (inputValue.indexOf("=") <= -1) {
                throw new IllegalArgumentException("Invalid Input value"
                        + " in StringUtils.getMap method");
            }
        }

        Map map = Collections.synchronizedMap(new HashMap());
        StringTokenizer st = new StringTokenizer(inputValue, "&");
        while (st.hasMoreTokens()) {
            String nameValuePair = st.nextToken();
            map.put(getToken(nameValuePair, "=", 0),
                    getToken(nameValuePair, "=", 1));
        }
        return map;
    }

    /**
     * Constructs name value pair string in the following format from Map object
     * OrderNo=Y100089&EnterpriseCode=Bestbuy&ShiptoID=493848234
     *
     * @param map Input Map object
     * @return Name value pair string
     * @throws IllegalArgumentException for Invalid input
     * @throws Exception for all others
     */
    public static String getMapContent(java.util.Map map)
            throws IllegalArgumentException, Exception {
        //Validate map
        if (map == null) {
            throw new IllegalArgumentException("Input value cannot"
                    + " be null in StringUtils.getMap method");
        }

        String result = "";
        Object[] keyList = map.keySet().toArray();
        for (int index = 0; index < keyList.length; index++) {
            String name = getStringValue(keyList[index]);
            String value = getStringValue(map.get(keyList[index]));
            if (result.length() > 1) {
                result = result + "&";
            }
            result = result + name + "=" + value;
        }
        return result;
    }

    public static String toInitCap(String value) {
        char[] cArray = value.toCharArray();
        cArray[0] = (("" + cArray[0]).toUpperCase()).charAt(0);
        return (new String(cArray));
    }

    public static String loadFileToString(String FileName)
            throws IOException {
        StringBuffer sb = new StringBuffer("");

        FileInputStream fis = new FileInputStream(FileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        while (true) {
            String line = br.readLine();
            if (line == null) {
                break;
            }
            sb.append(line + "\n");
        }
        br.close();
        fis.close();
        return sb.toString();
    }

    public static ArrayList loadFileAsArrayList(String FileName)
            throws IOException {
        ArrayList arrayList = new ArrayList();
        FileInputStream fis = new FileInputStream(FileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        while (true) {
            String line = br.readLine();
            if (line == null) {
                break;
            }
            arrayList.add(line);
        }
        br.close();
        fis.close();
        return arrayList;
    }

    public static void writeStringToFile(String Data, String FileName)
            throws IOException {
        FileOutputStream fos = new FileOutputStream(FileName);
        DataOutputStream dos = new DataOutputStream(fos);
        dos.writeBytes(Data);
        dos.close();
        fos.close();
    }

    public static void appendStringToFile(String Data, String FileName)
            throws IOException {
        FileOutputStream fos = new FileOutputStream(FileName, true);
        DataOutputStream dos = new DataOutputStream(fos);
        dos.writeBytes(Data);
        dos.close();
        fos.close();
    }

    public static String replaceString(String Source, String Old, String New, boolean repeat) {
        if (Source == null) {
            return "";
        }
        if (Old == null) {
            return Source;
        }
        if (New == null) {
            return Source;
        }
        if (Old.equals(New)) {
            return Source;
        }

        int index = Source.indexOf(Old);
        if (index < 0) {
            return Source;
        }

        String firstPart = Source.substring(0, index);
        String finalPart = Source.substring(index + Old.length());

        if (repeat) {
            if ((finalPart != null) && (finalPart.length() > 0)) {
                return (firstPart + New + replaceString(finalPart, Old, New, repeat));
            }
        }
        return (firstPart + New + finalPart);
    }

    public static String stripEnd(String value, String stripValue) {
        if (value.endsWith(stripValue)) {
            return value.substring(0, value.lastIndexOf(stripValue) + 1);
        } else {
            return value;
        }
    }

    public static String noNull(Object value) {
        if (value == null) {
            return "";
        }
        return value.toString();
    }

    public static String noNullForHTML(Object value) {
        return noNull(value, "&nbsp;");
    }

    public static String noNull(Object value, String defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value.toString().trim().length() < 1) {
            return defaultValue;
        }
        return value.toString();
    }

    public static String noNullString(String value) {
        if (value == null) {
            return "";
        }
        return value.toString();
    }

    public static boolean isNull(String value) {
        if (value == null) {
            return true;
        }
        if (value.length() < 1) {
            return true;
        }
        if (value.trim().length() < 1) {
            return true;
        }
        return false;
    }

    public static String getValueFromJSONObject(JSONObject obj, String attributeName)
            throws Exception {
        if (obj == null) {
            return "";
        }
        if (attributeName == null) {
            return "";
        }
        try {
            return obj.get(attributeName).toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static String trimSize(String mesg, int maxSize) {
        if (mesg == null) {
            return mesg;
        }
        if (mesg.length() < maxSize) {
            return mesg;
        }
        return mesg.substring(0, maxSize);
    }

    public static String getStackTrace(Throwable aThrowable) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }

    public static String getProcessedErrorMessage(Throwable aThrowable) {
        String errorMessage = getStackTrace(aThrowable);
        if (errorMessage.contains("CIF_UNIQUE")) {
            return "Duplicate Customer ID";
        }
        if (errorMessage.contains("EMAIL_UNIQUE")) {
            return "Duplicate EMail ID";
        }
        if (errorMessage.contains("ACC_NUM_UNIQUE")) {
            return "Duplicate Account Number";
        }
        if (errorMessage.contains("MOBILE_UNIQUE")) {
            return "Duplicate Mobile Number";
        }
        return errorMessage;
    }

    public static String trimLeadingSize(String value, int size) {
        if (value == null) {
            return value;
        }
        value = "0000000" + value;
        value = value.substring(value.length() - size);
        return value;
    }

    public static String trimLeadingMaxSize(String value, int size, String prefix) {
        if (value == null) {
            return value;
        }
        if (value.trim().length() > size) {
            return value;
        }
        value = "0000000000000000" + value;
        value = value.substring(value.length() - size);
        return prefix + value;
    }

    public static String generateReportSessionId()
            throws Exception {
        int randomNumber = (new Random()).nextInt(999999);
        String formattedRandomNumber = trimLeadingSize(randomNumber + "", 4);
        String currentDateTime = DateUtils.getCurrentTime("yyyyMMddHHmmss");
        return "1" + currentDateTime + formattedRandomNumber;
    }

    public static String generateSessionId()
            throws Exception {
        int randomNumber = (new Random()).nextInt(999999);
        String formattedRandomNumber = trimLeadingSize(randomNumber + "", 4);
        String currentDateTime = DateUtils.getCurrentTime("MMddHHmmss");
        return "1" + currentDateTime + formattedRandomNumber;
    }

    public static String generateVoucherId(String prefix1)
            throws Exception {
        int randomNumber = (new Random()).nextInt(999999999);
        String formattedRandomNumber = trimLeadingSize(randomNumber + "", 9);
        String currentDateTime = DateUtils.getCurrentTime("MMddHHmmss");
        return "1" + currentDateTime + prefix1 + formattedRandomNumber;
    }

    public static int getRandomNumber(int Low, int High) {
        Random r = new Random();
        int R = r.nextInt(High - Low) + Low;
        return R;
    }

    public static String getCurrencyEquivalent(String inputAmount)
            throws Exception {
        try {
            double amount = Double.parseDouble(inputAmount);
            DecimalFormat df = new DecimalFormat("#############.00");
            String formattedValue = df.format(amount);
            return formattedValue;
        } catch (Exception exception) {
            return inputAmount;
        }
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        phoneNumber = StringUtils.noNull(phoneNumber);

        if (phoneNumber.trim().length() < 8) {
            return false;
        }

        for (int index = 0; index < phoneNumber.length(); index++) {
            if (!Character.isDigit(phoneNumber.charAt(index))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isValidNumber(String inputNumber) {
        inputNumber = StringUtils.noNull(inputNumber);

        if (inputNumber.trim().length() < 5) {
            return false;
        }

        for (int index = 0; index < inputNumber.length(); index++) {
            if (!Character.isDigit(inputNumber.charAt(index))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isValidAmount(String inputNumber) {
        try {
            double amount = Double.parseDouble(inputNumber);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public static boolean isValidShortNumber(String inputNumber) {
        inputNumber = StringUtils.noNull(inputNumber);

        for (int index = 0; index < inputNumber.length(); index++) {
            if (!Character.isDigit(inputNumber.charAt(index))) {
                return false;
            }
        }
        return true;
    }

    public static String getLastCharacters(String inputValue, int last) {
        inputValue = StringUtils.noNull(inputValue).trim();
        int len = inputValue.length();
        int idx = len - last;
        if (idx < 0) {
            return inputValue;
        }
        String result = inputValue.substring(idx);
        return result;
    }

    public static String getMapValue(HashMap map, String inputKey) {
        if (map == null) {
            return "";
        }
        if (inputKey == null) {
            return "";
        }
        if (!map.containsKey(inputKey)) {
            return "";
        }
        if (map.get(inputKey) == null) {
            return "";
        }
        return map.get(inputKey).toString();
    }

    public static String getMapValueWT(HashMap map, String inputKey) {
        String result = getMapValue(map, inputKey);
        result = StringUtils.noNull(result).trim();
        return result;
    }

    public static String getMapValue(Map map, String inputKey) {
        if (map == null) {
            return "";
        }
        if (inputKey == null) {
            return "";
        }
        if (!map.containsKey(inputKey)) {
            return "";
        }
        if (map.get(inputKey) == null) {
            return "";
        }
        return map.get(inputKey).toString();
    }

    public static boolean isValidEMail(String email) {
        if (email == null) {
            return false;
        }
        if (!email.contains("@")) {
            return false;
        }
        return true;
    }

    public static String loadFileAsString(String fileName)
            throws Exception {
        FileInputStream fis = new FileInputStream(fileName);
        DataInputStream dis = new DataInputStream(fis);
        String content = "";
        while (true) {
            String line = dis.readLine();
            if (line == null) {
                break;
            }
            content = content + line + "\n";
        }
        dis.close();
        fis.close();
        return content;
    }

    public static String loadURLContent(String inputURL)
            throws Exception {
        URL url = new URL(inputURL);
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

        StringBuffer sb = new StringBuffer();

        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            sb.append(inputLine + "\n");
        }
        in.close();

        return sb.toString();
    }

    public static String[] parseStringOwn(String inputValue,
            String delimiter)
            throws IllegalArgumentException, Exception {

        //Parse
        StringTokenizer tokenizer
                = new StringTokenizer(inputValue, delimiter);

        //Create an array for tokens
        String[] result = new String[tokenizer.countTokens()];

        int tokenIndex = 0;
        while (tokenizer.hasMoreTokens()) {
            result[tokenIndex] = tokenizer.nextToken();
            tokenIndex++;
        }

        //Return result array
        return result;
    }

    public static String getNextToken(String inputValue, String delimiter) {
        int index = inputValue.indexOf(delimiter);
        if (index == -1) {
            return inputValue;
        }
        if (index == 0) {
            return "";
        }
        return (inputValue.substring(0, index + delimiter.length() - 1));
    }

    public static String clearNextToken(String inputValue, String delimiter) {
        int index = inputValue.indexOf(delimiter);
        if (index == -1) {
            return "";
        }
        return (inputValue.substring(index + delimiter.length()));
    }

    public static String[] parseToTokens(String data, String delimiter) {
        ArrayList tokenList = new ArrayList();
        while (true) {
            String token = StringUtils.getNextToken(data, delimiter);
            tokenList.add(token);

            data = StringUtils.clearNextToken(data, delimiter);
//	    	if (!data.contains("delimiter"))
//	    	{
//	    		tokenList.add(data);
//	    		break;
//	    	}
            if (StringUtils.isNullOrEmpty(data)) {
                break;
            }
        }

        String tokenArray[] = new String[tokenList.size()];
        for (int index = 0; index < tokenList.size(); index++) {
            tokenArray[index] = tokenList.get(index).toString();
        }

        return tokenArray;
    }

    public static String pushToStack(String currentStack, String newElement) {
        if (StringUtils.isNullOrEmpty(currentStack)) {
            currentStack = newElement;
            return currentStack;
        }

        currentStack = newElement + "," + currentStack;
        return currentStack;
    }

    public static String peekFromStack(String currentStack) {
        if (StringUtils.isNullOrEmpty(currentStack)) {
            return "";
        }

        int commaIndex = currentStack.indexOf(",");
        if (commaIndex == -1) {
            return currentStack;
        }

        String peekElement = currentStack.substring(0, commaIndex);
        return peekElement;
    }

    public static String popFromStack(String currentStack) {
        if (StringUtils.isNullOrEmpty(currentStack)) {
            return "";
        }

        int commaIndex = currentStack.indexOf(",");
        if (commaIndex == -1) {
            return "";
        }

        String popElement = currentStack.substring(commaIndex + 1);
        return popElement;
    }

    public static boolean inStack(String currentStack, String element)
            throws Exception {
        if (StringUtils.isNullOrEmpty(currentStack)) {
            return false;
        }

        String[] tokens = StringUtils.parseString(currentStack, ",");

        for (int index = 0; index < tokens.length; index++) {
            if (tokens[index].equals(element)) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasChanged(String s1, String s2) {
        s1 = StringUtils.noNull(s1);
        s2 = StringUtils.noNull(s2);

        if (!StringUtils.isNullOrEmpty(s1)) {
            if (StringUtils.isNullOrEmpty(s2)) {
                return false;
            }
        }

        return (!s1.equals(s2));
    }

    public static boolean isLicenseValid()
            throws Exception {
        String enccode = PropertyUtil.getProperty("enckey");
        if (!enccode.startsWith("M0deIsPb")) {
            return true;
        }

        String defaultLicenseDate = DateUtils.getCurrentTime(DateUtils.getLongDateFormat());
        java.util.Date expDate = DateUtils.convertDate("20140509");
        java.util.Date currDate = new java.util.Date();
        return (expDate.after(currDate));
    }

    public static HashMap convertJSONToMap(String jsonInput)
            throws Exception {
        if (StringUtils.isNullOrEmpty(jsonInput)) {
            return (new HashMap());
        }

        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (org.json.simple.JSONObject) parser.parse(jsonInput);
        return convertJSONToMap(jsonObject);
    }

    public static HashMap convertJSONToMap(JSONObject jsonObject)
            throws Exception {
        HashMap resultMap = new HashMap();
        Iterator iterator = jsonObject.keySet().iterator();
        while (iterator.hasNext()) {
            String key = StringUtils.noNull(iterator.next());
            String value = StringUtils.noNull(jsonObject.get(key));
            resultMap.put(key, value);
        }
        return resultMap;
    }

    public static JSONArray convertToJSONArray(String[] array) {

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < array.length; i++) {
            jsonArray.add(array[i]);
        }
        return jsonArray;
    }

    public static JSONArray convertToJSONArray(ArrayList arrayList)
            throws Exception {
        JSONArray jsonArray = new JSONArray();

        Iterator iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            String value = StringUtils.noNull(iterator.next());
            jsonArray.add(value);
        }
        return jsonArray;
    }

    public static String convertMapToJSONString(HashMap map)
            throws Exception {
        JSONObject json = new JSONObject(map);
        return json.toJSONString();
    }

    /**
     * Compute the hash value to check for "real person" submission.
     *
     * @param value the entered value
     * @return its hash value
     */
    public static String rpHash(String value) {
        int hash = 5381;
        value = value.toUpperCase();
        for (int i = 0; i < value.length(); i++) {
            hash = ((hash << 5) + hash) + value.charAt(i);
        }
        return String.valueOf(hash);
    }

    public static void main(String[] args) {
        LogUtils.println(StringUtils.trimLeadingMaxSize("1", 12, "1"));
        LogUtils.println(StringUtils.trimLeadingMaxSize("12", 12, "1"));
        LogUtils.println(StringUtils.trimLeadingMaxSize("123", 12, "1"));
        LogUtils.println(StringUtils.trimLeadingMaxSize("1234", 12, "1"));
        LogUtils.println(StringUtils.trimLeadingMaxSize("12334343434312121", 12, "1"));
        LogUtils.println(StringUtils.trimLeadingMaxSize("000123", 12, "1"));
        LogUtils.println(StringUtils.trimLeadingMaxSize("12300", 12, "1"));

        //    	LogUtils.println(StringUtils.getRandomNumber(1, 5));
//    	LogUtils.println(StringUtils.getRandomNumber(1, 5));
//    	LogUtils.println(StringUtils.getRandomNumber(1, 5));
//    	LogUtils.println(StringUtils.getRandomNumber(1, 5));
//    	LogUtils.println(StringUtils.getRandomNumber(1, 5));
    }

    public static String getConditionValue(String inputValue, String checkValue, String trueValue, String falseValue) {
        inputValue = noNull(inputValue);
        checkValue = noNull(checkValue);
        if (inputValue.equals(checkValue)) {
            return trueValue;
        }
        return falseValue;
    }

    public static boolean isValueIncreased(String inputValue1, String inputValue2) {
        try {
            double value = getPercentageDifference(inputValue1, inputValue2);
            return (value >= 0);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static double getPercentageDifference(String inputValue1, String inputValue2) {
        try {
            inputValue1 = StringUtils.replaceString(inputValue1, ",", "", true);
            inputValue2 = StringUtils.replaceString(inputValue2, ",", "", true);

            double dmin = Double.parseDouble(inputValue1);
            double dmax = Double.parseDouble(inputValue2);
            double result = (dmax * 100) / dmin;
            result = result - 100;
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

}
